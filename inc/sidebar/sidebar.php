<?php	
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hantus
 */

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function hantus_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar Widget Area', 'hantus-pro' ),
		'id' => 'hantus-sidebar-primary',
		'description' => __( 'The Primary Widget Area', 'hantus-pro' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Widget Area', 'hantus-pro' ),
		'id' => 'hantus-footer-widget-area',
		'description' => __( 'The Footer Widget Area', 'hantus-pro' ),
		'before_widget' => '<div class="col-lg-3 col-md-6 col-sm-12 mb-lg-0 mb-md-0 mb-4"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	
	register_sidebar( array(
		'name' => __( 'WooCommerce Widget Area', 'hantus-pro' ),
		'id' => 'woocommerce-1',
		'description' => __( 'This Widget area for WooCommerce Widget', 'hantus-pro' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><div class="title-border"></div>',
	) );
	
}
add_action( 'widgets_init', 'hantus_widgets_init' );
 
?>