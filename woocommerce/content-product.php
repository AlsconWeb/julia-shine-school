<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<li <?php wc_product_class( '', $product ); ?>>
	<div class="shop-product text-center">
		<?php if ( $product->is_on_sale() ) : ?>
			<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="sale">' . esc_html__( 'Sale!', 'hantus-pro' ) . '</span>', $post, $product ); ?>
		<?php endif; ?>
		<div class="product-img">
			<a
					href="#"
					class="modal-btn"
					data-title="<?php echo wp_strip_all_tags( get_the_title( get_the_ID() ) ); ?>"
					data-image_src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium') ?>"
					data-description="<?php echo wp_strip_all_tags( get_the_content( get_the_ID() ) ); ?>"
			>
				<?php the_post_thumbnail(); ?>
			</a>
		</div>
		<ul class="rate">
			<li>
				<?php if ( $average = $product->get_average_rating() ) : ?>
					<?php echo '<i class="fa fa-star" title="' . sprintf( __( 'Rated %s out of 5', 'hantus-pro' ), $average ) . '"><span style="width:' . ( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">' . $average . '</strong> ' . __( 'out of 5', 'hantus-pro' ) . '</span></i>'; ?>
				<?php endif; ?>
			</li>
		</ul>
		<h5>
			<a
					class="modal-btn"
					data-title="<?php echo wp_strip_all_tags( get_the_title( get_the_ID() ) ); ?>"
					data-description="<?php echo wp_strip_all_tags( get_the_content( get_the_ID() ) ); ?>"
			>
				<?php echo the_title(); ?>
			</a>
		</h5>
		<div class="price"><?php echo $product->get_price_html(); ?></div>

		<div class="product-action">
			<div class="button-top">
			</div>
		</div>
	</div>
</li>


