<?php
/**
 * hantus Theme Customizer.
 *
 * @package hantus
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function hantus_customizer_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
	$wp_customize->get_setting('custom_logo')->transport = 'refresh';
}
add_action( 'customize_register', 'hantus_customizer_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function hantus_customize_preview_js() {
	wp_enqueue_script( 'hantus_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'hantus_customize_preview_js' );



/**
 * Customizer Controls
 *
 * @since 1.0.0
 * @return void
 */
function hantus_controls_scripts() {
		$js_prefix  = '.js';
		$css_prefix = '.css';
		
	// Customizer Core.
	wp_enqueue_script( 'hantus-customizer-controls-toggle-js', get_template_directory_uri() . '/assets/js/customizer-toggle-control' . $js_prefix, array(), '1.0', true );

	// Customizer Controls.
	wp_enqueue_script( 'hantus-customizer-controls-js', get_template_directory_uri() . '/assets/js/customizer-control' . $js_prefix, array( 'hantus-customizer-controls-toggle-js' ), '1.0', true );
}
add_action( 'customize_controls_enqueue_scripts', 'hantus_controls_scripts' );

/**
 * Register panels for Customizer.
 *
 * @since Hantus 1.0
 */
function hantus_customize_register( $wp_customize ) {


	$wp_customize->add_panel(
		'hantus_frontpage_sections', array(
			'priority' => 32,
			'title' => esc_html__( 'Frontpage Sections', 'hantus-pro' ),
			'description' => esc_html__( 'Drag and drop panels to change the order of sections.','hantus-pro' ),
		)
	);
}
add_action( 'customize_register', 'hantus_customize_register' );
?>