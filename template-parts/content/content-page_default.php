<?php


get_header();
 ?>
<?php
$hide_show_blog_meta= get_theme_mod('hide_show_blog_meta','1');
?>
 <section id="blog-content" class="full-width section-padding">
        <div class="container-fluid">
            <div class="row full-width" id="grid">
				<?php 
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$args = array( 'post_type' => 'post','paged'=>$paged );	
					$loop = new WP_Query( $args );
				?>
				<?php if( $loop->have_posts() ): ?>
				<?php while( $loop->have_posts() ): $loop->the_post(); ?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <article class="blog-post">
                        <div class="post-thumb">
							<?php get_template_part('template-parts/content/content','sticky'); ?>
                            <?php the_post_thumbnail(); ?>
                        </div>

                        <div class="post-content">
                           <?php if($hide_show_blog_meta == 1 ) {?>
								<ul class="meta-info">
									<li class="post-date"><a href="<?php echo esc_url(get_month_link(get_post_time('Y'),get_post_time('m'))); ?>"><?php esc_html_e('On','hantus-pro'); ?> <?php echo get_the_date('j'); ?> <?php echo get_the_date('M'); ?>, <?php echo get_the_date('Y'); ?></a></li>
									<li class="posted-by"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php esc_html_e('by','hantus-pro'); ?> <?php the_author(); ?></a></li>
								</ul>
							<?php } ?>	
                            <?php     
								if ( is_single() ) :
								
								the_title('<h4 class="post-title">', '</h4>' );
								
								else:
								
								the_title( sprintf( '<h4 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
								
								endif; 
							?>
						<?php 
							the_content( 
								sprintf( 
									__( 'Read More', 'hantus-pro' ), 
									'<span class="screen-reader-text">  '.get_the_title().'</span>' 
								) 
							);
						?>
                        </div>
                    </article>  
                </div>  
				<?php 
						endwhile;
					endif;
				?>
            </div>
        </div>
    </section>


<div class="clearfix"></div>

<?php get_footer(); ?>
