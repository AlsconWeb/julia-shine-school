=== Hantus ===

Contributors: Nayra Themes
Requires at least: WordPress 4.4
Tested up to: WordPress 5.7
Requires PHP: 5.6
Stable tag: 2.7
Version: 2.7
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, left-sidebar, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu,  custom-logo, featured-image-header, featured-images, footer-widgets, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready, blog, e-commerce, portfolio, editor-style, grid-layout

== Copyright ==

Hantus WordPress Theme, Copyright 2021 Nayra Themes
Hantus is distributed under the terms of the GNU GPL


== Description ==

Hantus is an elegant and modern WordPress theme for spa and wellness centers but it is also very easy to customize it for other business like hairdressing, ayurvedic, manicures, pedicures, makeup, mashups, nails, detox, tanning, male grooming, facials, waxing, spa and body treatments, massages or other Hair and Barber related services.


== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Hantus includes support for Infinite Scroll in Jetpack and other following Plugins.
1. Clever Fox

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in Hantus WordPress Theme
-------------------------------------------------------------------------------------------------

1) Package Structure
Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

2) Font Awesome
=================================================================================================
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - Font: SIL OFL 1.1, CSS: MIT License(http://fontawesome.io/license)
Source: http://fontawesome.io

3) Bootstrap Framework
=================================================================================================
Bootstrap (http://getbootstrap.com/)
Copyright (c) 2011-2019 Twitter, Inc.
License - MIT License (https://github.com/twbs/bootstrap/blob/master/LICENSE)

4) Owl Carousel 2.2.1
=================================================================================================
Owl Carousel 2.2.1 - by @David Deutsch - https://github.com/OwlCarousel2
License - MIT License(https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE)

6) Sticky Js
=================================================================================================
Sticky Js by @Anthony Garand - http://stickyjs.com/
License - MIT License (https://github.com/garand/sticky/blob/master/LICENSE.md)
Source: http://stickyjs.com/

7) Meanmenu Js
=================================================================================================
jquery.meanmenu.js by Chris Wharton
License - GNU LESSER GENERAL PUBLIC LICENSE (https://github.com/meanthemes/meanMenu/blob/master/gpl.txt)
Source: http://www.meanthemes.com/plugins/meanmenu/

8) Sainitization
=================================================================================================
License - GNU General Public License (https://github.com/WPTRT/code-examples/blob/master/LICENSE)
Source: https://github.com/WPTRT/code-examples

9) wp-bootstrap-navwalker
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker

10) pure-css-toggle-buttons
=================================================================================================
License -   MIT license (https://github.com/soderlind/class-customizer-toggle-control/blob/master/pure-css-toggle-buttons/license.txt)
Source: https://github.com/soderlind/class-customizer-toggle-control

11) WordPress Customizer Toggle Control
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://www.gnu.org/licenses/)
Source: https://github.com/soderlind/class-customizer-toggle-control

12) Screenshot Image
=================================================================================================
Screenshot Image
URL: https://stocksnap.io/photo/YWGBUF1QKI
Source: https://stocksnap.io
License: CC0 Public Domain (https://stocksnap.io/license)

13) Breadcrumb Section Image
=================================================================================================
Screenshot Image
URL: https://stocksnap.io/photo/5A5JLDXM4R
Source: https://stocksnap.io
License: CC0 Public Domain (https://stocksnap.io/license)

14) Image Folder Images
=================================================================================================
All other Images have been used in images folder, Created by Nayra Themes. Also they are GPL Licensed and free to use and free to redistribute further.


== Changelog ==

@version 2.7
* Cosmics Theme Functionality Added

@version 2.6
* WooCommerce Issues Fixed

@version 2.5
* Thai Spa Theme Functionality Added

@version 2.1
* Tested With WordPress 5.6
* Tabs Control Removed
* Heading Control Added

@version 2.0
* Woocommerce Issues Fixed

@version 1.5
* Tested With WordPress 5.5

@version 1.0
* Initial release