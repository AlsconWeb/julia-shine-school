<!-- Header Slider -->
<?php
/**
 * Get default values for slider section.
 *
 * @since 1.0
 * @access public
 */		
?>
<?php 
$default_content 				= hantus_get_slides_default();
$slider 						= get_theme_mod('slider',$default_content);
$hide_show_slider				= get_theme_mod('hide_show_slider','1'); 
$hide_show_slider_excerpt		= get_theme_mod('hide_show_slider_excerpt');
$slider_excerpt_length			= get_theme_mod('slider_excerpt_length'); 
$slider_animation_in			= get_theme_mod('slider_animation_in','pulse'); 
$slider_animation_out			= get_theme_mod('slider_animation_out','fadeOut');
$slider_animation_speed			= get_theme_mod('slider_animation_speed','3000'); 
$hide_show_revolution			= get_theme_mod('hide_show_revolution');
$slider_shortcode				= get_theme_mod('slider_shortcode');
	
	$settings=array('animateIn'=>$slider_animation_in,'animateOut'=>$slider_animation_out,'animationSpeed'=>$slider_animation_speed);
	
	wp_register_script('hantus-slider',get_template_directory_uri().'/assets/js/homepage/slider.js',array('jquery'));
	wp_localize_script('hantus-slider','slider_settings',$settings);
	wp_enqueue_script('hantus-slider');			
?>
	
    <!-- Start: Header Slider
    ============================= -->
	<?php if($hide_show_slider == '1') { ?>
	<?php hantus_before_slider_section_trigger(); ?>
    <section id="slider">
       <div class="header-slider owl-carousel">
			<?php

				if ( ! empty( $slider ) ) {
				$allowed_html = array(
				'br'     => array(),
				'em'     => array(),
				'strong' => array(),
				'b'      => array(),
				'i'      => array(),
				);
				$slider = json_decode( $slider );
				foreach ( $slider as $slide_item ) {
					//$icon = ! empty( $service_item->icon_value ) ? apply_filters( 'hantus_translate_single_string', $service_item->icon_value, 'service section' ) : '';
					$title = ! empty( $slide_item->title ) ? apply_filters( 'hantus_translate_single_string', $slide_item->title, 'slider section' ) : '';
					$subtitle = ! empty( $slide_item->subtitle ) ? apply_filters( 'hantus_translate_single_string', $slide_item->subtitle, 'slider section' ) : '';
					$text = ! empty( $slide_item->text ) ? apply_filters( 'hantus_translate_single_string', $slide_item->text, 'slider section' ) : '';
					$button = ! empty( $slide_item->text2) ? apply_filters( 'hantus_translate_single_string', $slide_item->text2,'slider section' ) : '';
					$link = ! empty( $slide_item->link ) ? apply_filters( 'hantus_translate_single_string', $slide_item->link, 'slider section' ) : '';
					$image = ! empty( $slide_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $slide_item->image_url, 'slider section' ) : '';
					$open_new_tab = ! empty( $slide_item->open_new_tab ) ? apply_filters( 'hantus_translate_single_string', $slide_item->open_new_tab, 'slider section' ) : '';
					$align = $slide_item->slide_align;
			?>
            <div class="item">
                <?php if ( ! empty( $image ) ) : ?>
					<img src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
				<?php endif; ?>
                <div class="header-single-slider theme-slider">
					<div class="theme-table">
						<div class="theme-table-cell">
                            <div class="container">
			                    <div class="theme-content text-<?php echo esc_attr($align); ?>">
									<?php if ( ! empty( $title ) ) : ?>
										<h3><?php echo esc_attr( $title ); ?></h3>
									<?php endif; ?>
									<?php if ( ! empty( $subtitle ) ) : ?>
										<h1><?php echo esc_attr( $subtitle ); ?></h1>
									<?php endif; ?>
		                            <?php if ( ! empty( $text ) ) : ?>
										<p><?php echo esc_attr( $text ); ?></p>
									<?php endif; ?>
									<?php if ( ! empty( $button ) ) : ?>
		                            <a href="<?php echo esc_url( $link ); ?>" <?php if($open_new_tab== 'yes' || $open_new_tab== '1') { echo "target='_blank'"; } ?> class="boxed-btn"><?php echo esc_attr( $button ); ?></a>
									<?php endif; ?>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
            </div>
			<?php } ?>
		</div>
		<?php } ?>
    </section>
	<?php 
		hantus_after_slider_section_trigger();
		} 
	?>
    <!-- End: Header Slider
    ============================= -->
    <!-- End: Header
    ============================= -->