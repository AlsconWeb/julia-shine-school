<?php
		$hide_show_info			= get_theme_mod('hide_show_info','1');
		$info_type				= get_theme_mod('info_type','style-1');
		$info_first_img_setting	= get_theme_mod('info_first_img_setting',get_template_directory_uri() . '/assets/images/icons/icon01.jpg');
		$info_title				= get_theme_mod('info_title','Opening Time');
		$info_description		= get_theme_mod('info_description','Mon - Sat: 10h00 - 18h00');
		$info_btn				= get_theme_mod('info_btn','Read More');
		$info_link				= get_theme_mod('info_link','#');
		
		$info_second_img_setting= get_theme_mod('info_second_img_setting',get_template_directory_uri() . '/assets/images/icons/icon02.jpg');
		$info_title2			= get_theme_mod('info_title2','Address');
		$info_description2		= get_theme_mod('info_description2','40 Baria Sreet, NY USAm');
		$info_btn2				= get_theme_mod('info_btn2','Read More');
		$info_link2				= get_theme_mod('info_link2','#');
		
		$info_third_img_setting	= get_theme_mod('info_third_img_setting',get_template_directory_uri() . '/assets/images/icons/icon03.jpg');		
		$info_title3			= get_theme_mod('info_title3','Telephone');
		$info_description3		= get_theme_mod('info_description3','+12 345 678 9101'); 
		$info_btn3				= get_theme_mod('info_btn3','Read More');
		$info_link3				= get_theme_mod('info_link3','#');	
		
		$info_first_icon_setting= get_theme_mod('info_first_icon_setting','fa-clock-o'); 
		$info_second_icon_setting= get_theme_mod('info_second_icon_setting','fa-clock-o'); 
		$info_third_icon_setting= get_theme_mod('info_third_icon_setting','fa-clock-o');
		
		$info_four_img_setting	= get_theme_mod('info_four_img_setting',get_template_directory_uri() . '/assets/images/icons/icon02.jpg');		
		$info_title4			= get_theme_mod('info_title4','Facials');
		$info_description4		= get_theme_mod('info_description4','$29'); 
		$info_btn4				= get_theme_mod('info_btn4','<i class="fa fa-shopping-cart" aria-hidden="true"></i>');
		$info_link4				= get_theme_mod('info_link4','#');	

		$info_five_img_setting	= get_theme_mod('info_five_img_setting',get_template_directory_uri() . '/assets/images/icons/icon01.jpg');		
		$info_title5			= get_theme_mod('info_title5','Waxing');
		$info_description5		= get_theme_mod('info_description5','$69'); 
		$info_btn5				= get_theme_mod('info_btn5','<i class="fa fa-shopping-cart" aria-hidden="true"></i>');
		$info_link5				= get_theme_mod('info_link5','#');		
		
		$select_theme	= get_theme_mod('select_theme','hantus-pro');
?>


<!-- Start: Features List
    ============================= -->
<?php if($hide_show_info == '1') { ?>
	<?php hantus_before_info_section_trigger(); ?>
	<?php if($select_theme == 'hantus-theme') { ?>
	<?php if($info_type == 'style-1') { ?>
	<section id="contact2" class="section-flash">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">
	            	<ul class="info-wrapper">
	                    <li class="info-first">
	                        <aside class="single-info">
	                        	<?php if ( ! empty( $info_first_img_setting ) ) { ?>
	                            <img src="<?php echo esc_url( $info_first_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php } ?>
	                        	<div class="info-area">
	                        		<div class="info-caption">
	                        			<p><?php echo esc_html( $info_description ); ?></p>
										<h4><?php echo esc_html( $info_title ); ?></h4>
									</div>
	                                <a href="<?php echo esc_url($info_link); ?>" class="btn-info"><?php echo esc_html($info_btn); ?></a>
	                            </div>
	                        </aside>
	                    </li>
	                    <li class="info-second">
	                        <aside class="single-info">
	                        	<?php if ( ! empty( $info_second_img_setting ) ) { ?>
									 <img src="<?php echo esc_url( $info_second_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php } ?>
	                        	<div class="info-area">
	                        		<div class="info-caption">
	                        			<p><?php echo esc_html( $info_description2 ); ?></p>
										<h4><?php echo esc_html( $info_title2 ); ?></h4>
									</div>
	                                <a href="<?php echo esc_url($info_link2); ?>" class="btn-info"><?php echo esc_html($info_btn2); ?></a>
	                            </div>
	                        </aside>
	                    </li>
	                    <li class="info-third">
	                        <aside class="single-info">
	                        	<?php if ( ! empty( $info_third_img_setting ) ) { ?>
									 <img src="<?php echo esc_url( $info_third_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php } ?>
	                        	<div class="info-area">
	                        		<div class="info-caption">
	                        			<p><?php echo esc_html( $info_description3 ); ?></p>
										<h4><?php echo esc_html( $info_title3 ); ?></h4>
									</div>
	                                <a href="<?php echo esc_url($info_link3); ?>" class="btn-info"><?php echo esc_html($info_btn3); ?></a>
	                            </div>
	                        </aside>
	                    </li>
	                </ul>
	            </div>
            </div>
        </div>
    </section>
	<?php } else {  ?>
    <section id="contact" class="section-flash">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<ul class="contact-wrapper info-wrapper">
	                    <li class="info-first">
		                	<div class="single-contact info-first">
								<i class="fa <?php echo esc_html($info_first_icon_setting);?>"></i>
			                    <div class="info-area">
			                    	<h4><?php echo esc_attr( $info_title ); ?></h4>
			                    	<p><?php echo esc_attr( $info_description ); ?></p>
									<a href="<?php echo esc_url($info_link); ?>" class="btn-info"><?php echo esc_html($info_btn); ?></a>
								</div>
							</div>
						</li>
						<li class="info-first">
		                	<div class="single-contact info-second">	
								<i class="fa <?php echo esc_html($info_second_icon_setting);?>"></i>
								<div class="info-area">
				                    <h4><?php echo esc_attr( $info_title2 ); ?></h4>
				                    <p><?php echo esc_attr( $info_description2 ); ?></p>
									 <a href="<?php echo esc_url($info_link2); ?>" class="btn-info"><?php echo esc_html($info_btn2); ?></a>
								</div>
							</div>
						</li>
						<li class="info-first">
		                	<div class="single-contact info-third">
								<i class="fa <?php echo esc_html($info_third_icon_setting);?>"></i>
			                    <div class="info-area">
				                    <h4><?php echo esc_attr( $info_title3 ); ?></h4>
				                    <p><?php echo esc_attr( $info_description3 ); ?></p>
									 <a href="<?php echo esc_url($info_link3); ?>" class="btn-info"><?php echo esc_html($info_btn3); ?></a>
								</div>
							</div>
						</li>
					</ul>
                </div>
            </div>
        </div>
    </section>
	<?php } } elseif($select_theme == 'cosmics-theme') { ?>
		<section id="contact2" class="info-cosmics">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="info-wrapper">
							<li class="info-first">
								<aside class="single-info-cosmics">
									<?php if ( ! empty( $info_first_img_setting ) ) { ?>
									<img src="<?php echo esc_url( $info_first_img_setting ); ?>" <?php if ( ! empty( $info_title ) ) : ?> alt="<?php echo esc_attr( $info_title ); ?>" title="<?php echo esc_attr( $info_title ); ?>" <?php endif; ?> />
									<?php } ?>
									<div class="info-area">
										<div class="info-caption">
											<h4><?php echo esc_html( $info_title ); ?></h4>
											<p><?php echo esc_html( $info_description ); ?></p>
										</div>
										<a href="<?php echo esc_url($info_link); ?>" class="btn-info"><?php echo wp_kses_post($info_btn); ?></a>
									</div>
								</aside>
							</li>
							<li class="info-second">
								<aside class="single-info-cosmics">
									<?php if ( ! empty( $info_second_img_setting ) ) { ?>
										 <img src="<?php echo esc_url( $info_second_img_setting ); ?>" <?php if ( ! empty( $info_title2 ) ) : ?> alt="<?php echo esc_attr( $info_title2 ); ?>" title="<?php echo esc_attr( $info_title2 ); ?>" <?php endif; ?> />
									<?php } ?>
									<div class="info-area">
										<div class="info-caption">
											<h4><?php echo esc_html( $info_title2 ); ?></h4>
											<p><?php echo esc_html( $info_description2 ); ?></p>
										</div>
										<a href="<?php echo esc_url($info_link2); ?>" class="btn-info"><?php echo wp_kses_post($info_btn2); ?></a>
									</div>
								</aside>
							</li>
							<li class="info-third">
								<aside class="single-info-cosmics">
									<?php if ( ! empty( $info_third_img_setting ) ) { ?>
										 <img src="<?php echo esc_url( $info_third_img_setting ); ?>" <?php if ( ! empty( $info_title3 ) ) : ?> alt="<?php echo esc_attr( $info_title3 ); ?>" title="<?php echo esc_attr( $info_title3 ); ?>" <?php endif; ?> />
									<?php } ?>
									<div class="info-area">
										<div class="info-caption">
											<h4><?php echo esc_html( $info_title3 ); ?></h4>
											<p><?php echo esc_html( $info_description3 ); ?></p>
										</div>
										<a href="<?php echo esc_url($info_link3); ?>" class="btn-info"><?php echo wp_kses_post($info_btn3); ?></a>
									</div>
								</aside>
							</li>
							<li class="info-four">
								<aside class="single-info-cosmics">
									<?php if ( ! empty( $info_four_img_setting ) ) { ?>
										 <img src="<?php echo esc_url( $info_four_img_setting ); ?>" <?php if ( ! empty( $info_title4 ) ) : ?> alt="<?php echo esc_attr( $info_title4 ); ?>" title="<?php echo esc_attr( $info_title4 ); ?>" <?php endif; ?> />
									<?php } ?>
									<div class="info-area">
										<div class="info-caption">
											<h4><?php echo esc_html( $info_title4 ); ?></h4>
											<p><?php echo esc_html( $info_description4 ); ?></p>
										</div>
										<a href="<?php echo esc_url($info_link4); ?>" class="btn-info"><?php echo wp_kses_post($info_btn4); ?></a>
									</div>
								</aside>
							</li>
							<li class="info-five">
								<aside class="single-info-cosmics">
									<?php if ( ! empty( $info_five_img_setting ) ) { ?>
										 <img src="<?php echo esc_url( $info_five_img_setting ); ?>" <?php if ( ! empty( $info_title5 ) ) : ?> alt="<?php echo esc_attr( $info_title5 ); ?>" title="<?php echo esc_attr( $info_title5 ); ?>" <?php endif; ?> />
									<?php } ?>
									<div class="info-area">
										<div class="info-caption">
											<h4><?php echo esc_html( $info_title5 ); ?></h4>
											<p><?php echo esc_html( $info_description5 ); ?></p>
										</div>
										<a href="<?php echo esc_url($info_link5); ?>" class="btn-info"><?php echo wp_kses_post($info_btn5); ?></a>
									</div>
								</aside>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	<?php } else { ?>
	<section id="contact2"  class="info-thaispa">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">
	            	<ul class="info-wrapper">
	                    <li class="info-first">
	                        <aside class="single-info-thaispa strip-hover">
								<div class="strip-hover-wrap">
									<div class="strip-overlay">
										<?php if ( ! empty( $info_first_img_setting ) || ! empty( $info_first_icon_setting )) { ?>
											<div class="icon-info-thaispa">
												<?php if ( ! empty( $info_first_img_setting ) && ! empty( $info_first_icon_setting )){ ?>
													<img src="<?php echo esc_url( $info_first_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }elseif ( ! empty( $info_first_img_setting )){ ?>
												
													<img src="<?php echo esc_url( $info_first_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }else{ ?> 	
													<i class="fa <?php echo esc_html($info_first_icon_setting);?>"></i>
												<?php } ?>	
											</div>
										<?php } ?>
										<div class="info-area-thaispa">
											<div class="info-caption-thaispa">
												<h4><?php echo esc_html( $info_title ); ?></h4>
												<p><?php echo esc_html( $info_description ); ?></p>
												<a href="<?php echo esc_url($info_link); ?>" class="btn-info"><?php echo esc_html($info_btn); ?></a>
											</div>
										</div>
									</div>
								</div>
	                        </aside>
	                    </li>
	                    <li class="info-second">
	                        <aside class="single-info-thaispa strip-hover">
								<div class="strip-hover-wrap">
									<div class="strip-overlay">
										<?php if ( ! empty( $info_second_img_setting ) || ! empty( $info_second_icon_setting )) { ?>
											<div class="icon-info-thaispa">
												<?php if ( ! empty( $info_second_img_setting ) && ! empty( $info_second_icon_setting )){ ?>
													<img src="<?php echo esc_url( $info_second_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }elseif ( ! empty( $info_second_img_setting )){ ?>
												
													<img src="<?php echo esc_url( $info_second_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }else{ ?> 	
													<i class="fa <?php echo esc_html($info_second_icon_setting);?>"></i>
												<?php } ?>	
											</div>
										<?php } ?>
										
										<div class="info-area-thaispa">
											<div class="info-caption-thaispa">
												<h4><?php echo esc_html( $info_title2 ); ?></h4>
												<p><?php echo esc_html( $info_description2 ); ?></p>
												<a href="<?php echo esc_url($info_link2); ?>" class="btn-info"><?php echo esc_html($info_btn2); ?></a>
											</div>
										</div>
									</div>
								</div>
	                        </aside>
	                    </li>
	                    <li class="info-third">
	                        <aside class="single-info-thaispa strip-hover">
								<div class="strip-hover-wrap">
									<div class="strip-overlay">
										<?php if ( ! empty( $info_third_img_setting ) || ! empty( $info_third_icon_setting )) { ?>
											<div class="icon-info-thaispa">
												<?php if ( ! empty( $info_third_img_setting ) && ! empty( $info_third_icon_setting )){ ?>
													<img src="<?php echo esc_url( $info_third_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }elseif ( ! empty( $info_third_img_setting )){ ?>
												
													<img src="<?php echo esc_url( $info_third_img_setting ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
													
												<?php }else{ ?> 	
													<i class="fa <?php echo esc_html($info_third_icon_setting);?>"></i>
												<?php } ?>	
											</div>
										<?php } ?>
										<div class="info-area-thaispa">
											<div class="info-caption-thaispa">
												<h4><?php echo esc_html( $info_title3 ); ?></h4>
												<p><?php echo esc_html( $info_description3 ); ?></p>
												<a href="<?php echo esc_url($info_link3); ?>" class="btn-info"><?php echo esc_html($info_btn3); ?></a>
											</div>
										</div>
									</div>
								</div>
	                        </aside>
	                    </li>
	                </ul>
	            </div>
            </div>
        </div>
    </section>
	<?php } ?>
<?php hantus_after_info_section_trigger(); } ?>
    <!-- End: Features List
    ============================= -->