<?php
/**
Template Name: Blog Right Sidebar
**/
get_header();
 ?>
<?php
	$blog_category_id = get_theme_mod('blog_category_id');
	$blog_display_num = get_theme_mod('blog_display_num','3');	
	$blog_display_col = get_theme_mod('blog_display_col','3');
	$blog_read_more = get_theme_mod('blog_read_more','Read More');
	$hide_show_blog_meta= get_theme_mod('hide_show_blog_meta','1');
?>
    <section id="blog-content" class="section-padding">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-8 col-md-12 mb-5 mb-lg-0">

                    <div class="row">
						<?php 
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$args = array( 'post_type' => 'post','paged'=>$paged );	
							$loop = new WP_Query( $args );
						?>
						<?php if( $loop->have_posts() ): ?>
						<?php while( $loop->have_posts() ): $loop->the_post(); ?>
                        <div class="col-md-6">
						
                            <?php get_template_part('template-parts/content/content','page'); ?>                        
                        </div>
						<?php 
								endwhile;
							endif;
						?>
                    </div>                    

                    <!-- Pagination -->
						<?php			
						$GLOBALS['wp_query']->max_num_pages = $loop->max_num_pages;						
						// Previous/next page navigation.
						the_posts_pagination( array(
						'prev_text'          => '<i class="fa fa-angle-double-left"></i>',
						'next_text'          => '<i class="fa fa-angle-double-right"></i>',
						) ); ?>
					<!-- Pagination -->	
                </div>

                <!-- Sidebar -->
               <?php get_sidebar(); ?>
            </div>

        </div>
    </section>
<?php get_footer(); ?>