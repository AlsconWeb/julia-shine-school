<!-- Start: Portfolio
    ============================= -->

<?php
	$post_type = 'hantus_menu_item';
	$tax = 'menu_item_categories'; 
	$tax_terms = get_terms($tax);
?>	
<?php 
	$portfolio_settings = get_theme_mod('portfolio_settings','1');
	$portfolio_title = get_theme_mod('portfolio_title','Portfolio');
	$portfolio_description = get_theme_mod('portfolio_description','You can judge my work by the portfolio we have done');
	$portfolio_display_num = get_theme_mod('portfolio_display_num','3');
	$portfolio_type = get_theme_mod('portfolio_type','Classic');
  if($portfolio_settings == '1') {	
  hantus_before_portfolio_section_trigger();
?>	
<section id="portfolio" class="pf-padding port-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
						<?php if ( ! empty( $portfolio_title ) ) : ?>
							<h2><?php echo esc_attr( $portfolio_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $portfolio_description ) ) : ?>
							<p><?php echo esc_attr( $portfolio_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>
			<?php if ( function_exists( 'hantus_menu_item' ) ){ ?>
			 <div class="row">
				<div class="col-lg-12">
					 <div class="row portfolio-tab">
						<div class="col-md-12 text-center">
							<?php if (!$tax_terms == 0) { ?>
							<ul class="portfolio-tab-sorting sorting-btn" id="filter">
								<?php	foreach ($tax_terms as $tax_term) { ?>
									<?php if($tax_term->name == 'ALL'){ ?>
									<li><a href="#" data-group="<?php echo $tax_term->slug; ?>" class="active"><?php echo $tax_term->name; ?></a></li>
									<?php }else{ ?>
									<li><a href="#" data-group="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a></li>
								<?php } } ?>
							</ul>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>		
			
			<?php
				//if($portfolio_type == 'standard') :
					get_template_part('template-parts/sections/hantus-portfolio','standard');
				// else :
					// get_template_part('template-parts/sections/hantus-portfolio','classic');
				// endif;
				// }else{
				// echo __('Please install & Activate Hantus Feature plugin for add Portfolio','hantus-pro');
				}
			?>		 
        </div>
 </section>
	 <?php hantus_after_portfolio_section_trigger(); } ?>	
<!-- End: Portfolio
============================= -->