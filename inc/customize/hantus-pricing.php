<?php
function hantus_pricing_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Pricing Section Panel
	=========================================*/
	//$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
		$wp_customize->add_section(
			'hantus_pricing', array(
				'title' => esc_html__( 'Pricing Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 30, 'hantus_pricing' ),
			)
		);
		
	
	/*=========================================
	Pricing Settings Section
	=========================================*/
	// Pricing Settings Section // 
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pricing_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pricing_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'hantus_pricing',
		)
	);
	
	$wp_customize->add_setting( 
		'hide_show_pricing' , 
			array(
			'default' => __('1','hantus-pro'),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_pricing', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'hantus_pricing',
			'settings'    => 'hide_show_pricing',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	//  Head 
	$wp_customize->add_setting(
		'pricing_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pricing_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'hantus_pricing',
		)
	);
	
	// Pricing Title // 
	$wp_customize->add_setting(
    	'pricing_title',
    	array(
	        'default'			=> __('Pricing','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'pricing_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'hantus_pricing',
			'type'           => 'text',
		)  
	);
	
	// Pricing Description // 
	$wp_customize->add_setting(
    	'pricing_description',
    	array(
	        'default'			=> __('You can select a package from the list below to save more','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'pricing_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'hantus_pricing',
			'type'           => 'textarea',
		)  
	);

	
	//  Content Head 
	$wp_customize->add_setting(
		'pricing_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'pricing_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'hantus_pricing',
		)
	);
	// price column // 
	$wp_customize->add_setting(
    	'price_sec_column',
    	array(
	        'default'			=> __('4','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 9,
		)
	);	

	$wp_customize->add_control( 
		'price_sec_column',
		array(
		    'label'   		=> __('Pricing Column','hantus-pro'),
		    'section' 		=> 'hantus_pricing',
			'settings'   	 => 'price_sec_column',
			'type' 			=> 'select',
			'description'   => __( '', 'hantus-pro' ),
			'choices'        => 
			array(
				'6' => __( '2 Column Layout', 'hantus-pro' ),
				'4' => __( '3 Column Layout', 'hantus-pro' ),
				'3' => __( '4 Column Layout', 'hantus-pro' ),
			) 
		)  
	);
}
add_action( 'customize_register', 'hantus_pricing_setting' );

// Pricing selective refresh
function hantus_home_pricing_section_partials( $wp_customize ){

	// hide_show_pricing
	$wp_customize->selective_refresh->add_partial(
		'hide_show_pricing', array(
			'selector' => '.price-section',
			'container_inclusive' => true,
			'render_callback' => 'hantus_pricing',
			'fallback_refresh' => true,
		)
	);
	// title
	$wp_customize->selective_refresh->add_partial( 'pricing_title', array(
		'selector'            => '.price-section .section-title h2',
		'settings'            => 'pricing_title',
		'render_callback'  => 'home_section_price_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'pricing_description', array(
		'selector'            => '.price-section .section-title p',
		'settings'            => 'pricing_description',
		'render_callback'  => 'home_section_price_desc_render_callback',
	
	) );
	// contents
	$wp_customize->selective_refresh->add_partial( 'hantus_pricing_setting', array(
		'selector'            => '.price-section .price_contents',
		'settings'            => 'hantus_pricing_setting',
		'render_callback'  => 'home_section_price_contents_render_callback',
	
	) );
	
	}
add_action( 'customize_register', 'hantus_home_pricing_section_partials' );

// title
function home_section_price_title_render_callback() {
	return get_theme_mod( 'pricing_title' );
}
// description
function home_section_price_desc_render_callback() {
	return get_theme_mod( 'pricing_description' );
}
// contents
function home_section_price_contents_render_callback() {
	return get_theme_mod( 'hantus_pricing_setting' );
}