<?php
function hantus_service_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Service Settings Section
	=========================================*/
		$wp_customize->add_section(
			'service_setting', array(
				'title' => esc_html__( 'Service Section', 'hantus-pro' ),
				'priority' => apply_filters( 'hantus_section_priority', 25, 'hantus_service' ),
				'panel' => 'hantus_frontpage_sections',
			)
		);

	// Setting  Head
	$wp_customize->add_setting(
		'service_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'service_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'service_setting',
		)
	);

	// service Hide/ Show Setting //
	$wp_customize->add_setting(
		'hide_show_service' ,
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'hide_show_service',
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'service_setting',
			'settings'    => 'hide_show_service',
			'type'        => 'ios', // light, ios, flat
		)
	));

	// Service Header Section //

	// Service  Head
	$wp_customize->add_setting(
		'service_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'service_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'service_setting',
		)
	);

	// Service Title //
	$wp_customize->add_setting(
    	'service_title',
    	array(
	        'default'			=> __('Our Services','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);

	$wp_customize->add_control(
		'service_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'service_setting',
			'settings'   	 => 'service_title',
			'type'           => 'text',
		)
	);

	// Service Description //
	$wp_customize->add_setting(
    	'service_description',
    	array(
	        'default'			=> __('These are the services we provide, these makes us stand apart.','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);

	$wp_customize->add_control(
		'service_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'service_setting',
			'settings'   	 => 'service_description',
			'type'           => 'textarea',
		)
	);

	// Service content Section //

	// Content  Head
	$wp_customize->add_setting(
		'service_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'service_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'service_setting',
		)
	);

	/**
	 * Customizer Repeater for add service
	 */

		$wp_customize->add_setting( 'service_contents',
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'transport'         => $selective_refresh,
			 'default' => hantus_get_service_default(),
			 'priority' => 9,
			)
		);

		$wp_customize->add_control(
			new hantus_Repeater( $wp_customize,
				'service_contents',
					array(
						'label'   => esc_html__('Service','hantus-pro'),
						'section' => 'service_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Service', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_text2_control'=> true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_pricing_control' => true,
						'customizer_repeater_old_pricing_control' => true,
					)
				)
			);
	// service column //
	$wp_customize->add_setting(
    	'service_sec_column',
    	array(
	        'default'			=> __('3','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 10,
		)
	);

	$wp_customize->add_control(
		'service_sec_column',
		array(
		    'label'   		=> __('Service Column','hantus-pro'),
		    'section' 		=> 'service_setting',
			'settings'   	 => 'service_sec_column',
			'type'			=> 'select',
			'choices'        =>
			array(
				'6' => __( '2 Column Layout', 'hantus-pro' ),
				'4' => __( '3 Column Layout', 'hantus-pro' ),
				'3' => __( '4 Column Layout', 'hantus-pro' ),
			)
		)
	);

	// Service Background Section //

	// Background Head
	$wp_customize->add_setting(
		'service_bg_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 12,
		)
	);

	$wp_customize->add_control(
	'service_bg_head',
		array(
			'type' => 'hidden',
			'label' => __('Background','hantus-pro'),
			'section' => 'service_setting',
		)
	);

	// Background Image //
    $wp_customize->add_setting(
    	'service_bg_setting' ,
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/service/servicebg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 13,
		)
	);

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'service_bg_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'service_setting',
		)
	));



	$wp_customize->add_setting(
		'service_bg_position' ,
			array(
			'default' => __( 'fixed', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 14,
		)
	);

	$wp_customize->add_control(
		'service_bg_position' ,
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'service_setting',
				'type'           => 'radio',
				'choices'        =>
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)
		)
	);
}

add_action( 'customize_register', 'hantus_service_setting' );

// service selective refresh
function hantus_home_service_section_partials( $wp_customize ){
		// hide_show_service
	$wp_customize->selective_refresh->add_partial(
		'hide_show_service', array(
			'selector' => '#services',
			'container_inclusive' => true,
			'render_callback' => 'service_setting',
			'fallback_refresh' => true,
		)
	);

	// service title
	$wp_customize->selective_refresh->add_partial( 'service_title', array(
		'selector'            => '#services .service-section h2',
		'settings'            => 'service_title',
		'render_callback'  => 'home_section_service_title_render_callback',

	) );
	// service description
	$wp_customize->selective_refresh->add_partial( 'service_description', array(
		'selector'            => '#services .service-section p',
		'settings'            => 'service_description',
		'render_callback'  => 'home_section_service_desc_render_callback',

	) );
	// service content
	$wp_customize->selective_refresh->add_partial( 'service_contents', array(
		'selector'            => '.servicesss'

	) );

	}

add_action( 'customize_register', 'hantus_home_service_section_partials' );

// service title
function home_section_service_title_render_callback() {
	return get_theme_mod( 'service_title' );
}


// service description
function home_section_service_desc_render_callback() {
	return get_theme_mod( 'service_description' );
}
