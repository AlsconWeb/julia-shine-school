<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */

?>
<?php
	$hide_show_blog_meta = get_theme_mod('hide_show_blog_meta','1'); 
	$blog_read_more = get_theme_mod('blog_read_more','Read More');
?>
 <article class="blog-post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-thumb">
		<?php get_template_part('template-parts/content/content','sticky'); ?>
		<?php if ( has_post_thumbnail() ) { ?>
			<?php the_post_thumbnail(); ?>
		<?php } ?>	
	</div>

	<div class="post-content">
		<ul class="meta-info">
			<li class="post-date"><a href="<?php echo esc_url(get_month_link(get_post_time('Y'),get_post_time('m'))); ?>"><?php esc_html_e('On','hantus-pro'); ?> <?php echo get_the_date('j'); ?>  <?php echo get_the_date('M'); ?>  <?php echo get_the_date('Y'); ?></a></li>
			<li class="posted-by"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php esc_html_e('by','hantus-pro'); ?> <?php the_author(); ?></a></li>
		</ul>
		<?php     
			if ( is_single() ) :
			
			the_title('<h4  class="post-title">', '</h4 >' );
			
			else:
			
			the_title( sprintf( '<h4  class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4 >' );
			
			endif; 
		?> 
		 <?php 
			the_content( 
				sprintf( 
					__( 'Read More', 'hantus-pro' ), 
					'<span class="screen-reader-text">  '.get_the_title().'</span>' 
				) 
			);
		?>
		<a href="<?php the_permalink(); ?>" class="read-more"><?php echo esc_attr($blog_read_more); ?> <i class="fa fa-angle-double-right"></i></a>
	</div>
</article>                   