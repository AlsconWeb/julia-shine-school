<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */

get_header(); ?>
<?php
$hide_show_blog_meta= get_theme_mod('hide_show_blog_meta','1');
?>
<?php 
		get_template_part('template-parts/content/content','page_default');
?>
<?php get_footer(); ?>
