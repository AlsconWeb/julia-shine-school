
<script type="text/javascript">
<?php

include( get_template_directory() . '/inc/dynamic-style.php');

$theme_color 				= get_theme_mod('theme_color','#f22853');
?>
// header style
var themeBg = '.woocommerce span.onsale,.product-carousel .owl-nav [class*=owl-]:hover,.thaispa-theme .inner-thaispa .inner-text-thaispa:after,.thaispa-theme .meta-info li.post-date a,.thaispa-theme .blog-post a.more-link:hover,.thaispa-theme .blog-post a.more-link:focus,.thaispa-theme .inner-thaispa .inner-text-thaispa:after,.thaispa-theme .strip-hover .strip-hover-wrap:before,.thaispa-theme .strip-hover .strip-hover-wrap:after,.thaispa-theme .strip-hover .strip-overlay:before,.thaispa-theme .strip-hover .strip-overlay:after,.thaispa-theme.header-transparent .cart-count,.woocommerce-store-notice, p.woocommerce-store-notice, p.demo_store,.select2-container--default li.select2-results__option--highlighted[aria-selected],.select2-container--default li.select2-results__option--highlighted[data-selected],.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:focus,.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:hover,.woocommerce nav.woocommerce-pagination ul.page-numbers li span.current.page-numbers,.btn-info, .pricing-box h3:before, .pricing-box h3:after, .section-title h2:before, .section-title h2:after, .service-box h4:before, .service-box h4:after, .contact-wrapper, #footer-widgets .widget-title:after,#subscribe,.boxed-btn,.days li .active,.feature-box:hover .feature-icon,.feature-box:hover .feature-icon:after,.gallery-page .gallery-item figcaption ul li a:hover,.gallery-tab-sorting li a:after,.header-slider .boxed-btn,.header-slider .owl-next:hover,.header-slider .owl-prev:hover,.header-social li a:hover,.header-social li a:focus,.main-menu>ul>li a:before,.main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a, .nav-tabs .nav-link.active,.portfolio-tab-sorting li a.active,.preloader,.pricing-box hr:after,.pricing-box hr:before,.pricing-box:hover .boxed-btn,.product-carousel .owl-nav [class*=owl-]:hover,.recomended-text,.scrollup,.search-result .posts-navigation .nav-links a,.section-title hr:after,.section-title hr:before,.service-box figure figcaption .boxed-btn,.single-beauticians .social li a:hover,.shop-product .overlay li a:hover,.shop-product .sale:not(.product),.subscribe-wrapper button,.weekdays,.widget-search input[type=button],.widget-tag li a:hover,.widget_search input[type=submit],li.page-item .page-link:hover,li.page-item.active .page-link,.calendar_wrap thead tr,.pagination a:hover,.pagination span,.widget_product_search button[type=submit],.widget_product_tag_cloud .tagcloud a:hover,.widget_search input[type=button],.widget_tag_cloud .tagcloud a:hover,.woocommerce #payment #place_order,.woocommerce #respond input#submit,.woocommerce .price_slider_wrapper .ui-slider .ui-slider-handle,.woocommerce .price_slider_wrapper .ui-slider-horizontal .ui-slider-range,.woocommerce a.button,.woocommerce button.button,.woocommerce button.button.alt,.woocommerce input.button,.woocommerce input.button.alt,.woocommerce input.button:disabled,.woocommerce input.button:disabled[disabled],.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,.woocommerce-cart table.cart input.button,.woocommerce-page #payment #place_order,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart,button,input.search-btn,input[type=button],input[type=reset],p.form-submit .submit,table th,table#wp-calendar tfoot,td#today,.recomended .boxed-btn,.wpcf7-form .wpcf7-submit, .cosmics-theme #header-top, .single-info-cosmics::before';
var themeTextColor = '.woocommerce div.product p.price,.woocommerce div.product span.price,.woocommerce .widget_layered_nav_filters ul li a:hover,.woocommerce .widget_layered_nav_filters ul li a:focus,.widget_recent_reviews a:hover,.widget_recent_reviews a:focus,a.woocommerce-review-link:hover,a.woocommerce-review-link:focus,.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:hover,.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:focus,.product-name a,.woocommerce .shop-product h5 a:hover,.woocommerce .shop-product h5 a:focus,.header-slider .theme-slider .theme-content h1,.thaispa-theme .service-thaispa .price-thaispa h5,.post-comment-area h2,.blog-post .post-title a:focus,.blog-post ul:not(.meta-info) li a,.blog-post ol:not(.meta-info) li a,.blog-post table a, strong a, blockquote a, code a, p a,span a,a.shipping-calculator-button,.woocommerce #review_form #respond input#submit:hover,div.woocommerce-info a,p.woocommerce-info a,div.woocommerce-info::before,p.woocommerce-info::before,.woocommerce-account .addresses .title .edit,.woocommerce table.my_account_orders td.woocommerce-orders-table__cell-order-number a,.woocommerce-account .woocommerce-MyAccount-content p a,.blog-author-social a, .meta-info li a:hover, .meta-info li a:focus, .widget_product_search button[type=submit]:hover, .widget_product_search button[type=submit]:focus, .pricing-box.recomended a.boxed-btn:hover, .pricing-box a.boxed-btn:hover, .service-box figure figcaption .boxed-btn:hover, .service-box figure figcaption .boxed-btn:focus, .contact-wrapper a.btn-info, #slider .theme-content h1, .header-info-text .icons-info .icons,  #breadcrumb-area ul li a,#counter .single-box i,#header-top p i, #header-top ul:not(.header-social) li i,#subscribe-form button,#wcu ul li:before,#welcome .section-title h3,.blog-post .post-title a:hover,.blog-post:hover a.read-more-link,.cart-remove:hover,.coming-soon-wrapper h1 span,.comment-date:hover,.comment-metadata a:hover,.copyright a,.copyright-text a:hover,.count-area ul li,.gallery-page .gallery-item figcaption ul li a,.gallery-tab-sorting li a.active,.gallery-tab-sorting li a:hover,.header-slider h3,.info-box i,.main-menu li.c-dropdowns.active li>a:hover,.main-menu li.c-dropdowns.focus li>a:hover,.main-menu ul li ul.cr-dropdown-menu li.active>a,.main-menu ul li ul.cr-dropdown-menu li.focus>a,.mc4wp-form input[type=submit],.media ul li.comment-replay,.meta-data ul li a:hover,.nav-tabs a.nav-link:hover,.recent-post h6:hover,.sb-search-input,.scrollup:hover,.search-result .posts-navigation .nav-links a:hover,.service-box p.price,.sidenav .cart-item-description p,.sidenav .close-sidenav,.single-beauticians .social li a:not(:hover),.single-post .post-header .text-right li a:hover,.shop-product .overlay li a:not(:hover),.shop-product .price,.tab-content .tab-list h4 .price,.tab-content .tab-list h4:hover,.testimonial-carousel .owl-next:hover,.testimonial-carousel .owl-prev:hover,.welcome-box:hover a,.widget-acrhives li a:hover,.widget-search input[type=button]:hover,.widget_archive li a:hover,.widget_links ul li a:hover,.widget_search input[type=submit]:hover,.woo-container .button-cart .added_to_cart,.wp-calendar-nav a,div#calendar_wrap tbody a,li.latest-news h6:hover,#recent-posts-2 ul li a:hover,.pricing-box:not(:hover):not(.recomended) .boxed-btn,.product_meta a:hover,.widget_categories ul li a:hover,.widget_meta ul li a:hover,.widget_nav_menu ul li a:hover,.widget_pages ul li a:hover,.widget_product_categories ul li a:hover,.widget_product_tag_cloud .tagcloud a:hover,.widget_products ul li a:hover,.widget_recent_comments ul li a:hover,.widget_recent_entries ul li a:hover,.widget_recently_viewed_products ul li a:hover,.widget_top_rated_products ul li a:hover,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,cite,em,h1.page-title,input.search-btn:hover,input[type=button],input[type=reset],input[type=submit]:hover,p.form-submit .submit:hover,q,.beautician-footer-text h5,.boxed-btn:hover,.boxed-btn:focus,.wpcf7-form .wpcf7-submit:hover, .logged-in-as a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-cart-form__cart-item .product-name a, .boxed-btn:focus:before, .boxed-btn:focus:after, .wpcf7-form .wpcf7-submit:focus:before, .wpcf7-form .wpcf7-submit:focus:after, .form-submit .submit:focus:before, .form-submit .submit:focus:after, .pricing-box.recomended a.boxed-btn:focus, .pricing-box a.boxed-btn:focus, .header-slider .boxed-btn:focus, .main-menu ul.cr-dropdown-menu li.focus > a, .main-menu ul.cr-dropdown-menu li:hover > a,.wpcf7-form .wpcf7-submit:focus, .cosmics-theme #header-top .header-social i, .single-info-cosmics:hover .btn-info, div#calendar_wrap tbody tr td a, .wp-calendar-table td a';
var themeBorderColor = 'a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:focus,.thaispa-theme ._2Pfbi,div.woocommerce-info,p.woocommerce-info,.appoinment-wrapper input:focus,.appoinment-wrapper select:focus,.appoinment-wrapper textarea:focus,.feature-box:hover .feature-icon,.pagination span,.portfolio-tab-sorting li a:hover,.pricing-box .boxed-btn,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,.wpcf7-form input:focus,.wpcf7-form textarea:focus,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,input[type=email]:focus,input[type=text]:focus,input[type=url]:focus,li.page-item .page-link:hover,li.page-item.active .page-link,p.form-submit .submit:hover,textarea:focus';
var themeBorderBottomColor = '.coupon input[type="text"]';
var themeBorderLeftColor = '.woocommerce-MyAccount-navigation ul li.is-active a,.woocommerce-MyAccount-navigation ul li a:hover,.woocommerce-MyAccount-navigation ul li a:focus,blockquote';
var themeStrokeColor = '.input__field--hantus:focus+.input__label--hantus .graphic--hantus,.input--filled .graphic--hantus';
var themeTextWhiteColor = '.pricing-box.recomended:hover .boxed-btn, .main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a';
var themeBackgroundWhite = '.scrollup:hover,.widget_search input[type=submit]:hover';
var themeBackgroundNone = '.pricing-box:not(.recomended):not(:hover) .boxed-btn';

jQuery(document).on('click', '.color-changer', function(e){
	//console.log(localStorage.getItem('customstyle'));
	if(localStorage.getItem('customstyle') ==null){
		resetButtonShow();
		var id 	=	jQuery(this).attr('id');
		var color 	=	jQuery(this).attr('data-myval');
		jQuery('.style-palette li a').removeClass('active');
		jQuery('#'+id).addClass('active');
		localStorage.setItem('stylesheet', color);
		var cssClass = ''+ themeBg + '{background:' + color + ' !important;}'+ themeTextColor +'{color:' + color + ' !important;}'+ themeBorderColor +'{ border-color:' + color + ' !important;}'+ themeBorderLeftColor +'{ border-left:' + color + ' !important;}'+themeBorderBottomColor +'{ border-bottom:' + color + ' !important;}'+ themeStrokeColor +'{stroke:' + color + ' !important;}'+ themeTextWhiteColor +'{color:' + '#ffffff' + ' !important;}'+ themeBackgroundNone +'{background:' + 'none' + ' !important;}'+ themeBackgroundWhite +'{background:' + '#ffffff' + ' !important;}';

    	jQuery('#hantusCustomCss').html(cssClass);
	}
	else{
		// jQuery('.error_msg').html('Please Reset Custom Color');
		alertify.error('Please Reset Custom Color');
	}
});
//Colors & Background
jQuery(document).on('click', '.web-btn', function(e){
	jQuery(".web-btn").removeClass('active');
	resetButtonShow();
	jQuery(this).addClass('active');
	localStorage.setItem("layout", jQuery(this).attr('id'));			
	if( jQuery(this).attr('id') == 'wide'){
		jQuery("body").removeClass("boxed");
		jQuery("#bg").hide();
	} else{
		localStorage.setItem("layout", jQuery(this).attr('id'));				
		jQuery("body").addClass("boxed");
		jQuery("#bg").show();
	}
});
jQuery(document).on('click', '.bg li a span', function(e){
	layout = localStorage.getItem('layout');
	if(layout == 'boxed') {
		resetButtonShow();
		var bg = jQuery(this).css("backgroundImage");
		jQuery("body").css("backgroundImage",bg);
		localStorage.setItem("backgroundImage", bg);
	}
	// else {
		// jQuery('.error_bg').html('Background Patterns will work with Boxed Layout');
	// }
});

function noStyleChange(){
	//console.log(localStorage);
	//localStorage.removeItem('layout')
	//boxed
	var layout = localStorage.getItem('layout');
	if( layout ==null){
		jQuery('.style-palette-bx li a').removeClass('active');
		jQuery('#wide').addClass('active');
		jQuery("body").removeClass("boxed"),
		localStorage.setItem('layout','wide');
		jQuery("#bg").hide();
	}
	jQuery('#'+layout).addClass('active');
	if( layout == 'wide'){
		resetButtonShow();
		jQuery("body").removeClass("boxed");
		jQuery("#bg").hide();
	}
	else if( layout == 'boxed'){
		resetButtonShow();
		jQuery("body").addClass("boxed");
		jQuery("#bg").show();
	}
	
	var layout_p = localStorage.getItem('layout');	
	if(layout_p == 'boxed') {
		resetButtonShow();
		//console.log(localStorage);
		var bg = localStorage.getItem('backgroundImage');
		jQuery("body").css("backgroundImage",bg);
	}
	if(localStorage.getItem('stylesheet') !='' && localStorage.getItem('stylesheet')!='undefined' && localStorage.getItem('stylesheet') !=null){
			resetButtonShow();
			var id 	=	jQuery(this).attr('id');
			color = localStorage.getItem('stylesheet');			
			jQuery('#'+id).addClass('active');
			var cssClass = ''+ themeBg + '{background:' + color + ' !important;}'+ themeTextColor +'{color:' + color + ' !important;}'+ themeBorderColor +'{ border-color:' + color + ' !important;}'+ themeBorderLeftColor +'{ border-left:' + color + ' !important;}'+ themeBorderBottomColor +'{ border-bottom:' + color + ' !important;}'+ themeStrokeColor +'{stroke:' + color + ' !important;}'+ themeTextWhiteColor +'{color:' + '#ffffff' + ' !important;}'+ themeBackgroundNone +'{background:' + 'none' + ' !important;}'+ themeBackgroundWhite +'{background:' + '#ffffff' + ' !important;}';
    	jQuery('#hantusCustomCss').html(cssClass);
	}
	else{
		jQuery('.style-palette li a').removeClass('active');
		jQuery(this).attr('data-myval','<?php echo esc_attr($theme_color); ?>');
	}
	if(localStorage.getItem('customstyle')!='' && localStorage.getItem('customstyle')!='undefined' && localStorage.getItem('customstyle') !=null){
		resetButtonShow();
		jscolor = '#' + localStorage.getItem('customstyle');
		var css = ''+ themeBg + '{background:' + jscolor + ' !important;}'+ themeTextColor +'{color:' + jscolor + ' !important;}'+ themeBorderColor +'{ border-color:' + jscolor + ' !important;}'+ themeBorderLeftColor +'{ border-left:' + jscolor + ' !important;}'+ themeBorderBottomColor +'{ border-bottom:' + jscolor + ' !important;}'+ themeStrokeColor +'{stroke:' + jscolor + ' !important;}'+ themeTextWhiteColor +'{color:' + '#ffffff' + ' !important;}'+ themeBackgroundNone +'{background:' + 'none' + ' !important;}'+ themeBackgroundWhite +'{background:' + '#ffffff' + ' !important;}';
    	jQuery('#hantusCustomCss').html(css);	
	}
}
function pickColor(jscolor) {
	resetButtonShow();
    localStorage.setItem('customstyle', jscolor);
    jscolor = '#' + jscolor;
	localStorage.removeItem('stylesheet');
	jQuery('.style-palette li a').removeClass('active');
    var css = ''+ themeBg + '{background:' + jscolor + ' !important;}'+ themeTextColor +'{color:' + jscolor + ' !important;}'+ themeBorderColor +'{ border-color:' + jscolor + ' !important;}'+ themeBorderLeftColor +'{ border-left:' + jscolor + ' !important;}'+ themeBorderBottomColor +'{ border-bottom:' + jscolor + ' !important;}'+ themeStrokeColor +'{stroke:' + jscolor + ' !important;}'+ themeTextWhiteColor +'{color:' + '#ffffff' + ' !important;}'+ themeBackgroundNone +'{background:' + 'none' + ' !important;}'+ themeBackgroundWhite +'{background:' + '#ffffff' + ' !important;}';
    jQuery('#hantusCustomCss').html(css);
}

function resetColor(e) {
    jQuery(e).prev().css('background-color', '<?php echo esc_attr($theme_color); ?>');
    jQuery('#hantusCustomCss').html('');
    localStorage.removeItem('customstyle');
	jQuery('.error_msg').html('');
	//console.log(localStorage);
}
// LocalStorage Clear
jQuery(document).on('click', '.clr_localStorage', function(e){
	localStorage.clear();
	noStyleChange();
	jQuery('#hantusClassCss').html('');
	jQuery('#hantusCustomCss').html('');
	resetButtonHide();
});
function resetButtonShow(){
	jQuery('.bbtn').show();
	//console.log('hello');
}
function resetButtonHide(){
	jQuery('.bbtn').hide();
	//console.log('Byy');
}
</script>