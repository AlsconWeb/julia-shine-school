<?php
/**
 * Template part master modal.
 *
 * @package hantus/theme
 */

?>
<div class="modal fade" id="master-modal" tabindex="-1" aria-labelledby="master-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="image">
					<img src="" alt="">
				</div>
				<div class="content"></div>
			</div>
		</div>
	</div>
</div>
