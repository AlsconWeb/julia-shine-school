<?php
function hantus_sponsers_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	funfact Section Panel
	=========================================*/
		$wp_customize->add_section(
			'sponsers_setting', array(
				'title' => esc_html__( 'Sponsor Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 42, 'hantus_Funfact' ),
			)
		);
	/*=========================================
	Funfact Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'sponsor_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'sponsor_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'sponsers_setting',
		)
	);
	
	// Slider Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_sponser' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_sponser', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'sponsers_setting',
			'settings'    => 'hide_show_sponser',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// funfact content Section // 
	
	// Content  Head 
	$wp_customize->add_setting(
		'sponsor_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'sponsor_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'sponsers_setting',
		)
	);
	
	
	/**
	 * Customizer Repeater for add funfact
	 */
		$wp_customize->add_setting( 'sponser_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'transport'         => $selective_refresh,
			   'default' => hantus_get_sponsers_default(),
			   'priority' => 6,
			)
		);
		
		$wp_customize->add_control( 
			new Hantus_Repeater( $wp_customize, 
				'sponser_contents', 
					array(
						'label'   => esc_html__('Sponser','hantus-pro'),
						'section' => 'sponsers_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Sponser', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_link_control' => true,
					) 
				) 
			);
			// Sponsors speed// 
	$wp_customize->add_setting( 
		'sponsor_animation_speed' , 
			array(
			'default' => '3000',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 7,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'sponsor_animation_speed', 
		array(
			'section'  => 'sponsers_setting',
			'settings' => 'sponsor_animation_speed',
			'label'    => __( 'Sponsors Speed','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 500,
				'max'    => 10000,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
	// sponsors no.// 
	$wp_customize->add_setting( 
		'sponsor_count' , 
			array(
			'default' => '6',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 8,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'sponsor_count', 
		array(
			'section'  => 'sponsers_setting',
			'settings' => 'sponsor_count',
			'label'    => __( 'Sponsors Count','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 3,
				'max'    => 7,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
}
add_action( 'customize_register', 'hantus_sponsers_setting' );

// funfact selective refresh
function hantus_home_sponsers_section_partials( $wp_customize ){
	// hide_show_sponser
	$wp_customize->selective_refresh->add_partial(
		'hide_show_sponser', array(
			'selector' => '#partner',
			'container_inclusive' => true,
			'render_callback' => 'sponsers_setting',
			'fallback_refresh' => true,
		)
	);
	// funfact
	$wp_customize->selective_refresh->add_partial( 'sponser_contents', array(
		'selector'            => '#partnert .partner-carousel',
	
	) );
	
	}
add_action( 'customize_register', 'hantus_home_sponsers_section_partials' );