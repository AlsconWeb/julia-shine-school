<?php
/**
Template Name: Page Left Sidebar
**/

get_header();
?>

 <section id="blog-content" class="full-width section-padding">
        <div class="container">
           <div class="row">	
			<?php get_sidebar(); ?>
			<div class="col-lg-8 col-md-12" >
				<div class="site-content">
					<?php the_post(); the_content(); ?>
				</div>
				
				<?php comments_template( '', true ); // show comments ?>	
			</div><!-- /.col -->
						
		</div><!-- /.row -->
		
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>

