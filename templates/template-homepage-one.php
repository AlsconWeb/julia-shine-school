<?php
/**
 * Template Name: Homepage Two
 */
?>
<?php
get_header();


$front_page = get_theme_mod( 'front_page_data', 'Info,Service,Portfolio,Feature,Pricing,Funfact,Product,Testimonial,Team,Appointment,Sponser,Newsletter,Blog,Custom,Custoa,Custob,Custoc' );
get_template_part( 'template-parts/sections/section', 'slider' );

$data = is_array( $front_page ) ? $front_page : explode( ",", $front_page );

if ( $data ) {
	foreach ( $data as $key => $value ) {
		switch ( $value ) {
			case 'Info':
				get_template_part( 'template-parts/sections/section', 'flash' );
				break;

			case 'Portfolio':
				get_template_part( 'template-parts/sections/section', 'portfolio' );
				break;

			case 'Feature';
				get_template_part( 'template-parts/sections/section', 'features' );
				break;

			case 'Service';
				get_template_part( 'template-parts/sections/section', 'service' );
				break;

			case 'Funfact';
				get_template_part( 'template-parts/sections/section', 'funfact' );
				break;

			case 'Product';
				get_template_part( 'template-parts/sections/section', 'product' );
				break;

			case 'Testimonial';
				get_template_part( 'template-parts/sections/section', 'testimonial' );
				break;

			case 'Team';
				get_template_part( 'template-parts/sections/section', 'team' );
				break;

			case 'Appointment';
				get_template_part( 'template-parts/sections/section', 'appointment' );
				break;

			case 'Sponser';
				get_template_part( 'template-parts/sections/section', 'sponser' );
				break;


			case 'Newsletter';
				get_template_part( 'template-parts/sections/section', 'newsletter' );
				break;

			case 'Pricing';
				get_template_part( 'template-parts/sections/section', 'pricing' );
				break;

			case 'Blog';
				get_template_part( 'template-parts/sections/section', 'blog' );
				break;

			case 'Custom';
				get_template_part( 'template-parts/sections/section', 'custom' );
				break;

			case 'Custom two';
				get_template_part( 'template-parts/sections/section', 'custom-two' );
				break;

			case 'Custom tree';
				get_template_part( 'template-parts/sections/section', 'custom-tree' );
				break;

			case 'Custom four';
				get_template_part( 'template-parts/sections/section', 'custom-four' );
				break;

			case 'Video';
				get_template_part( 'template-parts/sections/section', 'video' );
				break;
		}
	}
}

get_footer(); ?>
