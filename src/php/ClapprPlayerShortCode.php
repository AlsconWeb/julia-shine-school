<?php
/**
 * Clappr player short code.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use Hantus\Theme\Helpers\FrontEndHelpers;

/**
 * ClapprPlayerShortCode class file.
 */
class ClapprPlayerShortCode {
	/**
	 * ClapprPlayerShortCode construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_shortcode( 'clappr', [ $this, 'output_short_code' ] );
	}

	/**
	 * Output short code clappr player.
	 *
	 * @param array $atts Attribute.
	 *
	 * @return void
	 */
	public function output_short_code( $atts ): void {
		?>
		<div
				id="clappr-<?php echo esc_attr( $atts['key'] ?? FrontEndHelpers::generate_unic_id() ); ?>"
				class="video-clappr"
				data-short="true"
				data-thumbnail="<?php echo esc_url( $atts['poster'] ?? '' ); ?>"
				data-video="<?php echo esc_url( $atts['url'] ); ?>"></div>
		<?php
	}
}
