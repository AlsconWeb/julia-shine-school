<?php
/**
 * CSVReader helpers class.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme\Helpers;

/**
 * CSVReader class file.
 */
class CSVReader {
	/**
	 * CSV file url.
	 */
	const CSV_URL = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQOincWI2MkdeGsENpFs7X6hZqPlsDWTjDjU3wln2JLJPhUZCFwulPK0xIpN5A_5bI2TG_TqcDpK8vM/pub?gid=0&single=true&output=csv';

	/**
	 * ID course and cell CSV file.
	 */
	const CSV_CELL_NUMBERS = [
		'586' => 3,
		'72'  => 9,
		'519' => 15,
	];

	/**
	 * CSVReader construct.
	 */
	public function __construct() {

	}

	/**
	 * Read file and set transient.
	 *
	 * @return bool
	 */
	public function read_file(): bool {

		$course_sync = get_transient( 'ju_course_count_sync' );

		if ( $course_sync ) {
			return true;
		}

		$response = wp_remote_post(
			self::CSV_URL,
			[
				'method'  => 'GET',
				'timeout' => 15,
			]
		);

		if ( empty( $response['body'] ) ) {
			return false;
		}

		$csv_data = str_getcsv( $response['body'], "\n" );
		$csv_rows = array_map( 'str_getcsv', $csv_data );

		$flags_status = [];
		foreach ( $csv_rows[ count( $csv_rows ) - 1 ] as $key => $cell ) {
			if ( $course = array_search( $key, self::CSV_CELL_NUMBERS ) ) {
				$course_transient_name = 'ju_course_count_' . $course;
				$flags_status[]        = [ $course => true ];
				set_transient( $course_transient_name, [ 'count' => $cell ], DAY_IN_SECONDS );
			}
		}

		if ( count( $flags_status ) === count( self::CSV_CELL_NUMBERS ) ) {
			set_transient( 'ju_course_count_sync', true, DAY_IN_SECONDS );
		}

		return false;
	}

	/**
	 * @param int $id
	 *
	 * @return int
	 */
	public static function get_course_count( int $id ): int {
		return (int) get_transient( 'ju_course_count_' . $id )["count"] ?? 0;
	}
}
