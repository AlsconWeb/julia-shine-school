<?php
/**
Taxonomy Portfolio Category Archive
**/
get_header();
?>

<?php
	$portfolio_taxo_title= get_theme_mod('portfolio_taxo_title','Portfolio'); 
	$portfolio_taxo_description= get_theme_mod('portfolio_taxo_description','You can judge my work by the portfolio we have done'); 
	$portfolio_type = get_theme_mod('portfolio_type','standard');
?>
<section id="portfolio" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
						<?php if ( ! empty( $portfolio_taxo_title ) ) : ?>
							<h2><?php echo esc_attr( $portfolio_taxo_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $portfolio_taxo_description ) ) : ?>
							<p><?php echo esc_attr( $portfolio_taxo_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>
			<section id="portfolio-page" class="section-padding portfolio-page">
				 <div class="container">
					<div class="row portfolio" id="grid">
						<?php
							$j=1;		
							if( have_posts() )
						{	while ( have_posts() ) : the_post();
						?>
						<?php
							$portfolio_descriptions 		= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_description', true ));
							$portfolio_price 					= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_price', true ));
							$portfolio_link 			= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link', true ));
							$portfolio_date 				= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_date', true ));
							$portfolio_button_link_target 	= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link_target', true ));
						
						?>	
						 <?php
							if($portfolio_type == 'classic') :
						?>
						<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 portfolio-item" >
						<figure>
							<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail();
								}
							?>	
							<figcaption>
								<div class="inner-text">
									<h4><?php echo the_title(); ?></h4>
									<h6><?php echo esc_attr($portfolio_date); ?></h6>
									<p><?php echo esc_attr($portfolio_descriptions); ?></p>
									<a href="<?php echo esc_url($portfolio_link); ?>" <?php  if($portfolio_button_link_target) { echo "target='_blank'"; }  ?>><?php echo esc_attr($portfolio_price); ?></a>
								</div>
							</figcaption>
						</figure>
					</div>	
					<?php else : ?>
					<div class="tab-content">
						<div >
							<ul>
								<li>
									<?php 
										if ( has_post_thumbnail() ) {
											the_post_thumbnail();
										}
									?>
									 <a href="<?php echo esc_url($portfolio_link); ?>" <?php  if($portfolio_button_link_target) { echo "target='_blank'"; } ?><h4><?php echo the_title(); ?><span class="price"><?php echo esc_attr($portfolio_price); ?></span></h4></a>
									<p><?php echo esc_attr($portfolio_descriptions); ?></p>
								</li>
							</ul>
						</div>
					</div>
					<?php 
							endif;
						if($j%3==0){ echo "<div class='clearfix'></div>"; } $j++; 	endwhile;  ?>
						<?php 
					 } 
					?>
					</div>
				</div>
			</section>
<?php get_footer(); ?>