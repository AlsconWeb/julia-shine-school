<?php
function hantus_page_template_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Page Template Settings Panel
	=========================================*/
	$wp_customize->add_panel( 
		'page_temp_section', 
		array(
			'priority'      => 33,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Page Template', 'hantus-pro'),
			'description' 	=> __('This is the Page Template Sections. Here You can Manage All Page Template.','hantus-pro'),
		) 
	);

	/*=========================================
	Service Template Section
	=========================================*/
	$wp_customize->add_section(
        'service_page_setting',
        array(
        	'priority'      => 2,
            'title' 		=> __('Service Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_service_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_service_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'service_page_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'service_newsletter_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'service_newsletter_hide_show', 
		array(
			'label'	      => esc_html__( 'Newsletter Action Hide/Show', 'hantus-pro' ),
			'section'     => 'service_page_setting',
			'settings'    => 'service_newsletter_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	$wp_customize->add_setting( 
		'service_service_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'service_service_hide_show', 
		array(
			'label'	      => esc_html__( 'Service Section Hide/Show', 'hantus-pro' ),
			'section'     => 'service_page_setting',
			'settings'    => 'service_service_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Service  Head 
	$wp_customize->add_setting(
		'pg_service_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_service_head',
		array(
			'type' => 'hidden',
			'label' => __('Service','hantus-pro'),
			'section' => 'service_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'service_page_title',
    	array(
	        'default'			=> __('Our Services','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);
	
	$wp_customize->add_control( 
		'service_page_title',
		array(
		    'label'   => __('Service Title','hantus-pro'),
		    'section' => 'service_page_setting',
			'settings'=> 'service_page_title',
			'type' => 'text',
			'description'    => __('', 'hantus-pro' ),
		)  
	);
	
	$wp_customize->add_setting(
    	'service_page_description',
    	array(
	        'default'			=> __('These are the services we provide, these makes us stand apart.','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	

	$wp_customize->add_control( 
		'service_page_description',
		array(
		    'label'   => __('Service Description','hantus-pro'),
		    'section' => 'service_page_setting',
			'settings'=> 'service_page_description',
			'type' => 'textarea',
			'description'    => __('', 'hantus-pro' ),
		)  
	);
	/**
	 * Customizer Repeater for add service
	 */
	
		$wp_customize->add_setting( 'service_page_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'default' => hantus_get_page_service_default(),
			 'priority' => 8,
			)
		);
		
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'service_page_contents', 
					array(
						'label'   => esc_html__('Service','hantus-pro'),
						'section' => 'service_page_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Service', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_text2_control'=> true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_pricing_control' => true,
					) 
				) 
			);
	
	/*=========================================
	Portfolio Template Section
	=========================================*/
	$wp_customize->add_section(
        'portfolio_page_setting',
        array(
        	'priority'      => 4,
            'title' 		=> __('Portfolio Page','hantus-pro'),
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_portfolio_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_portfolio_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'portfolio_page_setting',
		)
	);
	
	// portfolio Hide Show // 
	$wp_customize->add_setting( 
		'portfolio_page_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'portfolio_page_hide_show', 
		array(
			'label'	      => esc_html__( 'Portfolio Hide/Show', 'hantus-pro' ),
			'section'     => 'portfolio_page_setting',
			'settings'    => 'portfolio_page_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// portfolio Hide Show // 
	$wp_customize->add_setting( 
		'portfolio_page_news_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability'     => 'edit_theme_options',
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'portfolio_page_news_hide_show', 
		array(
			'label'	      => esc_html__( 'Newsletter Hide/Show', 'hantus-pro' ),
			'section'     => 'portfolio_page_setting',
			'settings'    => 'portfolio_page_news_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Content  Head 
	$wp_customize->add_setting(
		'pg_portfolio_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_portfolio_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'portfolio_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'portfolio_page_per_post',
    	array(
	        'default'			=> __('3','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback'	=> 'hantus_sanitize_integer',
			'priority' => 6,
		)
	);
	$wp_customize->add_control( 
		'portfolio_page_per_post',
		array(
		    'label'   => __('Portfolio Post Per Page','hantus-pro'),
		    'section' => 'portfolio_page_setting',
			'settings'=> 'portfolio_page_per_post',
			'type' => 'number',
		)  
	);
	/*=========================================
	Page Template Section
	=========================================*/
	// About page Setting Section // 
	$wp_customize->add_section(
        'about_page_setting',
        array(
        	'priority'      => 1,
            'title' 		=> __('About Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_about_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_about_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'about_page_setting',
		)
	);
	
	//team section hide show
	
	$wp_customize->add_setting( 
		'about_team_hide_show' , 
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'about_team_hide_show', 
		array(
			'label'	      => esc_html__( 'Team Section Hide/Show', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_team_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	$wp_customize->add_setting( 
		'about_welcome_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'about_welcome_hide_show', 
		array(
			'label'	      => esc_html__( 'Welcome Section Hide/Show', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_welcome_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	$wp_customize->add_setting( 
		'about_testimonial_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 4,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'about_testimonial_hide_show', 
		array(
			'label'	      => esc_html__( 'Testimonial Section Hide/Show', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_testimonial_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	$wp_customize->add_setting( 
		'appoinment_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 5,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'appoinment_hide_show', 
		array(
			'label'	      => esc_html__( 'Appointment Section Hide/Show', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'appoinment_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	$wp_customize->add_setting( 
		'sponsers_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 6,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'sponsers_hide_show', 
		array(
			'label'	      => esc_html__( 'Sponsers Section Hide/Show', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'sponsers_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Welcome  Head 
	$wp_customize->add_setting(
		'pg_about_welcome_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 11,
		)
	);

	$wp_customize->add_control(
	'pg_about_welcome_head',
		array(
			'type' => 'hidden',
			'label' => __('Welcome','hantus-pro'),
			'section' => 'about_page_setting',
		)
	);
	
	//title
	$wp_customize->add_setting( 
		'about_welcome_title' , 
			array(
			'default' => esc_html__( 'Welcome to', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 12,
		) 
	);
	
	$wp_customize->add_control( 
	'about_welcome_title', 
		array(
			'label'	      => esc_html__( 'Title', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_welcome_title',
			'type'        => 'text', 
		) 
	);
	//subtitle
	$wp_customize->add_setting( 
		'about_welcome_subtitle' , 
			array(
			'default' => esc_html__( 'Hantus Spa', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 13,
		) 
	);
	
	$wp_customize->add_control( 
	'about_welcome_subtitle', 
		array(
			'label'	      => esc_html__( 'Subtitle', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_welcome_subtitle',
			'type'        => 'text', 
		) 
	);
	//description
	$wp_customize->add_setting( 
		'about_welcome_description' , 
			array(
			'default' => esc_html__( 'It is a long established fact that a reader will be distracted by the readable.', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 14,
		) 
	);
	
	$wp_customize->add_control(
	'about_welcome_description', 
		array(
			'label'	      => esc_html__( 'Description', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_welcome_description',
			'type'        => 'textarea', 
		) 
	);
	/**
	 * Customizer Repeater for add Welcome
	 */
		$wp_customize->add_setting( 'welcome_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 	  'default' => hantus_get_welcome_default(),
				  'priority' => 15,
			)
		);
		
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'welcome_contents', 
					array(
						'label'   => esc_html__('Welcome Blog','hantus-pro'),
						'section' => 'about_page_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Welcome Blog', 'hantus-pro' ),
						
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_text2_control' => true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_image_control' => true,
					) 
				) 
			);
	// about page welcome column // 
	$wp_customize->add_setting(
    	'about_wel_sec_column',
    	array(
	        'default'			=> __('4','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 16,
		)
	);	

	$wp_customize->add_control(
		'about_wel_sec_column',
		array(
		    'label'   		=> __('Welcome Column','hantus-pro'),
		    'section' 		=> 'about_page_setting',
			'settings'   	 => 'about_wel_sec_column',
			'type'			=> 'select',
			'choices'        => 
			array(
				'6' => __( '2 Column Layout', 'hantus-pro' ),
				'4' => __( '3 Column Layout', 'hantus-pro' ),
				'3' => __( '4 Column Layout', 'hantus-pro' ),
			) 
		)  
	);
	
	
// about page contents

	// About  Head 
	$wp_customize->add_setting(
		'pg_about_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 21,
		)
	);

	$wp_customize->add_control(
	'pg_about_content_head',
		array(
			'type' => 'hidden',
			'label' => __('About Us','hantus-pro'),
			'section' => 'about_page_setting',
		)
	);
//about title
	$wp_customize->add_setting( 
		'about_page_contents_title' , 
			array(
			'default' => esc_html__( 'Why Choose Us', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 22,
		) 
	);
	
	$wp_customize->add_control( 
	'about_page_contents_title', 
		array(
			'label'	      => esc_html__( 'Title', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_page_contents_title',
			'type'        => 'text', 
		) 
	);
	// about subtitle
	$wp_customize->add_setting( 
		'about_page_content_subtitle' , 
			array(
			'default' => esc_html__( 'We Proudly give quality, thorough chiropractic to the group and the encompassing regions.', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 23,
		) 
	);
	
	$wp_customize->add_control( 
	'about_page_content_subtitle', 
		array(
			'label'	      => esc_html__( 'Subtitle', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_page_content_subtitle',
			'type'        => 'textarea', 
		) 
	);
	// about description
	$wp_customize->add_setting( 
		'about_page_content_description' , 
			array(
			'default' => esc_html__( '<li>Created from natural herbs</li> <li>100% safe for your skin</li> <li>Quality product from SpaLabs</li><li>Special gifts & offers for you</li><li>Created from natural herbs</li><li>100% safe for your skin</li>', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 24,
		) 
	);
	
	$wp_customize->add_control(
	'about_page_content_description', 
		array(
			'label'	      => esc_html__( 'Description', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_page_content_description',
			'type'        => 'textarea', 
		) 
	);	
		//about button
	$wp_customize->add_setting( 
		'about_page_btn_lbl_title' , 
			array(
			'default' => esc_html__( 'Watch More', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 25,
		) 
	);
	
	$wp_customize->add_control( 
	'about_page_btn_lbl_title', 
		array(
			'label'	      => esc_html__( 'Video button Label', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_page_btn_lbl_title',
			'type'        => 'text', 
		) 
	);
		//about button link
	$wp_customize->add_setting( 
		'about_page_btn_lbl_link' , 
			array(
			'default' => esc_html__( '#', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 26,
		) 
	);
	
	$wp_customize->add_control( 
	'about_page_btn_lbl_link', 
		array(
			'label'	      => esc_html__( 'Video button Link', 'hantus-pro' ),
			'section'     => 'about_page_setting',
			'settings'    => 'about_page_btn_lbl_link',
			'type'        => 'text', 
		) 
	);
	// video // 
    $wp_customize->add_setting(
    	'about_page_video',
    	array(
	        'default'			=> __('https://www.youtube.com/watch?v=pGbIOC83-So&t=35s','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 27,
		)
	);
	
	$wp_customize->add_control( 
		'about_page_video',
		array(
		    'label'   => __(' Video Link','hantus-pro'),
		    'section' => 'about_page_setting',
			'settings'=> 'about_page_video',
			'type' => 'textarea',
			'description'    => __('', 'hantus-pro' ),
		)  
	);
	// Video section Image // 
    $wp_customize->add_setting( 
    	'about_video_img_setting' , 
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/about-page/wcu.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 28,			
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'about_video_img_setting' ,
		array(
			'label'          => __( 'Video section Image', 'hantus-pro' ),
			'section'        => 'about_page_setting',
			'settings'   	 => 'about_video_img_setting',
		) 
	));
	// about page funfact
	/**
	 * Customizer Repeater for add Welcome
	 */
		$wp_customize->add_setting( 'about_page_funfacts_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 	  'default' => hantus_get_about_funfact_default(),
				  'priority' => 29,
			)
		);
		
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'about_page_funfacts_contents', 
					array(
						'label'   => esc_html__('Funfact','hantus-pro'),
						'section' => 'about_page_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Funfact', 'hantus-pro' ),
						
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control' => true,
					) 
				) 
			);
	/*=========================================
	Gallery Template Section
	=========================================*/
	$wp_customize->add_section(
        'gallery_page_setting',
        array(
        	'priority'      => 4,
            'title' 		=> __('Gallery Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_gallery_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_gallery_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'gallery_page_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'hide_show_gallery' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_gallery', 
		array(
			'label'	      => esc_html__( 'Gallery Hide/Show', 'hantus-pro' ),
			'section'     => 'gallery_page_setting',
			'settings'    => 'hide_show_gallery',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	$wp_customize->add_setting( 
		'hide_show_gallery_subscribe' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_gallery_subscribe', 
		array(
			'label'	      => esc_html__( 'Newsletter Hide/Show', 'hantus-pro' ),
			'section'     => 'gallery_page_setting',
			'settings'    => 'hide_show_gallery_subscribe',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// content  Head 
	$wp_customize->add_setting(
		'pg_gallery_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_gallery_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'gallery_page_setting',
		)
	);
	
	/**
	 * Customizer Repeater for add Gallery
	 */
		$wp_customize->add_setting( 'gallery_page_img_setting', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'transport'         => $selective_refresh,
			 'default' => hantus_get_gallery_default(),
			 'priority' => 6,
			)
		);
		
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'gallery_page_img_setting', 
					array(
						'label'   => esc_html__('Image','hantus-pro'),
						'section' => 'gallery_page_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Image', 'hantus-pro' ),
						
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control'	 => true,
						'customizer_repeater_subtitle_control' => true,
						'customizer_repeater_link_control' => true
					) 
				) 
			);	
// gallery icon first // 
	$wp_customize->add_setting(
    	'gallery_icon_first',
    	array(
	        'default' => 'fa-link',
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
			'priority' => 7,
			
		)
	);	

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'gallery_icon_first',
		array(
		    'label'   		=> __('Gallery Icon First','hantus-pro'),
		    'section' 		=> 'gallery_page_setting',
			'iconset' => 'fa',
			'settings' 		 => 'gallery_icon_first',
			
		))  
	);	
// address info icon hantus // 
	$wp_customize->add_setting(
    	'gallery_icon_second',
    	array(
	        'default' => 'fa-eye',
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
			'priority' => 8,
			
		)
	);	

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'gallery_icon_second',
		array(
		    'label'   		=> __('Gallery Icon Second','hantus-pro'),
		    'section' 		=> 'gallery_page_setting',
			'iconset' => 'fa',
			'settings' 		 => 'gallery_icon_second',
			
		))  
	);	
	/*=========================================
	Contact Template Section
	=========================================*/
	$wp_customize->add_section(
        'contact_page_setting',
        array(
        	'priority'      => 4,
            'title' 		=> __('Contact Page','hantus-pro'),
			'panel'  		=> 'page_temp_section',
		)
    );
	
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_contact_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_contact_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'contact_page_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'hide_show_google_map' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability'     => 'edit_theme_options',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_google_map', 
		array(
			'label'	      => esc_html__( 'Google Map Hide/Show', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'hide_show_google_map',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	//contact form 
		$wp_customize->add_setting( 
		'hide_show_contact_page_form' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability'     => 'edit_theme_options',
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_contact_page_form', 
		array(
			'label'	      => esc_html__( 'Contact Section Hide/Show', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'hide_show_contact_page_form',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	// newsletter hide show
	//contact form 
		$wp_customize->add_setting( 
		'hide_show_contact_page_news' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'sanitize_callback' => 'sanitize_text_field',
			'capability'     => 'edit_theme_options',
			'priority' => 4,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_contact_page_news', 
		array(
			'label'	      => esc_html__( 'Newsletter Hide/Show', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'hide_show_contact_page_news',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	
	// Contact  Head 
	$wp_customize->add_setting(
		'pg_contact_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 6,
		)
	);

	$wp_customize->add_control(
	'pg_contact_head',
		array(
			'type' => 'hidden',
			'label' => __('Contact','hantus-pro'),
			'section' => 'contact_page_setting',
		)
	);
	
	
	//contact title
	$wp_customize->add_setting( 
		'contact_page_form_title' , 
			array(
			'default' => esc_html__( 'Contact Form', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 7,
		) 
	);
	
	$wp_customize->add_control( 
	'contact_page_form_title', 
		array(
			'label'	      => esc_html__( 'Title', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'contact_page_form_title',
			'type'        => 'text', 
		) 
	);
	
	$wp_customize->add_setting(
    	'contact_page_map_lattitude',
    	array(
	        'default'			=> '40.6700',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);
	
	$wp_customize->add_control( 
		'contact_page_map_lattitude',
		array(
		    'label'   => __('Google Map Latitude','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'contact_page_map_lattitude',
			'type' => 'text',
		)  
	);
	
	$wp_customize->add_setting(
    	'contact_page_map_longitude',
    	array(
	        'default'			=> '-73.9400',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 9,
		)
	);
	
	$wp_customize->add_control( 
		'contact_page_map_longitude',
		array(
		    'label'   => __('Google Map Longitude','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'contact_page_map_longitude',
			'type' => 'text',
		)  
	);
	
	
	
	$wp_customize->add_setting(
    	'contact_page_form_shortcode',
    	array(
	        'default'			=> __('','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 10,
		)
	);
	
	$wp_customize->add_control( 
		'contact_page_form_shortcode',
		array(
		    'label'   => __('Contact Form Shortcode','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'contact_page_form_shortcode',
			'type' => 'textarea',
		)  
	);
	
	
	
	// Adress  Head 
	$wp_customize->add_setting(
		'pg_contact_adress_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 16,
		)
	);

	$wp_customize->add_control(
	'pg_contact_adress_head',
		array(
			'type' => 'hidden',
			'label' => __('Address','hantus-pro'),
			'section' => 'contact_page_setting',
		)
	);
	
	// contact info title
	$wp_customize->add_setting( 
		'contact_page_info_title' , 
			array(
			'default' => esc_html__( 'Contact Information', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 17,
		) 
	);
	
	$wp_customize->add_control( 
	'contact_page_info_title', 
		array(
			'label'	      => esc_html__( 'Title', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'contact_page_info_title',
			'type'        => 'text', 
		) 
	);
	// contact info description
	$wp_customize->add_setting( 
		'contact_page_info_desc' , 
			array(
			'default' => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 18,
		) 
	);
	
	$wp_customize->add_control( 
	'contact_page_info_desc', 
		array(
			'label'	      => esc_html__( 'Description', 'hantus-pro' ),
			'section'     => 'contact_page_setting',
			'settings'    => 'contact_page_info_desc',
			'type'        => 'textarea', 
		) 
	);
	$wp_customize->add_setting(
    	'cotact_page_address_first_i',
    	array(
	        'default'			=> __('fa-map-marker','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 19,
		)
	);
	
	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'cotact_page_address_first_i',
		array(
		    'label'   => __('Icon','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_first_i',
			'iconset' => 'fa',
			'description'    => __('', 'hantus-pro' ),
			
		) ) 
	);
	
	$wp_customize->add_setting(
    	'cotact_page_address_first_t',
    	array(
	        'default'			=> __('Store Address One:','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 20,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_first_t',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_first_t',
			'type' => 'text',
			
		)  
	);
	$wp_customize->add_setting(
    	'cotact_page_address_first_a',
    	array(
	        'default'			=> __('481-7473 Cum Rd. Yorba Linda South Carolina <br> 2-345-678-90112 <br> email2@companyname.com','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 21,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_first_a',
		array(
		    'label'   => __('description','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_first_a',
			'type' => 'textarea',
			'description'    => __('', 'hantus-pro' ),
			
		)  
	);
	// address 2
	
	$wp_customize->add_setting(
    	'cotact_page_address_second_i',
    	array(
	        'default'			=> __('fa-map-marker','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 22,
		)
	);
	
	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'cotact_page_address_second_i',
		array(
		    'label'   => __('Icon','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_second_i',
			'iconset' => 'fa',
			'description'    => __('', 'hantus-pro' ),
			
		))  
	);
	
	$wp_customize->add_setting(
    	'cotact_page_address_second_t',
    	array(
	        'default'			=> __('Store Address two:','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 23,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_second_t',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_second_t',
			'type' => 'text',
			
		)  
	);
	$wp_customize->add_setting(
    	'cotact_page_address_second_a',
    	array(
	        'default'			=> __('481-7473 Cum Rd. Yorba Linda South Carolina <br> 2-345-678-90112 <br> email2@companyname.com','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 24,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_second_a',
		array(
		    'label'   => __('description','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_second_a',
			'type' => 'textarea',
			
		)  
	);
			// address 3
	
	$wp_customize->add_setting(
    	'cotact_page_address_third_i',
    	array(
	        'default'			=> __('fa-clock-o','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 25,
		)
	);
	
	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'cotact_page_address_third_i',
		array(
		    'label'   => __('Icon','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_third_i',
			'iconset' => 'fa',
			
		))  
	);
	
	$wp_customize->add_setting(
    	'cotact_page_address_third_t',
    	array(
	        'default'			=> __('Opening Hours:','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 26,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_third_t',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_third_t',
			'type' => 'text',
			
		)  
	);
	$wp_customize->add_setting(
    	'cotact_page_address_third_a',
    	array(
	        'default'			=> __('Monday-Friday: 10 Am to 6 Pm <br> Saturday: 10 Am to 6 Pm <br> Sunday: Close','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 27,
		)
	);
	
	$wp_customize->add_control( 
		'cotact_page_address_third_a',
		array(
		    'label'   => __('description','hantus-pro'),
		    'section' => 'contact_page_setting',
			'settings'=> 'cotact_page_address_third_a',
			'type' => 'textarea',
			
		)  
	);
	
	/*=========================================
	Blog Page
	=========================================*/
	$wp_customize->add_section(
        'blog_page_setting',
        array(
        	'priority'      => 8,
            'title' 		=> __('Blog Page','hantus-pro'),
			'panel'  		=> 'page_temp_section',
		)
    );
	
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_blog_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_blog_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'blog_page_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'hide_show_blog_meta' , 
			array(
			'default' => '1',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 2,
		) 
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_blog_meta', 
		array(
			'label'	      => esc_html__( 'Blog Meta Hide/Show', 'hantus-pro' ),
			'section'     => 'blog_page_setting',
			'settings'    => 'hide_show_blog_meta',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// sticky post 
	
	// Sticky  Head 
	$wp_customize->add_setting(
		'pg_blog_sticky_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_blog_sticky_head',
		array(
			'type' => 'hidden',
			'label' => __('Sticky','hantus-pro'),
			'section' => 'blog_page_setting',
		)
	);
	
	if ( class_exists( 'hantus_Customize_Control_Radio_Image' ) ) {

		$wp_customize->add_setting(
			'sticky_type', array(
				'sanitize_callback' => 'hantus_sanitize_text',
				'default' => 'Circle',
				'priority' => 6,
			)
		);

		$wp_customize->add_control(
			new hantus_Customize_Control_Radio_Image(
				$wp_customize, 'sticky_type', array(
					'label'     => esc_html__( 'Sticky Layout', 'hantus-pro' ),
					'section'   => 'blog_page_setting',
					'choices'   => array(
						'Square' => array(
							'url' => apply_filters( 'hantus_layout_control_image_left', trailingslashit( get_template_directory_uri() ) . 'inc/custom-controls/radio-image/img/sticky-suare.png' ),
							'label' => esc_html__( 'Square', 'hantus-pro' ),
						),
						'Circle' => array(
							'url' => apply_filters( 'hantus_layout_control_image_right', trailingslashit( get_template_directory_uri() ) . 'inc/custom-controls/radio-image/img/sticky-circle.png' ),
							'label' => esc_html__( 'Circle', 'hantus-pro' ),
						),
					),
				)
			)
		);
	}
	
	$wp_customize->add_setting(
    	'sticky_content',
    	array(
	        'default'			=> '<i class="fa fa-thumb-tack"></i>',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 7,
		)
	);
	
	$wp_customize->add_control( 
		'sticky_content',
		array(
		    'label'   => __('Sticky Content','hantus-pro'),
		    'section' => 'blog_page_setting',
			'settings'=> 'sticky_content',
			'type' => 'text',
		)  
	);
	
	// Sticky Bg Color
	$wp_customize->add_setting(
	'sticky_bg_color', 
	array(
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '#1ed12f',
		'priority' => 8,
    ));
	
	$wp_customize->add_control( 
		new WP_Customize_Color_Control
		($wp_customize, 
			'sticky_bg_color', 
			array(
				'label'      => __( 'Bg Color', 'hantus-pro' ),
				'section'    => 'blog_page_setting',
				'settings'   => 'sticky_bg_color',
			) 
		) 
	);	
	
	
	// Blog Filter  Head 
	$wp_customize->add_setting(
		'pg_blog_filter_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 11,
		)
	);

	$wp_customize->add_control(
	'pg_blog_filter_head',
		array(
			'type' => 'hidden',
			'label' => __('Blog FIlter','hantus-pro'),
			'section' => 'blog_page_setting',
		)
	);
	
	//Blog Filter Title // 
	$wp_customize->add_setting(
    	'blog_filter_title',
    	array(
	        'default'			=> __('Blog Title','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 12,
		)
	);	
	
	$wp_customize->add_control( 
		'blog_filter_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'blog_page_setting',
			'settings' => 'blog_filter_title',
			'type' => 'text',
		)  
	);
	
	// Blog Filter Description // 
	$wp_customize->add_setting(
    	'blog_filter_desc',
    	array(
	        'default'			=> __('Blog Description','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 13,
		)
	);	
	
	$wp_customize->add_control( 
		'blog_filter_desc',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'blog_page_setting',
			'settings' => 'blog_filter_desc',
			'type' => 'textarea',
		)  
	);
	/*=========================================
	Pricing Template Section
	=========================================*/
	$wp_customize->add_section(
        'pricing_page_setting',
        array(
        	'priority'      => 3,
            'title' 		=> __('Pricing Page','hantus-pro'),
			'panel'  		=> 'page_temp_section',
		)
    );
	
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_pricing_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_pricing_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'pricing_page_setting',
		)
	);
	
	// newsletter Hide Show // 
	$wp_customize->add_setting( 
		'Pricing_newsletter_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'Pricing_newsletter_hide_show', 
		array(
			'label'	      => esc_html__( ' Newsletter Hide/Show', 'hantus-pro' ),
			'section'     => 'pricing_page_setting',
			'settings'    => 'Pricing_newsletter_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	// price Hide Show // 
	$wp_customize->add_setting( 
		'Pricing_price_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 3,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'Pricing_price_hide_show', 
		array(
			'label'	      => esc_html__( ' Pricing Hide/Show', 'hantus-pro' ),
			'section'     => 'pricing_page_setting',
			'settings'    => 'Pricing_price_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	//  Head 
	$wp_customize->add_setting(
		'pg_pricing_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_pricing_head',
		array(
			'type' => 'hidden',
			'label' => __('Pricing','hantus-pro'),
			'section' => 'pricing_page_setting',
		)
	);
	
	
	// Pricing Title // 
	$wp_customize->add_setting(
    	'pricing_pg_title',
    	array(
	        'default'			=> __('Pricing','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'pricing_pg_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'pricing_page_setting',
			'type'           => 'text',
		)  
	);
	
	// Pricing Description // 
	$wp_customize->add_setting(
    	'pricing_pg_description',
    	array(
	        'default'			=> __('You can select a package from the list below to save more','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'pricing_pg_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'pricing_page_setting',
			'type'           => 'textarea',
		)  
	);
	/*=========================================
	Menu Item  Page
	=========================================*/
	$wp_customize->add_section(
        'menu_item_page_setting',
        array(
        	'priority'      => 7,
            'title' 		=> __('Menu Item Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'pg_menu_item_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'pg_menu_item_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'menu_item_page_setting',
		)
	);
	
	// Newsletter Hide Show // 
	$wp_customize->add_setting( 
		'menu_news_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'menu_news_hide_show', 
		array(
			'label'	      => esc_html__( ' Newsletter Hide/Show', 'hantus-pro' ),
			'section'     => 'menu_item_page_setting',
			'settings'    => 'menu_news_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Content  Head 
	$wp_customize->add_setting(
		'pg_menu_item_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'pg_menu_item_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'menu_item_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'menu_item_title',
    	array(
	        'default'			=> __('Pricing','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);
	
	$wp_customize->add_control( 
		'menu_item_title',
		array(
		    'label'   => __('Menu Title','hantus-pro'),
		    'section' => 'menu_item_page_setting',
			'settings'=> 'menu_item_title',
			'type' => 'text',
		)  
	);
	$wp_customize->add_setting(
    	'menu_item_description',
    	array(
	        'default'			=> __('You can judge my work by the portfolio we have done','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);
	
	$wp_customize->add_control( 
		'menu_item_description',
		array(
		    'label'   => __('Menu Description','hantus-pro'),
		    'section' => 'menu_item_page_setting',
			'settings'=> 'menu_item_description',
			'type' => 'textarea',
		)  
	);	
/*=========================================
	Taxonomy Page
	=========================================*/
	$wp_customize->add_section(
        'taxonomy_page_setting',
        array(
        	'priority'      => 8,
            'title' 		=> __('Taxonomy Page','hantus-pro'),
			'panel'  		=> 'page_temp_section',
		)
    );
	
	
	// Portfolio  Head 
	$wp_customize->add_setting(
		'texonomy_portfolio_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'texonomy_portfolio_head',
		array(
			'type' => 'hidden',
			'label' => __('Portfolio','hantus-pro'),
			'section' => 'taxonomy_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'portfolio_taxo_title',
    	array(
	        'default'			=> __('Portfolio','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 2,
		)
	);
	
	$wp_customize->add_control( 
		'portfolio_taxo_title',
		array(
		    'label'   => __('Portfolio Taxonomy Title','hantus-pro'),
		    'section' => 'taxonomy_page_setting',
			'settings'=> 'portfolio_taxo_title',
			'type' => 'text',
			'description'    => __('', 'hantus-pro' ),
		)  
	);
	$wp_customize->add_setting(
    	'portfolio_taxo_description',
    	array(
	        'default'			=> __('You can judge my work by the portfolio we have done','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 3,
		)
	);
	
	$wp_customize->add_control( 
		'portfolio_taxo_description',
		array(
		    'label'   => __('Portfolio Taxonomy Description','hantus-pro'),
		    'section' => 'taxonomy_page_setting',
			'settings'=> 'portfolio_taxo_description',
			'type' => 'textarea',
		)  
	);
	
	
	// Pricing  Head 
	$wp_customize->add_setting(
		'texonomy_pricing_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 4,
		)
	);

	$wp_customize->add_control(
	'texonomy_pricing_head',
		array(
			'type' => 'hidden',
			'label' => __('Pricing','hantus-pro'),
			'section' => 'taxonomy_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'pricing_taxo_title',
    	array(
	        'default'			=> __('Pricing','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 5,
		)
	);
	
	$wp_customize->add_control( 
		'pricing_taxo_title',
		array(
		    'label'   => __('Pricing Taxonomy Title','hantus-pro'),
		    'section' => 'taxonomy_page_setting',
			'settings'=> 'pricing_taxo_title',
			'type' => 'text',
		)  
	);
	
	$wp_customize->add_setting(
    	'pricing_taxo_description',
    	array(
	        'default'			=> __('You can select a package from the list below to save more','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 6,
		)
	);
	
	$wp_customize->add_control( 
		'pricing_taxo_description',
		array(
		    'label'   => __('Pricing Taxonomy Description','hantus-pro'),
		    'section' => 'taxonomy_page_setting',
			'settings'=> 'pricing_taxo_description',
			'type' => 'textarea',
		)  
	);	
	
	/*=========================================
	Shop Page
	=========================================*/
	$wp_customize->add_section(
        'shop_page_setting',
        array(
        	'priority'      => 8,
            'title' 		=> __('Shop Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Title // 
	$wp_customize->add_setting(
    	'shop_pg_title',
    	array(
	        'default'			=> __('Title','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
		)
	);	
	
	$wp_customize->add_control( 
		'shop_pg_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'shop_page_setting',
			'settings' => 'shop_pg_title',
			'type' => 'text',
		)  
	);
	
	// Description // 
	$wp_customize->add_setting(
    	'shop_pg_desc',
    	array(
	        'default'			=> __('Description','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
		)
	);	
	
	$wp_customize->add_control( 
		'shop_pg_desc',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'shop_page_setting',
			'settings' => 'shop_pg_desc',
			'type' => 'textarea',
		)  
	);
	/*=========================================
	404 Page
	=========================================*/
	$wp_customize->add_section(
        'error_page_setting',
        array(
        	'priority'      => 10,
            'title' 		=> __('404 Page','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'page_temp_section',
		)
    );
	
	// Setting  Head 
	$wp_customize->add_setting(
		'error_page_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'error_page_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'error_page_setting',
		)
	);
	
	// Error Image Hide Show // 
	$wp_customize->add_setting( 
		'error_img_hide_show' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'error_img_hide_show', 
		array(
			'label'	      => esc_html__( ' Image Hide/Show', 'hantus-pro' ),
			'section'     => 'error_page_setting',
			'settings'    => 'error_img_hide_show',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Content  Head 
	$wp_customize->add_setting(
		'error_page_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'error_page_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'error_page_setting',
		)
	);
	
	$wp_customize->add_setting(
    	'error_page_title',
    	array(
	        'default'			=> 'Oops..',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 6,
		)
	);
	
	$wp_customize->add_control( 
		'error_page_title',
		array(
		    'label'   => __('Error Page Title','hantus-pro'),
		    'section' => 'error_page_setting',
			'settings'=> 'error_page_title',
			'type' => 'text',
		)  
	);
	
	$wp_customize->add_setting(
    	'error_page_description',
    	array(
	        'default'			=> 'Something Went Wrong Here',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 7,
		)
	);
	
	$wp_customize->add_control( 
		'error_page_description',
		array(
		    'label'   => __('Error Page Description','hantus-pro'),
		    'section' => 'error_page_setting',
			'settings'=> 'error_page_description',
			'type' => 'textarea',
		)  
	);
	$wp_customize->add_setting(
    	'error_page_btn_lbl',
    	array(
	        'default'			=> 'Back To Home',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 8,
		)
	);
	
	$wp_customize->add_control( 
		'error_page_btn_lbl',
		array(
		    'label'   => __('Buton Label','hantus-pro'),
		    'section' => 'error_page_setting',
			'settings'=> 'error_page_btn_lbl',
			'type' => 'text',
		)  
	);
	$wp_customize->add_setting(
    	'error_page_btn_link',
    	array(
	        'default'			=> '#',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 9,
		)
	);
	
	$wp_customize->add_control( 
		'error_page_btn_link',
		array(
		    'label'   => __('Buton Link','hantus-pro'),
		    'section' => 'error_page_setting',
			'settings'=> 'error_page_btn_link',
			'type' => 'text',
		)  
	);
	// Error  Image // 
    $wp_customize->add_setting( 
    	'error_page_image' , 
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/404-image.png',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 10,			
			
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'error_page_image' ,
		array(
			'label'          => __( 'Error Image', 'hantus-pro' ),
			'section'        => 'error_page_setting',
			'settings'   	 => 'error_page_image',
		) 
	));
}

add_action( 'customize_register', 'hantus_page_template_setting' );

// page template  selective refresh
function hantus_page_template_partials( $wp_customize ){
	// hide show welcome
	$wp_customize->selective_refresh->add_partial(
		'about_welcome_hide_show', array(
			'selector' => '#welcome',
			'container_inclusive' => true,
			'render_callback' => 'about_page_setting',
			'fallback_refresh' => true,
		)
	);
	//portfolio_page_hide_show
	$wp_customize->selective_refresh->add_partial(
		'portfolio_page_hide_show', array(
			'selector' => '.port-page',
			'container_inclusive' => true,
			'render_callback' => 'portfolio_page_setting',
			'fallback_refresh' => true,
		)
	);
	//service_service_hide_show
	$wp_customize->selective_refresh->add_partial(
		'service_service_hide_show', array(
			'selector' => '#services',
			'container_inclusive' => true,
			'render_callback' => 'service_page_setting',
			'fallback_refresh' => true,
		)
	);
	//  welcome title
	$wp_customize->selective_refresh->add_partial( 'about_welcome_title', array(
		'selector'            => '#welcome .section-title h3',
		'settings'            => 'about_welcome_title',
		'render_callback'  => 'template_about_welcome_title_render_callback',
	
	) );
	// welcome subtitle
	$wp_customize->selective_refresh->add_partial( 'about_welcome_subtitle', array(
		'selector'            => '#welcome .section-title h2',
		'settings'            => 'about_welcome_subtitle',
		'render_callback'  => 'template_about_welcome_subtitle_render_callback',
	
	) );
	// welcome description
	$wp_customize->selective_refresh->add_partial( 'about_welcome_description', array(
		'selector'            => '#welcome .section-title p',
		'settings'            => 'about_welcome_description',
		'render_callback'  => 'template_about_welcome_description_render_callback',
	
	) );
	//  about title
	$wp_customize->selective_refresh->add_partial( 'about_page_contents_title', array(
		'selector'            => '#wcu  h2#wcu-content-h2',
		'settings'            => 'about_page_contents_title',
		'render_callback'  => 'about_page_contents_title_render_callback',
	
	) );
	// about subtitle
	$wp_customize->selective_refresh->add_partial( 'about_page_content_subtitle', array(
		'selector'            => '#wcu  p#wcu-content-p',
		'settings'            => 'about_page_content_subtitle',
		'render_callback'  => 'about_page_content_subtitle_render_callback',
	
	) );
	// about description
	$wp_customize->selective_refresh->add_partial( 'about_page_content_description', array(
		'selector'            => '#wcu .wcu-content ul',
		'settings'            => 'about_page_content_description',
		'render_callback'  => 'about_page_content_description_render_callback',
	
	) );
	//  about_page_btn_lbl_title
	$wp_customize->selective_refresh->add_partial( 'about_page_btn_lbl_title', array(
		'selector'            => '#wcu .watch-more',
		'settings'            => 'about_page_btn_lbl_title',
		'render_callback'  => 'about_page_btn_lbl_title_render_callback',
	
	) );
	// about_page_funfacts_contents
	$wp_customize->selective_refresh->add_partial( 'about_page_funfacts_contents', array(
		'selector'            => '#wcu .fun-fact',
		'settings'            => 'about_page_funfacts_contents',
		'render_callback'  => 'about_page_funfacts_contents_subtitle_render_callback',
	
	) );
	// hide show gallery
	$wp_customize->selective_refresh->add_partial(
		'hide_show_gallery', array(
			'selector' => '#gallery',
			'container_inclusive' => true,
			'render_callback' => 'gallery_page_setting',
			'fallback_refresh' => true,
		)
	);
	// hide show subscribe
	$wp_customize->selective_refresh->add_partial(
		'hide_show_gallery_subscribe', array(
			'selector' => '#subscribe',
			'container_inclusive' => true,
			'render_callback' => 'gallery_page_setting',
			'fallback_refresh' => true,
		)
	);
	// gallery_page_img_setting
	$wp_customize->selective_refresh->add_partial( 'gallery_page_img_setting', array(
		'selector'            => '.gallery-page #grid',
		'settings'            => 'gallery_page_img_setting',
		'render_callback'  => 'gallery_page_img_setting_render_callback',
	
	) );
	// contact_page_form_shortcode
	$wp_customize->selective_refresh->add_partial( 'contact_page_form_shortcode', array(
		'selector'            => '#contact .contact-form',
		'settings'            => 'contact_page_form_shortcode',
		'render_callback'  => 'contact_page_form_shortcode_render_callback',
	
	) );
	// contact_page_form_title
	$wp_customize->selective_refresh->add_partial( 'contact_page_form_title', array(
		'selector'            => '#contact .contact-form h2',
		'settings'            => 'contact_page_form_title',
		'render_callback'  => 'contact_page_form_title_render_callback',
	
	) );
	// contact_page_info_title
	$wp_customize->selective_refresh->add_partial( 'contact_page_info_title', array(
		'selector'            => '#contact .contact-info h2',
		'settings'            => 'contact_page_info_title',
		'render_callback'  => 'hantus_contact_page_info_title_render_callback',
	
	) );
	// contact_page_info_desc
	$wp_customize->selective_refresh->add_partial( 'contact_page_info_desc', array(
		'selector'            => 'p#contct-info-dsc',
		'settings'            => 'contact_page_info_desc',
		'render_callback'  => 'hantus_contact_page_info_desc_render_callback',
	
	) );
// cotact_page_address_first_i first
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_first_i', array(
		'selector'            => '#info-box-first i',
		'settings'            => 'cotact_page_address_first_i',
		'render_callback'  => 'hantus_contact_address_icon_first_render_callback',
	
	) );
// cotact_page_address_first_t
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_first_t', array(
		'selector'            => '#contact #info-box-first h4',
		'settings'            => 'cotact_page_address_first_t',
		'render_callback'  => 'hantus_contact_address_first_t_render_callback',
	
	) );
//cotact_page_address_first_a

$wp_customize->selective_refresh->add_partial( 'cotact_page_address_first_a', array(
		'selector'            => '#contact #info-box-first p',
		'settings'            => 'cotact_page_address_first_a',
		'render_callback'  => 'hantus_contact_address_first_a_render_callback',
	
	) );	
	
// cotact_page_address_second_i
	
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_second_i', array(
		'selector'            => '#info-box-second i',
		'settings'            => 'cotact_page_address_second_i',
		'render_callback'  => 'hantus_contact_address_icon_second_render_callback',
	
	) );
// cotact_page_address_second_t
	
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_second_t', array(
		'selector'            => '#info-box-second h4',
		'settings'            => 'cotact_page_address_second_t',
		'render_callback'  => 'hantus_contact_address_second_t_render_callback',
	
	) );
// cotact_page_address_second_a
	
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_second_a', array(
		'selector'            => ' #info-box-second p',
		'settings'            => 'cotact_page_address_second_a',
		'render_callback'  => 'hantus_contact_address_second_a_render_callback',
	
	) );	
	
//cotact_page_address_third_i
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_third_i', array(
		'selector'            => '#info-box-third i',
		'settings'            => 'cotact_page_address_third_i',
		'render_callback'  => 'hantus_contact_address_icon_third_render_callback',
	
	) );
//cotact_page_address_third_t
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_third_t', array(
		'selector'            => '#info-box-third h4',
		'settings'            => 'cotact_page_address_third_t',
		'render_callback'  => 'hantus_contact_address_third_t_render_callback',
	
	) );	
//cotact_page_address_third_a
	$wp_customize->selective_refresh->add_partial( 'cotact_page_address_third_a', array(
		'selector'            => '#info-box-third p',
		'settings'            => 'cotact_page_address_third_a',
		'render_callback'  => 'hantus_contact_address_third_a_render_callback',
	
	) );	
	//service_page_title
	$wp_customize->selective_refresh->add_partial( 'service_page_title', array(
		'selector'            => '#service_pg .section-title h2',
		'settings'            => 'service_page_title',
		'render_callback'  => 'hantus_service_page_title_render_callback',
	
	) );	
//service_page_description
	$wp_customize->selective_refresh->add_partial( 'service_page_description', array(
		'selector'            => '#service_pg .section-title p',
		'settings'            => 'service_page_description',
		'render_callback'  => 'hantus_service_page_description_render_callback',
	
	) );	
	
	// menu page title
	$wp_customize->selective_refresh->add_partial( 'menu_item_title', array(
		'selector'            => '.pricing-page .section-title h2',
		'settings'            => 'menu_item_title',
		'render_callback'  => 'menu_page_section_title_render_callback',
	
	) );
	// menu page  description
	$wp_customize->selective_refresh->add_partial( 'menu_item_description', array(
		'selector'            => '.pricing-page .section-title p',
		'settings'            => 'menu_item_description',
		'render_callback'  => 'menu_page_section_desc_render_callback',
	
	) );
	
	// Price title
	$wp_customize->selective_refresh->add_partial( 'pricing_pg_title', array(
		'selector'            => '.price-pg .section-title h2',
		'settings'            => 'pricing_pg_title',
		'render_callback'  => 'home_section_pricing_pg_title_render_callback',
	
	) );
	// Price description
	$wp_customize->selective_refresh->add_partial( 'pricing_pg_description', array(
		'selector'            => '.price-pg .section-title p',
		'settings'            => 'pricing_pg_description',
		'render_callback'  => 'home_section_pricing_pg_description_render_callback',
	
	) );
}
add_action( 'customize_register', 'hantus_page_template_partials' );

// pricing_pg_title
function home_section_pricing_pg_title_render_callback() {
	return get_theme_mod( 'pricing_pg_title' );
}
// pricing_pg_description
function home_section_pricing_pg_description_render_callback() {
	return get_theme_mod( 'pricing_pg_description' );
}

// menu_item_title
function menu_page_section_title_render_callback() {
	return get_theme_mod( 'menu_item_title' );
}
// subtitle
function menu_page_section_desc_render_callback() {
	return get_theme_mod( 'menu_item_description' );
}	
// title
function template_about_welcome_title_render_callback() {
	return get_theme_mod( 'about_welcome_title' );
}
// subtitle
function template_about_welcome_subtitle_render_callback() {
	return get_theme_mod( 'about_welcome_subtitle' );
}
// description
function template_about_welcome_description_render_callback() {
	return get_theme_mod( 'about_welcome_description' );
}	
// about title
function about_page_contents_title_render_callback() {
	return get_theme_mod( 'about_page_contents_title' );
}
// about subtitle
function about_page_content_subtitle_render_callback() {
	return get_theme_mod( 'about_page_content_subtitle' );
}
// about description
function about_page_content_description_render_callback() {
	return get_theme_mod( 'about_page_content_description' );
}	
// about_page_btn_lbl_title
function about_page_btn_lbl_title_render_callback() {
	return get_theme_mod( 'about_page_btn_lbl_title' );
}	
// about_page_funfacts_contents
function about_page_funfacts_contents_subtitle_render_callback() {
	$about_page_funfacts_contents =  get_theme_mod( 'about_page_funfacts_contents' );
	about_page_funfacts_contents( $about_page_funfacts_contents, true );
	
}
// gallery_page_img_setting
function gallery_page_img_setting_render_callback() {
	$gallery_page_img_setting =  get_theme_mod( 'gallery_page_img_setting' );
	gallery_page_img_setting( $gallery_page_img_setting, true );
	
}
// contact_page_form_title
function contact_page_form_title_render_callback() {
	return get_theme_mod( 'contact_page_form_title' );
}

// contact_page_address_icon
function hantus_contact_address_icon_first_render_callback() {
	return get_theme_mod( 'cotact_page_address_first_i' );
}

// contact_page_address_first cont
function hantus_contact_address_first_t_render_callback() {
	return get_theme_mod( 'cotact_page_address_first_t' );
}
//hantus_contact_address_first_a_render_callback
function hantus_contact_address_first_a_render_callback() {
	return get_theme_mod( 'cotact_page_address_first_a' );
}
//hantus_contact_address_icon_second_render_callback
function hantus_contact_address_icon_second_render_callback() {
	return get_theme_mod( 'cotact_page_address_second_i' );
}
//hantus_contact_address_icon_second_render_callback
function hantus_contact_address_second_a_render_callback() {
	return get_theme_mod( 'cotact_page_address_second_a' );
}
//hantus_contact_address_icon_third_render_callback
function hantus_contact_address_icon_third_render_callback() {
	return get_theme_mod( 'cotact_page_address_third_i' );
}
//hantus_contact_address_third_t_render_callback
function hantus_contact_address_third_t_render_callback() {
	return get_theme_mod( 'cotact_page_address_third_t' );
}
//hantus_contact_address_third_a_render_callback
function hantus_contact_address_third_a_render_callback() {
	return get_theme_mod( 'cotact_page_address_third_a' );
}
// contact_page_info_title
function hantus_contact_page_info_title_render_callback() {
	return get_theme_mod( 'contact_page_info_title' );
}

// contact_page_info_desc
function hantus_contact_page_info_desc_render_callback() {
	return get_theme_mod( 'contact_page_info_desc' );
}
// contact_page_form_shortcode
function contact_page_form_shortcode_render_callback() {
	return get_theme_mod( 'contact_page_form_shortcode' );
}
// service_page_title
function hantus_service_page_title_render_callback() {
	return get_theme_mod( 'service_page_title' );
}
// service_page_description
function hantus_service_page_description_render_callback() {
	return get_theme_mod( 'service_page_description' );
}