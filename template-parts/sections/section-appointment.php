<?php
/**
 * Get default values for funfact section.
 *
 * @since 1.1.31
 * @access public
 */
 $default_content = null;
	if ( current_user_can( 'edit_theme_options' ) ) {
			$default_content = hantus_get_appoint_time_default();
		}

function hantus_get_appoint_time_default() {
	return apply_filters(
		'hantus_get_appoint_time_default', json_encode(
				 array(
				array(
					'title'            => esc_html__( 'Monday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_001',
				),
				array(
					'title'            => esc_html__( 'Tuesday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_002',
				
				),
				array(
					'title'            => esc_html__( 'Wednesday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_003',
			
				),
				array(
					'title'            => esc_html__( 'Thursday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_004',
					
				),
				array(
					'title'            => esc_html__( 'Friday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_005',
			
				),
				array(
					'title'            => esc_html__( 'Saturday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_006',
					
				),
				array(
					'title'            => esc_html__( 'Sunday :    Close', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_007',
			
				),
			)
		)
	);
}
?>	
 <?php
			$appointement_sec_settings			= get_theme_mod('appointement_sec_settings','1');
			$appointment_title					= get_theme_mod('appointment_title','Opening Hours');
			$appointment_subtitle				= get_theme_mod('appointment_subtitle','A collection of textile samples lay spread out on the table Samsa was a travelling salesman.');
			$appoint_time						= get_theme_mod('appoint_time',$default_content);
			$appointment_form_shortcode			= get_theme_mod('appointment_form_shortcode','');
			$appointment_background_setting		= get_theme_mod('appointment_background_setting',get_template_directory_uri() . '/assets/images/bg/appoinmentbg.jpg');
			$appointment_background_position	= get_theme_mod('appointment_background_position','fixed');
			

	?>
	<?php if($appointement_sec_settings) {?>
	<?php hantus_before_appointment_section_trigger(); ?>
 <!-- Start: Appoinment
    ============================= -->
    <?php if ( ! empty( $appointment_background_setting ) ) { ?>
    <section id="appoinment" class="section-padding" style="background:url('<?php echo esc_url($appointment_background_setting); ?>') no-repeat center / cover <?php echo esc_attr($appointment_background_position); ?>;">
        <?php } else { ?>
	<section id="appoinment" class="section-padding" style="background:#f7f7f7;">
		<?php } ?>
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="opening-hours">
						<?php if ( ! empty( $appointment_title ) ) : ?>
							<h3><?php echo esc_attr( $appointment_title ); ?></h3>
						<?php endif; ?>	
						<?php if ( ! empty( $appointment_subtitle ) ) : ?>
							<p><?php echo esc_attr( $appointment_subtitle ); ?></p>
						<?php endif; ?>	
                        <ul>
							<?php
								if ( ! empty( $appoint_time ) ) {
								$allowed_html = array(
								'br'     => array(),
								'em'     => array(),
								'strong' => array(),
								'b'      => array(),
								'i'      => array(),
								);
								$appoint_time = json_decode( $appoint_time );
								foreach ( $appoint_time as $appoint_time_item ) {
									$text = ! empty( $appoint_time_item->title ) ? apply_filters( 'hantus_translate_single_string', $appoint_time_item->title, 'sponser section' ) : '';
							?>
								<li><?php echo esc_html( $text ); ?></li>
							<?php } }?>	
                        </ul>
                    </div>
                    <div class="appoinment-wrapper">
					<?php 
						if($appointment_form_shortcode != '') {
							echo do_shortcode( $appointment_form_shortcode );
						}else{
					?>
                        <form action="#">
                            <select id="options">
                                <option value="">Massage Oil Full Body </option>
                                <option value="">Massage Oil Full Body </option>
                                <option value="">Massage Oil Full Body </option>
                                <option value="">Massage Oil Full Body </option>
                            </select>
                            <div class="dtb">
                                <input type="date">
                                <input type="time">
                                <select id="person">
                                    <option value="">5 person</option>
                                    <option value="">5 person</option>
                                    <option value="">5 person</option>
                                </select>
                            </div>

                            <span class="input input-hantus">
                                <input class="input_field" type="text" id="input-08" />
                                <label class="input_label" for="input-08">Name</label>
                            </span>


                            <span class="input input-hantus">
                                <input class="input_field" type="text" id="input-09" />
                                <label class="input_label" for="input-09">Phone</label>
                            </span>
        
                            <span class="input input-hantus textarea">
                                <textarea class="input_field" rows="5" id="input-10"></textarea>
                                <label class="input_label" for="input-10">Message</label>
                            </span>

                            <button type="submit" class="boxed-btn">Submit Now</button>
                        </form>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
	</section>
		<?php hantus_after_appointment_section_trigger(); } ?>
    <!-- End: Appoinment
    ============================= -->