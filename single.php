<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */

get_header();
?>
<?php
	$hide_show_blog_meta= get_theme_mod('hide_show_blog_meta','1');
	$blog_page_type = get_theme_mod('blog_page_type','grid');

?>
    <section id="blog-content" class="section-padding">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="<?php esc_attr(hantus_post_layout()); ?> mb-5 mb-lg-0">
						<?php if( have_posts() ): ?>
					
						<?php while( have_posts() ): the_post(); ?>
                            <article class="blog-post">
                                <div class="post-thumb">
                                     <?php the_post_thumbnail(); ?>
                                </div>

                                <div class="post-content">
									<?php if($hide_show_blog_meta == 1 ) {?>
										<ul class="meta-info">
											<li class="post-date"><a href="<?php echo esc_url(get_month_link(get_post_time('Y'),get_post_time('m'))); ?>"><?php esc_html_e('On','hantus-pro'); ?><?php echo get_the_date('j'); ?> <?php echo get_the_date('M'); ?>, <?php echo get_the_date('Y'); ?></a></li>
											<li class="posted-by"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php esc_html_e('by','hantus-pro'); ?> <?php the_author(); ?></a></li>
										</ul>
									<?php } ?>	
                                   <?php     
											if ( is_single() ) :
											
											the_title('<h4 class="post-title">', '</h4>' );
											
											else:
											
											the_title( sprintf( '<h4 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
											
											endif; 
										?>
                                    <?php 
										the_content( 
											sprintf( 
												__( 'Read More', 'hantus-pro' ), 
												'<span class="screen-reader-text">  '.get_the_title().'</span>' 
											) 
										);
									?>
                                </div>
								<article class="blog-post author-details">
									<div class="media">
										<div class="auth-mata">
											<?php echo get_avatar( get_the_author_meta('ID'), 200); ?>
										</div>
										<div class="media-body author-meta-det">
											<h6><?php the_author_link(); ?></h6>
											<p><?php the_author_meta( 'description' ); ?></p>
											<ul class="blog-author-social">
												<?php $facebook_profile = get_the_author_meta( 'facebook_profile' ); if ( $facebook_profile && $facebook_profile != '' ): ?>
												<li class="facebook"><a href="<?php echo esc_url($facebook_profile); ?>"><i class="fa fa-facebook"></i></a></li>
												<?php endif; ?>
												<?php $linkedin_profile = get_the_author_meta( 'linkedin_profile' ); if ( $linkedin_profile && $linkedin_profile != '' ): ?>
												<li class="linkedin"><a href="<?php echo esc_url($linkedin_profile); ?>"><i class="fa fa-linkedin"></i></a></li>
												<?php endif; ?>
												<?php $twitter_profile = get_the_author_meta( 'twitter_profile' ); if ( $twitter_profile && $twitter_profile != '' ): ?>
												<li class="twitter"><a href="<?php echo esc_url($twitter_profile); ?>"><i class="fa fa-twitter"></i></a></li>
												<?php endif; ?>
												<?php $google_profile = get_the_author_meta( 'google_profile' ); if ( $google_profile && $google_profile != '' ): ?>
												<li class="googleplus"><a href="<?php echo esc_url($google_profile); ?>"><i class="fa fa-google-plus"></i></a></li>
												<?php endif; ?>
										   </ul>
										</div>
									</div>	
								</article>
								<?php comments_template( '', true ); // show comments  ?>
                            </article>   
						<?php endwhile; ?>						
					<?php endif; ?>
						
                </div>
				 <?php get_sidebar(); ?>
                <!-- Sidebar -->
                
            </div>

        </div>
    </section>

<div class="clearfix"></div>

<?php get_footer(); ?>
