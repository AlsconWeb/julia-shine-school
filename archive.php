<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */

get_header();
?>
<section id="blog-content" class="section-padding">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="<?php esc_attr(hantus_post_layout()); ?> mb-5 mb-lg-0">

                    <div class="row">
						<?php if( have_posts() ): ?>
						<?php while( have_posts() ): the_post(); ?>
                        <div class="col-md-12">
							<?php get_template_part('template-parts/content/content','page'); ?>		
                        </div>
						<?php 
								endwhile;
							endif;
						?>
                    </div>                    

                    <!-- Pagination -->
						<?php								
						// Previous/next page navigation.
						the_posts_pagination( array(
						'prev_text'          => '<i class="fa fa-angle-double-left"></i>',
						'next_text'          => '<i class="fa fa-angle-double-right"></i>',
						) ); ?>
					<!-- Pagination -->	
                </div>

                <!-- Sidebar -->
                <?php get_sidebar(); ?>
            </div>

        </div>
    </section>


<?php get_footer(); ?>
