<?php
function hantus_testimonial_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Testimonial Section Panel
	=========================================*/
		$wp_customize->add_section(
			'testimonial_setting', array(
				'title' => esc_html__( 'Testimonial Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 37, 'hantus_Testimonial' ),
			)
		);
	/*=========================================
	Testimonial Settings Section
	=========================================*/

	// Setting  Head
	$wp_customize->add_setting(
		'testimonial_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'testimonial_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'testimonial_setting',
		)
	);

	$wp_customize->add_setting(
		'hide_show_testimonial' ,
			array(
			'default' =>  esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'hide_show_testimonial',
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'testimonial_setting',
			'settings'    => 'hide_show_testimonial',
			'type'        => 'ios', // light, ios, flat
		)
	));

	/*=========================================
	Testimonial Header Section
	=========================================*/
	// Testimonial Content Section //

	// Content  Head
	$wp_customize->add_setting(
		'testimonial_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'testimonial_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'testimonial_setting',
		)
	);

	// demo video
	$wp_customize->add_setting(
		'testimonial_video_deo'
		,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
		'testimonial_video_deo',
		array(
			'type' => 'checkbox',
			'label' => __('Demo video','hantus-pro'),
			'section' => 'testimonial_setting',
		)
	);

	/**
	 * Customizer Repeater for add Testimonial
	 */
		$wp_customize->add_setting( 'testimonial_contents',
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			    'default' => hantus_get_testimonial_default(),
				'priority' => 6,
			)
		);

		$wp_customize->add_control(
			new hantus_Repeater( $wp_customize,
				'testimonial_contents',
					array(
						'label'   => esc_html__('Testimonial','hantus-pro'),
						'section' => 'testimonial_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Testimonial', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_designation_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_ju_video_url_control' => true,
						'customizer_repeater_ju_video_url_safari_control' => true,
						'customizer_repeater_ju_key_hd_control' => true,
						'customizer_repeater_ju_key_sd_control' => true,
						'customizer_repeater_ju_key_audio_control' => true,
						'customizer_repeater_poster_control' => true,
					)
				)
			);
	// testimonial Background Section //

	// BG  Head
	$wp_customize->add_setting(
		'testimonial_bg_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'testimonial_bg_head',
		array(
			'type' => 'hidden',
			'label' => __('Background','hantus-pro'),
			'section' => 'testimonial_setting',
		)
	);

	// Background Image //
    $wp_customize->add_setting(
    	'testimonial_background_setting' ,
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/bg/testimonial-bg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 9,
		)
	);

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'testimonial_background_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'testimonial_setting',
			'settings'   	 => 'testimonial_background_setting',
		)
	));



	$wp_customize->add_setting(
		'testimonial_background_position' ,
			array(
			'default' => __( 'fixed', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 10,
		)
	);

	$wp_customize->add_control(
		'testimonial_background_position' ,
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'testimonial_setting',
				'settings'       => 'testimonial_background_position',
				'type'           => 'radio',
				'choices'        =>
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)
		)
	);
}
add_action( 'customize_register', 'hantus_testimonial_setting' );

// Testimonial selective refresh
function hantus_home_testimonial_section_partials( $wp_customize ){
		// hide_show_testimonial
	$wp_customize->selective_refresh->add_partial(
		'hide_show_testimonial', array(
			'selector' => '#testimonial',
			'container_inclusive' => true,
			'render_callback' => 'testimonial_setting',
			'fallback_refresh' => true,
		)
	);
	$wp_customize->selective_refresh->add_partial( 'testimonial_contents', array(
		'selector'            => '#testimonial .tst_contents',
		'settings'            => 'testimonial_contents',
		'render_callback'  => 'home_section_testimonial_contents_render_callback',

	) );

	}

add_action( 'customize_register', 'hantus_home_testimonial_section_partials' );

// contents
function home_section_testimonial_contents_render_callback() {
	return get_theme_mod( 'testimonial_contents' );
}
