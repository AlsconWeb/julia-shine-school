<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package hantus
 */

get_header();
?>



<!-- Blog & Sidebar Section -->
 <section id="product" class="section-padding">
        <div class="container">
            <div class="row">
			<!--Blog Detail-->
			<div class="col-md-12">
					
					<?php woocommerce_content(); ?>
			</div>
			<!--/End of Blog Detail-->
			<?php get_sidebar('woocommerce'); ?>
		</div>	
	</div>
</section>
<!-- End of Blog & Sidebar Section -->
 
<div class="clearfix"></div>

<?php get_footer(); ?>

