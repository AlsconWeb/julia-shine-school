<?php
function hantus_section_appointment_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';

	/*=========================================
	Call Action Section Panel
	=========================================*/
		$wp_customize->add_section(
			'appointment_setting', array(
				'title' => esc_html__( 'Store Hour Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority',41, 'hantus_welcome' ),
			)
		);
	
	/*=========================================
	Call Action Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'appointment_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'appointment_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'appointment_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'appointement_sec_settings' , 
			array(
			'default' => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'appointement_sec_settings', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'appointment_setting',
			'settings'    => 'appointement_sec_settings',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	//  Head 
	$wp_customize->add_setting(
		'appointment_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'appointment_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'appointment_setting',
		)
	);
	
	//title
	$wp_customize->add_setting(
    	'appointment_title',
    	array(
	        'default'			=> __('Opening Hours','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);
	
	$wp_customize->add_control( 
		'appointment_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'appointment_setting',
			'settings'=> 'appointment_title',
			'type' => 'text',
			'description'    => __('', 'hantus-pro' ),
		)  
	);
	//subtitle
	$wp_customize->add_setting(
    	'appointment_subtitle',
    	array(
	        'default'			=> __('A collection of textile samples lay spread out on the table Samsa was a travelling salesman.','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);
	
	$wp_customize->add_control( 
		'appointment_subtitle',
		array(
		    'label'   => __('Subtitle','hantus-pro'),
		    'section' => 'appointment_setting',
			'settings'=> 'appointment_subtitle',
			'type' => 'textarea',
		)  
	);
	
	//  Content Head 
	$wp_customize->add_setting(
		'appointment_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 11,
		)
	);

	$wp_customize->add_control(
	'appointment_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'appointment_setting',
		)
	);

	/**
	 * Customizer Repeater for add funfact
	 */
		$wp_customize->add_setting( 'appoint_time', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'priority' => 12,
			   'default' => json_encode( 
			 array(
				array(
					'title'            => esc_html__( 'Monday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_001',
				),
				array(
					'title'            => esc_html__( 'Tuesday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_002',
				
				),
				array(
					'title'            => esc_html__( 'Wednesday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_003',
			
				),
				array(
					'title'            => esc_html__( 'Thursday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_004',
					
				),
				array(
					'title'            => esc_html__( 'Friday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_005',
			
				),
				array(
					'title'            => esc_html__( 'Saturday :    8:00am - 21:00pm', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_006',
					
				),
				array(
					'title'            => esc_html__( 'Sunday :    Close', 'hantus-pro' ),
					'id'              => 'customizer_repeater_appoint_time_007',
			
				),
			)
			 )
			)
		);
		
		$wp_customize->add_control( 
			new Hantus_Repeater( $wp_customize, 
				'appoint_time', 
					array(
						'label'   => esc_html__('Time','hantus-pro'),
						'section' => 'appointment_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Time', 'hantus-pro' ),
						'customizer_repeater_title_control' => true,
					) 
				) 
			);
	
	$wp_customize->add_setting(
    	'appointment_form_shortcode',
    	array(
	        'default'			=> __('','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 13,
		)
	);
	
	$wp_customize->add_control( 
		'appointment_form_shortcode',
		array(
		    'label'   => __('Appointment Form Shortcode','hantus-pro'),
		    'section' => 'appointment_setting',
			'settings'=> 'appointment_form_shortcode',
			'type' => 'textarea',
		)  
	);
	
	
	//  BG Head 
	$wp_customize->add_setting(
		'appointment_bg_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 14,
		)
	);

	$wp_customize->add_control(
	'appointment_bg_head',
		array(
			'type' => 'hidden',
			'label' => __('Background','hantus-pro'),
			'section' => 'appointment_setting',
		)
	);
	
	// Background Image // 
    $wp_customize->add_setting( 
    	'appointment_background_setting' , 
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/bg/appoinmentbg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 15,
			
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'appointment_background_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'appointment_setting',
			'settings'        => 'appointment_background_setting',
		) 
	));
	
	$wp_customize->add_setting( 
		'appointment_background_position' , 
			array(
			'default' => __( 'scroll', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 16,
		) 
	);
	
	$wp_customize->add_control(
		'appointment_background_position' , 
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'appointment_setting',
				'settings'       => 'appointment_background_position',
				'type'           => 'radio',
				'choices'        => 
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)  
		) 
	);
}

add_action( 'customize_register', 'hantus_section_appointment_setting' );

function hantus_home_appointment_section_partials( $wp_customize ){

		// appointement_sec_settings
	$wp_customize->selective_refresh->add_partial(
		'appointement_sec_settings', array(
			'selector' => '#appoinment',
			'container_inclusive' => true,
			'render_callback' => 'appointment_setting',
			'fallback_refresh' => true,
		)
	);
	//info  section first
	$wp_customize->selective_refresh->add_partial( 'appointment_title', array(
		'selector'            => '#appoinment .opening-hours h3',
		'settings'            => 'appointment_title',
		'render_callback'  => 'appointment_title_render_callback',
	
	) );
	
	$wp_customize->selective_refresh->add_partial( 'appointment_subtitle', array(
		'selector'            => '#appoinment .opening-hours p',
		'settings'            => 'appointment_subtitle',
		'render_callback'  => 'appointment_subtitle_render_callback',
	
	) );
	//description
		$wp_customize->selective_refresh->add_partial( 'appointment_description', array(
		'selector'            => '#appoinment .opening-hours ul',
		'settings'            => 'appointment_description',
		'render_callback'  => 'appointment_description_render_callback',
	
	) );
	//shortcode
		$wp_customize->selective_refresh->add_partial( 'appointment_form_shortcode', array(
		'selector'            => '#appoinment .appoinment-wrapper',
		'settings'            => 'appointment_form_shortcode',
		'render_callback'  => 'appointment_form_shortcode_render_callback',
	
	) );
	}

add_action( 'customize_register', 'hantus_home_appointment_section_partials' );

//  title
function appointment_title_render_callback() {
	return get_theme_mod( 'appointment_title' );
}
// subtitle
function appointment_subtitle_render_callback() {
	return get_theme_mod( 'appointment_subtitle' );
}
// description
function appointment_description_render_callback() {
	return get_theme_mod( 'appointment_description' );
}
// shortcode
function appointment_form_shortcode_render_callback() {
	return get_theme_mod( 'appointment_form_shortcode' );
}
