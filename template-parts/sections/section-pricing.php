<?php 
		$hide_show_pricing= get_theme_mod('hide_show_pricing','1'); 
		$pricing_title= get_theme_mod('pricing_title','Pricing');
		$pricing_description= get_theme_mod('pricing_description','You can select a package from the list below to save more');
		$pricing_display_num= get_theme_mod('pricing_display_num','4');
		$price_sec_column= get_theme_mod('price_sec_column','4');
		
	?>
<?php  if($hide_show_pricing == '1') { ?>
<?php  hantus_before_pricing_section_trigger(); ?>	
<!-- Service Section -->
    <section id="pricing" class="section-padding price-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
						<?php if( ($pricing_title) || ($pricing_description)!='' ) { ?>
							<h2><?php echo $pricing_title; ?></h2>
							<p><?php echo $pricing_description; ?></p>
						<?php } ?>
                    </div>
                </div>
            </div>
			<?php if ( function_exists( 'hantus_pricing' ) ){ ?>	
            <div class="row">
							<?php 
				if ( ! empty( $pricing_title )) {
				$args = array( 'post_type' => 'hantuss_pricing','posts_per_page' => $pricing_display_num);  	
				$plans = new WP_Query( $args ); 
				if( $plans->have_posts() )
				{
					while ( $plans->have_posts() ) : $plans->the_post();
			?>
                <div class="col-lg-<?php echo $price_sec_column; ?> col-md-6 mb-5 mb-lg-0 mx-sm-auto price-contents">
					<?php get_template_part('template-parts/pricing/pricing','content'); ?>
                </div>
				<?php 
				
					endwhile;
						} 
				 }
			?>
            </div>
			<?php
				}else{
				echo __('Please install & Activate Hantus Feature plugin for add Pricing','hantus-pro');
				}
			?>
        </div>
    </section>

<?php hantus_after_pricing_section_trigger(); } ?>	
<!-- /Service Section -->
