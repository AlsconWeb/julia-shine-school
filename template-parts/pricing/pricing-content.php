<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */
$plans_currency 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_currency', true ));
$plans_price 			= sanitize_text_field( get_post_meta( get_the_ID(),'plans_price', true ));
$plans_duration 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_duration', true ));
$plans_features_1 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_1', true ));

$plans_features_2 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_2', true ));
$plans_features_3 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_3', true ));
$plans_features_4 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_4', true ));
$plans_features_5 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_5', true ));

$plans_features_6 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_6', true ));
$plans_features_7 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_7', true ));
$plans_features_8 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_8', true ));
$plans_features_9 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_9', true ));

$plans_features_10 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_10', true ));
$price_recomended 		= sanitize_text_field( get_post_meta( get_the_ID(),'price_recomended', true ));
$plans_button_label 	= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_label', true ));
$plans_button_link 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_link', true ));
$plans_button_link_target 	= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_link_target', true ));
?>
<?php if ( ! empty( $price_recomended )) { ?>
	<div class="pricing-box recomended text-center" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;">
<?php }else{ ?>
	<div class="pricing-box text-center" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;">
<?php } ?>	
	  <?php if ( ! empty( $price_recomended ) ) : ?>
			<div class="recomended-text"><span><?php echo esc_html($price_recomended); ?></span></div>
	  <?php endif; ?>
	<h3><?php echo the_title(); ?></h3>
	<div class="price"><sup><?php echo esc_html($plans_currency); ?></sup> <span><?php echo esc_html($plans_price); ?></span><?php echo esc_html($plans_duration); ?></div>
	<ul class="pricing-content">
		<li><?php echo esc_html($plans_features_1); ?></li>
		<li><?php echo esc_html($plans_features_2); ?></li>
		<li><?php echo esc_html($plans_features_3); ?></li>
		<li><?php echo esc_html($plans_features_4); ?></li>
		<li><?php echo esc_html($plans_features_5); ?></li>
		<li><?php echo esc_html($plans_features_6); ?></li>
		<li><?php echo esc_html($plans_features_7); ?></li>
		<li><?php echo esc_html($plans_features_8); ?></li>
		<li><?php echo esc_html($plans_features_9); ?></li>
		<li><?php echo esc_html($plans_features_10); ?></li>
	</ul>
	<a href="<?php echo esc_url($plans_button_link); ?>" <?php  if($plans_button_link_target) { echo "target='_blank'"; } ?> class="boxed-btn"><?php echo esc_attr($plans_button_label); ?></a>
</div>