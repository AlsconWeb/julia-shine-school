<?php
/**
Template Name: Classic Portfolio 3 columns
**/
get_header();
?>
<?php 
	$portfolio_page_hide_show = get_theme_mod('portfolio_page_hide_show','1');
	$portfolio_page_news_hide_show = get_theme_mod('portfolio_page_news_hide_show','1');
	$post_type = 'hantus_portfolio';
	$tax = 'portfolio_categories'; 
	$tax_terms = get_terms($tax);
?>
<?php 
	$portfolio_page_per_post= get_theme_mod('portfolio_page_per_post','3'); 
?>
<?php hantus_before_port_page_section_trigger(); ?> 
<?php if ( function_exists( 'hantus_portfolio' ) ){ ?>
 <?php if($portfolio_page_hide_show == '1') {?>
  <!-- Start: Portfolio Masonary
    ============================= -->     
    <section id="portfolio-page" class="custom-padding portfolio-page port-page">
        <div class="container">
            <div class="row portfolio-tab">
                <div class="col-md-12 text-center">
                	<?php if (!$tax_terms == 0) { ?>
                    <ul class="portfolio-tab-sorting sorting-btn" id="filter">
                        <?php foreach ($tax_terms as $tax_term) { ?>
							<?php if($tax_term->name == 'ALL'){ ?>
							<li><a href="#" data-group="<?php echo $tax_term->slug; ?>" class="active"><?php echo $tax_term->name; ?></a></li>
							<?php }else{ ?>
							<li><a href="#" data-group="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a></li>
						<?php } } ?>
                    </ul>
                	<?php } ?>
                </div>
            </div>

            <div class="row portfolio" id="grid">
				 <?php 
				$args = array( 'post_type' => 'hantus_portfolio','posts_per_page' => $portfolio_page_per_post);  	
				$portfolio = new WP_Query( $args ); 
				if( $portfolio->have_posts() )
				{
					while ( $portfolio->have_posts() ) : $portfolio->the_post();
					$terms = get_the_terms( $post->ID, 'portfolio_categories' );
									
					if ( $terms && ! is_wp_error( $terms ) ) : 
						$links = array();

						foreach ( $terms as $term ) 
						{
							$links[] = $term->slug;
						}
						
						$tax = join( '","', $links );		
					else :	
						$tax = '';	
					endif;
			?>
			
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 portfolio-item" data-groups='["<?php echo strtolower($tax); ?>"]'>
                     <?php get_template_part('template-parts/portfolio/portfolio','content'); ?>
                </div>
				<?php 	
					endwhile; 
					}
				?>
            </div>
        </div>
    </section>

    <!-- End: Portfolio 3 Column
    ============================= -->
 <?php } 
		}else{
			echo __('Please install & Activate Hantus Feature plugin for add Portfolio','hantus-pro');
			}
 ?>
 <?php hantus_after_port_page_section_trigger(); ?>
 <!-- Start: Subscribe
    ============================= -->
    <?php if($portfolio_page_news_hide_show == '1') {?>
		<?php get_template_part('template-parts/sections/section','newsletter');	 ?>
    <?php } ?>
    <!-- End: Subscribe
    ============================= -->
<?php get_footer(); ?>	