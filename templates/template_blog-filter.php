<?php
/**
Template Name: Blog Filter
**/

get_header();
?>
<?php
	$tax = 'post'; 
	$categories = get_terms($tax);	
	$blog_read_more = get_theme_mod('blog_read_more','Read More');
	$hide_show_blog_meta= get_theme_mod('hide_show_blog_meta','1');
	$blog_filter_title = get_theme_mod('blog_filter_title','Blog Title');
	$blog_filter_desc = get_theme_mod('blog_filter_desc','Blog Description');
?>
 <section id="blog-content" class="full-width section-padding">
        <div class="container-fluid">
			<div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
						<div class="section-title service-section">
					<?php if($blog_filter_title) { ?>
						<h2><?php echo esc_html($blog_filter_title); ?></h2>
					<?php } ?>
					<?php if($blog_filter_desc) { ?>
						<p><?php echo esc_html($blog_filter_desc); ?></p>
					<?php } ?>	
					</div>
				</div>
			</div>
			<div class="row portfolio-tab">
                <div class="col-md-12 text-center">
                	<?php 
					$categories = get_categories();
					if (!$categories == 0) {
					?>
                    <ul class="portfolio-tab-sorting sorting-btn" id="filter">
						<?php foreach($categories as $category) { ?>
						<?php if($category->name == 'All'){ ?>
							<li><a href="#" class="active" data-group="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li>
					    <?php } else { ?>
							<li><a href="#" data-group="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li>
					    <?php } } ?>
                    </ul>
                	<?php } ?>
                </div>
            </div>
            <div class="row full-width" id="grid">
				<?php 
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$args = array( 'post_type' => 'post','paged'=>$paged );	
					$loop = new WP_Query( $args );
				?>
				<?php if( $loop->have_posts() ): ?>
					<?php while( $loop->have_posts() ): $loop->the_post(); ?>
					<?php
						$terms = get_the_category( $post->ID, 'post' );
											
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$links = array();

							foreach ( $terms as $term ) 
							{
								$links[] = $term->slug;
							}
							
							$tax = join( '","', $links );		
						else :	
							$tax = '';	
						endif;
					?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 clearfix" data-groups='["<?php echo strtolower($tax); ?>"]'>
                    <?php get_template_part('template-parts/content/content','page'); ?>
                </div>  
				<?php 
						endwhile;
					endif;
				?>
            </div>
        </div>
    </section>


<?php get_footer(); ?>
