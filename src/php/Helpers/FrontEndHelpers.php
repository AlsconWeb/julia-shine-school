<?php
/**
 * Front-End helpers.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme\Helpers;

class FrontEndHelpers {
	/**
	 * Generate_unic id.
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function generate_unic_id(): string {
		$length       = 10;
		$randomBytes  = random_bytes( $length );
		$base64String = base64_encode( $randomBytes );
		$lettersOnly  = preg_replace( "/[^A-Za-z]/", '', $base64String );

		return substr( $lettersOnly, 0, $length );
	}
}
