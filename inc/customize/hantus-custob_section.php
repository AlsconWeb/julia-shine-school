<?php
function hantus_custob_code( $wp_ustomizer ) {
$selective_refresh = isset( $wp_ustomizer->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Custob Section Panel
	=========================================*/
		$wp_ustomizer->add_section(
			'hantus_custob_setting', array(
				'title' => esc_html__( 'Custob Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 128, 'hantus_Custob' ),
			)
		);
		
	// Setting  Head 
	$wp_ustomizer->add_setting(
		'custob_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_ustomizer->add_control(
	'custob_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'hantus_custob_setting',
		)
	);
	
	// Custob Code Settings Section // 
	$wp_ustomizer->add_setting( 
		'hide_show_custob_section' , 
			array(
			'default' =>  0,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_ustomizer->add_control( new ustomizerr_Toggle_Control( $wp_ustomizer, 
	'hide_show_custob_section', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'hantus_custob_setting',
			'settings'    => 'hide_show_custob_section',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header Settings Section // 
	
	
	//  Head 
	$wp_ustomizer->add_setting(
		'custob_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_ustomizer->add_control(
	'custob_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'hantus_custob_setting',
		)
	);
	
	// Custob Section Title // 
	$wp_ustomizer->add_setting(
    	'custob_section_title',
    	array(
	        'default'			=> __('Title','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_ustomizer->add_control( 
		'custob_section_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'hantus_custob_setting',
			'settings' => 'custob_section_title',
			'type' => 'text',
		)  
	);
	
	// Custob Section Description // 
	$wp_ustomizer->add_setting(
    	'custob_section_description',
    	array(
	        'default'			=> __('Description','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_ustomizer->add_control( 
		'custob_section_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'hantus_custob_setting',
			'settings' => 'custob_section_description',
			'type' => 'textarea',
		)  
	);
	
	//  Content Head 
	$wp_ustomizer->add_setting(
		'custob_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_ustomizer->add_control(
	'custob_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'hantus_custob_setting',
		)
	);
	
	// custob content // 
	
	$page_editor_path = trailingslashit( get_template_directory() ) . 'inc/custob-controls/editor/ustomizerr-page-editor.php';
		if ( file_exists( $page_editor_path ) ) {
			require_once( $page_editor_path );
		}
	if ( class_exists( 'hantus_Page_Editor' ) ) {
		$frontpage_id = get_option( 'page_on_front' );
		$default = '';
		if ( ! empty( $frontpage_id ) ) {
			$default = get_post_field( 'post_content', $frontpage_id );
		}
		$wp_ustomizer->add_setting(
			'hantus_page_editor', array(
				'default' => __('Custob Section Description','hantus-pro'),
				'sanitize_callback' => 'wp_kses_post',
				'transport'         => $selective_refresh,
				'priority' => 9,
				
			)
		);

		$wp_ustomizer->add_control(
			new hantus_Page_Editor(
				$wp_ustomizer, 'hantus_page_editor', array(
					'label' => esc_html__( 'Content', 'hantus-pro' ),
					'section' => 'hantus_custob_setting',
					'priority' => 10,
					'needsync' => true,
				)
			)
		);
	}
	$default = '';
	
	
}
add_action( 'ustomizer_register', 'hantus_custob_code' );

// custob section selective refresh
function hantus_custobs_section_partials( $wp_ustomizer ){

	// hide_show_custob_section
	$wp_ustomizer->selective_refresh->add_partial(
		'hide_show_custob_section', array(
			'selector' => '#custob_section',
			'container_inclusive' => true,
			'render_callback' => 'hantus_custob_setting',
			'fallback_refresh' => true,
		)
	);
	//info  section first
	$wp_ustomizer->selective_refresh->add_partial( 'custob_section_title', array(
		'selector'            => '#custob_section .section-title h2',
		'settings'            => 'custob_section_title',
		'render_callback'  => 'custobs_section_title_render_callback',
	
	) );
	
	$wp_ustomizer->selective_refresh->add_partial( 'custob_section_description', array(
		'selector'            => '#custob_section .section-title p',
		'settings'            => 'custob_section_description',
		'render_callback'  => 'custobs_section_dis_render_callback',
	
	) );
	
	$wp_ustomizer->selective_refresh->add_partial( 'hantus_page_editor', array(
		'selector'            => '#custob_section .custob_editor',
		'settings'            => 'hantus_page_editor',
		'render_callback'  => 'custobs_section_editor_render_callback',
	
	) );
	}

add_action( 'ustomizer_register', 'hantus_custobs_section_partials' );

// cta editor
function custobs_section_title_render_callback() {
	return get_theme_mod( 'custob_section_title' );
}
// cta button label
function custobs_section_dis_render_callback() {
	return get_theme_mod( 'custob_section_description' );
}

function custobs_section_editor_render_callback() {
	return get_theme_mod( 'hantus_page_editor' );
}