<?php function hantus_typography_customizer( $wp_customize ) {
	$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
$wp_customize->add_panel( 'hantus_typography_setting', array(
		'priority'       => 38,
		'capability'     => 'edit_theme_options',
		'title'      => __('Typography','hantus-pro'),
	) );
	
// Typography Hide/ Show Setting // 

$wp_customize->add_section(
	'typography_setting' ,
		array(
		'title'      => __('Settings','hantus-pro'),
		'panel' => 'hantus_typography_setting',
		'priority'       => 1,
   	) );
	$wp_customize->add_setting( 
		'hide_show_typography' , 
			array(
			'default' => 0,
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_typography', 
		array(
			'label'	      => esc_html__( 'Enable Typography', 'hantus-pro' ),
			'section'     => 'typography_setting',
			'settings'    => 'hide_show_typography',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
$font_size = array();
for($i=9; $i<=100; $i++)
{			
	$font_size[$i] = $i;
}

$font_family = array('Dosis'=>'Dosis','Raleway'=>'Raleway','Arial, sans-serif'=>'Arial','Georgia, serif'=>'Georgia','Times New Roman, serif'=>'Times New Roman','Tahoma, Geneva, Verdana, sans-serif'=>'Tahoma','Palatino, Palatino Linotype, serif'=>'Palatino','Helvetica Neue, Helvetica, sans-serif'=>'Helvetica*','Calibri, Candara, Segoe, Optima, sans-serif'=>'Calibri*','Myriad Pro, Myriad, sans-serif'=>'Myriad Pro*','Lucida Grande, Lucida Sans Unicode, Lucida Sans, sans-serif'=>'Lucida','Arial Black, sans-serif'=>'Arial Black','Gill Sans, Gill Sans MT, Calibri, sans-serif'=>'Gill Sans*','Geneva, Tahoma, Verdana, sans-serif'=>'Geneva*','Impact, Charcoal, sans-serif'=>'Impact','Courier, Courier New, monospace'=>'Courier','Abel'=>'Abel','Abril Fatface'=>'Abril Fatface','Aclonica'=>'Aclonica','Actor'=>'Actor','Adamina'=>'Adamina','Aldrich'=>'Aldrich','Alice'=>'Alice','Alike'=>'Alike','Alike Angular'=>'Alike Angular','Allan'=>'Allan','Allerta'=>'Allerta','Allerta Stencil'=>'Allerta Stencil','Amaranth'=>'Amaranth','Amatic SC'=>'Amatic SC','Andada'=>'Andada','Andika'=>'Andika','Annie Use Your Telescope'=>'Annie Use Your Telescope','Anonymous Pro'=>'Anonymous Pro','Antic'=>'Antic','Anton'=>'Anton','Architects Daughter'=>'Architects Daughter','Arimo'=>'Arimo','Artifika'=>'Artifika','Arvo'=>'Arvo','Asset'=>'Asset','Astloch'=>'Astloch','Atomic Age'=>'Atomic Age','Aubrey'=>'Aubrey','Bangers'=>'Bangers','Bentham'=>'Bentham','Bevan'=>'Bevan','Bigshot One'=>'Bigshot One','Black Ops One'=>'Black Ops One','Bowlby One'=>'Bowlby One','Bowlby One SC'=>'Bowlby One SC','Brawler'=>'Brawler','Butcherman Caps'=>'Butcherman Caps','Cabin'=>'Cabin','Cabin Sketch'=>'Cabin Sketch','Cabin Sketch'=>'Cabin Sketch','Calligraffitti'=>'Calligraffitti','Candal'=>'Candal','Cantarell'=>'Cantarell','Cardo'=>'Cardo','Carme'=>'Carme','Carter One'=>'Carter One','Caudex'=>'Caudex','Cedarville Cursive'=>'Cedarville Cursive','Changa One'=>'Changa One','Cherry Cream Soda'=>'Cherry Cream Soda','Chewy'=>'Chewy','Chivo'=>'Chivo','Coda'=>'Coda','Comfortaa'=>'Comfortaa','Coming Soon'=>'Coming Soon','Contrail One'=>'Contrail One','Copse'=>'Copse','Corben'=>'Corben','Corben'=>'Corben','Cousine'=>'Cousine','Coustard'=>'Coustard','Covered By Your Grace'=>'Covered By Your Grace','Crafty Girls'=>'Crafty Girls','Creepster Caps'=>'Creepster Caps','Crimson Text'=>'Crimson Text','Crushed'=>'Crushed','Cuprum'=>'Cuprum','Damion'=>'Damion','Dancing Script'=>'Dancing Script','Dawning of a New Day'=>'Dawning of a New Day','Days One'=>'Days One','Delius'=>'Delius','Delius Swash Caps'=>'Delius Swash Caps','Delius Unicase'=>'Delius Unicase','Didact Gothic'=>'Didact Gothic','Dorsa'=>'Dorsa','Droid Sans'=>'Droid Sans','Droid Sans Mono'=>'Droid Sans Mono','Droid Serif'=>'Droid Serif','EB Garamond'=>'EB Garamond','Eater Caps'=>'Eater Caps','Expletus Sans'=>'Expletus Sans','Fanwood Text'=>'Fanwood Text','Federant'=>'Federant','Federo'=>'Federo','Roboto' => 'Roboto','Fontdiner Swanky'=>'Fontdiner Swanky','Forum'=>'Forum','Francois One'=>'Francois One','Gentium Book Basic'=>'Gentium Book Basic','Geo'=>'Geo','Geostar'=>'Geostar','Geostar Fill'=>'Geostar Fill','Give You Glory'=>'Give You Glory','Gloria Hallelujah'=>'Gloria Hallelujah','Goblin One'=>'Goblin One','Gochi Hand'=>'Gochi Hand','Goudy Bookletter 1911'=>'Goudy Bookletter 1911','Gravitas One'=>'Gravitas One','Gruppo'=>'Gruppo','Hammersmith One'=>'Hammersmith One','Holtwood One SC'=>'Holtwood One SC','Homemade Apple'=>'Homemade Apple','IM Fell DW Pica'=>'IM Fell DW Pica','IM Fell English'=>'IM Fell English','IM Fell English SC'=>'IM Fell English SC','Inconsolata'=>'Inconsolata','Indie Flower'=>'Indie Flower','Irish Grover'=>'Irish Grover','Irish Growler'=>'Irish Growler','Istok Web'=>'Istok Web','Jockey One'=>'Jockey One','Josefin Sans'=>'Josefin Sans','Josefin Slab'=>'Josefin Slab','Judson'=>'Judson','Julee'=>'Julee','Jura'=>'Jura','Just Another Hand'=>'Just Another Hand','Just Me Again Down Here'=>'Just Me Again Down Here','Kameron'=>'Kameron','Kelly Slab'=>'Kelly Slab','Kenia'=>'Kenia','Kranky'=>'Kranky','Kreon'=>'Kreon','Kristi'=>'Kristi','La Belle Aurore'=>'La Belle Aurore','Lato'=>'Lato','League Script'=>'League Script','Leckerli One'=>'Leckerli One','Lekton'=>'Lekton','Limelight'=>'Limelight','Linden Hill'=>'Linden Hill','Lobster'=>'Lobster','Lobster Two'=>'Lobster Two','Lora'=>'Lora','Love Ya Like A Sister'=>'Love Ya Like A Sister','Loved by the King'=>'Loved by the King','Luckiest Guy'=>'LuckiestGuy','Maiden Orange'=>'Maiden Orange');


$font_transform = array('inherit'=>'Inherit','lowercase'=>'Lowercase','uppercase'=>'Uppercase','capitalize'=>'capitalize');
$font_weight = array('normal'=>'normal', 'italic'=>'Italic','oblique'=>'oblique');	
// General typography section
$wp_customize->add_section(
	'Body_typography' ,
		array(
		'title'      => __('Body','hantus-pro'),
		'panel' => 'hantus_typography_setting',
		'priority'       => 2,
   	) );


		//base font family
		
		$wp_customize->add_setting( 
			'body_font_family' , 
				array(
				'default' => __( 'sans-serif', 'hantus-pro' ),
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

	$wp_customize->add_control(
	'body_font_family' , 
		array(
			'label'          => __( 'Font Family', 'hantus-pro' ),
			'section'        => 'Body_typography',
			'settings'   	 => 'body_font_family',
			'type'			=> 'select',
			'choices'        => $font_family,
		) 
	);
		//Secondary font weight
		
		$wp_customize->add_setting(
			'body_typography_font_weight',
			array(
				'default'           =>  __('normal','hantus-pro'),
				'capability'        =>  'edit_theme_options',
				'sanitize_callback' =>  'sanitize_text_field',
			)	
		);
		$wp_customize->add_control(
		'body_typography_font_weight', array(
				'label' => __('Font Style','hantus-pro'),
				'section' => 'Body_typography',
				'setting' => 'body_typography_font_weight',
				'choices'=>$font_weight,
				'type'			=> 'select',
			)
		);
		// Body font size// 
		$wp_customize->add_setting( 
			'body_font_size' , 
				array(
				'default' => __( '14','hantus-pro' ),
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
				
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'body_font_size', 
			array(
				'section'  => 'Body_typography',
				'settings' => 'body_font_size',
				'label'    => __( 'Font Size','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 10,
					'max'    => 40,
					'step'   => 1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		// Body line height// 
		$wp_customize->add_setting( 
			'body_line_height' , 
				array(
				'default' => 1.6,
				'capability'     => 'edit_theme_options',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'body_line_height', 
			array(
				'section'  => 'Body_typography',
				'settings' => 'body_line_height',
				'label'    => __( 'Line Height','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 0,
					'max'    => 3,
					'step'   => 0.1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);

		
		//H1 typography
		for ( $i = 1; $i <= 6; $i++ ) {
		if($i  == '1'){$j=36;}elseif($i  == '2'){$j=32;}elseif($i  == '3'){$j=28;}elseif($i  == '4'){$j=24;}elseif($i  == '5'){$j=20;}else{$j=16;}
		$wp_customize->add_section(
			'H' . $i . '_typography' ,
				array(
				'title'      => __('H' . $i .'','hantus-pro'),
				'panel' => 'hantus_typography_setting',
				'priority'       => 3,
			) 
		);
		
		//H1 font weight
		
		$wp_customize->add_setting(
			'h' . $i . '_font_weight',
			array(
				'default'           =>  'normal',
				'capability'        =>  'edit_theme_options',
				'sanitize_callback' =>  'sanitize_text_field',
			)	
		);
		$wp_customize->add_control(
		'h' . $i . '_font_weight', array(
				'label' => __('Font Style','hantus-pro'),
				'section' => 'H' . $i . '_typography',
				'setting' => 'h' . $i . '_font_weight',
				'choices'=>$font_weight,
				'type'			=> 'select',
			)
		);
		
		// H1 font size// 
		$wp_customize->add_setting( 
			'h' . $i . '_font_size' , 
				array(
				'default' => $j,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'h' . $i . '_font_size', 
			array(
				'section'  => 'H' . $i . '_typography',
				'settings' => 'h' . $i . '_font_size',
				'label'    => __( 'Font Size(px)','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 1,
					'max'    => 50,
					'step'   => 1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		// paragraph line height// 
		$wp_customize->add_setting( 
			'h' . $i . '_line_height' , 
				array(
				'default' => 1.2,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'h' . $i . '_line_height', 
			array(
				'section'  => 'H' . $i . '_typography',
				'settings' => 'h' . $i . '_line_height',
				'label'    => __( 'Line Height','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 0,
					'max'    => 3,
					'step'   => 0.1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		//H1 text transform
		
		$wp_customize->add_setting( 
			'h' . $i . '_text_transform' , 
				array(
				'default' => 'inherit',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

	$wp_customize->add_control(
	'h' . $i . '_text_transform' , 
		array(
			'label'          => __( 'Text Transform', 'hantus-pro' ),
			'section'        => 'H' . $i . '_typography',
			'settings'   	 => 'h' . $i . '_text_transform',
			'choices'        => $font_transform,
			'type'			=> 'select',
		) 
	);
	}
	

	// menu typography section
	$wp_customize->add_section(
		'menu_typography' ,
			array(
			'title'      => __('Menus','hantus-pro'),
			'panel' => 'hantus_typography_setting',
			'priority'       => 2,
		) );

			
		//menu font weight
		
		$wp_customize->add_setting(
			'menu_font_weight',
			array(
				'default'           =>  'normal',
				'capability'        =>  'edit_theme_options',
				'sanitize_callback' =>  'sanitize_text_field',
			)	
		);
		$wp_customize->add_control(
				'menu_font_weight',
				array(
					'label' => __('Font Style','hantus-pro'),
					'section' => 'menu_typography',
					'setting' => 'menu_font_weight',
					'choices'=>$font_weight,
					'type'			=> 'select',
				)
			);
		
		// menu font size// 
		$wp_customize->add_setting( 
			'menu_font_size' , 
				array(
				'default' => '15',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'menu_font_size', 
			array(
				'section'  => 'menu_typography',
				'settings' => 'menu_font_size',
				'label'    => __( 'Font Size(px)','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 1,
					'max'    => 20,
					'step'   => 1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		// Menu line height// 
		$wp_customize->add_setting( 
			'menu_line_height' , 
				array(
				'default' => 1.6,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'menu_line_height', 
			array(
				'section'  => 'menu_typography',
				'settings' => 'menu_line_height',
				'label'    => __( 'Line Height','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 0,
					'max'    => 3,
					'step'   => 0.1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		//menu text transform
		
		$wp_customize->add_setting( 
			'menu_text_transform' , 
				array(
				'default' => 'inherit',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

	$wp_customize->add_control(
	'menu_text_transform' , 
		array(
			'label'          => __( 'Text Transform', 'hantus-pro' ),
			'section'        => 'menu_typography',
			'settings'   	 => 'menu_text_transform',
			'choices'        => $font_transform,
			'type'			=> 'select',
		) 
	);
	
// Sections typography section
$wp_customize->add_section(
	'section_typography' ,
		array(
		'title'      => __('Sections','hantus-pro'),
		'panel' => 'hantus_typography_setting',
		'priority'       => 2,
   	) );
			
		//section font weight
		$wp_customize->add_setting(
			'section_tit_font_weight',
			array(
				'default'           =>  'normal',
				'capability'        =>  'edit_theme_options',
				'sanitize_callback' =>  'sanitize_text_field',
			)	
		);
		$wp_customize->add_control(
				'section_tit_font_weight',
				array(
					'label' => __(' Title Font Style','hantus-pro'),
					'section' => 'section_typography',
					'setting' => 'section_tit_font_weight',
					'choices'=>$font_weight,
					'type'			=> 'select',
				)
			);
		
		// section title font size// 
		$wp_customize->add_setting( 
			'section_tit_font_size' , 
				array(
				'default' => '36',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'section_tit_font_size', 
			array(
				'section'  => 'section_typography',
				'settings' => 'section_tit_font_size',
				'label'    => __( 'Title Font Size(px)','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 1,
					'max'    => 100,
					'step'   => 1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		// Section Title Height// 
		$wp_customize->add_setting( 
			'section_ttl_line_height' , 
				array(
				'default' => 1.6,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'section_ttl_line_height', 
			array(
				'section'  => 'section_typography',
				'settings' => 'section_ttl_line_height',
				'label'    => __( 'Title Line Height','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 0,
					'max'    => 3,
					'step'   => 0.1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		//Section Title transform
		$wp_customize->add_setting( 
			'sec_ttl_text_transform' , 
				array(
				'default' => 'inherit',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control(
		'sec_ttl_text_transform' , 
			array(
				'label'          => __( 'Text Transform', 'hantus-pro' ),
				'section'        => 'section_typography',
				'settings'   	 => 'sec_ttl_text_transform',
				'choices'        => $font_transform,
				'type'			=> 'select',
			) 
		);
		
		//section font weight
		$wp_customize->add_setting(
			'section_des_font_weight',
			array(
				'default'           =>  'normal',
				'capability'        =>  'edit_theme_options',
				'sanitize_callback' =>  'sanitize_text_field',
			)	
		);
		$wp_customize->add_control(
				'section_des_font_weight',
				array(
					'label' => __('Description Font Style','hantus-pro'),
					'section' => 'section_typography',
					'setting' => 'section_des_font_weight',
					'choices'=>$font_weight,
					'type'			=> 'select',
				)
			);
		
		// section title font size// 
		$wp_customize->add_setting( 
			'section_desc_font_size' , 
				array(
				'default' => '16',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'section_desc_font_size', 
			array(
				'section'  => 'section_typography',
				'settings' => 'section_desc_font_size',
				'label'    => __( 'Description Font Size(px)','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 1,
					'max'    => 50,
					'step'   => 1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		// Section Description Height// 
		$wp_customize->add_setting( 
			'section_desc_line_height' , 
				array(
				'default' => 1.6,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control( 
		new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'section_desc_line_height', 
			array(
				'section'  => 'section_typography',
				'settings' => 'section_desc_line_height',
				'label'    => __( 'Description Line Height','hantus-pro' ),
				'input_attrs' => array(
					'min'    => 0,
					'max'    => 3,
					'step'   => 0.1,
					//'suffix' => 'px', //optional suffix
				),
			) ) 
		);
		
		//Section Description transform
		$wp_customize->add_setting( 
			'sec_desc_text_transform' , 
				array(
				'default' => 'inherit',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			) 
		);

		$wp_customize->add_control(
		'sec_desc_text_transform' , 
			array(
				'label'          => __( 'Text Transform', 'hantus-pro' ),
				'section'        => 'section_typography',
				'settings'   	 => 'sec_desc_text_transform',
				'choices'        => $font_transform,
				'type'			=> 'select',
			) 
		);
}
add_action( 'customize_register', 'hantus_typography_customizer' );