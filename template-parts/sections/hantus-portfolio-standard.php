 <section id="portfolio" class="st-port">
        <div class="container">
		   <div class="row tab-content" id="grid">
				<?php 
						$portfolio_display_num = get_theme_mod('portfolio_display_num','3');
						$args = array( 'post_type' => 'hantus_menu_item','posts_per_page' =>$portfolio_display_num);  	
						$menu_item = new WP_Query( $args ); 
						if( $menu_item->have_posts() )
						{
							while ( $menu_item->have_posts() ) : $menu_item->the_post();
					?>
					<?php
						
						$menu_item_description 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_description', true ));
						$menu_item_currency 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_currency', true ));
						$menu_item_price 			= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_price', true ));
						$menu_item_link 			= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_link', true ));
						$menu_item_link_target 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_link_target', true ));
					
					?>
					<?php 	
						if($menu_item_link) { 
							$menu_item_link; 
						}	
						else { 
							$menu_item_link = get_post_permalink(); 
						} 
					?>
					 <?php
						$terms = get_the_terms( $post->ID, 'menu_item_categories' );
											
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$links = array();

							foreach ( $terms as $term ) 
							{
								$links[] = $term->slug;
							}
							
							$tax = join( '","', $links );		
						else :	
							$tax = '';	
						endif;
					?>
                        <div data-groups='["<?php echo strtolower($tax); ?>"]' class="col-md-6 tab-panel">
							<div class="tab-list">
								<?php 
									if ( has_post_thumbnail() ) {
										the_post_thumbnail();
									}
								?>
								<a href="<?php echo esc_url($menu_item_link); ?>" <?php  if($menu_item_link_target) { echo "target='_blank'"; } ?>><h4><?php echo the_title(); ?> <span class="price"><?php echo esc_attr($menu_item_currency); ?><?php echo esc_attr($menu_item_price); ?></span></h4></a>
								<p><?php echo esc_attr($menu_item_description); ?></p>
							</div>
                        </div>
						<?php 	
							endwhile; 
							}
						?>
			</div>
        </div>
    </section>