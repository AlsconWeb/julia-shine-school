<?php
function hantus_slider_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Slider Section Panel
	=========================================*/
		$wp_customize->add_section(
			'slider_setting', array(
				'title' => esc_html__( 'Slider Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 1, 'slider_setting' ),
			)
		);
	
	
	// Setting  Head 
	$wp_customize->add_setting(
		'slider_setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'slider_setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'slider_setting',
		)
	);
	
	
	// Slider Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_slider' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_slider', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'slider_setting',
			'settings'    => 'hide_show_slider',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	
	// Content  Head 
	$wp_customize->add_setting(
		'slider_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 3,
		)
	);

	$wp_customize->add_control(
	'slider_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'slider_setting',
		)
	);
	
	/**
	 * Customizer Repeater for add slides
	 */
	
		$wp_customize->add_setting( 'slider', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			  'default' => hantus_get_slides_default(),
			  'priority' => 4,
			)
		);
		
		$wp_customize->add_control( 
			new Hantus_Repeater( $wp_customize, 
				'slider', 
					array(
						'label'   => esc_html__('Slide','hantus-pro'),
						'section' => 'slider_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Slide', 'hantus-pro' ),
						
						'customizer_repeater_icon_control' => false,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_subtitle_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_text2_control'=> true,
						'customizer_repeater_link_control' => true,
						'customizer_repeater_slide_align' => true,
						'customizer_repeater_checkbox_control' => true,
						'customizer_repeater_image_control' => true,	
					) 
				) 
			);

	

	//Overlay Enable //
	if ( class_exists( 'Customizer_Toggle_Control' ) ) {	
	$wp_customize->add_setting( 
		'slider_overlay_enable' , 
			array(
			'default' => 0,
			'capability' => 'edit_theme_options',
			'priority' => 5,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'slider_overlay_enable', 
		array(
			'label'	      => esc_html__( 'Overlay Enable', 'hantus' ),
			'section'     => 'slider_setting',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	}
	
	//slider opacity
	$wp_customize->add_setting( 
		'slider_opacity' , 
			array(
			'default' => '0.2',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 6,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'slider_opacity', 
		array(
			'section'  => 'slider_setting',
			'settings' => 'slider_opacity',
			'label'    => __( 'Background Opacity','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 0,
				'max'    => 0.9,
				'step'   => 0.1,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
	
	// Overlay Color
	$wp_customize->add_setting(
	'slide_overlay_color', 
	array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'priority' => 7,
		'default' => '#fff'
    ));
	
	$wp_customize->add_control( 
		new WP_Customize_Color_Control
		($wp_customize, 
			'slide_overlay_color', 
			array(
				'label'      => __( 'Overlay Color', 'hantus' ),
				'section'    => 'slider_setting'
			) 
		) 
	);
	
	// Title Color
	$wp_customize->add_setting(
	'slide_title_color', 
	array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '#fff',
		'priority' => 8,
    ));
	
	$wp_customize->add_control( 
		new WP_Customize_Color_Control
		($wp_customize, 
			'slide_title_color', 
			array(
				'label'      => __( 'Title Color', 'hantus' ),
				'section'    => 'slider_setting'
			) 
		) 
	);
	
	// Subtitle Color
	$wp_customize->add_setting(
	'slide_sbtitle_color', 
	array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '#f22853',
		'priority' => 9,
    ));
	
	$wp_customize->add_control( 
		new WP_Customize_Color_Control
		($wp_customize, 
			'slide_sbtitle_color', 
			array(
				'label'      => __( 'Subtitle Color', 'hantus' ),
				'section'    => 'slider_setting'
			) 
		) 
	);
	
	// Description Color
	$wp_customize->add_setting(
	'slide_desc_color', 
	array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '#fff',
		'priority' => 10,
    ));
	
	$wp_customize->add_control( 
		new WP_Customize_Color_Control
		($wp_customize, 
			'slide_desc_color', 
			array(
				'label'      => __( 'Description Color', 'hantus' ),
				'section'    => 'slider_setting'
			) 
		) 
	);
	
	$wp_customize->add_setting( 
		'slider_animation_in' , 
			array(
			'default' => __('pulse', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 11,
		) 
	);

	$wp_customize->add_control('slider_animation_in', array(
    'label' => __('Animation In', 'hantus-pro'),
    'section' => 'slider_setting',
	'settings' => 'slider_animation_in',
	'type'			=> 'select',
	'choices'        => 
			array(
				''		=>__('Amimation 1', 'hantus-pro'),
				'pulse'		=>__('Amimation 2', 'hantus-pro'),
				'fadeIn'=>__('Amimation 3', 'hantus-pro'),
				'lightSpeedIn'=>__('Amimation 4', 'hantus-pro'),
				'rollIn'=>__('Amimation 5', 'hantus-pro'),
				'flipInX'=>__('Amimation 6', 'hantus-pro'), 	
				'bounceIn'=>__('Amimation 7', 'hantus-pro'),
			) 
));
	$wp_customize->add_setting( 
		'slider_animation_out' , 
			array(
			'default' => __('fadeOut', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 12,
		) 
	);
	$wp_customize->add_control('slider_animation_out', array(
    'label' => __('Animation Out', 'hantus-pro'),
    'section' => 'slider_setting',
	'type'			=> 'select',
	'settings' => 'slider_animation_out',
	'choices'        => 
			array(
				''		=>__('Animation 1', 'hantus-pro'),
				'fadeOut'=>__('Animation 2', 'hantus-pro'),
				'fadeOut'=>__('Animation 3', 'hantus-pro'),
				'lightSpeedOut'=>__('Animation 4', 'hantus-pro'),
				'rollOut'=>__('Animation 5', 'hantus-pro'),
				'flipInY'=>__('Animation 6', 'hantus-pro'),
				'bounceOut'=>__('Animation 7', 'hantus-pro'),
			) 
	));
	// Slider speed// 
	$wp_customize->add_setting( 
		'slider_animation_speed' , 
			array(
			'default' => '3000',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 13,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'slider_animation_speed', 
		array(
			'section'  => 'slider_setting',
			'settings' => 'slider_animation_speed',
			'label'    => __( 'Slider Speed','hantus-pro' ),
			'description' => __('range valid', 'hantus-pro'),
			'input_attrs' => array(
				'min'    => 500,
				'max'    => 10000,
				'step'   => 500,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
}
add_action( 'customize_register', 'hantus_slider_setting' );

// slider selective refresh
function hantus_home_slider_section_partials( $wp_customize ){

	// hide_show_slider
	$wp_customize->selective_refresh->add_partial(
		'hide_show_slider', array(
			'selector' => '.header-slider',
			'container_inclusive' => true,
			'render_callback' => 'slider_setting',
			'fallback_refresh' => true,
		)
	);
	// slider
	$wp_customize->selective_refresh->add_partial( 'slider', array(
		'selector'            => '#slider .header-slider figure',
		'settings'            => 'slider',
		'render_callback'  => 'home_section_slider_render_callback',
	
	) );
	
	}

add_action( 'customize_register', 'hantus_home_slider_section_partials' );

// social icons
function home_section_slider_render_callback() {
	return get_theme_mod( 'slider' );
}
