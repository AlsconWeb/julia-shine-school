<!-- Start: Portfolio
    ============================= -->
<?php
	$portfolio_display_num = get_theme_mod('portfolio_display_num','3');

?>	
 <section id="portfolio-page" class="portfolio-page">
     <div class="container">
		<div class="row portfolio" id="grid">
				<?php 
					$args = array( 'post_type' => 'hantus_portfolio');  
					$portfolio = new WP_Query( $args ); 
					if( $portfolio->have_posts() )
					{
						while ( $portfolio->have_posts() ) : $portfolio->the_post();
				?>
				<?php
					
					$portfolio_descriptions 		= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_description', true ));
					$portfolio_price 					= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_price', true ));
					$portfolio_link 			= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link', true ));
					$portfolio_button_link_target 	= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link_target', true ));
					$portfolio_date 				= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_date', true ));
				?>
				<?php 	
					if($portfolio_link) { 
						$portfolio_link; 
					}	
					else { 
						$portfolio_link = get_post_permalink(); 
					} 
				?>
				 <?php
					$terms = get_the_terms( $post->ID, 'portfolio_categories' );
										
					if ( $terms && ! is_wp_error( $terms ) ) : 
						$links = array();

						foreach ( $terms as $term ) 
						{
							$links[] = $term->slug;
						}
						
						$tax = join( '","', $links );		
					else :	
						$tax = '';	
					endif;
				?>
					<div class="col-lg-<?php echo esc_attr( $portfolio_display_num ); ?> col-md-4 col-sm-6 portfolio-item" data-groups='["<?php echo strtolower($tax); ?>"]'>
						<figure>
							<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail();
								}
							?>	
							<figcaption>
								<div class="inner-text">
									<h4><?php echo the_title(); ?></h4>
									<h6><?php echo esc_attr($portfolio_date); ?></h6>
									<p><?php echo esc_attr($portfolio_descriptions); ?></p>
									<a href="<?php echo esc_url($portfolio_link); ?>" <?php  if($portfolio_button_link_target) { echo "target='_blank'"; }  ?>><?php echo esc_attr($portfolio_price); ?></a>
								</div>
							</figcaption>
						</figure>
					</div>	
	
				<?php 	
					endwhile; 
					}
				?>
			</div>
		</div>	
	</section>	
<!-- End: Portfolio
============================= -->