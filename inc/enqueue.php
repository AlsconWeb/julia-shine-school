<?php
 /**
 * Enqueue scripts and styles.
 */
function hantus_scripts() {
	
	// Styles
	wp_enqueue_style('bootstrap-min',get_template_directory_uri().'/assets/css/bootstrap.min.css');
	wp_enqueue_style('hantus-pro-layout',get_template_directory_uri().'/assets/css/layout.css');
	wp_enqueue_style('meanmenu-min',get_template_directory_uri().'/assets/css/meanmenu.min.css');
	
	wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/fonts/font-awesome/css/font-awesome.min.css');
	
	wp_enqueue_style('owl-carousel-min',get_template_directory_uri().'/assets/css/owl.carousel.min.css');
	
	wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.css');
	  
	wp_enqueue_style('magnific-popup',get_template_directory_uri().'/assets/css/magnific-popup.css');
	wp_enqueue_style('hantus-wp-test',get_template_directory_uri().'/assets/css/wp-test.css');
	wp_enqueue_style('hantus-widget',get_template_directory_uri().'/assets/css/widget.css');
	wp_enqueue_style('hantus-woocommerce',get_template_directory_uri().'/assets/css/woo.css');
	wp_enqueue_style('typography', get_template_directory_uri() .'/assets/css/typography/typograhpy.css');
	
	wp_enqueue_style('default', get_template_directory_uri() . '/assets/css/colors/default.css');
	wp_enqueue_style( 'hantus-style', get_stylesheet_uri() );
	wp_enqueue_style('hantus-responsive',get_template_directory_uri().'/assets/css/responsive.css');
	wp_enqueue_script( 'jquery' );
	
	//wp_register_script( 'jquery3.2.1',get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js' );	
	wp_enqueue_script('popper', get_template_directory_uri() . '/assets/js/popper.min.js', array('jquery3.2.1'), '1.0', true);
	
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery3.2.1'), '1.0', true);
	
	wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), true);
	
	wp_enqueue_script('counterup', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array('jquery'), false, true);
	
	wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), false, true);
	
	wp_enqueue_script('hantus-shuffle', get_template_directory_uri() . '/assets/js/jquery.shuffle.min.js', array('jquery'), false, true);
	
	wp_enqueue_script('hantus-meanmenu', get_template_directory_uri() . '/assets/js/jquery.meanmenu.min.js', array('jquery'), false, true);
	wp_enqueue_script('wow-min', get_template_directory_uri() . '/assets/js/wow.min.js');
	wp_enqueue_script('hantus-custom-js', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'),true);
	wp_enqueue_script('hantus-map-link', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAqoWGSQYygV-G1P5tVrj-dM2rVHR5wOGY');


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hantus_scripts' );

//Admin Enqueue for Style Configurator
function hantus_admin_enqueue_scripts(){
	wp_enqueue_style('style-configurator', get_template_directory_uri() . '/assets/css/style-configurator.css');
}
add_action( 'admin_enqueue_scripts', 'hantus_admin_enqueue_scripts' );

?>