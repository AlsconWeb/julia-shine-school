<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Hantus
 */

get_header();
?>
<?php	
	$error_img_hide_show			= get_theme_mod('error_img_hide_show','1');
	$error_page_title				= get_theme_mod('error_page_title','Oops..');
	$error_page_description			= get_theme_mod('error_page_description','Someething Went Wrong Here');
	$error_page_btn_lbl				= get_theme_mod('error_page_btn_lbl','Back To Home');
	$error_page_btn_link			= get_theme_mod('error_page_btn_link','#');
	$error_page_image				= get_theme_mod('error_page_image',get_template_directory_uri() . '/assets/images/404-image.png');
?>
<!-- Start: 404 page
    ============================= -->
   <section id="page-404" class="section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
					 <?php  if($error_img_hide_show == '1') {  ?>
					<?php if ( ! empty( $error_page_image ) ) : ?>
						<img src="<?php echo esc_url( $error_page_image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
					<?php endif; ?>
					 <?php } ?>
                   <?php if($error_page_title) {?>
						<h2><?php echo esc_attr($error_page_title); ?></h2>
					<?php } ?>	
                    <?php if($error_page_description) {?>
						<h3><?php echo esc_attr($error_page_description); ?></h3>
					<?php } ?>	
					<?php if($error_page_btn_lbl) {?>
						<a href="<?php echo esc_url($error_page_btn_link); ?>" class="boxed-btn"><?php echo esc_attr($error_page_btn_lbl); ?></a>
					<?php } ?>	
                </div>
            </div>
        </div>
    </section>
    <!-- End: 404 page
    ============================= -->

<?php
get_footer();
