/**
 * Customizer controls toggles
 *
 * @package Hantus
 */

( function( $ ) {

	/* Internal shorthand */
	var api = wp.customize;

	/**
	 * Trigger hooks
	 */
	HANTUSControlTrigger = {

	    /**
	     * Trigger a hook.
	     *
	     * @since 1.0.0
	     * @method triggerHook
	     * @param {String} hook The hook to trigger.
	     * @param {Array} args An array of args to pass to the hook.
		 */
	    triggerHook: function( hook, args )
	    {
	    	$( 'body' ).trigger( 'hantus-control-trigger.' + hook, args );
	    },

	    /**
	     * Add a hook.
	     *
	     * @since 1.0.0
	     * @method addHook
	     * @param {String} hook The hook to add.
	     * @param {Function} callback A function to call when the hook is triggered.
	     */
	    addHook: function( hook, callback )
	    {
	    	$( 'body' ).on( 'hantus-control-trigger.' + hook, callback );
	    },

	    /**
	     * Remove a hook.
	     *
	     * @since 1.0.0
	     * @method removeHook
	     * @param {String} hook The hook to remove.
	     * @param {Function} callback The callback function to remove.
	     */
	    removeHook: function( hook, callback )
	    {
		    $( 'body' ).off( 'hantus-control-trigger.' + hook, callback );
	    },
	};

	/**
	 * Helper class that contains data for showing and hiding controls.
	 *
	 * @since 1.0.0
	 * @class HANTUSCustomizerToggles
	 */
	HANTUSCustomizerToggles = {
		
		/**
		 *  Info
		 */
		'select_theme' :
		[
			{
				controls: [
					'hnts_info_four_head',
					'info_four_img_setting',
					'info_title4',
					'info_description4',
					'info_btn4',
					'info_link4',
					'hnts_info_five_head',
					'info_five_img_setting',
					'info_title5',
					'info_description5',
					'info_btn5',
					'info_link5',
				],
				callback: function( select_theme ) {

					var select_theme = api( 'select_theme' ).get();

					if ( 'cosmics-theme' == select_theme ) {
						return true;
					}
					return false;
				}
			}
		],
	};

} )( jQuery );
