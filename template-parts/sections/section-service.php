<?php
/**
 * Get default values for service section.
 *
 * @since 1.0
 * @access public
 */
?>
<?php
		$default_content 			= hantus_get_service_default();
		$hide_show_service			= get_theme_mod('hide_show_service','1');
		$service_title				= get_theme_mod('service_title','Our Services');
		$service_description		= get_theme_mod('service_description','These are the services we provide, these makes us stand apart.');
		$service_contents			= get_theme_mod('service_contents',$default_content);
		$service_display_num		= get_theme_mod('service_display_num');
		$hide_show_service_excerpt	= get_theme_mod('hide_show_service_excerpt');
		$service_sec_column			= get_theme_mod('service_sec_column','3');
		$service_bg_setting			= get_theme_mod('service_bg_setting',get_template_directory_uri() . '/assets/images/service/servicebg.jpg');
		$service_bg_position		= get_theme_mod('service_bg_position','fixed');
		$select_theme				= get_theme_mod('select_theme','hantus-pro');
?>
<?php if($hide_show_service == '1') {?>
<?php hantus_before_service_section_trigger(); ?>
<?php if($select_theme !== 'thaispa-theme') { ?>
   	<section id="services" class="custom-padding">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title service-section">
                        <h2><?php echo $service_title; ?></h2>
                        <p><?php echo $service_description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row servicesss">
				<?php
					if ( ! empty( $service_contents ) ) {
					$allowed_html = array(
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array(),
					);
					$service_contents = json_decode( $service_contents );
					foreach ( $service_contents as $service_item ) {
						$title = ! empty( $service_item->title ) ? apply_filters( 'hantus_translate_single_string', $service_item->title, 'service section' ) : '';
						$text = ! empty( $service_item->text ) ? apply_filters( 'hantus_translate_single_string', $service_item->text, 'service section' ) : '';
						$pricing = ! empty( $service_item->pricing ) ? apply_filters( 'hantus_translate_single_string', $service_item->pricing, 'service section' ) : '';
						$old_pricing = ! empty( $service_item->old_pricing ) ? apply_filters( 'hantus_translate_single_string', $service_item->old_pricing, 'service section' ) : '';
						$text2 = ! empty( $service_item->text2) ? apply_filters( 'hantus_translate_single_string', $service_item->text2,'service section' ) : '';
						$link = ! empty( $service_item->link ) ? apply_filters( 'hantus_translate_single_string', $service_item->link, 'service section' ) : '';
						$image = ! empty( $service_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $service_item->image_url, 'service section' ) : '';
				?>
                <div class="col-lg-<?php echo $service_sec_column; ?> col-md-6 col-sm-6 serv-cont">
                    <div class="service-box text-center">
                        <figure>
                            <?php if ( ! empty( $image ) ) : ?>
								<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
							<?php endif; ?>
                            <figcaption>
                                <div class="inner-text">
									<?php if ( ! empty( $text2 ) ) : ?>
										<a href="<?php echo esc_html( $link ); ?>" class="boxed-btn"><?php echo esc_html( $text2 ); ?></a>
									<?php endif; ?>
                                </div>
                            </figcaption>
                        </figure>
							<?php if ( ! empty( $title ) ) : ?>
								<h4><?php echo esc_html( $title ); ?></h4>
							<?php endif; ?>
							<?php if ( ! empty( $text ) ) : ?>
								<p><?php echo esc_html( $text ); ?></p>
							<?php endif; ?>
	                    <?php if ( ! empty( $pricing ) ) : ?>
							<p class="price">
			                    <?php if ( $old_pricing ) { ?>
									<span class="old-price">
										<?php echo esc_html( $old_pricing ); ?>
									</span>
			                    <?php } ?>
			                    <?php echo esc_html( $pricing ); ?>
							</p>
	                    <?php endif; ?>
							<?php if ( ! empty( $text2 ) ) : ?>
								<a href="<?php echo esc_html( $link ); ?>" class="boxed-btn"><?php echo esc_html( $text2 ); ?></a>
							<?php endif; ?>
                    </div>
                </div>
					<?php }}?>
            </div>
        </div>
    </section>
<?php } else {  ?>
	<section id="services" class="custom-padding" style="background: url(<?php echo esc_url($service_bg_setting); ?>) center center / cover <?php echo esc_attr($service_bg_position); ?>;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title service-section">
                        <h2><?php echo $service_title; ?></h2>
                        <p><?php echo $service_description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row servicesss">
				<?php
					if ( ! empty( $service_contents ) ) {
					$allowed_html = array(
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array(),
					);
					$service_contents = json_decode( $service_contents );
					foreach ( $service_contents as $service_item ) {
						$title = ! empty( $service_item->title ) ? apply_filters( 'hantus_translate_single_string', $service_item->title, 'service section' ) : '';
						$text = ! empty( $service_item->text ) ? apply_filters( 'hantus_translate_single_string', $service_item->text, 'service section' ) : '';
						$pricing = ! empty( $service_item->pricing ) ? apply_filters( 'hantus_translate_single_string', $service_item->pricing, 'service section' ) : '';
						$text2 = ! empty( $service_item->text2) ? apply_filters( 'hantus_translate_single_string', $service_item->text2,'service section' ) : '';
						$link = ! empty( $service_item->link ) ? apply_filters( 'hantus_translate_single_string', $service_item->link, 'service section' ) : '';
						$image = ! empty( $service_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $service_item->image_url, 'service section' ) : '';
				?>
                <div class="col-lg-<?php echo $service_sec_column; ?> col-md-6 col-sm-6 serv-cont">
					<div class="service-thaispa text-center strip-hover">
						<div class="strip-hover-wrap">
							<div class="strip-overlay">
								<?php if ( ! empty( $image ) ) : ?>
								<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php endif; ?>
								<div class="inner-thaispa">
									<div class="inner-text-thaispa">
										<div class="inner-overlay">
											<?php if ( ! empty( $title ) ) : ?>
											<h4><?php echo esc_html( $title ); ?></h4>
											<?php endif; ?>
											<?php if ( ! empty( $text ) ) : ?>
											<p><?php echo esc_html( $text ); ?></p>
											<?php endif; ?>
											<?php if ( ! empty( $text2 ) ) : ?>
											<a href="<?php echo esc_html( $link ); ?>" class="boxed-btn"><?php echo esc_html( $text2 ); ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php if ( ! empty( $subtitle ) ) : ?>
						<div class="price-thaispa"><h5><?php echo esc_html( $subtitle ); ?></h5></div>
						<?php endif; ?>
                    </div>
                </div>
					<?php }}?>
            </div>
        </div>
    </section>
<?php } ?>
<?php
	hantus_after_service_section_trigger();
	}
?>
