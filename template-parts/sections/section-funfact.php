<!-- Start: Fun Fact
    ============================= -->
<?php
/**
 * Get default values for funfact section.
 *
 * @since 1.0
 * @access public
 */
$default_content 			= hantus_get_funfact_default();
$hide_show_funfact			= get_theme_mod('hide_show_funfact','1');
$funfact_contents			= get_theme_mod('funfact_contents',$default_content);
$funfact_background_setting	= get_theme_mod('funfact_background_setting',get_template_directory_uri() . '/assets/images/bg/counter-bg.jpg');
$funfact_background_position= get_theme_mod('funfact_background_position','fixed');
			
 if($hide_show_funfact == '1') { 
 hantus_before_funfact_section_trigger();
	?>
		<?php if ( ! empty( $funfact_background_setting ) ) { ?>
		<section id="counter" class="text-center" style="background:url('<?php echo esc_url($funfact_background_setting); ?>') no-repeat  center / 100% 100%;background-attachment:<?php echo esc_attr($funfact_background_position); ?>">
			<?php } else { ?>
				<section id="counter" class="text-center" style="background:#f7f7f7;">
			<?php } ?>
					<div class="container">
						<div class="row">
				<?php
					if ( ! empty( $funfact_contents ) ) {
					$allowed_html = array(
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array(),
					);
					$funfact_contents = json_decode( $funfact_contents );
					foreach ( $funfact_contents as $funfact_item ) {
						$icon = ! empty( $funfact_item->icon_value ) ? apply_filters( 'hantus_translate_single_string', $funfact_item->icon_value, 'funfact section' ) : '';
						$title = ! empty( $funfact_item->title ) ? apply_filters( 'hantus_translate_single_string', $funfact_item->title, 'funfact section' ) : '';
						$subtitle = ! empty( $funfact_item->subtitle ) ? apply_filters( 'hantus_translate_single_string', $funfact_item->subtitle, 'funfact section' ) : '';
						$image = ! empty( $funfact_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $funfact_item->image_url, 'funfact section' ) : '';
						$text = ! empty( $funfact_item->text ) ? apply_filters( 'hantus_translate_single_string', $funfact_item->text, 'funfact section' ) : '';
				?>
							<div class="col-lg-3 col-md-6 col-sm-6 col-12 single-box mb-5 mb-lg-0">
								<?php if ( ! empty( $image )  &&  ! empty( $icon )){ ?>
									<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php }elseif ( ! empty( $image ) ) { ?>
									<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
								<?php }elseif ( ! empty( $icon ) ) {?>
									<i class="fa <?php echo esc_html( $icon ); ?> txt-pink"></i>
								<?php } ?>
								<?php if ( ! empty( $title ) ) :?>
									<h3><span class="counter"><?php echo esc_html( $title ); ?></span>  <?php echo esc_html($subtitle);?></h3>
								<?php endif; ?>
								<?php if ( ! empty( $text ) ) :?>
									<p><?php echo esc_html( $text ); ?></p>
								<?php endif; ?>	
							</div>
					<?php } }?>	
						</div>
					</div>
				</section>
		</section>		
 <?php hantus_after_funfact_section_trigger(); } ?>
    <!-- End: Fun Fact
    ============================= -->