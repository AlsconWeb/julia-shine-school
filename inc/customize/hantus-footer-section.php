<?php
function hantus_footer( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	// Footer Panel // 
	$wp_customize->add_panel( 
		'footer_section', 
		array(
			'priority'      => 34,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Footer Section', 'hantus-pro'),
		) 
	);

	// Footer Setting Section // 
	$wp_customize->add_section(
        'footer_copyright',
        array(
            'title' 		=> __('Footer Bottom','hantus-pro'),
			'panel'  		=> 'footer_section',
		)
    );
	
	
	// Setting  Head 
	$wp_customize->add_setting(
		'footer_btm_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'footer_btm_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'footer_copyright',
		)
	);
	
	$wp_customize->add_setting( 
		'footer_btm_layout' , 
			array(
			'default' => 'style-1',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 1,
		) 
	);

	$wp_customize->add_control(
	'footer_btm_layout' , 
		array(
			'label'          => __( 'Style', 'hantus-pro' ),
			'section'        => 'footer_copyright',
			'type'			=> 'select',
			'choices'        => 
			array(
				'style-1'		=>__('Style 1', 'hantus-pro'),
				'style-2'=>__('Style 2', 'hantus-pro'),
				
			) 
		)
	);
	
	// Content  Head 
	$wp_customize->add_setting(
		'copyright_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'copyright_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Copyright','hantus-pro'),
			'section' => 'footer_copyright',
		)
	);
	
	// Copyright Content Hide/Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_copyright' , 
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 5,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_copyright', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'footer_copyright',
			'settings'    => 'hide_show_copyright',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Copyright Content Setting // 
	$hantus_copyright = esc_html__('Copyright &copy; [current_year] [site_title] | Powered by [theme_author]', 'hantus-pro' );
	$wp_customize->add_setting(
    	'copyright_content',
    	array(
	        'default'			=> $hantus_copyright,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	

	$wp_customize->add_control( 
		'copyright_content',
		array(
		    'label'   		=> __('Copyright Content','hantus-pro'),
		    'section'		=> 'footer_copyright',
			'settings'   	 => 'copyright_content',
			'type' 			=> 'textarea',
			'description'   => __( '', 'hantus-pro' ),
		)  
	);


	/*=========================================
	Footer Payment Icon Section
	=========================================*/

	// Content  Head 
	$wp_customize->add_setting(
		'payment_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 9,
		)
	);

	$wp_customize->add_control(
	'payment_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Payment Icon','hantus-pro'),
			'section' => 'footer_copyright',
		)
	);
	
		// Payment Icon Hide/Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_payment' , 
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 10,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_payment', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'footer_copyright',
			'settings'    => 'hide_show_payment',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	/**
	 * Customizer Repeater for add payment icon
	 */
		$wp_customize->add_setting( 'footer_Payment_icon', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'priority' => 11,
			  'default' => json_encode( 
			 array(
				array(
					'icon_value'	  =>  esc_html__( 'fa-google-plus', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_001',
					
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-visa', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_002',
				
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-mastercard', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_003',
			
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-amex', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_004',
					
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-user-md', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_005',
					
				),
			)
			 )
			)
		);
		
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'footer_Payment_icon', 
					array(
						'label'   => esc_html__('Payment Icon','hantus-pro'),
						'section' => 'footer_copyright',
						'add_field_label'                   => esc_html__( 'Add New Icon', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Icon', 'hantus-pro' ),
						
						'customizer_repeater_icon_control' => true,
						'customizer_repeater_image_control' => true,
						'customizer_repeater_link_control' => true,
						
				
					) 
				) 
			);

	// Footer Background Section // 
	$wp_customize->add_section(
        'footer_background',
        array(
            'title' 		=> __('Background','hantus-pro'),
			'panel'  		=> 'footer_section',
		)
    );
	// Background Image // 
    $wp_customize->add_setting( 
    	'footer_background_setting' , 
    	array(
			'default' 			=> '',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'footer_background_setting' ,
		array(
			'label'          => __( 'Set Background Image', 'hantus-pro' ),
			'section'        => 'footer_background',
			'description'    => __( 'Upload Image for Background', 'hantus-pro' ),
		) 
	));
}

add_action( 'customize_register', 'hantus_footer' );

// footer selective refresh
function hantus_home_footer_section_partials( $wp_customize ){
	
	// hide_show_copyright
	$wp_customize->selective_refresh->add_partial(
		'hide_show_copyright', array(
			'selector' => '#footer-copyright .copy-content',
			'container_inclusive' => true,
			'render_callback' => 'footer_copyright',
			'fallback_refresh' => true,
		)
	);
	// hide_show_payment
	$wp_customize->selective_refresh->add_partial(
		'hide_show_payment', array(
			'selector' => '#footer-copyright .payment-method',
			'container_inclusive' => true,
			'render_callback' => 'footer_icon',
			'fallback_refresh' => true,
		)
	);
	
	// copyright_content
	$wp_customize->selective_refresh->add_partial( 'copyright_content', array(
		'selector'            => '.copyright-text .copy-content a',
		'settings'            => 'copyright_content',
		'render_callback'  => 'home_section_copyright_render_callback',
	
	) );
	
	}

add_action( 'customize_register', 'hantus_home_footer_section_partials' );

// social icons
function home_section_copyright_render_callback() {
	return get_theme_mod( 'copyright_content' );
}
