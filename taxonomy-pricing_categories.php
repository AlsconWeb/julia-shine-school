<?php
/**
Taxonomy Pricing Category Archive
**/
get_header();
?>

<!--======================================
    Pricing Section
========================================-->
<?php 
	$pricing_taxo_title= get_theme_mod('pricing_taxo_title','Pricing'); 
	$pricing_taxo_description= get_theme_mod('pricing_taxo_description','You can select a package from the list below to save more');
?>
<section id="pricing" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <?php if ( ! empty( $pricing_taxo_title ) ) : ?>
							<h2><?php echo esc_attr( $pricing_taxo_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $pricing_taxo_description ) ) : ?>
							<p><?php echo esc_attr( $pricing_taxo_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
				<?php
				$j=1;		
				if( have_posts() )
				{	while ( have_posts() ) : the_post();
				?>
				<?php
					$plans_currency 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_currency', true ));
					$plans_price 			= sanitize_text_field( get_post_meta( get_the_ID(),'plans_price', true ));
					$plans_duration 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_duration', true ));
					$plans_features_1 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_1', true ));
					
					$plans_features_2 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_2', true ));
					$plans_features_3 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_3', true ));
					$plans_features_4 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_4', true ));
					$plans_features_5 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_5', true ));
					
					$plans_features_6 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_6', true ));
					$plans_features_7 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_7', true ));
					$plans_features_8 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_8', true ));
					$plans_features_9 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_9', true ));
					
					$plans_features_10 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_features_10', true ));
					$plans_button_label 	= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_label', true ));
					$plans_button_link 		= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_link', true ));
					$plans_button_link_target 	= sanitize_text_field( get_post_meta( get_the_ID(),'plans_button_link_target', true ));
					
				?>
               	<div class="col-lg-4 col-md-6 mb-5 mb-lg-0 price-contents">
                    <div class="pricing-box text-center" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;">
                        <h3><?php echo the_title(); ?></h3>
                        <div class="price"><sup><?php echo esc_attr($plans_currency); ?></sup> <span><?php echo esc_attr($plans_price); ?></span>/<?php echo esc_attr($plans_duration); ?></div>
                        <ul class="pricing-content">
                            <li><?php echo esc_attr($plans_features_1); ?></li>
							<li><?php echo esc_attr($plans_features_2); ?></li>
							<li><?php echo esc_attr($plans_features_3); ?></li>
							<li><?php echo esc_attr($plans_features_4); ?></li>
							<li><?php echo esc_attr($plans_features_5); ?></li>
							<li><?php echo esc_attr($plans_features_6); ?></li>
							<li><?php echo esc_attr($plans_features_7); ?></li>
							<li><?php echo esc_attr($plans_features_8); ?></li>
							<li><?php echo esc_attr($plans_features_9); ?></li>
							<li><?php echo esc_attr($plans_features_10); ?></li>
                        </ul>
                        <a href="<?php echo esc_url($plans_button_link); ?>" <?php  if($plans_button_link_target) { echo "target='_blank'"; } ?> class="boxed-btn"><?php echo esc_attr($plans_button_label); ?></a>
                    </div>
                </div>
				<?php 	
					endwhile; 
					}	
				?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>