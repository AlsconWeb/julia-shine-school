<!-- Start: Testimonial
============================= -->
<?php
/**
 * Get default values for testimonial section.
 *
 * @since  1.0
 * @access public
 */

use Hantus\Theme\Helpers\FrontEndHelpers;

$default_content                 = hantus_get_testimonial_default();
$hide_show_testimonial           = get_theme_mod( 'hide_show_testimonial', '1' );
$testimonial_contents            = get_theme_mod( 'testimonial_contents', $default_content );
$testimonial_background_setting  = get_theme_mod( 'testimonial_background_setting', get_template_directory_uri() . '/assets/images/bg/testimonial-bg.jpg' );
$testimonial_background_position = get_theme_mod( 'testimonial_background_position', 'fixed' );
$demo                            = get_theme_mod( 'testimonial_video_deo', 'fixed' );

if ( $hide_show_testimonial == '1' ) {
hantus_before_testimonial_section_trigger();
?>
<?php if ( ! empty( $testimonial_background_setting ) ) { ?>
<section id="testimonial" class="section-padding"
		 style="background: url('<?php echo esc_url( $testimonial_background_setting ); ?>') no-repeat center / 100% 100%;background-attachment:<?php echo esc_attr( $testimonial_background_position ); ?>">
	<?php }else{ ?>
	<section id="testimonial" class="section-padding" style="background:#f7f7f7;">
		<?php } ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="testimonial-carousel text-center">
						<div class="testimonial-content owl-carousel">
							<?php if ( ! empty( $testimonial_contents ) ) {
								$allowed_html         = [
									'br'     => [],
									'em'     => [],
									'strong' => [],
									'b'      => [],
									'i'      => [],
								];
								$testimonial_contents = json_decode( $testimonial_contents );
								foreach ( $testimonial_contents as $testimonial_item ) {

									$title       = ! empty( $testimonial_item->title ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->title, 'testimonial section' ) : '';
									$designation = ! empty( $testimonial_item->designation ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->designation, 'testimonial section' ) : '';
									$text        = ! empty( $testimonial_item->text ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->text, 'testimonial section' ) : '';
									$image       = ! empty( $testimonial_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->image_url, 'testimonial section' ) : '';

									?>
									<div class="single-testimonial">
										<?php
										if ( ! empty( $testimonial_item->video_url ) ) {
											$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

											$is_browser = preg_match( '/((Win|Macintosh).*?(Chrome|Edg|Trident|Safari))(?!.*OPR)/i', $user_agent );

											if ( $is_browser || 1 === (int) $demo ) {
												$keys = [
													$testimonial_item->key_hd,
													$testimonial_item->key_sd,
													$testimonial_item->key_audio,
												];
												?>
												<div class="video-block">
													<video
															style="height: auto; width: 270px;"
															id="video-shaka-<?php echo FrontEndHelpers::generate_unic_id(); ?>"
															data-source="<?php echo esc_url( $testimonial_item->video_url ?? '' ); ?>"
															data-source_safari="<?php echo esc_url( $testimonial_item->video_safari ?? '' ); ?>"
															data-uid="<?php echo esc_attr( get_current_user_id() ); ?>"
															data-keys="<?php echo base64_encode( json_encode( $keys ) ); ?>"
															poster="<?php echo esc_url( $testimonial_item->poster ?? '' ); ?>"
															controls></video>
													<?php if ( ! empty( $text ) ) : ?>
														<p>“<?php echo esc_attr( $text ); ?>”</p>
													<?php endif; ?>
													<?php if ( ! empty( $title ) ) : ?>
														<h5><?php echo esc_attr( $title ); ?></h5>
													<?php endif; ?>
													<?php if ( ! empty( $designation ) ) : ?>
														<p class="title"><?php echo esc_attr( $designation ); ?></p>
													<?php endif; ?>
												</div>
											<?php } else { ?>
												<div class="warning-wrapper">
													<h3>
														<?php esc_html_e( 'Your browser does not support this video.', 'hantus-pro' ); ?>
													</h3>
													<p>
														<?php esc_html_e( 'You can view this lecture in the following browsers:', 'hantus-pro' ); ?>
													</p>
													<p><b><?php esc_html_e( 'Windows:', 'hantus-pro' ); ?></b></p>
													<ul>
														<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
														<li><?php esc_html_e( 'Internet Explorer', 'hantus-pro' ); ?></li>
														<li><?php esc_html_e( 'Edge', 'hantus-pro' ); ?></li>
													</ul>
													<p><b><?php esc_html_e( 'Mac:' ); ?></b></p>
													<ul>
														<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
														<li><?php esc_html_e( 'Safari', 'hantus-pro' ); ?></li>
													</ul>
												</div>
											<?php } ?>
										<?php } ?>
										<?php if ( ! empty( $text ) ) : ?>
											<p>“<?php echo esc_attr( $text ); ?>”</p>
										<?php endif; ?>
										<?php if ( ! empty( $title ) ) : ?>
											<h5><?php echo esc_attr( $title ); ?></h5>
										<?php endif; ?>
										<?php if ( ! empty( $designation ) ) : ?>
											<p class="title"><?php echo esc_attr( $designation ); ?></p>
										<?php endif; ?>
									</div>
									<?php
								}
							}
							?>
						</div>
						<div class="testimonial-thumb owl-carousel text-center">
							<?php
							if ( ! empty( $testimonial_contents ) ) {
								foreach ( $testimonial_contents as $testimonial_item ) {
									$image = ! empty( $testimonial_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->image_url, 'testimonial section' ) : '';
									echo '<img src=' . esc_url( $image ) . ' class="item" />';
								}
							}
							?>
						</div>
					</div>
					<a href="/product/lesson-level-1/"
					   class="boxed-btn"><?php esc_attr_e( 'Buy now', 'hantus-pro' ); ?></a>
				</div>
			</div>
		</div>
	</section>
	<?php hantus_after_testimonial_section_trigger();
	} ?>
