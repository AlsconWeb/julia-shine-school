<?php
function hantus_header_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Header Settings Panel
	=========================================*/
	$wp_customize->add_panel(
		'header_section',
		array(
			'priority'      => 30,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Header Section', 'hantus-pro'),
		)
	);


	/*=========================================
	Header Settings Section
	=========================================*/
	// Header Settings Section //
	$wp_customize->add_section(
        'header_setting',
        array(
        	'priority'      => 1,
            'title' 		=> __('Top Left Header','hantus-pro'),
			'panel'  		=> 'header_section',
		)
    );

	// Setting Head
	$wp_customize->add_setting(
		'hdr_setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'hdr_setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'header_setting',
		)
	);

	// Social Icons Hide/Show Setting //
	$wp_customize->add_setting(
		'hide_show_social_icon' ,
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'hide_show_social_icon',
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'header_setting',
			'settings'    => 'hide_show_social_icon',
			'type'        => 'ios', // light, ios, flat
		)
	));

	// Time Head
	$wp_customize->add_setting(
		'hdr_time_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 4,
		)
	);

	$wp_customize->add_control(
	'hdr_time_head',
		array(
			'type' => 'hidden',
			'label' => __('Time','hantus-pro'),
			'section' => 'header_setting',
		)
	);

	// address info icon hantus //
	$wp_customize->add_setting(
    	'hantus_time_icon',
    	array(
	        'default' => 'fa-clock-o',
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize,
		'hantus_time_icon',
		array(
		    'label'   		=> __('Time Icon','hantus-pro'),
		    'section' 		=> 'header_setting',
			'iconset' => 'fa',
			'settings' 		 => 'hantus_time_icon',
			'description'   => __( '', 'hantus-pro' ),

		))
	);

	// address info hantus //
	$wp_customize->add_setting(
    	'hantus_timing',
    	array(
	        'default'			=> __('Opening Hours - 10 Am to 6 PM','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);

	$wp_customize->add_control(
		'hantus_timing',
		array(
		    'label'   		=> __('Address','hantus-pro'),
		    'section' 		=> 'header_setting',
			'settings' 		 => 'hantus_timing',
			'description'   => __( '', 'hantus-pro' ),
			'type'		 =>	'textarea'
		)
	);


	// Social Head
	$wp_customize->add_setting(
		'hdr_social_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 9,
		)
	);

	$wp_customize->add_control(
	'hdr_social_head',
		array(
			'type' => 'hidden',
			'label' => __('Social Icon','hantus-pro'),
			'section' => 'header_setting',
		)
	);

	/**
	 * Customizer Repeater
	 */
		$wp_customize->add_setting( 'social_icons',
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'priority' => 10,
			 'default' => json_encode(
			array(
				array(

					'id'              => 'customizer_repeater_56d7ea7f40c56',
					'social_repeater' => json_encode(
						array(
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb908674e06',
								'link' => 'facebook.com',
								'icon' => 'fa-facebook',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb9148530ft',
								'link' => 'plus.google.com',
								'icon' => 'fa-google-plus',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb9148530fc',
								'link' => 'twitter.com',
								'icon' => 'fa-twitter',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb9150e1e89',
								'link' => 'linkedin.com',
								'icon' => 'fa-linkedin',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb9150e1e90',
								'link' => '#',
								'icon' => 'fa-google-plus',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-57fb9150e1e91',
								'link' => '#',
								'icon' => 'fa-behance',
							),
						)
					),
				),
			)
		)
		)
		);

		$wp_customize->add_control(
			new Hantus_Repeater( $wp_customize,
				'social_icons',
					array(
						'label'   => esc_html__('Social Icons','hantus-pro'),
						'section' => 'header_setting',
						'customizer_repeater_icon_control' => true,
						'customizer_repeater_link_control' => true,
					)
				)
			);

		$wp_customize->add_setting(
			'black_list_country_insta',
			[
				'transport'=>'refresh'
			]
		);

		$wp_customize->add_control(
			'black_list_country_insta',
			[
				'section'  => 'header_setting', // id секции
				'label'    => __('Black list instagram','hantus-pro' ),
				'type'     => 'text' // текстовое поле
			]
		);
	/*=========================================
	Header Contact Settings Section
	=========================================*/
	// Header Contact Setting Section //
	$wp_customize->add_section(
        'header_contact',
        array(
        	'priority'      => 2,
            'title' 		=> __('Top Right Header','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );

	// Header Contact Indo Hide/Show Setting //


	// Right Header Setting  Head
	$wp_customize->add_setting(
		'right_hdr_setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'right_hdr_setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'header_contact',
		)
	);

	// Social Icons Hide/Show Setting //
	$wp_customize->add_setting(
		'hide_show_contact_infot' ,
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'hide_show_contact_infot',
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'header_contact',
			'settings'    => 'hide_show_contact_infot',
			'type'        => 'ios', // light, ios, flat
		)
	));


	// Right Header Email  Head
	$wp_customize->add_setting(
		'right_hdr_email_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 3,
		)
	);

	$wp_customize->add_control(
	'right_hdr_email_head',
		array(
			'type' => 'hidden',
			'label' => __('Email','hantus-pro'),
			'section' => 'header_contact',
		)
	);

	// Header Email icon Setting //
	$wp_customize->add_setting(
    	'header_email_icon',
    	array(
	        'default'			=> __('fa-envelope-o','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'priority' => 4,
		)
	);

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize,
		'header_email_icon',
		array(
		    'label'   => __('Email Icon','hantus-pro'),
		    'section' => 'header_contact',
			'settings'=> 'header_email_icon',
			'iconset' => 'fa',
			'description'    => __( '', 'hantus-pro' ),
		))
	);

	// Header Email Setting //
	$wp_customize->add_setting(
    	'header_email',
    	array(
	        'default'			=> __('email@companyname.com','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
		'header_email',
		array(
		    'label'   => __('Email','hantus-pro'),
		    'section' => 'header_contact',
			'settings'=> 'header_email',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)
	);

	// Right Header Contact  Head
	$wp_customize->add_setting(
		'right_hdr_contact_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 6,
		)
	);

	$wp_customize->add_control(
	'right_hdr_contact_head',
		array(
			'type' => 'hidden',
			'label' => __('Phone','hantus-pro'),
			'section' => 'header_contact',
		)
	);

	// Header Contact Number Setting //
	$wp_customize->add_setting(
    	'header_phone_icon',
    	array(
	        'default'			=> __('fa-phone','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'priority' => 7,
		)
	);

	$wp_customize->add_control( new Hantus_Icon_Picker_Control($wp_customize,
		'header_phone_icon',
		array(
		    'label'   => __('Phone Icon','hantus-pro'),
		    'section' => 'header_contact',
			'settings'=> 'header_phone_icon',
			'iconset' => 'fa',
			'description'    => __( '', 'hantus-pro' ),
		) )
	);

	$wp_customize->add_setting(
    	'header_phone_number',
    	array(
	        'default'			=> __('+12 345 678 910','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
		'header_phone_number',
		array(
		    'label'   => __('Phone Number','hantus-pro'),
		    'section' => 'header_contact',
			'settings'=> 'header_phone_number',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)
	);
	/*=========================================
	Header search & cart Settings Section
	=========================================*/

	// Header search & cart Setting Section //
	$wp_customize->add_section(
        'header_contact_cart',
        array(
        	'priority'      => 3,
            'title' 		=> __('Header Search & Cart','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );

	// Header Search  Head
	$wp_customize->add_setting(
		'hdr_search_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'hdr_search_head',
		array(
			'type' => 'hidden',
			'label' => __('Search','hantus-pro'),
			'section' => 'header_contact_cart',
		)
	);
	// hide/show  search settings
	$wp_customize->add_setting(
		'search_header_setting' ,
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'search_header_setting',
		array(
			'label'	      => esc_html__( 'Hide / Show Search', 'hantus-pro' ),
			'section'     => 'header_contact_cart',
			'settings'    => 'search_header_setting',
			'type'        => 'ios', // light, ios, flat
		)
	));

	// Header search Setting //
	$wp_customize->add_setting(
    	'header_search',
    	array(
	        'default'			=> __('fa-search','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'priority' => 3,
		)
	);

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize,
		'header_search',
		array(
		    'label'   => __('Search Icon','hantus-pro'),
		    'section' => 'header_contact_cart',
			'settings'=> 'header_search',
			'iconset' => 'fa',
			'description'    => __( '', 'hantus-pro' ),
		) )
	);

	// Header Cart  Head
	$wp_customize->add_setting(
		'hdr_cart_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'hdr_cart_head',
		array(
			'type' => 'hidden',
			'label' => __('Cart','hantus-pro'),
			'section' => 'header_contact_cart',
		)
	);

	// hide/show  cart settings
	$wp_customize->add_setting(
		'cart_header_setting' ,
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'cart_header_setting',
		array(
			'label'	      => esc_html__( 'Hide / Show Cart', 'hantus-pro' ),
			'section'     => 'header_contact_cart',
			'settings'    => 'cart_header_setting',
			'type'        => 'ios', // light, ios, flat
		)
	));

	// Header cart Setting //
	$wp_customize->add_setting(
    	'header_cart',
    	array(
	        'default'			=> __('fa-shopping-bag','hantus-pro'),
			'sanitize_callback' => 'hantus_sanitize_text',
			'capability' => 'edit_theme_options',
			'priority' => 7,
		)
	);

	$wp_customize->add_control( new Hantus_Icon_Picker_Control($wp_customize,
		'header_cart',
		array(
		    'label'   => __('Cart Icon','hantus-pro'),
		    'section' => 'header_contact_cart',
			'settings'=> 'header_cart',
			'iconset' => 'fa',
		) )
	);

	/*=========================================
	Header contact
	=========================================*/
	$wp_customize->add_section(
        'header_menu_contact',
        array(
        	'priority'      => 3,
            'title' 		=> __('Header Contact','hantus'),
			'panel'  		=> 'header_section',
		)
    );

	// Setting  Head
	$wp_customize->add_setting(
		'hdr_menu_contact_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'hdr_menu_contact_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'header_menu_contact',
		)
	);

	// hide/show
	$wp_customize->add_setting(
		'menu_contact_hs' ,
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_checkbox',
			'transport'         => $selective_refresh,
			'priority' => 2,
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'menu_contact_hs',
		array(
			'label'	      => esc_html__( 'Hide / Show', 'hantus' ),
			'section'     => 'header_menu_contact',
			'settings'    => 'menu_contact_hs',
			'type'        => 'ios', // light, ios, flat
		)
	));


	// Content  Head
	$wp_customize->add_setting(
		'hdr_menu_contact_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'hdr_menu_contact_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'header_menu_contact',
		)
	);

	// Icon //
	$wp_customize->add_setting(
    	'menu_contact_icon',
    	array(
			'default' => 'fa-wechat',
			'sanitize_callback' => 'hantus_sanitize_html',
			'capability' => 'edit_theme_options',
			'priority' => 6,
		)
	);

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize,
		'menu_contact_icon',
		array(
		    'label'   => __('Icon','hantus'),
		    'section' => 'header_menu_contact',
			'settings'=> 'menu_contact_icon',
			'iconset' => 'fa',
		) )
	);
	// Title //
	$wp_customize->add_setting(
    	'menu_contact_title',
    	array(
			'default'	      => esc_html__( 'Have Any Questions?', 'hantus' ),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 7,
		)
	);

	$wp_customize->add_control(
		'menu_contact_title',
		array(
		    'label'   => __('Title','hantus'),
		    'section' => 'header_menu_contact',
			'settings'   	 => 'menu_contact_title',
			'type'           => 'text',
		)
	);

	// Subtitle //
	$wp_customize->add_setting(
    	'menu_contact_subtitle',
    	array(
			'default'	      => esc_html__( '+12 345 678 910', 'hantus' ),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
		'menu_contact_subtitle',
		array(
		    'label'   => __('Subtitle','hantus'),
		    'section' => 'header_menu_contact',
			'settings'   	 => 'menu_contact_subtitle',
			'type'           => 'text',
		)
	);

	// Link //
	$wp_customize->add_setting(
    	'menu_contact_link',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 9,
		)
	);

	$wp_customize->add_control(
		'menu_contact_link',
		array(
		    'label'   => __('Link','hantus'),
		    'section' => 'header_menu_contact',
			'settings'   	 => 'menu_contact_link',
			'type'           => 'text',
		)
	);
	/*=========================================
	Sticky Header Section
	=========================================*/
	$wp_customize->add_section(
        'sticky_header',
        array(
        	'priority'      => 3,
            'title' 		=> __('Sticky Header','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );


	$wp_customize->add_setting(
		'sticky_header_setting' ,
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)
	);

	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize,
	'sticky_header_setting',
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'sticky_header',
			'settings'    => 'sticky_header_setting',
			'type'        => 'ios', // light, ios, flat
		)
	));
}

add_action( 'customize_register', 'hantus_header_setting' );


// header selective refresh
function hantus_home_header_section_partials( $wp_customize ){
	// hide_show_social_icon
	$wp_customize->selective_refresh->add_partial(
		'hide_show_social_icon', array(
			'selector' => '.header-social, .time-details',
			'container_inclusive' => true,
			'render_callback' => 'header_setting',
			'fallback_refresh' => true,
		)
	);
	// hide_show_contact_infot
	$wp_customize->selective_refresh->add_partial(
		'hide_show_contact_infot', array(
			'selector' => '.header-top-right',
			'container_inclusive' => true,
			'render_callback' => 'header_contact',
			'fallback_refresh' => true,
		)
	);

	// search_header_setting
	$wp_customize->selective_refresh->add_partial(
		'search_header_setting', array(
			'selector' => '#sb-search',
			'container_inclusive' => true,
			'render_callback' => 'header_contact_cart',
			'fallback_refresh' => true,
		)
	);
	// cart_header_setting
	$wp_customize->selective_refresh->add_partial(
		'cart_header_setting', array(
			'selector' => '.cart-icon',
			'container_inclusive' => true,
			'render_callback' => 'header_contact_cart',
			'fallback_refresh' => true,
		)
	);

	// menu_contact_hs
	$wp_customize->selective_refresh->add_partial(
		'menu_contact_hs', array(
			'selector' => '.header-info-bg',
			'container_inclusive' => true,
			'render_callback' => 'header_menu_contact',
			'fallback_refresh' => true,
		)
	);

	// social icons
	$wp_customize->selective_refresh->add_partial( 'social_icons', array(
		'selector'            => '#header-top .header-social',
		'settings'            => 'social_icons',
		'render_callback'  => 'header_section_social_render_callback',

	) );
	// hantus_timing
	$wp_customize->selective_refresh->add_partial( 'hantus_timing', array(
		'selector'            => '#header-top p',
		'settings'            => 'hantus_timing',
		'render_callback'  => 'header_section_hantus_timing_render_callback',

	) );
	// address
	$wp_customize->selective_refresh->add_partial( 'hantus_address_icon', array(
		'selector'            => '#header-top .address i',
		'settings'            => 'hantus_address_icon',
		'render_callback'  => 'header_section_add_icon_render_callback',

	) );


	// email text

	$wp_customize->selective_refresh->add_partial( 'header_email', array(
		'selector'            => '#header-top .h-t-e ',
		'settings'            => 'header_email',
		'render_callback'  => 'header_section_header_email_render_callback',

	) );



	// header_phone_number

	$wp_customize->selective_refresh->add_partial( 'header_phone_number', array(
		'selector'            => '#header-top .h-t-p ',
		'settings'            => 'header_phone_number',
		'render_callback'  => 'header_section_header_phone_number_render_callback',

	) );
	// header_search_icon

	$wp_customize->selective_refresh->add_partial( 'header_search', array(
		'selector'            => '#header .header-right-bar .search-button i',
		'settings'            => 'header_search',
		'render_callback'  => 'header_section_search_render_callback',

	) );

	// header_cart_icon

	$wp_customize->selective_refresh->add_partial( 'header_cart', array(
		'selector'            => '#header .header-right-bar .cart-icon i',
		'settings'            => 'header_cart',
		'render_callback'  => 'header_section_cart_render_callback',

	) );
	}

add_action( 'customize_register', 'hantus_home_header_section_partials' );

// social icons
function header_section_social_render_callback() {
	return get_theme_mod( 'social_icons' );
}
// hantus_timing
function header_section_hantus_timing_render_callback() {
	return get_theme_mod( 'hantus_timing' );
}
// address icon
function header_section_add_icon_render_callback() {
	return get_theme_mod( 'hantus_address_icon' );
}
// header_email
function header_section_header_email_render_callback() {
	return get_theme_mod( 'header_email' );
}
// header_phone_number
function header_section_header_phone_number_render_callback() {
	return get_theme_mod( 'header_phone_number' );
}
// search
function header_section_search_render_callback() {
	return get_theme_mod( 'header_search' );
}
// cart
function header_section_cart_render_callback() {
	return get_theme_mod( 'header_cart' );
}
