<!-- Start: Header
============================= -->
<?php

use Hantus\Theme\API\GeoFromIP;

$default_content         = hantus_get_social_icon_default();
$hide_show_preloader     = get_theme_mod( 'hide_show_preloader', '0' );
$hide_show_social_icon   = get_theme_mod( 'hide_show_social_icon', '1' );
$social_icons            = get_theme_mod( 'social_icons', $default_content );
$hantus_time_icon        = get_theme_mod( 'hantus_time_icon', 'fa-clock-o' );
$hantus_timing           = get_theme_mod( 'hantus_timing', 'Opening Hours - 10 Am to 6 PM' );
$hide_show_contact_infot = get_theme_mod( 'hide_show_contact_infot', '1' );
$header_email_icon       = get_theme_mod( 'header_email_icon', 'fa-envelope-o' );
$header_email            = get_theme_mod( 'header_email', 'email@companyname.com' );
$header_phone_icon       = get_theme_mod( 'header_phone_icon', 'fa-phone' );
$header_phone_number     = get_theme_mod( 'header_phone_number', '+12 345 678 910' );
$sticky_header_setting   = get_theme_mod( 'sticky_header_setting', '1' );
$header_image            = get_header_image();

$cart_header_setting   = get_theme_mod( 'cart_header_setting', '1' );
$hantus_header_search  = get_theme_mod( 'header_search', 'fa-search' );
$header_cart           = get_theme_mod( 'header_cart', 'fa-shopping-bag' );
$search_header_setting = get_theme_mod( 'search_header_setting', '1' );
$header_btn_icon       = get_theme_mod( 'header_btn_icon', 'icofont icofont-search' );
$header_btn_lbl        = get_theme_mod( 'header_btn_lbl', 'Book Now' );
$header_btn_link       = get_theme_mod( 'header_btn_link', '#' );

$hantus_menu_contact_hs       = get_theme_mod( 'menu_contact_hs', '1' );
$hantus_menu_contact_icon     = get_theme_mod( 'menu_contact_icon', 'fa-wechat' );
$hantus_menu_contact_title    = get_theme_mod( 'menu_contact_title', 'Have Any Questions?' );
$hantus_menu_contact_subtitle = get_theme_mod( 'menu_contact_subtitle', '+12 345 678 910' );
$hantus_menu_contact_link     = get_theme_mod( 'menu_contact_link' );

?>

<?php if ( $hide_show_preloader == '1' ) {
	if ( is_customize_preview() ) {
	} else {
		?>
		<div class="preloader">
			<div class="wrapper">
				<div class="circle circle-1"></div>
				<div class="circle circle-1a"></div>
				<div class="circle circle-2"></div>
				<div class="circle circle-3"></div>
			</div>
			<h1>Loading&hellip;</h1>
		</div>
	<?php }
} ?>
<!-- End: Preloader
============================= -->

<!-- Start: Navigation
============================= -->
<section class="navbar-wrapper">
	<!-- Start: Header Top
	============================= -->
	<?php
	if ( ! empty( $header_image ) ) {
		?>
		<img src="<?php echo( get_header_image() ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>"/>
		<?php
	} else {
	}
	hantus_before_header_section_trigger();
	?>
	<div id="header-top">
		<div class="container">

			<div class="row">

				<div class="col-lg-6 col-md-6 text-center text-md-left left-top-header">
					<?php if ( $hide_show_social_icon == '1' ) { ?>
						<p class="time-details"><i
									class="fa <?php echo $hantus_time_icon; ?>"></i><?php echo $hantus_timing; ?></p>
						<ul class="header-social d-inline-block">
							<?php
							$social_icons = json_decode( $social_icons );
							if ( $social_icons != '' ) {
								foreach ( $social_icons as $social_item ) {
									$social_icon = ! empty( $social_item->icon_value ) ? apply_filters( 'hantus_translate_single_string', $social_item->icon_value, 'Header section' ) : '';
									$social_link = ! empty( $social_item->link ) ? apply_filters( 'hantus_translate_single_string', $social_item->link, 'Header section' ) : '';

									$user_ip = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : '0.0.0.0';
									$geo_api = new GeoFromIP( $user_ip );
									if ( 'fa-instagram' === $social_item->icon_value && $geo_api->in_black_list_country() ) {
										$social_link = get_theme_mod( 'black_list_country_insta' ) ?? '#';
									}
									?>
									<li><a target="_blank" href="<?php echo esc_url( $social_link ); ?>">
											<i class="fa <?php echo esc_attr( $social_icon ); ?> "></i>
										</a>
									</li>
									<?php
								}
							}
							?>
						</ul>
						<?php
					}
					?>
				</div>
				<?php ?>
				<?php if ( $hide_show_contact_infot == '1' ) { ?>
					<div class="col-lg-6 col-md-6 text-center text-md-right header-top-right">
						<ul class="text-details">
							<li class="h-t-e">
								<a href="mailto:<?php echo esc_attr( $header_email ); ?>">
									<i class="fa <?php echo esc_attr( $header_email_icon ); ?>"></i>
									<?php echo $header_email; ?>
								</a>
							</li>
							<li class="h-t-p">
								<a href="tel:<?php echo esc_html( $header_phone_number ); ?>">
									<i class="fa <?php echo esc_attr( $header_phone_icon ); ?>"></i>
									<?php echo $header_phone_number; ?>
								</a>
							</li>
						</ul>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="navbar-area <?php echo hantus_sticky_menu(); ?>">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-6 my-auto">
					<div class="logo main">
						<?php
						if ( has_custom_logo() ) {
							the_custom_logo();
						} else {
							?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<h4 class="site-title">
									<?php
									echo esc_html( bloginfo( 'name' ) );
									?>
								</h4>
							</a>
							<?php
						}
						?>
						<?php
						$description = get_bloginfo( 'description' );
						if ( $description ) : ?>
							<p class="site-description"><?php echo $description; ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-9 col-4 d-none d-lg-block text-right my-auto">
					<div class="navigation">
						<nav class="main-menu">
							<?php
							wp_nav_menu(
								[
									'theme_location' => 'primary_menu',
									'container'      => '',
									'menu_class'     => '',
									'fallback_cb'    => 'hantus_fallback_page_menu',
									'walker'         => new hantus_nav_walker(),

									// 'theme_location' => 'primary_menu',
									// 'container'  => '',
									// 'menu_class' => '',
									// 'fallback_cb' => 'hantus_fallback_page_menu',
									// 'items_wrap'  => $extra_html,
									// 'walker' => new hantus_nav_walker()
								]
							);
							?>
						</nav>
						<div class="mbl-right">
							<ul class="mbl">
								<?php if ( $hantus_menu_contact_hs == '1' ) { ?>
									<li class="header-info-bg">
										<div class="header-info-text">
											<div class="icons-info">
												<a
														class="dot"
														href="<?php echo esc_url( $hantus_menu_contact_link ); ?>">
													<div class="icons">
														<i class="fa <?php echo esc_attr( $hantus_menu_contact_icon ); ?>"></i>
													</div>
												</a>
												<div class="info">
													<span class="info-title"><?php echo esc_html( $hantus_menu_contact_title ); ?></span>
													<span class="info-subtitle">
														<a class="dot"
														   href="<?php echo esc_url( $hantus_menu_contact_link ); ?>">
															<?php echo esc_html( $hantus_menu_contact_subtitle ); ?>
														</a>
													</span>
												</div>
											</div>
										</div>
									</li>
								<?php } ?>
								<?php if ( ( $cart_header_setting == '1' ) && class_exists( 'woocommerce' ) ) {
									$count    = WC()->cart->cart_contents_count;
									$cart_url = wc_get_cart_url();
									?>
									<li class="cart-icon">
										<button type="button" class="cart-icon-wrapper cart--open">
											<i class="fa <?php echo esc_attr( $header_cart ); ?>"></i>
											<?php if ( $count > 0 ) { ?>
												<span class="cart-count"><?php echo esc_html( $count ); ?></span>
											<?php } else { ?>
												<span class="cart-count">0</span>
											<?php } ?>
										</button>
									</li>
								<?php } ?>
								<?php if ( $search_header_setting == '1' ) { ?>
									<li class="search-button">
										<div class="sb-search">
											<button type="button" id='search-clicker'
													class="sb-icon-search sb-search-toggle"><i
														class="fa <?php echo esc_attr( $hantus_header_search ); ?>"></i>
											</button>
										</div>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-6 text-right d-block d-lg-none my-auto">
					<div class="mbl-right">
						<ul class="mbl">
							<?php if ( ( $cart_header_setting == '1' ) && class_exists( 'woocommerce' ) ) {
								$count    = WC()->cart->cart_contents_count;
								$cart_url = wc_get_cart_url();
								?>
								<li class="cart-icon">
									<button type="button" class="cart-icon-wrapper cart--open">
										<i class="fa <?php echo esc_attr( $header_cart ); ?>"></i>
										<?php if ( $count > 0 ) { ?>
											<span class="cart-count"><?php echo esc_html( $count ); ?></span>
										<?php } else { ?>
											<span class="cart-count">0</span>
										<?php } ?>
									</button>
								</li>
							<?php } ?>
							<?php if ( $search_header_setting == '1' ) { ?>
								<li class="search-button">
									<div class="sb-search">
										<button type="button" id='search-clicker'
												class="sb-icon-search sb-search-toggle"><i
													class="fa <?php echo esc_attr( $hantus_header_search ); ?>"></i>
										</button>
									</div>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="sb-search sb-search-popup">
					<div class="sb-search-pop">
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input class="sb-search-input" placeholder="<?php esc_attr_e( 'Search', 'hantus-pro' ); ?>"
								   type="search" value="" name="s" id="s">
							<button type="button" id='search-clicker' class="sb-icon-search"><i class="fa fa-close"></i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Start Mobile Menu -->
		<div class="mobile-menu-area d-lg-none">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mobile-menu">
							<nav class="mobile-menu-active">
								<?php
								wp_nav_menu(
									[
										'theme_location' => 'primary_menu',
										'container'      => '',
										'menu_class'     => '',
										'fallback_cb'    => 'hantus_fallback_page_menu',
										'walker'         => new hantus_nav_walker(),
									]
								);
								?>
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Mobile Menu -->
	</div>
</section>
<!-- End: Navigation
============================= -->

<?php if ( class_exists( 'woocommerce' ) ) { ?>
	<div class="sidenav cart ">
		<div class="sidenav-div">
			<div class="sidenav-header">
				<button type="button" class="close-sidenav">
					<i class="fa fa-times"></i>
				</button>
				<h3><?php esc_html_e( 'Your cart', 'hantus-pro' ); ?></h3>
			</div>
			<div class="woo-cart cart-content">
				<?php get_template_part( 'woocommerce/cart/mini', 'cart' ); ?>
			</div>
		</div>
	</div>
	<span class="cart-overlay"></span>
<?php } ?>

<?php
hantus_after_header_section_trigger();
if ( is_page_template( 'templates/template-homepage-one.php' ) ) {
} elseif ( is_page_template( 'templates/template-homepage-two.php' ) ) {
} elseif ( is_page_template( 'templates/template-homepage-rev.php' ) ) {
} else {
	hantus_breadcrumbs_style();
} ?>
