<?php
/**
Template Name: Contact Page
**/

get_header();
	$cotact_page_address_first_i 		= get_theme_mod('cotact_page_address_first_i','fa-map-marker'); 
	$cotact_page_address_first_t 		= get_theme_mod('cotact_page_address_first_t','Store Address One:'); 
	$cotact_page_address_first_a 		= get_theme_mod('cotact_page_address_first_a','481-7473 Cum Rd. Yorba Linda South Carolina <br> 2-345-678-90112 <br> email2@companyname.com '); 
	$cotact_page_address_second_i 		= get_theme_mod('cotact_page_address_second_i','fa-map-marker'); 
	$cotact_page_address_second_t 		= get_theme_mod('cotact_page_address_second_t','Store Address One:'); 
	$cotact_page_address_second_a 		= get_theme_mod('cotact_page_address_second_a','481-7473 Cum Rd. Yorba Linda South Carolina <br> 2-345-678-90112 <br> email2@companyname.com '); 
	$cotact_page_address_third_i 		= get_theme_mod('cotact_page_address_third_i','fa-clock-o'); 
	$cotact_page_address_third_t 		= get_theme_mod('cotact_page_address_third_t','Opening Hours:'); 
	$cotact_page_address_third_a 		= get_theme_mod('cotact_page_address_third_a','Monday-Friday: 10 Am to 6 Pm <br> Saturday: 10 Am to 6 Pm <br> Sunday: Close'); 
	
	$contact_page_form_title			= get_theme_mod('contact_page_form_title','Contact Form'); 
	$contact_page_form_shortcode		= get_theme_mod('contact_page_form_shortcode'); 
	$hide_show_google_map				= get_theme_mod('hide_show_google_map','1'); 
	$hide_show_contact_page_form		= get_theme_mod('hide_show_contact_page_form','1'); 
	$hide_show_contact_page_news		= get_theme_mod('hide_show_contact_page_news','1'); 
	$contact_page_info_title		= get_theme_mod('contact_page_info_title','Contact Information'); 
	$contact_page_info_desc				= get_theme_mod('contact_page_info_desc','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'); 
	$contact_page_map_lattitude			= get_theme_mod('contact_page_map_lattitude','40.6700'); 
	$contact_page_map_longitude			= get_theme_mod('contact_page_map_longitude','-73.9400'); 
	$settings=array('lattitude'=>$contact_page_map_lattitude,'longitude'=>$contact_page_map_longitude);
	wp_register_script('hantus-contact-map',get_template_directory_uri().'/assets/js/map-script.js',array('jquery'));
	wp_localize_script('hantus-contact-map','contact_map_settings',$settings);
	wp_enqueue_script('hantus-contact-map');	 
?>	
<!-- Start: Contact
    ============================= -->
 <?php hantus_before_contact_page_section_trigger(); ?>   
    <section id="contact" class="section-padding">
        <div class="container">
            <div class="row">
				<?php if($hide_show_google_map == '1') { ?>
					<div class="col-lg-12">
						<div id="map"></div>
					</div>
				<?php } ?>
				<?php	 if($hide_show_contact_page_form == '1') { ?>
                <div class="col-lg-6 contact-form">
					<?php if($contact_page_form_title) { ?>
						<h2><?php echo esc_attr($contact_page_form_title); ?></h2>
					<?php } ?>	
					<?php 
							if($contact_page_form_shortcode != '') {
								echo do_shortcode( $contact_page_form_shortcode );
							}else{
						?>
                    <form action="#">

                        <span class="input input-hantus">
                            <input class="input_field" type="text" id="input-01" />
                            <label class="input_label" for="input-01">Name</label>
                        </span>

                        <span class="input input-hantus">
                            <input class="input_field" type="text" id="input-02" />
                            <label class="input_label" for="input-02">Email</label>
                        </span>

                        <span class="input input-hantus">
                            <input class="input_field" type="text" id="input-03" />
                            <label class="input_label" for="input-03">Subject</label>
                        </span>

                        <span class="input input-hantus textarea">
                            <textarea class="input_field" rows="6" id="input-04"></textarea>
                            <label class="input_label" for="input-04">Message</label>
                        </span>
                                                
                        <button class="boxed-btn">Send Message</button>
                    </form>
					<?php } ?>
                </div>
                <div class="col-lg-6 contact-info mt-5 mt-lg-0">
					<?php if($contact_page_info_title) { ?>
						<h2><?php echo esc_attr($contact_page_info_title); ?></h2>
					<?php } ?>
					<?php if($contact_page_info_desc) { ?>
						<p id="contct-info-dsc"><?php echo esc_attr( $contact_page_info_desc ); ?></p>
					<?php } ?>
                    <div class="info-box mt-4" id="info-box-first">
                        <i class="fa <?php echo esc_attr($cotact_page_address_first_i); ?>"></i>
                        <h4><?php echo esc_attr($cotact_page_address_first_t); ?></h4>
                        <p><?php echo wp_kses_post($cotact_page_address_first_a); ?></p>
                    </div>

                    <div class="info-box" id="info-box-second">
                        <i class="fa <?php echo esc_attr($cotact_page_address_second_i); ?>"></i>
                        <h4><?php echo esc_attr($cotact_page_address_second_t); ?></h4>
                        <p><?php echo wp_kses_post($cotact_page_address_second_a); ?> </p>
                    </div>

                    <div class="info-box" id="info-box-third">
                        <i class="fa <?php echo esc_attr($cotact_page_address_third_i); ?>"></i>
                        <h4><?php echo esc_attr($cotact_page_address_third_t); ?></h4>
                        <p><?php echo wp_kses_post($cotact_page_address_third_a); ?></p>
                    </div>
                </div>
				<?php } ?> 
            </div>
        </div>
    </section>
<?php hantus_after_contact_page_section_trigger(); ?>	
    <!-- End: Contact
    ============================= -->

    <!-- Start: Subscribe
    ============================= -->
    <?php if($hide_show_contact_page_news == '1') {?>
		<?php get_template_part('template-parts/sections/section','newsletter');	 ?>
    <?php } ?>
    <!-- End: Subscribe
    ============================= -->
<?php get_footer(); ?>