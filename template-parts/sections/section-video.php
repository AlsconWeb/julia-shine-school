<?php
/**
 * Video section
 *
 * @package hantus/theme
 */

use Hantus\Theme\Helpers\FrontEndHelpers;

$custom_section_title     = carbon_get_the_post_meta( 'ju_video_section_title' );
$custom_section_sub_title = carbon_get_the_post_meta( 'ju_video_section_sub_title' );
$section_id               = carbon_get_the_post_meta( 'ju_video_section_id' );
$videos                   = carbon_get_the_post_meta( 'ju_videos' );
$section_class            = carbon_get_the_post_meta( 'ju_video_section_class' ) ?? '';
$demo                     = carbon_get_the_post_meta( 'ju_custom_section_demo' ) ?? '';
?>

<section
		id="<?php echo esc_attr( $section_id ) ?? ''; ?>"
		class="section-padding <?php echo esc_attr( $section_class ); ?> section-padding move-section start-section">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<div class="section-title service-section">
					<?php if ( $custom_section_title ) { ?>
						<h2><?php echo esc_attr( $custom_section_title ); ?></h2>
					<?php } ?>
					<p>
					<?php
					if ( ! empty( $custom_section_sub_title ) ) {
						echo wp_kses_post( $custom_section_sub_title );
					}
					?>
					</p>
				</div>
			</div>
		</div>
		<div class="row text-center video_row">
			<?php
			$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

			$is_browser = preg_match( '/((Win|Macintosh).*?(Chrome|Edg|Trident|Safari))(?!.*OPR)/i', $user_agent );

			if ( ! empty( $videos ) && $is_browser || $demo ) {
				foreach ( $videos as $key => $video ) {
					$keys = [
						$video['url_key_hd'],
						$video['url_key_sd'],
						$video['url_key_audio'],
					];
					?>
					<div class="col-lg-4 col-md-6 col-sm-12">
						<div class="video-box text-center">
							<video
									style="height: auto; width: 100%;"
									id="video-shaka-<?php echo FrontEndHelpers::generate_unic_id(); ?>"
									data-source="<?php echo esc_url( $video['url_chrome'] ?? '' ); ?>"
									data-source_safari="<?php echo esc_url( $video['url_mac'] ?? '' ); ?>"
									data-uid="<?php echo esc_attr( get_current_user_id() ); ?>"
									data-keys="<?php echo base64_encode( json_encode( $keys ) ); ?>"
									poster="<?php echo esc_url( $video['thumbnail'] ?? '' ); ?>"
									controls></video>
							<h4 class="title"><?php echo esc_html( $video['title'] ); ?></h4>
							<div class="description">
								<?php echo wp_kses_post( wpautop( $video['description'] ) ); ?>
							</div>
						</div>
					</div>
					<?php
				}
			} else {
				?>
				<div class="warning-wrapper">
					<h3>
						<?php esc_html_e( 'Your browser does not support this video.', 'hantus-pro' ); ?>
					</h3>
					<p>
						<?php esc_html_e( 'You can view this lecture in the following browsers:', 'hantus-pro' ); ?>
					</p>
					<p><b><?php esc_html_e( 'Windows:', 'hantus-pro' ); ?></b></p>
					<ul>
						<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
						<li><?php esc_html_e( 'Internet Explorer', 'hantus-pro' ); ?></li>
						<li><?php esc_html_e( 'Edge', 'hantus-pro' ); ?></li>
					</ul>
					<p><b><?php esc_html_e( 'Mac:' ); ?></b></p>
					<ul>
						<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
						<li><?php esc_html_e( 'Safari', 'hantus-pro' ); ?></li>
					</ul>
				</div>
				<?php
			}
			?>
		</div>
		<a class="boxed-btn" id="more-video" href="#"><?php esc_html_e('More videos', 'hantus-pro'); ?></a>

	</div>
</section>
