<?php
function hantus_features_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Features Section Panel
	=========================================*/
		$wp_customize->add_section(
			'features_setting', array(
				'title' => esc_html__( 'Feature Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority',28 , 'hantus_feature' ),
			)
		);
		
		
	// Setting  Head 
	$wp_customize->add_setting(
		'feature_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'feature_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'features_setting',
		)
	);
	
	// Slider Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_feature' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_feature', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'features_setting',
			'settings'    => 'hide_show_feature',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Features Header Section // 
	
	//  Head 
	$wp_customize->add_setting(
		'feature_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'feature_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'features_setting',
		)
	);
	
	// feature Title // 
	$wp_customize->add_setting(
    	'features_title',
    	array(
	        'default'			=> __('Features','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'features_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'features_setting',
			'settings'   	 => 'features_title',
			'type'           => 'text',
		)  
	);
	
	// feature Description // 
	$wp_customize->add_setting(
    	'features_description',
    	array(
	        'default'			=> __('How to Have a Healthier and More Productive Home Office','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'features_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'features_setting',
			'settings'   	 => 'features_description',
			'type'           => 'textarea',
		)  
	);
	
	// Features content Section // 
	
	// Content Head 
	$wp_customize->add_setting(
		'feature_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'feature_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'features_setting',
		)
	);
	
	/**
	 * Customizer Repeater for add features
	 */
		$wp_customize->add_setting( 'feature_content', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			  'default' => hantus_get_feature_default(),
			  'priority' => 9,
			)
		);
		
		$wp_customize->add_control( 
			new Hantus_Repeater( $wp_customize, 
				'feature_content', 
					array(
						'label'   => esc_html__('Features','hantus-pro'),
						'section' => 'features_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Feature', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_link_control' => true,
					) 
				) 
			);
	$wp_customize->add_setting( 
		'feature_columns' , 
			array(
			'default' => __('3', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 10,
		) 
	);

	$wp_customize->add_control(
	'feature_columns' , 
		array(
			'label'          => __( 'Feature Column', 'hantus-pro' ),
			'section'        => 'features_setting',
			'settings'   	 => 'feature_columns',
			'type'			=> 'select',
			'choices'        => 
			array(
				'6'		=>__('2 column', 'hantus-pro'),
				'4'=>__('3 column', 'hantus-pro'),
				'3'=>__('4 column', 'hantus-pro'),
				
			) 
		)
	);
	
	// Features Background Section // 
	
	// Background Head 
	$wp_customize->add_setting(
		'feature_bg_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 12,
		)
	);

	$wp_customize->add_control(
	'feature_bg_head',
		array(
			'type' => 'hidden',
			'label' => __('Background','hantus-pro'),
			'section' => 'features_setting',
		)
	);
	
	// Background Image // 
    $wp_customize->add_setting( 
    	'features_background_setting' , 
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/bg/feature-bg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',	
			'priority' => 13,
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'features_background_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'features_setting',
			'settings'   	 => 'features_background_setting',
		) 
	));
	

	
	$wp_customize->add_setting( 
		'features_background_position' , 
			array(
			'default' => __( 'fixed', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 14,
		) 
	);
	
	$wp_customize->add_control(
		'features_background_position' , 
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'features_setting',
				'settings'       => 'features_background_position',
				'type'           => 'radio',
				'choices'        => 
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)  
		) 
	);
}

add_action( 'customize_register', 'hantus_features_setting' );

// feature selective refresh
function hantus_home_feature_section_partials( $wp_customize ){
	// hide_show_feature
	$wp_customize->selective_refresh->add_partial(
		'hide_show_feature', array(
			'selector' => '#feature',
			'container_inclusive' => true,
			'render_callback' => 'features_setting',
			'fallback_refresh' => true,
		)
	);
	
	
	//title
	$wp_customize->selective_refresh->add_partial( 'features_title', array(
		'selector'            => '#feature .section-title h2',
		'settings'            => 'features_title',
		'render_callback'  => 'feature_section_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'features_description', array(
		'selector'            => '#feature .section-title p',
		'settings'            => 'features_description',
		'render_callback'  => 'feature_section_desc_render_callback',
	
	) );
	
	// feature content
		
	$wp_customize->selective_refresh->add_partial( 'feature_content', array(
		'selector'            => '#ffeaturec',
		'settings'            => 'feature_content',
		'render_callback'  => 'feature_section_content_render_callback',
	
	) );
	}

add_action( 'customize_register', 'hantus_home_feature_section_partials' );

// feature title
function feature_section_title_render_callback() {
	return get_theme_mod( 'features_title' );
}
// feature description
function feature_section_desc_render_callback() {
	return get_theme_mod( 'features_description' );
}
// feature description
function feature_section_content_render_callback() {
	return get_theme_mod( 'feature_content' );
}