<?php

/**
 * Carbon Fields init class.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarboneFields class file.
 */
class CarboneFields {

	/**
	 * CarboneFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'load_carbone' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_custom_section_settings' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_custom_html_to_course' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_custom_videos_settings' ] );
	}

	/**
	 * Load carbone fields.
	 *
	 * @return void
	 */
	public function load_carbone(): void {
		Carbon_Fields::boot();
	}


	/**
	 * Add custom section settings in front page.
	 *
	 * @return void
	 */
	public function add_custom_section_settings(): void {
		Container::make( 'post_meta', __( 'Customs sections', 'hantus-pro' ) )
			->where( 'post_id', '=', get_option( 'page_on_front' ) )
			->add_tab(
				__( 'Custom section one', 'hantus-pro' ),
				[
					Field::make( 'text', 'ju_custom_section_one_id', __( 'Section ID', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_one_class', __( 'Section class', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_one_title', __( 'Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'textarea', 'ju_custom_section_one_description', __( 'Description', 'hantus-pro' ) )
						->set_rows( 4 ),
					Field::make( 'checkbox', 'ju_custom_section_one_demo', __( 'Demo', 'hantus-pro' ) )
						->set_option_value( 'on' ),
					Field::make( 'complex', 'ju_videos_single_section_one', __( 'Videos', 'hantus-pro' ) )
						->set_max(1)
						->add_fields(
							[
								Field::make( 'text', 'url_chrome', __( 'Video url chrome', 'hantus-pro' ) ),
								Field::make( 'text', 'url_mac', __( 'Video url safati', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_hd', __( 'HD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_sd', __( 'SD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_audio', __( 'AUDIO key', 'hantus-pro' ) ),
								Field::make( 'text', 'title', __( 'Title', 'hantus-pro' ) ),
								Field::make( 'textarea', 'description', __( 'Description', 'hantus-pro' ) ),
								Field::make( 'image', 'thumbnail', __( 'Thumbnail', 'hantus-pro' ) )
									->set_value_type( 'url' ),
							]
						),
					Field::make( 'rich_text', 'ju_custom_section_one_content', __( 'Content one' ) ),
				]
			)->add_tab(
				__( 'Custom section two', 'hantus-pro' ),
				[
					Field::make( 'text', 'ju_custom_section_two_id', __( 'Section ID', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_two_class', __( 'Section class', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_two_title', __( 'Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'textarea', 'ju_custom_section_two_description', __( 'Description', 'hantus-pro' ) )
						->set_rows( 4 ),
					Field::make( 'checkbox', 'ju_custom_section_two_demo', __( 'Demo', 'hantus-pro' ) )
						->set_option_value( 'on' ),
					Field::make( 'complex', 'ju_videos_single_section_two', __( 'Videos', 'hantus-pro' ) )
						->set_max(1)
						->add_fields(
							[
								Field::make( 'text', 'url_chrome', __( 'Video url chrome', 'hantus-pro' ) ),
								Field::make( 'text', 'url_mac', __( 'Video url safati', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_hd', __( 'HD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_sd', __( 'SD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_audio', __( 'AUDIO key', 'hantus-pro' ) ),
								Field::make( 'text', 'title', __( 'Title', 'hantus-pro' ) ),
								Field::make( 'textarea', 'description', __( 'Description', 'hantus-pro' ) ),
								Field::make( 'image', 'thumbnail', __( 'Thumbnail', 'hantus-pro' ) )
									->set_value_type( 'url' ),
							]
						),
					Field::make( 'rich_text', 'ju_custom_section_two_content', __( 'Content two' ) ),
				]
			)->add_tab(
				__( 'Custom section three', 'hantus-pro' ),
				[
					Field::make( 'text', 'ju_custom_section_three_id', __( 'Section ID', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_three_class', __( 'Section class', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_three_title', __( 'Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'textarea', 'ju_custom_section_three_description', __( 'Description', 'hantus-pro' ) )
						->set_rows( 4 ),
					Field::make( 'checkbox', 'ju_custom_section_three_demo', __( 'Demo', 'hantus-pro' ) )
						->set_option_value( 'on' ),
					Field::make( 'complex', 'ju_videos_single_section_three', __( 'Videos', 'hantus-pro' ) )
						->set_max(1)
						->add_fields(
							[
								Field::make( 'text', 'url_chrome', __( 'Video url chrome', 'hantus-pro' ) ),
								Field::make( 'text', 'url_mac', __( 'Video url safati', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_hd', __( 'HD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_sd', __( 'SD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_audio', __( 'AUDIO key', 'hantus-pro' ) ),
								Field::make( 'text', 'title', __( 'Title', 'hantus-pro' ) ),
								Field::make( 'textarea', 'description', __( 'Description', 'hantus-pro' ) ),
								Field::make( 'image', 'thumbnail', __( 'Thumbnail', 'hantus-pro' ) )
									->set_value_type( 'url' ),
							]
						),
					Field::make( 'rich_text', 'ju_custom_section_three_content', __( 'Content' ) ),
				]
			)->add_tab(
				__( 'Custom section four', 'hantus-pro' ),
				[
					Field::make( 'text', 'ju_custom_section_four_id', __( 'Section ID', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_four_class', __( 'Section class', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_custom_section_four_title', __( 'Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'textarea', 'ju_custom_section_four_description', __( 'Description', 'hantus-pro' ) )
						->set_rows( 4 ),
					Field::make( 'checkbox', 'ju_custom_section_four_demo', __( 'Demo', 'hantus-pro' ) )
						->set_option_value( 'on' ),
					Field::make( 'complex', 'ju_videos_single_section_four', __( 'Videos', 'hantus-pro' ) )
						->set_max(1)
						->add_fields(
							[
								Field::make( 'text', 'url_chrome', __( 'Video url chrome', 'hantus-pro' ) ),
								Field::make( 'text', 'url_mac', __( 'Video url safati', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_hd', __( 'HD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_sd', __( 'SD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_audio', __( 'AUDIO key', 'hantus-pro' ) ),
								Field::make( 'text', 'title', __( 'Title', 'hantus-pro' ) ),
								Field::make( 'textarea', 'description', __( 'Description', 'hantus-pro' ) ),
								Field::make( 'image', 'thumbnail', __( 'Thumbnail', 'hantus-pro' ) )
									->set_value_type( 'url' ),
							]
						),
					Field::make( 'rich_text', 'ju_custom_section_four_content', __( 'Content' ) ),
				]
			)->add_tab(
				__( 'Video section', 'hantus-pro' ),
				[
					Field::make( 'text', 'ju_video_section_id', __( 'Section ID', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_video_section_class', __( 'Section class', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_video_section_title', __( 'Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'text', 'ju_video_section_sub_title', __( 'Sub Title', 'hantus-pro' ) )
						->set_width( 50 ),
					Field::make( 'checkbox', 'ju_custom_section_demo', __( 'Demo', 'hantus-pro' ) )
						->set_option_value( 'on' ),
					Field::make( 'complex', 'ju_videos', __( 'Videos', 'hantus-pro' ) )
						->add_fields(
							[
								Field::make( 'text', 'url_chrome', __( 'Video url chrome', 'hantus-pro' ) ),
								Field::make( 'text', 'url_mac', __( 'Video url safati', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_hd', __( 'HD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_sd', __( 'SD key', 'hantus-pro' ) ),
								Field::make( 'text', 'url_key_audio', __( 'AUDIO key', 'hantus-pro' ) ),
								Field::make( 'text', 'title', __( 'Title', 'hantus-pro' ) ),
								Field::make( 'textarea', 'description', __( 'Description', 'hantus-pro' ) ),
								Field::make( 'image', 'thumbnail', __( 'Thumbnail', 'hantus-pro' ) )
									->set_value_type( 'url' ),
							]
						),
				]
			);
	}

	/**
	 * Add free course settings page.
	 *
	 * @return void
	 */
	public function add_theme_options(): void {
		Container::make( 'theme_options', __( 'Free Course settings', 'hantus-pro' ) )
			->add_fields(
				[
					Field::make( 'association', 'ju_free_course_settings', __( 'Settings', 'hantus-pro' ) )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'courses',
								],
							]
						),
				]
			);
	}

	/**
	 * Customs HTML before login
	 *
	 * @return void
	 */
	public function add_custom_html_to_course(): void {
		Container::make( 'post_meta', __( 'Customs HTML before login', 'hantus-pro' ) )
			->where( 'post_type', '=', 'courses' )
			->add_fields(
				[
					Field::make( 'rich_text', 'ju_custom_html_to_login_in_course', __( 'Custom html', 'hantus-pro' ) ),
				]
			);
	}


	public function add_custom_videos_settings(): void {
		Container::make( 'post_meta', __( 'Customs HTML before login', 'hantus-pro' ) )
			->where( 'post_type', '=', 'courses' )
			->add_fields(
				[
					Field::make( 'complex', 'ju_course_videos', __( 'Course Videos settings', 'hantus-pro' ) )
						->add_fields(
							[
								Field::make( 'text', 'lessons_name', __( 'Lessons Name', 'hantus-pro' ) ),
								Field::make( 'complex', 'ju_videos', __( 'Videos settings', 'hantus-pro' ) )
									->add_fields(
										[
											Field::make( 'text', 'id', __( 'ID', 'hantus-pro' ) ),
										]
									),
							]
						),
				]
			);
	}
}
