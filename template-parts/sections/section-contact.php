<!-- Start: Contact Us
    ============================= -->

    <section id="contact-area">
        <div id="map"></div>
        <div class="contact-box">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="contact-us-info">
                        <h3>Contact Us</h3>
                        <ul class="contact-info">
                            <li>
                                <i class="icofont icofont-location-pin"></i>
                                <p>South Windsor, CT Build 06074 sarts</p>
                            </li>
                            <li>
                                <i class="icofont icofont-email"></i>
                                <p>amil@company.com
                                    <br> info@domain.com</p>
                            </li>
                            <li>
                                <i class="icofont icofont-ui-call"></i>
                                <p>+1-202-555-0104
                                    <br> +5-402-555-0107</p>
                            </li>
                        </ul>
                        <a href="#" class="live-chat boxed-btn">Live Chat</a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="get-in-touch">
                        <h3>Get in Touch</h3>
                        <p>Feel free to contact with us</p>
                        <form action="index.html" method="post">
                            <input type="text" name="your_name" class="form-control" id="name" placeholder="Your name" required>
                            <input type="email" name="your_mail" class="form-control" id="email" placeholder="Email here" required>
                            <textarea name="your_message" class="form-control" rows="1" placeholder="Type your message" required></textarea>
                            <button type="submit" class="boxed-btn"> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Contact Us
    ============================= -->