<?php
function hantus_breadcrumb_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Breadcrumb Settings Panel
	=========================================*/
	$wp_customize->add_panel( 
		'breadcrumb_setting', 
		array(
			'priority'      => 31,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Page Breadcrumb', 'hantus-pro'),
			'description' 	=> __('This is the Home Page Sectons. Here You can Manage Content of Home Page Sections.','hantus-pro'),
		) 
	);

	/*=========================================
	Background Section
	=========================================*/ 
	$wp_customize->add_section(
        'breadcrumb_design',
        array(
        	'priority'      => 1,
            'title' 		=> __('Settings','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'breadcrumb_setting',
		)
    );
	
	$wp_customize->add_setting( 
		'hide_show_breadcrumb' , 
			array(
			'default' => '1',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_breadcrumb', 
		array(
			'label'	      => esc_html__( 'Breadcrumb Hide/Show', 'hantus-pro' ),
			'section'     => 'breadcrumb_design',
			'settings'    => 'hide_show_breadcrumb',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	//title hide show
	
	$wp_customize->add_setting( 
		'hide_show_breadcrumb_title' , 
			array(
			'default' => '1',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_breadcrumb_title', 
		array(
			'label'	      => esc_html__( 'Title Hide/Show', 'hantus-pro' ),
			'section'     => 'breadcrumb_design',
			'settings'    => 'hide_show_breadcrumb_title',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	//path hide show
	
	$wp_customize->add_setting( 
		'hide_show_breadcrumb_path' , 
			array(
			'default' => __( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_breadcrumb_path', 
		array(
			'label'	      => esc_html__( 'Page Path Hide/Show', 'hantus-pro' ),
			'section'     => 'breadcrumb_design',
			'settings'    => 'hide_show_breadcrumb_path',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	//path seprator
	
	$wp_customize->add_setting(
    	'breadcrumb_separater',
    	array(
	        'default'			=> __('>>','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
		)
	);	
	
	$wp_customize->add_control( 
		'breadcrumb_separater',
		array(
		    'label'   => __('Breadcrumb Seprater','hantus-pro'),
		    'section' => 'breadcrumb_design',
			'settings'   	 => 'breadcrumb_separater',
			'type'           => 'text',
		)  
	);
	
	$wp_customize->add_section(
        'breadcrumb_background',
        array(
        	'priority'      => 2,
            'title' 		=> __('Background','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'breadcrumb_setting',
		)
    );
	
	// Background Image // 
    $wp_customize->add_setting( 
    	'breadcrumb_background_setting' , 
    	array(
			'default' 			=> get_template_directory_uri() .'/assets/images/bg/breadcrumb-bg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',	
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'breadcrumb_background_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'breadcrumb_background',
			'settings'   	 => 'breadcrumb_background_setting',
		) 
	));
	
	$wp_customize->add_setting( 
		'breadcrumb_background_position' , 
			array(
			'default' => __( 'fixed', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
		) 
	);
	
	$wp_customize->add_control(
		'breadcrumb_background_position' , 
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'breadcrumb_background',
				'settings'       => 'breadcrumb_background_position',
				'type'           => 'radio',
				'choices'        => 
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)  
		) 
	);
	
	
	// background opacity// 
	$wp_customize->add_setting( 
		'breadcrumb_opacity' , 
			array(
			'default' => 0.3,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'breadcrumb_opacity', 
		array(
			'section'  => 'breadcrumb_background',
			'settings' => 'breadcrumb_opacity',
			'label'    => __( 'Background Opacity','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 0,
				'max'    => 0.9,
				'step'   => 0.1,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
}

add_action( 'customize_register', 'hantus_breadcrumb_setting' );

// breadcrumb selective refresh
function hantus_home_breadcrumb_section_partials( $wp_customize ){

	// hide show breadcrumb path
	$wp_customize->selective_refresh->add_partial(
		'hide_show_breadcrumb_path', array(
			'selector' => '#breadcrumb-area .breadcrumb-nav',
			'container_inclusive' => true,
			'render_callback' => 'breadcrumb_design',
			'fallback_refresh' => true,
		)
	);
	// hide show breadcrumb title
	$wp_customize->selective_refresh->add_partial(
		'hide_show_breadcrumb_title', array(
			'selector' => '#breadcrumb-area h1',
			'container_inclusive' => true,
			'render_callback' => 'breadcrumb_design',
			'fallback_refresh' => true,
		)
	);
	// hide show breadcrumb
	$wp_customize->selective_refresh->add_partial(
		'hide_show_breadcrumb', array(
			'selector' => '#breadcrumb-area',
			'container_inclusive' => true,
			'render_callback' => 'breadcrumb_design',
			'fallback_refresh' => true,
		)
	);
	// breadcrumb
	$wp_customize->selective_refresh->add_partial( 'breadcrumb_background_setting', array(
		'selector'            => '#breadcrumb-area .breadcrumb-nav',
		'settings'            => 'breadcrumb_background_setting',
		'render_callback'  => 'home_section_breadcrumb_render_callback',
	
	) );
	
	}

add_action( 'customize_register', 'hantus_home_breadcrumb_section_partials' );

// social icons
function home_section_breadcrumb_render_callback() {
	return get_theme_mod( 'breadcrumb_background_setting' );
}