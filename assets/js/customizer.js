/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title, .site-description' ).css( {
					'clip': 'auto',
					'position': 'relative'
				} );
				$( '.site-title, .site-description' ).css( {
					'color': to
				} );
			}
		} );
	} );
	
	$(document).ready(function ($) {
        $('input[data-input-type]').on('input change', function () {
            var val = $(this).val();
            $(this).prev('.cs-range-value').html(val);
            $(this).val(val);
        });
    })
	
	//hantus_timing
	wp.customize(
		'hantus_timing', function( value ) {
			value.bind(
				function( newval ) {
					$( '#header-top p' ).text( newval );
				}
			);
		}
	);
	
	//header_email
	wp.customize(
		'header_email', function( value ) {
			value.bind(
				function( newval ) {
					$( '#header-top .h-t-e' ).text( newval );
				}
			);
		}
	);
	
	//header_phone_number
	wp.customize(
		'header_phone_number', function( value ) {
			value.bind(
				function( newval ) {
					$( '#header-top .h-t-p' ).text( newval );
				}
			);
		}
	);
	
	//info_title
	wp.customize(
		'info_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-first h4, #contact2 .info-first h4' ).text( newval );
				}
			);
		}
	);
	
	//info_description
	wp.customize(
		'info_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-first p, #contact2 .info-first p' ).text( newval );
				}
			);
		}
	);
	
	//info_title2
	wp.customize(
		'info_title2', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-second h4, #contact2 .info-second h4' ).text( newval );
				}
			);
		}
	);
	
	//info_description2
	wp.customize(
		'info_description2', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-second p, #contact2 .info-second p' ).text( newval );
				}
			);
		}
	);
	
	//info_title3
	wp.customize(
		'info_title3', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-third h4, #contact2 .info-third h4' ).text( newval );
				}
			);
		}
	);
	
	//info_description3
	wp.customize(
		'info_description3', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .info-third p, #contact2 .info-third p' ).text( newval );
				}
			);
		}
	);
	
	//service_title
	wp.customize(
		'service_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#services .service-section h2' ).text( newval );
				}
			);
		}
	);
	
	//service_description
	wp.customize(
		'service_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#services .service-section p' ).text( newval );
				}
			);
		}
	);
	
	//portfolio_title
	wp.customize(
		'portfolio_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '.port-section .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//portfolio_description
	wp.customize(
		'portfolio_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '.port-section .section-title p' ).text( newval );
				}
			);
		}
	);
	
	//features_title
	wp.customize(
		'features_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#feature .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//features_description
	wp.customize(
		'features_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#feature .section-title p' ).text( newval );
				}
			);
		}
	);
	//pricing_title
	wp.customize(
		'pricing_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '.price-section .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//pricing_description
	wp.customize(
		'pricing_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '.price-section .section-title p' ).text( newval );
				}
			);
		}
	);
	//product_title
	wp.customize(
		'product_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#product .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//product_description
	wp.customize(
		'product_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#product .section-title p' ).text( newval );
				}
			);
		}
	);
	//team_title
	wp.customize(
		'team_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#beauticians .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//team_description
	wp.customize(
		'team_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#beauticians .section-title p' ).text( newval );
				}
			);
		}
	);
	//appointment_title
	wp.customize(
		'appointment_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#appoinment .opening-hours h3' ).text( newval );
				}
			);
		}
	);
	//appointment_subtitle
	wp.customize(
		'appointment_subtitle', function( value ) {
			value.bind(
				function( newval ) {
					$( '#appoinment .opening-hours p' ).text( newval );
				}
			);
		}
	);
	
	//appointment_description
	wp.customize(
		'appointment_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#appoinment .opening-hours ul' ).text( newval );
				}
			);
		}
	);
	//newsletter_title
	wp.customize(
		'newsletter_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#subscribe h3' ).text( newval );
				}
			);
		}
	);
	//newsletter_description
	wp.customize(
		'newsletter_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#subscribe p' ).text( newval );
				}
			);
		}
	);
	
	//blog_title
	wp.customize(
		'blog_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#blog-content .section-title h2' ).text( newval );
				}
			);
		}
	);
	//blog_description
	wp.customize(
		'blog_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#blog-content .section-title p' ).text( newval );
				}
			);
		}
	);
	
	//custom_section_title
	wp.customize(
		'custom_section_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custom_section .section-title h2' ).text( newval );
				}
			);
		}
	);
	//custom_section_description
	wp.customize(
		'custom_section_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custom_section .section-title p' ).text( newval );
				}
			);
		}
	);
	//copyright_content
	wp.customize(
		'copyright_content', function( value ) {
			value.bind(
				function( newval ) {
					$( '.copyright-text .copy-content a' ).text( newval );
				}
			);
		}
	);
	
	//about_welcome_title
	wp.customize(
		'about_welcome_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#welcome .section-title h3' ).text( newval );
				}
			);
		}
	);
	//about_welcome_subtitle
	wp.customize(
		'about_welcome_subtitle', function( value ) {
			value.bind(
				function( newval ) {
					$( '#welcome .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//about_welcome_description
	wp.customize(
		'about_welcome_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#welcome .section-title p' ).text( newval );
				}
			);
		}
	);
	
	//about_page_contents_title
	wp.customize(
		'about_page_contents_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#wcu  h2#wcu-content-h2' ).text( newval );
				}
			);
		}
	);
	//about_page_content_subtitle
	wp.customize(
		'about_page_content_subtitle', function( value ) {
			value.bind(
				function( newval ) {
					$( '#wcu  p#wcu-content-p' ).text( newval );
				}
			);
		}
	);
	
	//about_page_content_description
	wp.customize(
		'about_page_content_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#wcu .wcu-content ul' ).text( newval );
				}
			);
		}
	);
	
	//about_page_btn_lbl_title
	wp.customize(
		'about_page_btn_lbl_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#wcu .watch-more' ).text( newval );
				}
			);
		}
	);
	
	//contact_page_form_title
	wp.customize(
		'contact_page_form_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .contact-form h2' ).text( newval );
				}
			);
		}
	);
	
	//contact_page_info_title
	wp.customize(
		'contact_page_info_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact .contact-info h2' ).text( newval );
				}
			);
		}
	);
	
	//contact_page_info_desc
	wp.customize(
		'contact_page_info_desc', function( value ) {
			value.bind(
				function( newval ) {
					$( 'p#contct-info-dsc' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_first_t
	wp.customize(
		'cotact_page_address_first_t', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact #info-box-first h4' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_first_a
	wp.customize(
		'cotact_page_address_first_a', function( value ) {
			value.bind(
				function( newval ) {
					$( '#contact #info-box-first p' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_second_t
	wp.customize(
		'cotact_page_address_second_t', function( value ) {
			value.bind(
				function( newval ) {
					$( '#info-box-second h4' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_second_a
	wp.customize(
		'cotact_page_address_second_a', function( value ) {
			value.bind(
				function( newval ) {
					$( '#info-box-second p' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_third_t
	wp.customize(
		'cotact_page_address_third_t', function( value ) {
			value.bind(
				function( newval ) {
					$( '#info-box-third h4' ).text( newval );
				}
			);
		}
	);
	
	//cotact_page_address_third_a
	wp.customize(
		'cotact_page_address_third_a', function( value ) {
			value.bind(
				function( newval ) {
					$( '#info-box-third p' ).text( newval );
				}
			);
		}
	);
	
	//service_page_title
	wp.customize(
		'service_page_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#service_pg .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//service_page_description
	wp.customize(
		'service_page_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#service_pg .section-title p' ).text( newval );
				}
			);
		}
	);
	
	//menu_item_title
	wp.customize(
		'menu_item_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '.pricing-page .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//menu_item_description
	wp.customize(
		'menu_item_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '.pricing-page .section-title p' ).text( newval );
				}
			);
		}
	);
	
	//pricing_pg_title
	wp.customize(
		'pricing_pg_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '.price-pg .section-title h2' ).text( newval );
				}
			);
		}
	);
	
	//pricing_pg_description
	wp.customize(
		'pricing_pg_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '.price-pg .section-title p' ).text( newval );
				}
			);
		}
	);




	//custoa_section_title
	wp.customize(
		'custoa_section_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custoa_section .section-title h2' ).text( newval );
				}
			);
		}
	);
	//custoa_section_description
	wp.customize(
		'custoa_section_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custoa_section .section-title p' ).text( newval );
				}
			);
		}
	);




	//custob_section_title
	wp.customize(
		'custob_section_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custob_section .section-title h2' ).text( newval );
				}
			);
		}
	);
	//custob_section_description
	wp.customize(
		'custob_section_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custob_section .section-title p' ).text( newval );
				}
			);
		}
	);


	//custoc_section_title
	wp.customize(
		'custoc_section_title', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custoc_section .section-title h2' ).text( newval );
				}
			);
		}
	);
	//custoc_section_description
	wp.customize(
		'custoc_section_description', function( value ) {
			value.bind(
				function( newval ) {
					$( '#custoc_section .section-title p' ).text( newval );
				}
			);
		}
	);




} )( jQuery );