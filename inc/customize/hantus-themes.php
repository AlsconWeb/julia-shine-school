<?php
function hantus_select_themes( $wp_customize ) {
	/*=========================================
	Select Theme
	=========================================*/
	$wp_customize->add_section(
	    'select_theme',
	    array(
	    	'priority'      => 1,
	        'title' 		=> __('Select Theme','hantus-pro'),
		)
	);

	$wp_customize->add_setting(
		'select_theme',
		array(
	        'default'			=> 'hantus-theme',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
		)
	);

	$wp_customize->add_control(
		'select_theme',
		array(
		    'label'   		=> __('Select Theme','hantus-pro'),
		    'section' 		=> 'select_theme',
			'type'			=> 'select',
			'choices'       =>
			array(
				'hantus-theme' => __( 'Hantus', 'hantus-pro' ),
				'thaispa-theme' => __( 'Thai Spa', 'hantus-pro' ),
				'cosmics-theme' => __( 'Cosmics', 'hantus-pro' ),
			)
		)
	);
}

add_action( 'customize_register', 'hantus_select_themes' );