<?php
/**
 * WPML and Polylang compatibility functions.
 *
 * @package hantus-pro
 * @since 1.0
 */

/**
 * Filter to translate strings
 */
function hantus_translate_single_string( $original_value, $domain ) {
	if ( is_customize_preview() ) {
		$wpml_translation = $original_value;
	} else {
		$wpml_translation = apply_filters( 'wpml_translate_single_string', $original_value, $domain, $original_value );
		if ( $wpml_translation === $original_value && function_exists( 'pll__' ) ) {
			return pll__( $original_value );
		}
	}
	return $wpml_translation;
}
add_filter( 'hantus_translate_single_string', 'hantus_translate_single_string', 10, 2 );

/**
 * Helper to register pll string.
 *
 * @param String    $theme_mod Theme mod name.
 * @param bool/json $default Default value.
 * @param String    $name Name for polylang backend.
 */
function hantus_pll_string_register_helper( $theme_mod ) {
	if ( ! function_exists( 'pll_register_string' ) ) {
		return;
	}
	
	$repeater_content = get_theme_mod( $theme_mod );
	$repeater_content = json_decode( $repeater_content );
	if ( ! empty( $repeater_content ) ) {
		foreach ( $repeater_content as $repeater_item ) {
			foreach ( $repeater_item as $field_name => $field_value ) {
				if ( $field_value !== 'undefined' ) {
					if ( $field_name !== 'id' ) {
						$f_n = ucfirst( $field_name );
						pll_register_string( $f_n, $field_value);
					}
				}
			}
		}
	}
}

/**
 * Header Social icon section. Register strings for translations.
 *
 * @modified 1.1.30
 * @access public
 */
 
function hantus_header_social_icon_register_strings() {
	hantus_pll_string_register_helper( 'social_icons', 'Header section' );
}

/**
 * Slider section. Register strings for translations.
 */
 function hantus_slider_register_strings() {
	hantus_pll_string_register_helper( 'slider','Slider section' );
}
/**
 * Feature section. Register strings for translations.
 */
function hantus_feature_register_strings() {
	hantus_pll_string_register_helper( 'feature_content', 'Feature section' );
}

/**
 * Service section. Register strings for translations.
 */
function hantus_service_register_strings() {
	hantus_pll_string_register_helper( 'service_contents', 'Service section' );
}

/**
 * Funfact section. Register strings for translations.
 */
function hantus_funfact_register_strings() {
	hantus_pll_string_register_helper( 'funfact_contents', 'Funfact section' );
}

/**
 * Team section. Register strings for translations.
 */
function hantus_team_register_strings() {
	hantus_pll_string_register_helper( 'team_contents', 'Team section' );
}

/**
 * Testimonial section. Register strings for translations.
 */
function hantus_testimonial_register_strings() {
	hantus_pll_string_register_helper( 'testimonial_contents', 'Testimonial section' );
}

/**
 * Appointment section. Register strings for translations.
 */
function hantus_appointment_register_strings() {
	hantus_pll_string_register_helper( 'appoint_time', 'Appointment section' );
}

/**
 * Sponsor section. Register strings for translations.
 */
function hantus_sponsor_register_strings() {
	hantus_pll_string_register_helper( 'sponser_contents', 'Sponsor section' );
}

/**
 * Welcome section. Register strings for translations.
 */
function hantus_welcome_register_strings() {
	hantus_pll_string_register_helper( 'welcome_contents', 'Welcome section' );
}

/**
 * About Funfact section. Register strings for translations.
 */
function hantus_about_funfact_register_strings() {
	hantus_pll_string_register_helper( 'about_page_funfacts_contents', 'About Funfact section' );
}

/**
 * Gallery section. Register strings for translations.
 */
function hantus_gallery_register_strings() {
	hantus_pll_string_register_helper( 'gallery_page_img_setting', 'Gallery section' );
}

/**
 * payment section Register strings for translations.
 */
function hantus_payment_section_register_strings() {
	hantus_pll_string_register_helper( 'footer_Payment_icon', 'Payment section' );
}
/**
 * Page Service section Register strings for translations.
 */
function hantus_page_service_section_register_strings() {
	hantus_pll_string_register_helper( 'service_page_contents', 'Page Service section' );
}
if ( function_exists( 'pll_register_string' ) ) {
	add_action( 'after_setup_theme', 'hantus_header_social_icon_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_slider_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_feature_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_service_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_funfact_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_team_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_testimonial_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_appointment_register_strings', 11 );	
	add_action( 'after_setup_theme', 'hantus_sponsor_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_welcome_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_about_funfact_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_gallery_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_payment_section_register_strings', 11 );
	add_action( 'after_setup_theme', 'hantus_page_service_section_register_strings', 11 );
}


