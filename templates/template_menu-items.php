<?php
/**
Template Name: Menu Item
**/

get_header();
 ?>
<?php 
	$post_type = 'hantus_menu_item';
	$tax = 'menu_item_categories'; 
	$tax_terms = get_terms($tax);
	$menu_item_title = get_theme_mod('menu_item_title','Pricing');
	$menu_item_description = get_theme_mod('menu_item_description','You can judge my work by the portfolio we have done');
	$menu_display_num = get_theme_mod('menu_display_num','9');
	$menu_news_hide_show = get_theme_mod('menu_news_hide_show','1');
?>
<?php if ( function_exists( 'hantus_menu_item' ) ){ ?>
 <section id="portfolio" class="section-padding pricing-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                       <?php if ( ! empty( $menu_item_title ) ) : ?>
							<h2><?php echo esc_attr( $menu_item_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $menu_item_description ) ) : ?>
							<p><?php echo esc_attr( $menu_item_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-tabs">
                    	<?php if (!$tax_terms == 0) { ?>
                        <ul class="portfolio-tab-sorting" role="tablist">
							<?php foreach ($tax_terms as $tax_term) { ?>
								<?php if($tax_term->name == 'ALL'){ ?>
									<li>
										<a href="javascript:void(0)" data-group="<?php echo $tax_term->slug; ?>" class="active"><?php echo $tax_term->name; ?></a>
									</li>
								<?php }else{ ?> 
									<li>
										<a href="javascript:void(0)" data-group="<?php echo $tax_term->slug; ?>" ><?php echo $tax_term->name; ?></a>
									</li>
							<?php } }?>	
                        </ul>
                    	<?php } ?>
                    </div>
                    <div class="row tab-content" id="grid">
					<?php 
						$args = array( 'post_type' => 'hantus_menu_item','posts_per_page' => $menu_display_num);  	
						$menu_item = new WP_Query( $args ); 
						if( $menu_item->have_posts() )
						{
							while ( $menu_item->have_posts() ) : $menu_item->the_post();
					?>
					<?php
						
						$menu_item_description 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_description', true ));
						$menu_item_currency 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_currency', true ));
						$menu_item_price 			= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_price', true ));
						$menu_item_link 			= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_link', true ));
						$menu_item_link_target 		= sanitize_text_field( get_post_meta( get_the_ID(),'menu_item_link_target', true ));
					
					?>
					<?php 	
						if($menu_item_link) { 
							$menu_item_link; 
						}	
						else { 
							$menu_item_link = get_post_permalink(); 
						} 
					?>
					 <?php
						$terms = get_the_terms( $post->ID, 'menu_item_categories' );
											
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$links = array();

							foreach ( $terms as $term ) 
							{
								$links[] = $term->slug;
							}
							
							$tax = join( '","', $links );		
						else :	
							$tax = '';	
						endif;
					?>
                        <div data-groups='["<?php echo strtolower($tax); ?>"]' class="col-md-6 tab-panel">
							<div class="tab-list">
								<?php 
									if ( has_post_thumbnail() ) {
										the_post_thumbnail();
									}
								?>
								<a href="<?php echo esc_url($menu_item_link); ?>" <?php  if($menu_item_link_target) { echo "target='_blank'"; } ?>><h4><?php echo the_title(); ?> <span class="price"><?php echo esc_attr($menu_item_currency); ?><?php echo esc_attr($menu_item_price); ?></span></h4></a>
								<p><?php echo esc_attr($menu_item_description); ?></p>
							</div>
                        </div>
						<?php 	
							endwhile; 
							}
						?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
		}else{
			echo __('Please install & Activate Hantus Feature plugin for add Menu Item','hantus-pro');
			}
?>

    <!-- End: Portfolio Masonary
    ============================= -->


    <!-- Start: Subscribe
    ============================= -->
    <?php if($menu_news_hide_show == '1') {?>
		<?php get_template_part('template-parts/sections/section','newsletter');	 ?>
    <?php } ?>
    <!-- End: Subscribe
    ============================= -->
<?php get_footer(); ?>