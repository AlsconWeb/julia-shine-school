<?php
function hantus_product_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	product Section Panel
	=========================================*/
		$wp_customize->add_section(
			'product_setting', array(
				'title' => esc_html__( 'product Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 35, 'hantus_product' ),
			)
		);
		
	// Setting  Head 
	$wp_customize->add_setting(
		'product_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'product_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'product_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'product_settings' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'product_settings', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'product_setting',
			'settings'    => 'product_settings',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// product Header Section // 
	
	//  Head 
	$wp_customize->add_setting(
		'product_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'product_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'product_setting',
		)
	);
	
	// product Title // 
	$wp_customize->add_setting(
    	'product_title',
    	array(
	        'default'			=> __('Our Product','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'product_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'product_setting',
			'settings'   	 => 'product_title',
			'type'           => 'text',
		)  
	);
	
	// Service Description // 
	$wp_customize->add_setting(
    	'product_description',
    	array(
	        'default'			=> __('We are using only the high quality original product','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'product_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'product_setting',
			'settings'   	 => 'product_description',
			'type'           => 'textarea',
		)  
	);
	
	
	//  Content Head 
	$wp_customize->add_setting(
		'product_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'product_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'product_setting',
		)
	);
	
	// Product speed// 
	$wp_customize->add_setting( 
		'product_animation_speed' , 
			array(
			'default' => '3000',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 9,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'product_animation_speed', 
		array(
			'section'  => 'product_setting',
			'settings' => 'product_animation_speed',
			'label'    => __( 'Speed','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 500,
				'max'    => 10000,
				'step'   => 500,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
	
	// Products// 
	$wp_customize->add_setting( 
		'product_count' , 
			array(
			'default' => '4',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 10,
		) 
	);

	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'product_count', 
		array(
			'section'  => 'product_setting',
			'settings' => 'product_count',
			'label'    => __( 'Count','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 2,
				'max'    => 8,
				'step'   => 1,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
}

add_action( 'customize_register', 'hantus_product_setting' );

// product 
function hantus_home_product_section_partials( $wp_customize ){
	
	// product_settings
	$wp_customize->selective_refresh->add_partial(
		'product_settings', array(
			'selector' => '#product',
			'container_inclusive' => true,
			'render_callback' => 'product_setting',
			'fallback_refresh' => true,
		)
	);
	//title
	$wp_customize->selective_refresh->add_partial( 'product_title', array(
		'selector'            => '#product .section-title h2',
		'settings'            => 'product_title',
		'render_callback'  => 'product_section_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'product_description', array(
		'selector'            => '#product .section-title p',
		'settings'            => 'product_description',
		'render_callback'  => 'product_section_desc_render_callback',
	
	) );
		$wp_customize->selective_refresh->add_partial( 'products_contents', array(
		'selector'            => '.product-carousel',
		'settings'            => 'products_contents',
		'render_callback'  => 'product_section_cont_render_callback',
	
	) );
	}

add_action( 'customize_register', 'hantus_home_product_section_partials' );

// product title
function product_section_title_render_callback() {
	return get_theme_mod( 'product_title' );
}
// product description
function product_section_desc_render_callback() {
	return get_theme_mod( 'product_description' );
}
// product contents
function product_section_cont_render_callback() {
	return get_theme_mod( 'products_contents' );
}