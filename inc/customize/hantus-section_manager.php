<?php // Adding customizer layout manager settings
function hantus_layout_manager_customizer( $wp_customize ) {

	class WP_hantus_layout_Customize_Control extends WP_Customize_Control {

		public $type = 'new_menu';

		public function render_content() {

			$front_page         = get_theme_mod( 'front_page_data', 'Info,Service,Portfolio,Feature,Pricing,Funfact,Product,Testimonial,Team,Appointment,Sponser,Newsletter,Blog,Custom' );
			$data_enable        = explode( ",", $front_page );
			$defaultenableddata = [
				'Info',
				'Service',
				'Portfolio',
				'Feature',
				'Pricing',
				'Funfact',
				'Product',
				'Testimonial',
				'Team',
				'Appointment',
				'Sponser',
				'Newsletter',
				'Blog',
				'Custom',
				'Custom two',
				'Custom three',
				'Custom four',
				'Video'
			];
			$layout_disable     = array_diff( $defaultenableddata, $data_enable );
			?>

			<h3><?php esc_attr_e( 'Enable', 'hantus-pro' ); ?></h3>
			<ul class="sortable customizer_layout" id="enable">
				<?php if ( ! empty( $data_enable[0] ) ) {
					foreach ( $data_enable as $value ) { ?>
						<li class="ui-state" id="<?php echo $value; ?>"><?php echo $value; ?></li>
					<?php }
				} ?>
			</ul>


			<h3><?php esc_attr_e( 'Disable', 'hantus-pro' ); ?></h3>
			<ul class="sortable customizer_layout" id="disable">
				<?php if ( ! empty( $layout_disable ) ) {
					foreach ( $layout_disable as $val ) { ?>
						<li class="ui-state" id="<?php echo $val; ?>"><?php echo $val; ?></li>
					<?php }
				} ?>
			</ul>
			<div class="section">
				<p><b><?php esc_attr_e( 'Slider has fixed position on homepage', 'hantus-pro' ); ?></b></p>
				<p>
					<b><?php esc_attr_e( 'Note', 'hantus-pro' ); ?> </b> <?php esc_attr_e( 'By default, all the sections are enabled on the homepage. If you do not want to display any section just drag that section to the disabled box.', 'hantus-pro' ); ?>
				<p>
			</div>
			<script>
				jQuery( document ).ready( function( $ ) {
					$( '.sortable' ).sortable( {
						connectWith: '.sortable'
					} );
				} );

				jQuery( document ).ready( function( $ ) {

					// Get items id you can chose
					function hantusItems( nayra ) {
						var columns = [];
						$( nayra + ' #enable' ).each( function() {
							columns.push( $( this ).sortable( 'toArray' ).join( ',' ) );
						} );
						return columns.join( '|' );
					}

					function nayraItems_disable( nayra ) {
						var columns = [];
						$( nayra + ' #disable' ).each( function() {
							columns.push( $( this ).sortable( 'toArray' ).join( ',' ) );
						} );
						return columns.join( '|' );
					}

					//onclick check id
					$( '#enable .ui-state,#disable .ui-state' ).mouseleave( function() {
						var enable = hantusItems( '#customize-control-layout_manager' );
						$( '#customize-control-front_page_data input[type = \'text\']' ).val( enable );
						$( '#customize-control-front_page_data input[type = \'text\']' ).change();
					} );

				} );
			</script>
		<?php }
	}

	/* layout manager section */
	$wp_customize->add_section( 'frontpage_layout', [
		'title'    => __( 'Theme Layout Manager', 'hantus-pro' ),
		'priority' => 39,
	] );

	$wp_customize->add_setting(
		'layout_manager',
		[
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',

		]
	);
	$wp_customize->add_control( new WP_hantus_layout_Customize_Control( $wp_customize, 'layout_manager', [
			'section' => 'frontpage_layout',
			'setting' => 'layout_manager',
		] )
	);

	$wp_customize->add_setting(
		'front_page_data',
		[
			'default'           => 'Info,Service,Portfolio,Feature,Pricing,Funfact,Product,Testimonial,Team,Appointment,Newsletter,Sponser,Blog,Custom',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		]
	);
	$wp_customize->add_control( 'front_page_data', [
		'label'   => __( 'Enable', 'hantus-pro' ),
		'section' => 'frontpage_layout',
		'type'    => 'text',
	] );     // enable textbox

}

add_action( 'customize_register', 'hantus_layout_manager_customizer' );
