<?php
function hantus_custoc_code( $wp_ustomizer ) {
$selective_refresh = isset( $wp_ustomizer->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Custoc Section Panel
	=========================================*/
		$wp_ustomizer->add_section(
			'hantus_custoc_setting', array(
				'title' => esc_html__( 'Custoc Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 128, 'hantus_Custoc' ),
			)
		);
		
	// Setting  Head 
	$wp_ustomizer->add_setting(
		'custoc_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_ustomizer->add_control(
	'custoc_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'hantus_custoc_setting',
		)
	);
	
	// Custoc Code Settings Section // 
	$wp_ustomizer->add_setting( 
		'hide_show_custoc_section' , 
			array(
			'default' =>  0,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_ustomizer->add_control( new ustomizerr_Toggle_Control( $wp_ustomizer, 
	'hide_show_custoc_section', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'hantus_custoc_setting',
			'settings'    => 'hide_show_custoc_section',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header Settings Section // 
	
	
	//  Head 
	$wp_ustomizer->add_setting(
		'custoc_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_ustomizer->add_control(
	'custoc_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'hantus_custoc_setting',
		)
	);
	
	// Custoc Section Title // 
	$wp_ustomizer->add_setting(
    	'custoc_section_title',
    	array(
	        'default'			=> __('Title','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_ustomizer->add_control( 
		'custoc_section_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'hantus_custoc_setting',
			'settings' => 'custoc_section_title',
			'type' => 'text',
		)  
	);
	
	// Custoc Section Description // 
	$wp_ustomizer->add_setting(
    	'custoc_section_description',
    	array(
	        'default'			=> __('Description','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_ustomizer->add_control( 
		'custoc_section_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'hantus_custoc_setting',
			'settings' => 'custoc_section_description',
			'type' => 'textarea',
		)  
	);
	
	//  Content Head 
	$wp_ustomizer->add_setting(
		'custoc_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_ustomizer->add_control(
	'custoc_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'hantus_custoc_setting',
		)
	);
	
	// custoc content // 
	
	$page_editor_path = trailingslashit( get_template_directory() ) . 'inc/custoc-controls/editor/ustomizerr-page-editor.php';
		if ( file_exists( $page_editor_path ) ) {
			require_once( $page_editor_path );
		}
	if ( class_exists( 'hantus_Page_Editor' ) ) {
		$frontpage_id = get_option( 'page_on_front' );
		$default = '';
		if ( ! empty( $frontpage_id ) ) {
			$default = get_post_field( 'post_content', $frontpage_id );
		}
		$wp_ustomizer->add_setting(
			'hantus_page_editor', array(
				'default' => __('Custoc Section Description','hantus-pro'),
				'sanitize_callback' => 'wp_kses_post',
				'transport'         => $selective_refresh,
				'priority' => 9,
				
			)
		);

		$wp_ustomizer->add_control(
			new hantus_Page_Editor(
				$wp_ustomizer, 'hantus_page_editor', array(
					'label' => esc_html__( 'Content', 'hantus-pro' ),
					'section' => 'hantus_custoc_setting',
					'priority' => 10,
					'needsync' => true,
				)
			)
		);
	}
	$default = '';
	
	
}
add_action( 'ustomizer_register', 'hantus_custoc_code' );

// custoc section selective refresh
function hantus_custocs_section_partials( $wp_ustomizer ){

	// hide_show_custoc_section
	$wp_ustomizer->selective_refresh->add_partial(
		'hide_show_custoc_section', array(
			'selector' => '#custoc_section',
			'container_inclusive' => true,
			'render_callback' => 'hantus_custoc_setting',
			'fallback_refresh' => true,
		)
	);
	//info  section first
	$wp_ustomizer->selective_refresh->add_partial( 'custoc_section_title', array(
		'selector'            => '#custoc_section .section-title h2',
		'settings'            => 'custoc_section_title',
		'render_callback'  => 'custocs_section_title_render_callback',
	
	) );
	
	$wp_ustomizer->selective_refresh->add_partial( 'custoc_section_description', array(
		'selector'            => '#custoc_section .section-title p',
		'settings'            => 'custoc_section_description',
		'render_callback'  => 'custocs_section_dis_render_callback',
	
	) );
	
	$wp_ustomizer->selective_refresh->add_partial( 'hantus_page_editor', array(
		'selector'            => '#custoc_section .custoc_editor',
		'settings'            => 'hantus_page_editor',
		'render_callback'  => 'custocs_section_editor_render_callback',
	
	) );
	}

add_action( 'ustomizer_register', 'hantus_custocs_section_partials' );

// cta editor
function custocs_section_title_render_callback() {
	return get_theme_mod( 'custoc_section_title' );
}
// cta button label
function custocs_section_dis_render_callback() {
	return get_theme_mod( 'custoc_section_description' );
}

function custocs_section_editor_render_callback() {
	return get_theme_mod( 'hantus_page_editor' );
}