<?php
function hantus_import_files() {
    return array(
        array(
            'import_file_name'             => 'hantus-pro',
            'categories'                   => array( 'Hantus' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/hantus-site.xml',
			
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/hantus-widget.json',
			
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/hantus-settings.dat',
			
            'import_preview_image_url'     => 'http://nayrathemes.com/demo/import/hantus/hantus.jpg',
            'import_notice'                => __( 'Demo Importing process will take some time. Kindly be patience.', 'hantus-pro' ),
        ),
		array(
            'import_file_name'             => 'Thai-spa-pro',
            'categories'                   => array( 'Thai Spa' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/thaispa-site.xml',
			
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/thaispa-widget.json',
			
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/thaispa-settings.dat',
			
            'import_preview_image_url'     => 'http://nayrathemes.com/demo/import/hantus/thai-spa.jpg',
            'import_notice'                => __( 'Demo Importing process will take some time. Kindly be patience.', 'hantus-pro' ),
        ),
		array(
            'import_file_name'             => 'cosmics-pro',
            'categories'                   => array( 'Cosmics' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/cosmics-site.xml',
			
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/cosmics-widget.json',
			
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/inc/demo-import/file/cosmics-settings.dat',
			
            'import_preview_image_url'     => 'http://nayrathemes.com/demo/import/hantus/cosmics.jpg',
            'import_notice'                => __( 'Demo Importing process will take some time. Kindly be patience.', 'hantus-pro' ),
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'hantus_import_files' );


function hantus_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
	$footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary_menu' => $main_menu->term_id,
        )
    );
	
	set_theme_mod( 'nav_menu_locations', array(
            'footer_menu' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'hantus_after_import_setup' );


function hantus_plugin_intro_text( $default_text ) {
    $default_text .= '<div class="ocdi__intro-text"><h4>Do you want to import Premade Demos? Just click on Import button<h4></div>';

    return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'hantus_plugin_intro_text' );


function hantus_plugin_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'themes.php';
    $default_settings['page_title']  = esc_html__( 'One Click Demo Import' , 'hantus-pro' );
    $default_settings['menu_title']  = esc_html__( 'Premade Demos' , 'hantus-pro' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'one-click-demo-import';

    return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'hantus_plugin_page_setup' );