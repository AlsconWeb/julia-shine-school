<?php
function hantus_blog_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';	
	/*=========================================
	Blog Section Panel
	=========================================*/
		$wp_customize->add_section(
			'blog_setting', array(
				'title' => esc_html__( 'Blog Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 128, 'hantus_blog' ),
			)
		);
	/*=========================================
	Blog Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'blog_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'blog_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'blog_setting',
		)
	);
	
	// Blog Settings Section // 
	$wp_customize->add_setting( 
		'hide_show_blog' , 
			array(
			'default' =>  esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_blog', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'blog_setting',
			'settings'    => 'hide_show_blog',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Blog Header Section // 
	
	//  Head 
	$wp_customize->add_setting(
		'blog_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'blog_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'blog_setting',
		)
	);
	
	// Blog Title // 
	$wp_customize->add_setting(
    	'blog_title',
    	array(
	        'default'			=> __('Recent Blog','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'blog_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'blog_setting',
			'settings'   	 => 'blog_title',
			'type'           => 'text',
		)  
	);
	
	// Blog Description // 
	$wp_customize->add_setting(
    	'blog_description',
    	array(
	        'default'			=> __('Publishing packages and web page editors now use Lorem Ipsum as their default model text','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'blog_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'blog_setting',
			'settings'   	 => 'blog_description',
			'type'           => 'textarea',
		)  
	);
	
	// Blog Content Section // 
	
	//  Content Head 
	$wp_customize->add_setting(
		'blog_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 9,
		)
	);

	$wp_customize->add_control(
	'blog_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'blog_setting',
		)
	);
	
	$wp_customize->add_setting(
    'blog_category_id',
		array(
		'capability' => 'edit_theme_options',
		'default' => 1,
		'priority' => 10,
		)
	);	
	$wp_customize->add_control( new Category_Dropdown_Custom_Control( $wp_customize, 
	'blog_category_id', 
		array(
		'label'   => __('Select category for Blog Section','hantus-pro'),
		'section' => 'blog_setting',
		'settings'   => 'blog_category_id',
		) 
	) );
	
	
	// Blog Display Setting // 
	$wp_customize->add_setting(
    	'blog_display_num',
    	array(
	        'default'			=> __('3','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 11,
		)
	);
	
	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'blog_display_num', 
		array(
			'section'  => 'blog_setting',
			'settings' => 'blog_display_num',
			'label'    => __( 'No of Posts Display','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 1,
				'max'    => 500,
				'step'   => 1,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
	
	//no. ofblog display in a column
				$wp_customize->add_setting(
    	'blog_display_col',
    	array(
	        'default'			=> __('4','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 12,
		)
	);	

	$wp_customize->add_control( 
		'blog_display_col',
		array(
		    'label'   		=> __('Blog Column','hantus-pro'),
		    'section' 		=> 'blog_setting',
			'settings'   	 => 'blog_display_col',
			'description'   => __( 'Select column for Blog', 'hantus-pro' ),
			'type'			=> 'select',
			'choices'        => 
			array(
				'6' 	=> __( '2 Column', 'hantus-pro' ),
				'4' 	=> __( '3 Column', 'hantus-pro' ),
				'3' 	=> __( '4 Column', 'hantus-pro' ),
			)  
		) 
	);
}
add_action( 'customize_register', 'hantus_blog_setting' );

// Blog selective refresh
function hantus_home_blog_section_partials( $wp_customize ){

	// hide show blog
	$wp_customize->selective_refresh->add_partial(
		'hide_show_blog', array(
			'selector' => '#blog-content',
			'container_inclusive' => true,
			'render_callback' => 'blog_setting',
			'fallback_refresh' => true,
		)
	);
	// title
	$wp_customize->selective_refresh->add_partial( 'blog_title', array(
		'selector'            => '#blog-content .section-title h2',
		'settings'            => 'blog_title',
		'render_callback'  => 'home_section_blog_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'blog_description', array(
		'selector'            => '#blog-content .section-title p',
		'settings'            => 'blog_description',
		'render_callback'  => 'home_section_blog_desc_render_callback',
	
	) );
	// blog_read_more
	$wp_customize->selective_refresh->add_partial( 'blog_read_more', array(
		'selector'            => '#blog-content .read-more',
		'settings'            => 'blog_read_more',
		'render_callback'  => 'home_section_blog_read_more_render_callback',
	
	) );
	
	
	}

add_action( 'customize_register', 'hantus_home_blog_section_partials' );

// title
function home_section_blog_title_render_callback() {
	return get_theme_mod( 'blog_title' );
}
// description
function home_section_blog_desc_render_callback() {
	return get_theme_mod( 'blog_description' );
}
// blog_read_more
function home_section_blog_read_more_render_callback() {
	return get_theme_mod( 'blog_read_more' );
}
