<?php
/**
 * Geo position from user ip.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme\API;

/**
 * GeoFromIP class file.
 */
class GeoFromIP {
	/**
	 * Base API URL.
	 */
	const BASE_API_URL = 'https://pro.ip-api.com/json/';

	const BLACK_LIST_COUNTRY = [
		'India',
		'Mexico',
	];

	/**
	 * User ip.
	 *
	 * @var string
	 */
	private $user_ip = '';

	/**
	 * GeoFromIP construct.
	 */
	public function __construct( $user_ip ) {
		$this->user_ip = $user_ip;

		if ( WP_DEBUG ) {
			$this->user_ip = '64.227.148.160'; // indian
//			$this->user_ip = '216.238.73.164'; // mex
//			$this->user_ip = '188.146.116.135'; // Real

		}
	}

	/**
	 * Get user data.
	 *
	 * @return array|\stdClass
	 */
	public function get_user_data() {
		$api_key = '';
		if ( defined( 'IP_GEO_API_KEY' ) ) {
			$api_key = IP_GEO_API_KEY;
		}

		$response = wp_remote_post(
			self::BASE_API_URL . $this->user_ip . '?key=' . $api_key,
			[
				'method'  => 'GET',
				'timeout' => 15,
			]
		);

		$body = json_decode( $response['body'] );

		if ( 'success' === $body->status ) {
			return $body;
		}

		return [];
	}

	/**
	 * In black list country.
	 *
	 * @return bool
	 */
	public function in_black_list_country(): bool {
		$option_name = 'is_black_list_' . str_replace( '.', '-', $this->user_ip );
		$is_black    = get_transient( $option_name );
		$api_key     = '';

		if ( defined( 'IP_GEO_API_KEY' ) ) {
			$api_key = IP_GEO_API_KEY;
		}

		if ( ! empty( $is_black ) ) {
			return $is_black;
		}

		$url = self::BASE_API_URL . $this->user_ip . '?key=' . $api_key;

		$response = wp_remote_post(
			$url,
			[
				'method'  => 'GET',
				'timeout' => 15,
			]
		);

		$body = json_decode( $response['body'] );

		if ( 'success' === $body->status && in_array( $body->country, self::BLACK_LIST_COUNTRY ) ) {
			set_transient( $option_name, true, DAY_IN_SECONDS );

			return true;
		}

		return false;
	}
}
