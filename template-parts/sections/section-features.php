<?php
/**
 * Get default values for features section.
 *
 * @since 1.0
 * @access public
 */		
?>

<?php 
	$default_content 				= hantus_get_feature_default();
	$hide_show_feature				= get_theme_mod('hide_show_feature','1'); 
	$features_title					= get_theme_mod('features_title','Features');
	$features_description			= get_theme_mod('features_description','How to Have a Healthier and More Productive Home Office');
	$feature_content				= get_theme_mod('feature_content',$default_content);
	$feature_columns				= get_theme_mod('feature_columns','3');
	$features_background_setting	= get_theme_mod('features_background_setting',get_template_directory_uri() .'/assets/images/bg/feature-bg.jpg');
	$features_background_position	= get_theme_mod('features_background_position','scroll');
	 if($hide_show_feature == '1') { 
	 hantus_before_feature_section_trigger();
?>	
<?php if ( ! empty( $features_background_setting ) ) { ?>
<section id="feature" class="section-padding" style="background:url('<?php echo esc_url($features_background_setting); ?>') no-repeat center <?php echo esc_attr($features_background_position); ?>">
<?php }else{ ?>
<section id="feature" class="section-padding" style="background:#f7f7f7;">
<?php } ?>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title">
						<?php if ( ! empty( $features_title ) || is_customize_preview() ) : ?>
							<h2><?php echo $features_title; ?></h2>
						<?php endif; ?>
						<?php if($features_description) {?>
							<p><?php echo $features_description; ?></p>
						<?php } ?>
                    </div>
                </div>
            </div>
            <div class="row" id="featurec">
				<?php
					if ( ! empty( $feature_content ) ) {
					$allowed_html = array(
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array(),
					);
					$feature_content = json_decode( $feature_content );
					foreach ( $feature_content as $service_item ) {
						$title = ! empty( $service_item->title ) ? apply_filters( 'hantus_translate_single_string', $service_item->title, 'features section' ) : '';
						$text = ! empty( $service_item->text ) ? apply_filters( 'hantus_translate_single_string', $service_item->text, 'features section' ) : '';
						$link = ! empty( $service_item->link ) ? apply_filters( 'hantus_translate_single_string', $service_item->link, 'features section' ) : '';
						$image = ! empty( $service_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $service_item->image_url, 'features section' ) : '';
				?>
                <div class="col-lg-<?php echo $feature_columns; ?> col-md-6 col-sm-6 mb-4 feature-col">                    
                    <div class="feature-box text-center">                        
                        <div class="feature-icon">
							<?php if ( ! empty( $image ) ) : ?>
								<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
							<?php endif; ?>
                        </div>
						<?php if ( ! empty( $title ) ) :?>
							<a href="<?php echo esc_url($link);?>"><h4><?php echo esc_attr( $title ); ?></h4></a>
						<?php endif; ?>
						<?php if ( ! empty( $text ) ) :?>
							<p><?php echo esc_attr( $text ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
					<?php }} ?>	
            </div>
        </div>
		</section>
    </section>

<?php hantus_after_feature_section_trigger(); } ?>