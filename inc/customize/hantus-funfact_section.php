<?php
function hantus_funfact_setting( $wp_customize ) {

$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	funfact Section Panel
	=========================================*/
		$wp_customize->add_section(
			'Funfact_setting', array(
				'title' => esc_html__( 'Funfact Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 32, 'hantus_Funfact' ),
			)
		);
	/*=========================================
	Funfact Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'funfact_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'funfact_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'Funfact_setting',
		)
	);
	
	// Slider Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_funfact' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_funfact', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'Funfact_setting',
			'settings'    => 'hide_show_funfact',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// funfact content Section // 
	
	// Content Head 
	$wp_customize->add_setting(
		'funfact_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'funfact_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'Funfact_setting',
		)
	);
	
	/**
	 * Customizer Repeater for add funfact
	 */
		$wp_customize->add_setting( 'funfact_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			 'transport'         => $selective_refresh,
			   'default' =>hantus_get_funfact_default(),
			   'priority' => 6,
			)
		);
		
		$wp_customize->add_control( 
			new Hantus_Repeater( $wp_customize, 
				'funfact_contents', 
					array(
						'label'   => esc_html__('Funfact','hantus-pro'),
						'section' => 'Funfact_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Funfact', 'hantus-pro' ),
						'customizer_repeater_icon_control' => true,
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_subtitle_control' => true,
						'customizer_repeater_text_control' => true,
					) 
				) 
			);
	
	// Funfact Background Section // 
	
	// BG Head 
	$wp_customize->add_setting(
		'funfact_bg_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'funfact_bg_head',
		array(
			'type' => 'hidden',
			'label' => __('Background','hantus-pro'),
			'section' => 'Funfact_setting',
		)
	);
	
	// Background Image // 
    $wp_customize->add_setting( 
    	'funfact_background_setting' , 
    	array(
			'default' 			=> get_template_directory_uri() . '/assets/images/bg/counter-bg.jpg',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_url',
			'priority' => 9,			
		) 
	);
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize , 'funfact_background_setting' ,
		array(
			'label'          => __( 'Background Image', 'hantus-pro' ),
			'section'        => 'Funfact_setting',
			'settings'   	 => 'funfact_background_setting',
		) 
	));

	$wp_customize->add_setting( 
		'funfact_background_position' , 
			array(
			'default' => __( 'fixed', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 10,
		) 
	);
	
	$wp_customize->add_control(
		'funfact_background_position' , 
			array(
				'label'          => __( 'Image Position', 'hantus-pro' ),
				'section'        => 'Funfact_setting',
				'settings'       => 'funfact_background_position',
				'type'           => 'radio',
				'choices'        => 
				array(
					'fixed'=> __( 'Fixed', 'hantus-pro' ),
					'scroll' => __( 'Scroll', 'hantus-pro' )
			)  
		) 
	);
	
}
add_action( 'customize_register', 'hantus_funfact_setting' );

// funfact selective refresh
function hantus_home_funfact_section_partials( $wp_customize ){
	// hide_show_funfact
	$wp_customize->selective_refresh->add_partial(
		'hide_show_funfact', array(
			'selector' => '#counter',
			'container_inclusive' => true,
			'render_callback' => 'Funfact_setting',
			'fallback_refresh' => true,
		)
	);
	// funfact
	$wp_customize->selective_refresh->add_partial( 'funfact_contents', array(
		'selector'            => '#counter .text-center',	
	) );
	
	}

add_action( 'customize_register', 'hantus_home_funfact_section_partials' );