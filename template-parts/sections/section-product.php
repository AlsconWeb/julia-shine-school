<?php
/**
 * Get default values for features section.
 *
 * @since 1.1.31
 * @access public
 */
?>
<?php

	$product_settings 			= get_theme_mod('product_settings','1');
	$product_title 				= get_theme_mod('product_title','Our Product');
	$product_description 		= get_theme_mod('product_description','We are using only the high quality original product');
	$product_animation_speed	= get_theme_mod('product_animation_speed','3000');
	$product_count				= get_theme_mod('product_count','4');
	$settings=array('animationSpeed'=>$product_animation_speed,'items'=>$product_count);
	wp_register_script('hantus-product',get_template_directory_uri().'/assets/js/homepage/product.js',array('jquery'));
	wp_localize_script('hantus-product','product_settings',$settings);
	wp_enqueue_script('hantus-product');

 if($product_settings == '1') {
 hantus_before_product_section_trigger();
?>
    <section id="product" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <?php if ( ! empty( $product_title ) ) : ?>
							<h2><?php echo esc_attr( $product_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $product_description ) ) : ?>
							<p><?php echo esc_attr( $product_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>
			<?php
			if ( class_exists( 'woocommerce' ) ) {
				$args = [
					'post_type' => 'product',
					'tax_query' => [
						[
							'taxonomy' => 'product_cat',
							'field'    => 'id',
							'terms'    => [ 20 ],
						],
					],
				];
					?>
					<div class="row">
                <div class="col-md-12">
                    <div class="product-carousel">
					<?php
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        <div class="shop-product text-center" data-profile="<?php echo $loop->post->ID; ?>">
					
							<a
									href="#"
									class="modal-btn"
									data-title="<?php echo wp_strip_all_tags( get_the_title( get_the_ID() ) ); ?>"
									data-image_src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium') ?>"
									data-description="<?php echo wp_strip_all_tags( get_the_content( get_the_ID() ) ); ?>"
							>
								<?php if ( $product->is_on_sale() ) : ?>
									<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="sale">' . esc_html__( 'Sale!', 'hantus-pro' ) . '</span>', $post, $product ); ?>
								<?php endif; ?>
							<div class="d-flex flex-column flex-lg-row align-items-center justify-content-around">
								<div class="product-img">
									<?php the_post_thumbnail(); ?>
								</div>
									<ul class="rate">
										<li>
											<?php if ($average = $product->get_average_rating()) : ?>
											<?php echo '<i class="fa fa-star" title="'.sprintf(__( 'Rated %s out of 5', 'hantus-pro' ), $average).'"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'hantus-pro' ).'</span></i>'; ?>
											<?php endif; ?>
										</li>
									</ul>
									<div class='shop-product_item'>
										<h5><?php echo the_title(); ?></h5>
										<div class="price"><?php echo $product->get_price_html(); ?></div>
									</div>
							</div>
							</a>
                        </div>
						<?php endwhile; ?>
						<?php  wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
			<?php  } ?>
        </div>
    </section>
<div class="clearfix"></div>
	<?php
	 hantus_after_product_section_trigger();
 }
