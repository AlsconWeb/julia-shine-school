<?php
/**
 * Contact form 7
 * Add to email user info.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use Hantus\Theme\API\GeoFromIP;
use WPCF7_ContactForm;

/**
 * ContactFormSeven class file.
 */
class ContactFormSeven {
	/**
	 * ContactFormSeven construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_filter( 'wpcf7_mail_components', [ $this, 'add_user_info' ], 10, 2 );
	}

	/**
	 * Add user info in email.
	 *
	 * @param array $mail Email array.
	 * @param $form
	 *
	 * @return mixed
	 */
	public function add_user_info( array $mail, $from ) {
		$user_ip    = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : null;
		$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! empty( $user_ip ) ) {
			$geo_api = new GeoFromIP( $user_ip );

			$result = $geo_api->get_user_data();

			if ( ! empty( $result ) ) {
				$info = '<p> IP: ' . $user_ip . '<br>' . 'User agent: ' . $user_agent . '<br>' . 'Country: ' . $result->country . '<br>' . 'Region: ' . $result->regionName . '<br>' . 'City: ' . $result->city . '</p>';

				$mail['body'] = str_replace( '{{user_info}}', $info, $mail['body'] );
			}
		}

		return $mail;
	}
}
