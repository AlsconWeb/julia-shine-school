<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */
$portfolio_descriptions 		= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_description', true ));
$portfolio_date 				= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_date', true ));
$portfolio_price 				= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_price', true ));
$portfolio_link 				= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link', true ));
$portfolio_button_link_target 	= sanitize_text_field( get_post_meta( get_the_ID(),'portfolio_button_link_target', true ));

if($portfolio_link) { 
	$portfolio_link; 
}	
else { 
	$portfolio_link = get_post_permalink(); 
} 
?>
<figure>
	<?php 
		if ( has_post_thumbnail() ) {
			the_post_thumbnail();
		}
	?>	
	<figcaption>
		<div class="inner-text">
			<h4><?php echo the_title(); ?></h4>
			<h6><?php echo esc_attr($portfolio_date); ?></h6>
			<p><?php echo esc_attr($portfolio_descriptions); ?></p>
			<a href="<?php echo esc_url($portfolio_link); ?>" <?php  if($portfolio_button_link_target) { echo "target='_blank'"; }  ?>><?php echo esc_attr($portfolio_price); ?></a>
		</div>
	</figcaption>
</figure>