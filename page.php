<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hantus
 */

get_header();
?>

<section class="section-padding">
	<div class="container">
					
		<div class="row padding-top-60 padding-bottom-60">		
			<?php 
				if ( class_exists( 'woocommerce' ) ) 
				{
					
					if( is_account_page() || is_cart() || is_checkout() ) {
							echo '<div class="col-md-'.( !is_active_sidebar( "woocommerce-1" ) ?"12" :"8" ).'">'; 
					}
					else{ 
				
					echo '<div class="col-md-'.( !is_active_sidebar( "hantus-sidebar-primary" ) ?"12" :"8" ).'">'; 
					
					}
					
				}
				else
				{ 
				
					echo '<div class="col-md-'.( !is_active_sidebar( "hantus-sidebar-primary" ) ?"12" :"8" ).'">';
					
					
				} 
			?>
			<div class="site-content">
			
				<?php 
					
					if( have_posts()) :  the_post();
					
					the_content(); 
					endif;
					
					if( $post->comment_status == 'open' ) { 
						comments_template( '', true ); // show comments
					}
				?>
				

			</div><!-- /.posts -->
							
			</div><!-- /.col -->
			

			<?php 
				if ( class_exists( 'woocommerce' ) ) 
					{
						
						if( is_account_page() || is_cart() || is_checkout() ) {
							?>
							<div class="col-md-4">
								<div class="woo-sidebar">
									<?php dynamic_sidebar( 'woocommerce-1' ); ?>
								</div>
							</div>
								
								<?php
						}
						else{ 
					
						?>
						<?php get_sidebar(); ?>	
				<?php
						}
						
					}
				else
					{ 
					?>
						<?php get_sidebar(); ?>	
				<?php		
					} 
			?>
			
						
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>

