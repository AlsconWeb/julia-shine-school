<?php
function hantus_team_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';	
	/*=========================================
	Client Section Panel
	=========================================*/
		$wp_customize->add_section(
			'team_setting', array(
				'title' => esc_html__( 'Team Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 40, 'hantus_Team' ),
			)
		);
	/*=========================================
	Team Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'team_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'team_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'team_setting',
		)
	);
	
	// Team Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_team' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_team', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'team_setting',
			'settings'    => 'hide_show_team',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// Team Header Section // 
	
	//  Head 
	$wp_customize->add_setting(
		'team_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'team_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'team_setting',
		)
	);
	
	// team Title // 
	$wp_customize->add_setting(
    	'team_title',
    	array(
	        'default'			=> __('Expert Beauticians','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'team_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'team_setting',
			'settings'=> 'team_title',
			'type'    => 'text',
		)  
	);
	
	// Team Description // 
	$wp_customize->add_setting(
    	'team_description',
    	array(
	        'default'			=> __('These are the people behind our success and failures. These guys never lose a heart.','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'team_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'team_setting',
			'settings'   	 => 'team_description',
			'type'           => 'textarea',
		)  
	);
	
	
	// Team Content Section // 
	
	
	// Content Head 
	$wp_customize->add_setting(
		'team_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 11,
		)
	);

	$wp_customize->add_control(
	'team_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'team_setting',
		)
	);
	
	/**
	 * Customizer Repeater for add team
	 */
		$wp_customize->add_setting( 'team_contents', 
			array(
			 'sanitize_callback' => 'hantus_repeater_sanitize',
			  'default' => hantus_get_team_default(),
			  'priority' => 12,
			)
		);
		$wp_customize->add_control( 
			new hantus_Repeater( $wp_customize, 
				'team_contents', 
					array(
						'label'   => esc_html__('Team','hantus-pro'),
						'section' => 'team_setting',
						'add_field_label'                   => esc_html__( 'Add New', 'hantus-pro' ),
						'item_name'                         => esc_html__( 'Team', 'hantus-pro' ),
						'customizer_repeater_image_control' => true,
						'customizer_repeater_title_control' => true,
						'customizer_repeater_designation_control' => true,
						'customizer_repeater_text_control' => true,
						'customizer_repeater_repeater_control' => true,
						'customizer_repeater_new_control' => true,
						
				
					) 
				) 
			);
	$wp_customize->add_setting( 
		'butician_sec_columns' , 
			array(
			'default' => __('3', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_select',
			'priority' => 13,
		) 
	);

	$wp_customize->add_control(
	'butician_sec_columns' , 
		array(
			'label'          => __( 'Team Column', 'hantus-pro' ),
			'section'        => 'team_setting',
			'settings'   	 => 'butician_sec_columns',
			'type'			=> 'select',
			'choices'        => 
			array(
				'6'		=>__('2 column', 'hantus-pro'),
				'4'=>__('3 column', 'hantus-pro'),
				'3'=>__('4 column', 'hantus-pro'),
				
			) 
		)
	);
}
add_action( 'customize_register', 'hantus_team_setting' );

// Team selective refresh
function hantus_home_team_section_partials( $wp_customize ){

		// hide_show_team
	$wp_customize->selective_refresh->add_partial(
		'hide_show_team', array(
			'selector' => '#beauticians',
			'container_inclusive' => true,
			'render_callback' => 'team_setting',
			'fallback_refresh' => true,
		)
	);
	// title
	$wp_customize->selective_refresh->add_partial( 'team_title', array(
		'selector'            => '#beauticians .section-title h2',
		'settings'            => 'team_title',
		'render_callback'  => 'home_section_team_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'team_description', array(
		'selector'            => '#beauticians .section-title p',
		'settings'            => 'team_description',
		'render_callback'  => 'home_section_team_desc_render_callback',
	
	) );
	// contents
	$wp_customize->selective_refresh->add_partial( 'team_contents', array(
		'selector'            => '#team-contents',
		'settings'            => 'team_contents',
		'render_callback'  => 'home_section_team_contents_render_callback',
	) );
	
	}

add_action( 'customize_register', 'hantus_home_team_section_partials' );

// title
function home_section_team_title_render_callback() {
	return get_theme_mod( 'team_title' );
}
// description
function home_section_team_desc_render_callback() {
	return get_theme_mod( 'team_description' );
}
// contents
function home_section_team_contents_render_callback() {
	return get_theme_mod( 'team_contents' );
}