<!-- Start: Services
============================= -->
<?php

use Hantus\Theme\Helpers\FrontEndHelpers;

$hide_show_custom_section   = get_theme_mod( 'hide_show_custom_section' );
$custom_section_title       = carbon_get_the_post_meta( 'ju_custom_section_one_title' );
$custom_section_description = carbon_get_the_post_meta( 'ju_custom_section_one_description' );
$hantus_page_editor         = carbon_get_the_post_meta( 'ju_custom_section_one_content' );
$videos                     = carbon_get_the_post_meta( 'ju_videos_single_section_one' );
$section_id                 = carbon_get_the_post_meta( 'ju_custom_section_one_id' );
$section_class              = carbon_get_the_post_meta( 'ju_custom_section_one_class' ) ?? '';
$demo                       = carbon_get_the_post_meta( 'ju_custom_section_one_demo' ) ?? '';

if ( $hide_show_custom_section == '1' ) {
	?>
	<section
			id="<?php echo esc_attr( $section_id ) ?? ''; ?>"
			class="section-padding <?php echo esc_attr( $section_class ); ?>">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<div class="section-title service-section">
						<?php if ( $custom_section_title ) { ?>
							<h2><?php echo esc_attr( $custom_section_title ); ?></h2>
						<?php } ?>
						<?php if ( $custom_section_description ) { ?>
							<p><?php echo $custom_section_description; ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-lg-12 col-md-12 col-sm-12 col-12 mb-lg-4 mb-4 custom_editor">
					<?php
					$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

					$is_browser = preg_match( '/((Win|Macintosh).*?(Chrome|Edg|Trident|Safari))(?!.*OPR)/i', $user_agent );

					if ( ! empty( $videos ) ) {
						if ( $is_browser || $demo ) {
							foreach ( $videos as $key => $video ) {
								$keys = [
									$video['url_key_hd'],
									$video['url_key_sd'],
									$video['url_key_audio'],
								];
								?>
								<div class="video-block">
									<video
											style="height: auto; width: 100%;"
											id="video-shaka-<?php echo FrontEndHelpers::generate_unic_id(); ?>"
											data-source="<?php echo esc_url( $video['url_chrome'] ?? '' ); ?>"
											data-source_safari="<?php echo esc_url( $video['url_mac'] ?? '' ); ?>"
											data-uid="<?php echo esc_attr( get_current_user_id() ); ?>"
											data-keys="<?php echo base64_encode( json_encode( $keys ) ); ?>"
											poster="<?php echo esc_url( $video['thumbnail'] ?? '' ); ?>"
											controls></video>
									<h4 class="title"><?php echo esc_html( $video['title'] ); ?></h4>
									<div class="description">
										<?php echo wp_kses_post( wpautop( $video['description'] ) ); ?>
									</div>
								</div>
								<?php
							}
						} else {
							?>
							<div class="warning-wrapper">
								<h3>
									<?php esc_html_e( 'Your browser does not support this video.', 'hantus-pro' ); ?>
								</h3>
								<p>
									<?php esc_html_e( 'You can view this lecture in the following browsers:', 'hantus-pro' ); ?>
								</p>
								<p><b><?php esc_html_e( 'Windows:', 'hantus-pro' ); ?></b></p>
								<ul>
									<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
									<li><?php esc_html_e( 'Internet Explorer', 'hantus-pro' ); ?></li>
									<li><?php esc_html_e( 'Edge', 'hantus-pro' ); ?></li>
								</ul>
								<p><b><?php esc_html_e( 'Mac:' ); ?></b></p>
								<ul>
									<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
									<li><?php esc_html_e( 'Safari', 'hantus-pro' ); ?></li>
								</ul>
							</div>
							<?php
						}
					}
					echo do_shortcode( $hantus_page_editor );
					?>
				</div>
			</div>

		</div>
	</section>
<?php } ?>
<!-- End: Services
============================= -->
