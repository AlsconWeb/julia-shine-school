<?php
/**
 * Add free course after registration.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use TUTOR\Utils;

/**
 * AddFreeCourseAfterRegistration class file.
 */
class AddFreeCourseAfterRegistration {
	/**
	 * AddFreeCourseAfterRegistration construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'user_register', [ $this, 'add_free_course' ] );
	}

	/**
	 * Add free course.
	 *
	 * @param int $user_id User id.
	 *
	 * @return void
	 */
	public function add_free_course( int $user_id ): void {
		$free_courses = carbon_get_theme_option( 'ju_free_course_settings' );

		if ( empty( $user_id ) || empty( $free_courses ) ) {
			return;
		}

		$tuto_utils = new Utils();

		foreach ( $free_courses as $free_course ) {
			$tuto_utils->do_enroll( $free_course['id'], 0, $user_id );
		}
	}
}
