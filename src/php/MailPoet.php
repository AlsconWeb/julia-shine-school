<?php
/**
 * MailPoet actions and filter.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use Hantus\Theme\API\GeoFromIP;

/**
 * MailPoet class file.
 */
class MailPoet {

	/**
	 * MailPoet construct
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'mailpoet_subscription_before_subscribe', [ $this, 'add_user_info' ], 20, 3 );

		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts_localize' ], 40 );
	}

	/**
	 * @param array $data       Form data.
	 * @param array $segmentIds list id.
	 * @param       $form
	 *
	 * @return void
	 */
	public function add_user_info( $data, $segmentIds, $form ) {
		if ( 'Subscribe' === $form->getName() ) {
			$user_ip    = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : null;
			$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

			if ( ! empty( $user_ip ) ) {
				$geo_api = new GeoFromIP( $user_ip );
				$result  = $geo_api->get_user_data();
			}

			ob_start();
			?>
			<p><b><?php esc_attr_e( 'Email:', 'hantus-pro' ); ?></b> <?php echo esc_html( $data['email'] ); ?></p>
			<p><b><?php esc_attr_e( 'Name:', 'hantus-pro' ); ?></b> <?php echo esc_html( $data['first_name'] ); ?></p>
			<p><b><?php esc_attr_e( 'Phone:', 'hantus-pro' ); ?></b> <?php echo esc_html( $data['cf_1'] ); ?></p>
			<p><b><?php esc_attr_e( 'Country:', 'hantus-pro' ); ?></b> <?php echo esc_html( $data['cf_2'] ); ?></p>
			<p><b><?php esc_attr_e( 'Language:', 'hantus-pro' ); ?></b> <?php echo esc_html( $data['cf_3'] ); ?></p>
			<p><b><?php esc_attr_e( 'IP:', 'hantus-pro' ); ?></b> <?php echo esc_html( $user_ip ); ?></p>
			<p><b><?php esc_attr_e( 'User Agent:', 'hantus-pro' ); ?></b> <?php echo esc_html( $user_agent ); ?></p>
			<?php if ( ! empty( $result ) ) { ?>
				<p>
					<b><?php esc_attr_e( 'Country from IP:', 'hantus-pro' ); ?></b>
					<?php echo esc_html( $result->country ); ?>
				</p>
				<p>
					<b><?php esc_attr_e( 'Region:', 'hantus-pro' ); ?></b>
					<?php echo esc_html( $result->regionName ); ?>
				</p>
				<p>
					<b><?php esc_attr_e( 'City:', 'hantus-pro' ); ?></b>
					<?php echo esc_html( $result->city ); ?>
				</p>
				<?php
			}

			$mail_body = ob_get_clean();
			wp_mail(
				get_option( 'admin_email' ),
				__( 'New Subscriber', 'hantus-pro' ),
				$mail_body,
				[
					'From: julia-shine-school.com <noreplace@julia-shine-school.com>',
					'content-type: text/html',
				]
			);
		}
	}

	/**
	 * Localize user info.
	 *
	 * @return void
	 */
	public function add_scripts_localize(): void {
		$user_ip    = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : null;
		$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$option_name = 'ju_user_info_' . str_replace( '.', '-', $user_ip );

		$user_info_transient = get_transient( $option_name );

		if ( ! empty( $user_ip ) && empty( $user_info_transient ) ) {
			$api_geo = new GeoFromIP( $user_ip );
			$result  = $api_geo->get_user_data();

			if ( empty( $result ) ) {
				return;
			}

			$user_info =
				[
					'userIP'    => $user_ip,
					'userAgent' => $user_agent,
					'country'   => $result->country,
					'region'    => $result->regionName,
					'city'      => $result->city,
				];

			set_transient( $option_name, $user_info, DAY_IN_SECONDS );
		}

		wp_localize_script(
			'ju_main',
			'juUserInfo',
			! empty( $user_info_transient ) ? $user_info_transient : $user_info
		);
	}
}
