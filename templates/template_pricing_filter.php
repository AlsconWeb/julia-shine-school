<?php
/**
Template Name: Pricing Filter Page
**/

get_header();
?>
	<?php 
		$Pricing_newsletter_hide_show= get_theme_mod('Pricing_newsletter_hide_show','1'); 
		$Pricing_price_hide_show= get_theme_mod('Pricing_price_hide_show','1');
		$pricing_pg_title= get_theme_mod('pricing_pg_title','Pricing');
		$pricing_pg_description= get_theme_mod('pricing_pg_description','You can select a package from the list below to save more');
		$post_type = 'hantus_pricing';
		$tax = 'pricing_categories'; 
		$tax_terms = get_terms($tax);
	?>

	<?php 	
		if($Pricing_price_hide_show == '1' ) {
		hantus_before_pricing_page_section_trigger();	
	?>
	<section id="pricing" class="custom-padding price-pg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
						<?php if( ($pricing_pg_title) || ($pricing_pg_description)!='' ) { ?>
							<h2><?php echo esc_html($pricing_pg_title); ?></h2>
							<p><?php echo esc_html($pricing_pg_description); ?></p>
						<?php } ?>
                    </div>
                </div>
            </div>
			<?php if ( function_exists( 'hantus_pricing' ) ){ ?>
			<div class="row portfolio-tab">
                <div class="col-md-12 text-center">
                    <?php if (!$tax_terms == 0) { ?>
                    <ul class="portfolio-tab-sorting sorting-btn" id="filter">
                        <?php	foreach ($tax_terms as $tax_term) { ?>
							<?php if($tax_term->name == 'All'){ ?>
								<li><a href="#" class="active" data-group="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a></li>
							 <?php }else{ ?>
								<li><a href="#" data-group="<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a></li>
						<?php } } ?>
                    </ul>
                	<?php } ?>
                </div>
            </div>
            <div class="row" id="grid">
			<?php 
				if ( ! empty( $pricing_pg_title )) {
				$args = array( 'post_type' => 'hantuss_pricing','posts_per_page' => 100);  	
				$plans = new WP_Query( $args ); 
				if( $plans->have_posts() )
				{
					while ( $plans->have_posts() ) : $plans->the_post();
			?>
			<?php
				$terms = get_the_terms( $post->ID, 'pricing_categories' );
									
				if ( $terms && ! is_wp_error( $terms ) ) : 
					$links = array();

					foreach ( $terms as $term ) 
					{
						$links[] = $term->slug;
					}
					
					$tax = join( '","', $links );		
				else :	
					$tax = '';	
				endif;
			?>
                <div class="col-lg-4 col-md-6 price-contents" data-groups='["<?php echo strtolower($tax); ?>"]'>
                    <?php get_template_part('template-parts/pricing/pricing','content'); ?>
                </div>
				<?php 
				
					endwhile;
						} 
				 }
			?>
            </div>
			<?php
				}else{
				echo __('Please install & Activate Hantus Feature plugin for add Pricing','hantus-pro');
				}
			?>
        </div>
    </section>
	<?php	
		hantus_after_pricing_page_section_trigger(); }
	 ?>
	<?php 	
		if($Pricing_newsletter_hide_show == '1' ) {
	get_template_part('sections/section','newsletter');
		}
	 ?>
<?php get_footer(); ?>