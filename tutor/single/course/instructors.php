<?php
/**
 * Template for displaying course instructors/ instructor
 *
 * @package Tutor\Templates
 * @subpackage Single\Course
 * @author Themeum <support@themeum.com>
 * @link https://themeum.com
 * @since 1.0.0
 */

use TUTOR\Instructors_List;

do_action( 'tutor_course/single/enrolled/before/instructors' );

$instructors = tutor_utils()->get_instructors_by_course();

do_action( 'tutor_course/single/enrolled/after/instructors' );
