<?php 
$hide_show_breadcrumb= get_theme_mod('hide_show_breadcrumb','1');
$hide_show_breadcrumb_title= get_theme_mod('hide_show_breadcrumb_title','1');  
$hide_show_breadcrumb_path= get_theme_mod('hide_show_breadcrumb_path','1');  
$breadcrumb_background_setting= get_theme_mod('breadcrumb_background_setting',get_template_directory_uri() .'/assets/images/bg/breadcrumb-bg.jpg'); 
$breadcrumb_overlay= get_theme_mod('breadcrumb_overlay','#f6f6f6'); 
$breadcrumb_background_position= get_theme_mod('breadcrumb_background_position','fixed');
$breadcrumb_opacity= get_theme_mod('breadcrumb_opacity','0.3');

if($hide_show_breadcrumb == '1') :
?>
<style>
	#breadcrumb-area:before {
     opacity: <?php echo $breadcrumb_opacity; ?> !important; 
}
</style>
<section id="breadcrumb-area" style="background:url('<?php echo esc_url($breadcrumb_background_setting); ?>') no-repeat center / cover;background-attachment:<?php echo esc_attr($breadcrumb_background_position); ?>;">

	<div class="container">
            <div class="row">
                <div class="col-12 text-center">
				<?php if($hide_show_breadcrumb_title == '1') { ?>
					<h2>
						<?php 
						
							if ( is_home() || is_front_page()):
			
								single_post_title();
			
							elseif ( is_day() ) : 
							
								printf( __( 'Daily Archives: %s', 'hantus-pro' ), get_the_date() );
							
							elseif ( is_month() ) :
							
								printf( __( 'Monthly Archives: %s', 'hantus-pro' ), (get_the_date( 'F Y' ) ));
								
							elseif ( is_year() ) :
							
								printf( __( 'Yearly Archives: %s', 'hantus-pro' ), (get_the_date( 'Y' ) ) );
								
							elseif ( is_category() ) :
							
								printf( __( 'Category Archives: %s', 'hantus-pro' ), (single_cat_title( '', false ) ));

							elseif ( is_tag() ) :
							
								printf( __( 'Tag Archives: %s', 'hantus-pro' ), (single_tag_title( '', false ) ));
								
							elseif ( is_404() ) :

								printf( __( 'Error 404', 'hantus-pro' ));
								
							elseif ( is_author() ) :
							
								printf( __( 'Author: %s', 'hantus-pro' ), (get_the_author( '', false ) ));		
							
							elseif ( is_tax( 'portfolio_categories' ) ) :

								printf( __( 'Portfolio Categories: %s', 'hantus-pro' ), (single_term_title( '', false ) ));	
								
							elseif ( is_tax( 'pricing_categories' ) ) :

								printf( __( 'Pricing Categories: %s', 'hantus-pro' ), (single_term_title( '', false ) ));	
								
							elseif ( class_exists( 'woocommerce' ) ) : 
								
								if ( is_shop() ) {
									woocommerce_page_title();
								}
								
								elseif ( is_cart() ) {
									the_title();
								}
								
								elseif ( is_checkout() ) {
									the_title();
								}
								
								else {
									the_title();
								}
							else :
									the_title();
									
							endif;
							
						?>
					</h2>
				<?php } ?>
				<?php if($hide_show_breadcrumb_path == '1') { ?>
					<ul class="breadcrumb-nav list-inline">
						<?php if (function_exists('hantus_breadcrumbs')) hantus_breadcrumbs();?>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php 
endif;
?>