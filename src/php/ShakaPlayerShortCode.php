<?php
/**
 * Shaka player short code.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

use DateTime;
use Firebase\JWT\JWT;
use Hantus\Theme\Helpers\FrontEndHelpers;
use TUTOR\Utils;

/**
 * ShakaPlayerShortCode class file.
 */
class ShakaPlayerShortCode {
	/**
	 * Get Drm token action and nonce name.
	 */
	const JU_GET_TOKEN_ACTION_NAME = 'ju_get_drm_token';

	/**
	 * ShakaPlayerShortCode construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_shortcode( 'shaka_player', [ $this, 'output_short_code' ] );

		add_action( 'wp_ajax_' . self::JU_GET_TOKEN_ACTION_NAME, [ $this, 'get_drm_token' ] );
		add_action( 'wp_ajax_nopriv_' . self::JU_GET_TOKEN_ACTION_NAME, [ $this, 'get_drm_token' ] );
	}

	/**
	 * Show shaka player short code.
	 *
	 * @param array $atts Attribute.
	 *
	 * @throws \Exception
	 */
	public function output_short_code( $atts = [] ) {

		$user_agent = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;
		$demo       = ! empty( $atts['demo'] ) && filter_var( $atts['demo'], FILTER_VALIDATE_BOOL );

		$is_browser = preg_match( '/((Win|Macintosh).*?(Chrome|Edg|Trident|Safari))(?!.*OPR)/i', $user_agent );

		ob_start();

		if ( $is_browser || $demo  ) {
			?>
			<div class="video-copyright"></div>
			<video
					id="video-shaka-<?php echo FrontEndHelpers::generate_unic_id(); ?>"
					data-source="<?php echo esc_url( $atts['source'] ?? '' ); ?>"
					data-source_safari="<?php echo esc_url( $atts['source-safari'] ?? '' ); ?>"
					data-uid="<?php echo esc_attr( get_current_user_id() ); ?>"
					data-course_id="<?php echo esc_attr( $atts['course'] ?? 0 ); ?>"
					poster="<?php echo esc_url( $atts['poster'] ?? '' ); ?>"
					controls autoplay></video>
			<?php
		} else {
			?>
			<div class="warning-wrapper">
				<h3>
					<?php esc_html_e( 'Your browser does not support this video.', 'hantus-pro' ); ?>
				</h3>
				<p>
					<?php esc_html_e( 'You can view this lecture in the following browsers:', 'hantus-pro' ); ?>
				</p>
				<p><b><?php esc_html_e( 'Windows:', 'hantus-pro' ); ?></b></p>
				<ul>
					<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
					<li><?php esc_html_e( 'Internet Explorer', 'hantus-pro' ); ?></li>
					<li><?php esc_html_e( 'Edge', 'hantus-pro' ); ?></li>
				</ul>
				<p><b><?php esc_html_e( 'Mac:' ); ?></b></p>
				<ul>
					<li><?php esc_html_e( 'Chrome', 'hantus-pro' ); ?></li>
					<li><?php esc_html_e( 'Safari', 'hantus-pro' ); ?></li>
				</ul>
			</div>
			<?php
		}

		return ob_get_clean();
	}

	/**
	 * Get DRM token
	 *
	 * @return void
	 */
	public function get_drm_token(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::JU_GET_TOKEN_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code', 'hantus-pro' ) ] );
		}

		$course_id  = ! empty( $_POST['courseID'] ) ? filter_var( $_POST['courseID'], FILTER_SANITIZE_NUMBER_INT ) : null;
		$content_id = ! empty( $_POST['contentID'] ) ? filter_var( $_POST['contentID'], FILTER_SANITIZE_NUMBER_INT ) : null;
		$video_keys = ! empty( $_POST['keys'] ) ? filter_var( wp_unslash( $_POST['keys'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$decode_keys = json_decode( base64_decode( $video_keys ) );

		if ( empty( $course_id ) && empty( $decode_keys ) ) {
			wp_send_json_error( [ 'message' => __( 'Empty course ID', 'hantus-pro' ) ] );
		}

		if ( empty( $content_id ) && empty( $decode_keys ) ) {
			wp_send_json_error( [ 'message' => __( 'Empty content ID', 'hantus-pro' ) ] );
		}

		$line_key   = carbon_get_post_meta( $course_id, 'ju_course_videos' ) ?? [];
		$video_name = get_the_title( $content_id ) ?? '';

		$keys = [];
		if ( ! empty( $content_id ) ) {
			foreach ( $line_key as $item ) {
				if ( $item['lessons_name'] === $video_name ) {
					foreach ( $item['ju_videos'] as $video ) {
						if ( ! empty( $video ) ) {
							$keys[] = [
								'id'           => $video['id'],
								'usage_policy' => 'Policy B',
							];
						}
					}
				}
			}
		} else {
			foreach ( $decode_keys as $item ) {
				if ( ! empty( $item ) ) {
					$keys[] = [
						'id'           => $item,
						'usage_policy' => 'Policy B',
					];
				}
			}
		}

		$payload = [
			'type'                       => 'entitlement_message',
			'version'                    => 2,
			'content_keys_source'        => [
				'inline' => $keys,
			],
			'content_key_usage_policies' => [
				[
					'name'     => 'Policy A',
					'widevine' => [
						'device_security_level' => 'HW_SECURE_ALL',
						'cgms-a'                => 'once',
						'hdcp'                  => '2.0',
					],
				],
				[
					'name'     => 'Policy B',
					'widevine' => [
						'device_security_level' => 'SW_SECURE_CRYPTO',
					],
				],
			],
		];

		$currentDateTime           = new DateTime();
		$formatted_date_current    = $currentDateTime->format( 'Y-m-d\TH:i:sP' );
		$expiration_date           = $currentDateTime->modify( '+5 min' );
		$formatted_date_expiration = $expiration_date->format( 'Y-m-d\TH:i:sP' );

		$envelope = [
			'version'         => 1,
			'begin_date'      => $formatted_date_current,
			'expiration_date' => $formatted_date_expiration,
			'com_key_id'      => AXINOM_COM_KEY,
			'message'         => $payload,
		];

		wp_send_json_success( [ 'token' => JWT::encode( $envelope, base64_decode( AXINOM_TOKEN_KEY ), 'HS256' ) ] );
	}
}
