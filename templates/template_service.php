<?php
/**
Template Name: Service Page
**/

get_header();
$default_content 				= hantus_get_page_service_default();
$service_service_hide_show		= get_theme_mod('service_service_hide_show','1'); 
$service_page_title				= get_theme_mod('service_page_title','Our Services');
$service_page_description		= get_theme_mod('service_page_description','These are the services we provide, these makes us stand apart.');
$service_newsletter_hide_show	= get_theme_mod('service_newsletter_hide_show','1');
$service_page_contents			= get_theme_mod('service_page_contents',$default_content);	
?>
    <!-- Start: Our Service
    ============================= -->
	<?php if($service_service_hide_show == '1') {?>
	<?php hantus_before_serv_page_section_trigger(); ?>
    <section id="services" class="custom-padding">
        <div class="container">
            <div class="row" id="service_pg">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h2><?php echo $service_page_title; ?></h2>
                        <p><?php echo $service_page_description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
				<?php
					if ( ! empty( $service_page_contents ) ) {
					$allowed_html = array(
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
					'b'      => array(),
					'i'      => array(),
					);
					$service_page_contents = json_decode( $service_page_contents );
					foreach ( $service_page_contents as $service_page_item ) {
						$title = ! empty( $service_page_item->title ) ? apply_filters( 'hantus_translate_single_string', $service_page_item->title, 'page service section' ) : '';
						$text = ! empty( $service_page_item->text ) ? apply_filters( 'hantus_translate_single_string', $service_page_item->text, 'page service section' ) : '';
						$pricing = ! empty( $service_page_item->pricing ) ? apply_filters( 'hantus_translate_single_string', $service_page_item->pricing, 'page service section' ) : '';
						$text2 = ! empty( $service_page_item->text2) ? apply_filters( 'hantus_translate_single_string', $service_page_item->text2,'page service section' ) : '';
						$link = ! empty( $service_page_item->link ) ? apply_filters( 'hantus_translate_single_string', $service_page_item->link, 'page service section' ) : '';
						$image = ! empty( $service_page_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $service_page_item->image_url, 'page service section' ) : '';
				?>
                <div class="col-lg-3 col-md-6 col-sm-6 page-service-contents">                    
                    <div class="service-box text-center">                        
                        <figure>
                            <?php if ( ! empty( $image ) ) : ?>
								<img class="services_cols_mn_icon" src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
							<?php endif; ?>
                            <figcaption>
                                <div class="inner-text">
									<?php if ( ! empty( $text2 ) ) : ?>
										<a href="<?php echo esc_html( $link ); ?>" class="boxed-btn"><?php echo esc_html( $text2 ); ?></a>
									<?php endif; ?>
                                </div>
                            </figcaption>
                        </figure>
							<?php if ( ! empty( $title ) ) : ?>
								<h4><?php echo esc_html( $title ); ?></h4>
							<?php endif; ?>
							<?php if ( ! empty( $text ) ) : ?>
								<p><?php echo esc_html( $text ); ?></p>
							<?php endif; ?>
							<?php if ( ! empty( $pricing ) ) : ?>							
								<p class="price"><?php echo esc_html( $pricing ); ?></p>
							<?php endif; ?>	
                    </div>
                </div>
				<?php }}?>
            </div>
        </div>
    </section>
	<?php hantus_after_serv_page_section_trigger(); ?>
	<?php } ?>
    <!-- End:  Our Service
    ============================= -->
	<?php if($service_newsletter_hide_show == '1') {?>
		<?php get_template_part('template-parts/sections/section','newsletter');	 ?>
    <?php } ?>
	

   
<?php get_footer(); ?>