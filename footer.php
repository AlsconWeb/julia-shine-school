<?php
/**
 * Footer template.
 *
 * @package hantus/theme
 */
?>

<section class="section-padding move-section start-section" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-3">
				<div class="logo">
					<?php the_custom_logo(); ?>
				</div>
			</div>
			<div class="col-6 d-flex">
				<div class="row">
					<div class="col-md-12">
						<?php
						if ( has_nav_menu( 'footer_center_row' ) ) {
							wp_nav_menu(
								[
									'theme_location'  => 'footer_center_row',
									'container'       => '',
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'd-flex flex-column flex-xl-row',
									'menu_id'         => '',
									'echo'            => true,
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								]
							);
						}
						?>
					</div>
					<div class="col-12">
						<div class="contact_social">
							<?php
							if ( has_nav_menu( 'footer_social' ) ) {
								wp_nav_menu(
									[
										'theme_location'  => 'footer_social',
										'container'       => '',
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'd-flex justify-content-start justify-content-md-center',
										'menu_id'         => '',
										'echo'            => true,
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}
							?>
						</div>

					</div>
				</div>

			</div>
			<div class="col-sm-12 col-md-3">
				<?php
				if ( has_nav_menu( 'footer_left_col' ) ) {
					wp_nav_menu(
						[
							'theme_location'  => 'footer_left_col',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'echo'            => true,
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
				}
				?>
			</div>
		</div>
	</div>
</section>

<!-- End: Footer Copyright
============================= -->
<?php
get_template_part( 'template-parts/modals/modal', 'master' );

$front_pallate_enable = get_theme_mod( 'front_pallate_enable' );
if ( $front_pallate_enable == '1' ) :
	get_template_part( 'index', 'switcher' );
endif;
wp_footer();
?>
</body>
</html>
