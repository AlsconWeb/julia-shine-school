<?php

use Hantus\Theme\Main;

if ( ! function_exists( 'hantus_setup' ) ) :
	function hantus_setup() {
		/*
		 * Make theme available for translation.
		 */
		load_theme_textdomain( 'hantus-pro', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 */
		add_theme_support( 'title-tag' );
		add_theme_support( 'custom-header' );
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );

		//Add selective refresh for sidebar widget
		add_theme_support( 'customize-selective-refresh-widgets' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( [
			'primary_menu' => esc_html__( 'Primary Menu', 'hantus-pro' ),
			'footer_menu'  => esc_html__( 'Footer Menu', 'hantus-pro' ),
		] );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		] );


		add_theme_support( 'custom-logo' );

		/*
		 * WooCommerce Plugin Support
		 */
		add_theme_support( 'woocommerce' );

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( [ 'css/editor-style.css', hantus_google_font() ] );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hantus_custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		] ) );
	}
endif;
add_action( 'after_setup_theme', 'hantus_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hantus_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hantus_content_width', 1170 );
}

add_action( 'after_setup_theme', 'hantus_content_width', 0 );


//Background Image Pattern
function hantus_background_pattern() {
	$bg_pattern = get_theme_mod( 'bg_pattern', 'bg-img1.png' );
	if ( $bg_pattern != '' ) {
		echo '<style>body.boxed { background:url("' . get_template_directory_uri() . '/assets/images/bg-pattern/' . $bg_pattern . '");}</style>';
	}
}

add_action( 'wp_head', 'hantus_background_pattern', 10, 0 );


/**
 * All Styles & Scripts.
 */
require_once get_template_directory() . '/inc/enqueue.php';

/**
 * Nav Walker fo Bootstrap Dropdown Menu.
 */

require_once get_template_directory() . '/inc/hantus-nav-walker.php';

/**
 * Implement the Custom Header feature.
 */
require_once get_template_directory() . '/inc/custom-header.php';


/**
 * Called Breadcrumb
 */
require_once get_template_directory() . '/inc/breadcrumb/breadcrumb.php';

/**
 * Called Pagination
 */
require_once get_template_directory() . '/inc/pagination/pagination.php';

/**
 * Sidebar.
 */
require_once get_template_directory() . '/inc/sidebar/sidebar.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require_once get_template_directory() . '/inc/extras.php';

// custom control
require get_template_directory() . '/inc/custom-controls/controls/icon-picker/icon-picker-control.php';
require get_template_directory() . '/inc/custom-controls/controls/range-validator/range-slider-control.php';
define( 'OUR_DIRECTORY', get_template_directory() . '/inc/custom-controls/' );
define( 'OUR_DIRECTORY_URI', get_template_directory_uri() . '/inc/custom-controls/' );
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer-repeater/class/customizer-repeater-control.php';

// Radio image
require get_template_directory() . '/inc/custom-controls/radio-image/class/hantus-radio-image.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer-repeater/inc/customizer.php';
require_once get_template_directory() . '/inc/customizer.php';

/**
 * Dynamic Style
 */
require_once get_template_directory() . '/inc/dynamic-style.php';

/**
 * Load Jetpack compatibility file.
 */
require_once get_template_directory() . '/inc/jetpack.php';

/**
 * Load Sanitization file.
 */
require_once get_template_directory() . '/inc/sanitization.php';

/**
 * Hooks.
 */
require_once get_template_directory() . '/inc/hooks/hantus-hooks-settings.php';
require_once get_template_directory() . '/inc/hooks/functions.php';
require_once get_template_directory() . '/inc/hantus-hooks.php';

/**
 * Load Theme Updator File.
 */
function hantus_theme_updater() {
	require( get_template_directory() . '/inc/updater/theme-updater.php' );
}

add_action( 'after_setup_theme', 'hantus_theme_updater' );

/**
 * Called all the Customize file.
 */

require( get_template_directory() . '/inc/customize/hantus-themes.php' );
require( get_template_directory() . '/inc/customize/hantus-slider-section.php' );
require( get_template_directory() . '/inc/customize/hantus-header-section.php' );
require( get_template_directory() . '/inc/customize/hantus-info_section.php' );
require( get_template_directory() . '/inc/customize/hantus-sponsers.php' );
require( get_template_directory() . '/inc/customize/hantus-service.php' );
require( get_template_directory() . '/inc/customize/hantus-features.php' );
require( get_template_directory() . '/inc/customize/hantus-portfolio.php' );
require( get_template_directory() . '/inc/customize/hantus-funfact_section.php' );
require( get_template_directory() . '/inc/customize/hantus-pricing.php' );
require( get_template_directory() . '/inc/customize/hantus-products.php' );
require( get_template_directory() . '/inc/customize/hantus-testimonial.php' );
require( get_template_directory() . '/inc/customize/hantus-newsletter.php' );
require( get_template_directory() . '/inc/customize/hantus-appointment.php' );
require( get_template_directory() . '/inc/customize/hantus-blog.php' );
require( get_template_directory() . '/inc/customize/hantus-team.php' );
require( get_template_directory() . '/inc/customize/hantus-custom_section.php' );
require( get_template_directory() . '/inc/customize/hantus-page-template.php' );
require( get_template_directory() . '/inc/customize/hantus-post-slug.php' );
require( get_template_directory() . '/inc/customize/hantus-breadcrumb.php' );
require( get_template_directory() . '/inc/customize/hantus-style-configurator.php' );
require( get_template_directory() . '/inc/customize/hantus-footer-section.php' );
require( get_template_directory() . '/inc/customize/hantus-typography.php' );
require( get_template_directory() . '/inc/customize/hantus-section_manager.php' );
require( get_template_directory() . '/inc/customize/hantus-preloader.php' );
require( get_template_directory() . '/inc/wpml-pll/functions.php' );

/**
 * Called page editor
 */
require( get_template_directory() . '/inc/custom-controls/editor/class/class-hantus-page-editor.php' );

/**
 * Called Required Plugin Features
 */
require( get_template_directory() . '/inc/required-plugin/index.php' );

/**
 * Called Demo Import Features
 */
require( get_template_directory() . '/inc/demo-import/index.php' );

/**
 * Load Custom Toggle Control in Customizer
 */
require_once get_template_directory() . '/inc/custom-controls/toggle/class-customizer-toggle-control.php';

/**
 * Load Custom Category_Dropdown_Custom_Control in Customizer
 */
require_once get_template_directory() . '/inc/custom-controls/category/category-dropdown-custom-control.php';

/**
 * Add WooCommerce Cart Icon With Cart Count (https://isabelcastillo.com/woocommerce-cart-icon-count-theme-header)
 */
function hantus_add_to_cart_fragment( $fragments ) {

	ob_start();
	$count       = WC()->cart->cart_contents_count;
	$header_cart = get_theme_mod( 'header_cart', 'fa-shopping-bag' );
	?>
	<?php
	if ( $count > 0 ) {
		?>
		<span class="cart-count"><?php echo esc_html( $count ); ?></span>
		<?php
	} else {
		?>
		<span class="cart-count">0</span>
		<?php
	}
	?><?php

	$fragments['span.cart-count'] = ob_get_clean();

	return $fragments;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'hantus_add_to_cart_fragment' );


/**
 * Plugin Name: Admitad tracking code for Woocommerce
 * Version: 1.0
 * Description: This plugin will add Admitad tracking code to WooCommerce.
 */
add_action('init', 'set_custom_cookies');

function set_custom_cookies() {
    if (isset($_GET['tagtag_uid']) && isset($_GET['utm_source'])) {
        $tagtag_aid_value = sanitize_text_field($_GET['tagtag_uid']);
        $deduplication_cookie_value = sanitize_text_field($_GET['utm_source']);

        setcookie('tagtag_aid', $tagtag_aid_value, time() + 30 * 24 * 60 * 60, COOKIEPATH, COOKIE_DOMAIN, false);
        setcookie('deduplication_cookie', $deduplication_cookie_value, time() + 30 * 24 * 60 * 60, COOKIEPATH, COOKIE_DOMAIN, false);
    }
}

add_action('woocommerce_new_order', 'send_order_data_to_custom_endpoint', 10, 1);

function send_order_data_to_custom_endpoint($order_id) {
    $order = wc_get_order($order_id);

    if ($order->get_total() > 600) {
        $tagtag_aid = isset($_COOKIE['tagtag_aid']) ? sanitize_text_field($_COOKIE['tagtag_aid']) : '';
        $deduplication_cookie = isset($_COOKIE['deduplication_cookie']) ? sanitize_text_field($_COOKIE['deduplication_cookie']) : '';

        if ($deduplication_cookie === 'admitad') {
            $broker = 'adm';
        } elseif (empty($deduplication_cookie)) {
            $broker = 'na';
        } else {
            $broker = $deduplication_cookie;
        }

        $order_data = array(
            'order_id'            => $order_id,
            'order_total'         => $order->get_total(),
            'tagtag_aid'          => $tagtag_aid,
            'deduplication_cookie' => $deduplication_cookie,
            'broker'              => $broker,
        );

        $domains = array('z.asbmit.com', 'ad.admitad.com', 'lenkmio.com', 'pafutos.com', 'tjzuh.com');

        foreach ($domains as $domain) {
            $url = build_custom_endpoint_url($domain, $order_data);
            $response = wp_remote_get($url);

            if (is_wp_error($response)) {
            error_log('Error while sending GET-request to ' . $domain . ': ' . $response->get_error_message());
            }
        }
    }
}

function build_custom_endpoint_url($domain, $order_data) {
    $url = "https://$domain/tt";
    $url .= '?response_type=img';
    $url .= '&adm_method=gtm_template';
    $url .= '&payment_type=sale';
    $url .= '&uid=' . $order_data['tagtag_aid'];
    $url .= '&campaign_code=caeacef5dc';
    $url .= '&channel=' . $order_data['broker'];
    $url .= '&order_id=' . $order_data['order_id'];
    $url .= '&action_code=1';
    $url .= '&promocode=';
    $url .= '&tariff_code=1';
    $url .= '&position_id=1';
    $url .= '&position_count=1';
    $url .= '&price=' . $order_data['order_total'];
    $url .= '&quantity=1';
    $url .= '&currency_code=' . get_woocommerce_currency();
    $url .= '&product_id=total_cart';
    $url .= '&suid=gtm_template';
    $url .= '&action_useragent=gtm_template';
    $url .= '&domain=' . $domain;

    return $url;
}

function add_data_layer_on_add_to_cart()
{
    if (is_product())
    {
        global $product;
        $product_data = array
        (
            'price' => $product->get_price()
        );

        // after button click
        echo '<script>
                 jQuery(document).ready(function($) 
                 {
                     $(".single_add_to_cart_button").on("click", function() 
                     {
                         var added_product_data = 
                         {
                             "event": "addToCartGtm",
                             "ecommerce": 
                             {
                                 "add": 
                                 {
                                     "products": [' . json_encode($product_data) . ']
                                 }
                             }
                         };

                         dataLayer.push(added_product_data);
                     });
                 });
             </script>';
    }
}

add_action('wp_footer', 'add_data_layer_on_add_to_cart');

function add_data_layer_to_checkout()
{
    if (is_checkout())
    {
        $order_total = WC()->cart->get_total('float');

        ?>
        <script>
            jQuery(document).ready(function($)
            {
                var dataSent = true;


                $('form.checkout').submit(function(event)
                {
                    if (!dataSent) return;

                        var error = false;

                        var requiredFields = ['billing_email', 'billing_first_name', 'billing_last_name', 'billing_country', 'billing_address_1', 'billing_city', 'billing_state', 'billing_postcode', 'billing_phone'];
                        $.each(requiredFields, function(index, field)
                        {
                            var value = $('#' + field).val();
                            if (value === '')
                            {
                                error = true;
                                return false;
                            }
                        });

                        var termsCheckbox = $('#terms').prop('checked');
                        if (!termsCheckbox)
                        {
                            error = true;

                        }

                        var selectedPaymentMethod = $('input[name="payment_method"]:checked').val();
                        if (!selectedPaymentMethod)
                        {
                            error = true;
                        }

                        var errorMessage = $('.woocommerce-error');
                        if (errorMessage.length > 0)
                        {
                            error = true;
                        }

                        if (!error)
                        {
                            var orderTotal = <?php echo json_encode($order_total); ?>;
                            var submitted_data =
                            {
                                event: 'checkoutGtm',
                                ecommerce:
                                {
                                    checkout:
                                    {
                                        actionField:
                                        {
                                            value: orderTotal,
                                        }
                                    }
                                }
                            };

                            dataLayer.push(submitted_data);
                            dataSent = false;
                        }


                });
            });
        </script>
        <?php
    }
}
add_action('wp_footer', 'add_data_layer_to_checkout');

require_once __DIR__ . '/vendor/autoload.php';

new Main();
