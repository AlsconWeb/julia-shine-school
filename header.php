<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin:0 !important">
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PJDDCRXV');</script>
<!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<?php wp_head(); ?>

        <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/uploads/assets/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/uploads/assets/favicon-16x16.png">
        <link rel="shortcut icon" href="/wp-content/uploads/assets/favicon.ico">

        <meta name="facebook-domain-verification" content="u91m59gkr50z3ruotentn4p7509at6" />

<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:3788216,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>


    <script src="/wp-content/uploads/assets/countUp.js"></script>
</head>

<?php
$wide_boxed = get_theme_mod( 'wide_boxed', 'wide' );
if ( $wide_boxed == "boxed" ) {
	$class = "boxed";
} else {
	$class = "wide";
}
?>
<?php
$front_pallate_enable = get_theme_mod( 'front_pallate_enable' );
if ( $front_pallate_enable == '1' ) {
?>
<body <?php body_class( $class ); ?> onload="noStyleChange()">
<?php }else{ ?>
<body <?php body_class( $class ); ?>>
<?php } ?>

<?php
if(is_singular( 'lesson' )){
	echo '<div id="tutor-page-wrap" class="tutor-site-wrap site">';
}
?>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDDCRXV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	( function( m, e, t, r, i, k, a ) {
		m[ i ] = m[ i ] || function() {
			( m[ i ].a = m[ i ].a || [] ).push( arguments );
		};
		m[ i ].l = 1 * new Date();
		for ( var j = 0; j < document.scripts.length; j++ ) {
			if ( document.scripts[ j ].src === r ) {
				return;
			}
		}
		k = e.createElement( t ), a = e.getElementsByTagName( t )[ 0 ], k.async = 1, k.src = r, a.parentNode.insertBefore( k, a );
	} )
	( window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym' );

	ym( 95182344, 'init', {
		clickmap: true,
		trackLinks: true,
		accurateTrackBounce: true,
		webvisor: true,
		ecommerce: 'yandexDataLayer'
	} );
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/95182344" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


<?php
get_template_part( 'template-parts/sections/hantus', 'header' );
get_template_part( 'template-parts/sections/hantus', 'navigation' );
?>
