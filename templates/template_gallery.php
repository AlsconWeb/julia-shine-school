<?php
/**
Template Name: Gallery Page
**/

get_header();
$default_content 			= hantus_get_gallery_default();
$hide_show_gallery			= get_theme_mod('hide_show_gallery','1');
$hide_show_gallery_subscribe= get_theme_mod('hide_show_gallery_subscribe','1');
$gallery_page_img_setting	= get_theme_mod('gallery_page_img_setting',$default_content);
$gallery_icon_first			= get_theme_mod('gallery_icon_first','fa-link');
$gallery_icon_second			= get_theme_mod('gallery_icon_second','fa-eye');
?>		
<?php if($hide_show_gallery == '1') {?>	
<?php hantus_before_gallery_page_section_trigger(); ?>	
 <section id="gallery" class="section-padding gallery-page">
        <div class="container">
            <div class="row gallery" id="grid">
                <?php
						if ( ! empty( $gallery_page_img_setting ) ) {
						$allowed_html = array(
						'br'     => array(),
						'em'     => array(),
						'strong' => array(),
						'b'      => array(),
						'i'      => array(),
						);
						$gallery_page_img_setting = json_decode( $gallery_page_img_setting );
						foreach ( $gallery_page_img_setting as $gallery_item ) {
					$image = ! empty( $gallery_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $gallery_item->image_url, 'Gallery section' ) : '';
					$subtitle = ! empty( $gallery_item->subtitle ) ? apply_filters( 'hantus_translate_single_string', $gallery_item->subtitle, 'Gallery section' ) : '';
					$title = ! empty( $gallery_item->title ) ? apply_filters( 'hantus_translate_single_string', $gallery_item->title, 'Gallery section' ) : '';
					$link = ! empty( $gallery_item->link ) ? apply_filters( 'hantus_translate_single_string', $gallery_item->link, 'Gallery section' ) : '';
				?>
                <div class="col-lg-4 col-md-6 col-sm-6 gallery-item">
                    <figure>
                        <?php if ( ! empty( $image ) ) : ?>
							<img src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
						<?php endif; ?>
                        <figcaption>
                            <div class="inner-text">
                                <ul>
                                    <li><a href="<?php echo esc_url( $link ); ?>"><i class="fa <?php echo esc_attr( $gallery_icon_first ); ?>"></i></a></li>
                                    <li><a class="popup" href="<?php echo esc_url( $image ); ?>"><i class="fa <?php echo esc_attr( $gallery_icon_second ); ?>"></i></a></li>
                                </ul>
								<?php if ( ! empty( $title ) ) : ?>
									<h4><?php echo esc_attr( $title ); ?></h4>
								<?php endif; ?>
								<?php if ( ! empty( $subtitle ) ) : ?>
									<p><?php echo esc_attr( $subtitle ); ?></p>
								<?php endif; ?>
                            </div>
                        </figcaption>
                    </figure>
                </div>
				<?php 
				} } 
				?>
            </div>
        </div>
    </section>
<?php hantus_after_gallery_page_section_trigger(); ?>	
<?php } ?>
 <?php if($hide_show_gallery_subscribe == '1') {?>
		<?php get_template_part('template-parts/sections/section','newsletter');	 ?>
    <?php } ?>
<?php get_footer(); ?>