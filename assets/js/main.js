/* global juMainObject, juUserInfo, Clappr */

/**
 * @param juMainObject.ajaxUrl
 * @param juMainObject.addToCartAction
 * @param juMainObject.addToCartNonce
 * @param juMainObject.getDRMTokenAction
 * @param juMainObject.getDRMTokenNonce
 * @param juMainObject.currentUserEmail
 */
jQuery( document ).ready( function( $ ) {
	const addToCartBtn = $( '.add_to_cart_button' );

	if ( addToCartBtn ) {
		addToCartBtn.click( function( e ) {
			e.preventDefault();

			const btn = $( this );

			$.ajax( {
				type: 'POST',
				url: juMainObject.ajaxUrl,
				beforeSend: function() {
					btn.addClass( 'disable' );
				},
				data: {
					action: juMainObject.addToCartAction,
					nonce: juMainObject.addToCartNonce,
					productID: $( this ).data( 'product_id' ),
					quantity: $( this ).data( 'quantity' )
				},
				success: function( res ) {
					if ( res.success ) {
						location.reload();
					} else {
						alert( res.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );

		} );
	}

	// Modal
	const selectEL = $( 'select:not(.tutor-form-select)' );
	if ( selectEL.length ) {
		selectEL.select2();
	}

	const modalBtn = $( '.modal-btn' );
	if ( modalBtn.length ) {
		modalBtn.click( function( e ) {
			e.preventDefault();

			const title = $( this ).data( 'title' );
			const content = $( this ).data( 'description' );
			const imgSRC = $( this ).data( 'image_src' );
			$( '#master-modal .modal-body .image img' ).remove();

			$( '#master-modal .modal-title' ).text( title );
			$( '#master-modal .modal-body .content' ).text( content );
			$( '#master-modal .modal-body .image' ).append( '<img src="' + imgSRC + '" alt="' + title + '">' );

			$( '#master-modal' ).modal( 'show' );
		} );
	}

	const userInfoTextArea = $( '.hidden_user_info textarea' );
	if ( userInfoTextArea.length ) {
		let userInfoString = '';

		for ( let key in juUserInfo ) {
			userInfoString += key + ': ' + juUserInfo[ key ] + '\n';
		}

		userInfoTextArea.val( userInfoString );
	}

	const videoEl = $( '[id^=clappr-]' );
	if ( videoEl.length ) {
		let players = [];
		videoEl.map( ( i, el ) => {
			const short = $( el ).data( 'short' );
			if ( ! short ) {
				players.push(
					new Clappr.Player(
						{
							source: $( el ).data( 'video' ),
							parentId: '#' + $( el ).attr( 'id' ),
							bandwidth: '100%',
							width: '100%',
							autoPlay: false,
							poster: $( el ).data( 'thumbnail' ),
						} )
				);
			} else {
				new Clappr.Player(
					{
						source: $( el ).data( 'video' ),
						parentId: '#' + $( el ).attr( 'id' ),
						bandwidth: '100%',
						width: '100%',
						height: '100%',
						autoPlay: false,
						poster: $( el ).data( 'thumbnail' ),
					} );
			}
		} );

	}

	//video Shaka
	const videoShakaEl = $( '[id^=video-shaka-]' );
	if ( videoShakaEl.length ) {
		videoShakaEl.map(function(i, el){
			let now = moment();
				let validFrom = now.clone().subtract( 1, 'hour' ).format( 'ddd MMM DD YYYY HH:mm:ss [GMT]ZZ' );
				let validTo = now.clone().add( 2, 'hour' ).format( 'ddd MMM DD YYYY HH:mm:ss [GMT]ZZ' );

				const data = {
					action: juMainObject.getDRMTokenAction,
					nonce: juMainObject.getDRMTokenNonce,
					contentID: $( '#ju_content_id' ).val(),
					courseID: $( '#ju_course_id' ).val(),
					keys:$(el).data('keys'),
					license: {
						start_datetime: validFrom.toString(),
						expiration_datetime: validTo.toString(),
						allow_persistence: true
					}
				};

				$.ajax( {
					type: 'POST',
					url: juMainObject.ajaxUrl,
					data: data,
					success: function( res ) {
						if ( res.success ) {
							initApp( res.data.token, el );
						}
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
					}
				} );
		})
	}

	/**
	 * Check player support.
	 *
	 * @param token
	 * @param el
	 */
	function initApp( token, el ) {
		if ( shaka.Player.isBrowserSupported() ) {
			const IDEl = $( el ).attr( 'id' );
			let isSafari = /^((?!chrome|android).)*safari/i.test( navigator.userAgent );
			let source = isSafari ? $( el ).data( 'source_safari' ) : $( el ).data( 'source' );

			let protection = {};

			if ( isSafari ) {
				protection = {
					drm: {
						servers: {
							'com.widevine.alpha': 'https://drm-widevine-licensing.axprod.net/AcquireLicense',
							'com.microsoft.playready': 'https://drm-playready-licensing.axprod.net/AcquireLicense',
							'com.apple.fps': 'https://drm-fairplay-licensing.axprod.net/AcquireLicense'
						},
						advanced: {
							'com.apple.fps': {
								'serverCertificateUri': 'https://vtb.axinom.com/FPScert/fairplay.cer'
							}
						}
					}
				};
			} else {
				protection = {
					drm: {
						servers: {
							'com.widevine.alpha': 'https://drm-widevine-licensing.axprod.net/AcquireLicense'
						}
					}
				};
			}

			initPlayer( IDEl, token, source, protection );
		} else {
			alert( 'Your browser does not support video playback, open in a Chrome-based browser' );
		}
	}


	/**
	 * Init player.
	 *
	 * @param videoShakaEl
	 * @param token
	 * @param source
	 * @param protection
	 */
	function initPlayer( videoShakaEl, token, source, protection ) {
		let player = new shaka.Player( document.getElementById( videoShakaEl ) );
		shaka.polyfill.installAll();

		player.configure( protection );

		player.load( source ).then( function() {
			console.log( 'The video has now been loaded!!!!!!!' );
		} ).catch( ( error ) => {
			console.log( error );
		} );

		player.getNetworkingEngine().registerRequestFilter( function( type, request ) {
			if ( type === shaka.net.NetworkingEngine.RequestType.LICENSE ) {
				request.headers[ 'X-AxDRM-Message' ] = token;
			}
		} );

		$( '#' + videoShakaEl ).show();
		createCopyright();

		setInterval( function() {
			const copyrightDiv = $( '.video-copyright' );

			if ( ! copyrightDiv.length || ! $.trim( copyrightDiv.html() ) ) {
				if ( ! copyrightDiv.length ) {
					$( '<div class="video-copyright">' + juMainObject.currentUserEmail + '</div>' ).appendTo( '.tutor-video-player' );
				} else {
					createCopyright()
				}
			}
		}, 3000 );
	}

	/**
	 * Create copyright in video
	 */
	function createCopyright() {
		$( '.video-copyright' ).text( juMainObject.currentUserEmail );
	}
} );
