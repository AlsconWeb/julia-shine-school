<?php
/**
 * Main theme class.
 *
 * @package hantus/theme
 */

namespace Hantus\Theme;

/**
 * Main class.
 */
class Main {
	/**
	 * Version my file.
	 */
	const JU_THEME_VERSION = '1.0.0';

	/**
	 * Actions name and nonce code.
	 */
	const JU_ADD_TO_CART_ACTION_NAME = 'add_to_cart';

	/**
	 * Main construct.
	 */
	public function __construct() {
		new CarboneFields();
		new ContactFormSeven();
		new MailPoet();
		new ClapprPlayerShortCode();
		new AddFreeCourseAfterRegistration();
		new ShakaPlayerShortCode();

		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts_and_style' ] );

		// ajax add to cart ajax
		add_action( 'wp_ajax_' . self::JU_ADD_TO_CART_ACTION_NAME, [ $this, 'add_to_cart_ajax_handler' ] );
		add_action( 'wp_ajax_nopriv_' . self::JU_ADD_TO_CART_ACTION_NAME, [ $this, 'add_to_cart_ajax_handler' ] );

		add_action( 'admin_enqueue_scripts', [ $this, 'load_admin_styles' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );

		add_filter( 'woocommerce_add_to_cart_redirect', [ $this, 'redirect_to_checkout_after_add_to_cart' ] );
		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );

		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts_and_style(): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script(
			'ju_main',
			$url . '/assets/js/main' . $min . '.js',
			[
				'jquery',
				'jquery-migrate',
				'ju_shaka',
				'ju-moment',
			],
			self::JU_THEME_VERSION,
			true
		);

		wp_enqueue_script( 'ju-chart', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/chart.umd.js', [ 'jquery' ], '4.4.0', true );
		wp_enqueue_script( 'ju-common', $url . '/assets/js/common.js', [ 'jquery' ], self::JU_THEME_VERSION, false );
		wp_enqueue_script( 'ju-moment', $url . '/assets/js/moment.min.js', [ 'jquery' ], self::JU_THEME_VERSION, false );

		if ( is_shop() || is_front_page() || is_product_category() ) {
			wp_enqueue_script( 'ju-bootstrap', $url . '/assets/js/bootstrap5.min.js', [ 'jquery' ], '5.1.3', true );
		}

		wp_localize_script(
			'ju_main',
			'juMainObject',
			[
				'ajaxUrl'           => admin_url( 'admin-ajax.php' ),
				'addToCartAction'   => self::JU_ADD_TO_CART_ACTION_NAME,
				'addToCartNonce'    => wp_create_nonce( self::JU_ADD_TO_CART_ACTION_NAME ),
				'getDRMTokenAction' => ShakaPlayerShortCode::JU_GET_TOKEN_ACTION_NAME,
				'getDRMTokenNonce'  => wp_create_nonce( ShakaPlayerShortCode::JU_GET_TOKEN_ACTION_NAME ),
				'currentUserEmail'  => get_userdata( get_current_user_id() )->user_email ?? '',
			]
		);

		wp_enqueue_style( 'ju_select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', '', '4.1.0' );
		wp_enqueue_style( 'ju_shaka', '//cdn.jsdelivr.net/npm/shaka-player@4.3.5/dist/controls.min.css', '', '4.1.0' );
		wp_enqueue_script( 'ju_select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', [ 'jquery' ], '4.1.0', true );
		wp_enqueue_script( 'ju_clappr', '//cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js', [ 'jquery' ], '1.0.0' );
		wp_enqueue_script( 'ju_shaka', '//ajax.googleapis.com/ajax/libs/shaka-player/4.2.2/shaka-player.compiled.debug.js', [ 'jquery' ], '4.6.0' );
	}

	/**
	 * Add to cart ajax handler.
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function add_to_cart_ajax_handler(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::JU_ADD_TO_CART_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad Nonce code', 'hantus-pro' ) ] );
		}

		$product_id = ! empty( $_POST['productID'] ) ? filter_var( wp_unslash( $_POST['productID'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $product_id ) ) {
			wp_send_json_error( [ 'message' => __( 'Product ID is empty', 'hantus-pro' ) ] );
		}

		$quantity = ! empty( $_POST['quantity'] ) ? filter_var( wp_unslash( $_POST['quantity'] ), FILTER_SANITIZE_NUMBER_INT ) : 1;

		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$product_status    = get_post_status( $product_id );

		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {

			do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
				wc_add_to_cart_message( [ $product_id => $quantity ], true );
			}

			wp_send_json_success();

		} else {
			wp_send_json_error(
				[
					'error'       => true,
					'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id ),
					'message'     => __( 'The product was not added to the cart, an error occurred' ),
				]
			);
		}
	}

	/**
	 * Add style in admin.
	 *
	 * @param string $hook_suffix Hook suffix.
	 *
	 * @return void
	 */
	public function load_admin_styles( string $hook_suffix ): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		if ( 'post.php' === $hook_suffix ) {
			wp_enqueue_style( 'ju-admin-styles', $url . '/assets/css/admin/main' . $min . '.css', '', self::JU_THEME_VERSION );
		}
	}

	/**
	 * Redirect to check out after add to cart.
	 *
	 * @return string
	 */
	public function redirect_to_checkout_after_add_to_cart(): string {

		return wc_get_checkout_url();
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {
		register_nav_menus(
			[
				'footer_center_row' => __( 'Footer center line', 'hantus-pro' ),
				'footer_social'     => __( 'Footer social line', 'hantus-pro' ),
				'footer_left_col'   => __( 'Footer left col', 'hantus-pro' ),
			]
		);
	}
}
