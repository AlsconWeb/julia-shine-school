<?php
/**
Template Name: Product Filter
**/
get_header();
?>
<?php 
	$shop_pg_title = get_theme_mod('shop_pg_title','Title');
	$shop_pg_desc = get_theme_mod('shop_pg_desc','Description');
?>
    <section id="product" class="section-padding woo-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <?php if ( ! empty( $shop_pg_title ) ) : ?>
							<h2><?php echo esc_html( $shop_pg_title ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $shop_pg_desc ) ) : ?>
							<p><?php echo esc_html( $shop_pg_desc ); ?></p>
						<?php endif; ?>
                    </div>
                </div>                
            </div>
			<?php 
				if ( class_exists( 'woocommerce' ) ) {
				$args                   = array(
					'post_type' => 'product',
				);
				/* Exclude hidden products from the loop */
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'product_visibility',
						'field'    => 'name',
						'terms'    => 'exclude-from-catalog',
						'operator' => 'NOT IN',

					),
				);
			?>
			<div class="row portfolio-tab">
                <div class="col-md-12 text-center">
                	<?php 
            		$product_categories = get_terms( 'product_cat', $args );
					$count = count($product_categories);
					if ( $count > 0 ) {
                	?>
                    <ul class="portfolio-tab-sorting sorting-btn" id="filter">
                        <?php
							foreach ( $product_categories as $product_category ) {
								?>
								<?php if($product_category->name == 'All'){ ?>
									<li><a href="#" class="active" data-group="<?php echo $product_category->slug; ?>"><?php  echo $product_category->name; ?></a></li>
								<?php }else{ ?>	
									<li><a href="#" data-group="<?php echo $product_category->slug; ?>"><?php  echo $product_category->name; ?></a></li>
								<?php
								}
							}
						?>
                    </ul>
                	<?php } ?>
                </div>
            </div>
            <div class="row" id="grid">
				<?php
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
					<?php
					$terms = get_the_terms( $post->ID, 'product_cat' );
										
					if ( $terms && ! is_wp_error( $terms ) ) : 
						$links = array();

						foreach ( $terms as $term ) 
						{
							$links[] = $term->slug;
						}
						
						$tax = join( '","', $links );		
					else :	
						$tax = '';	
					endif;
				?>
                <div class="col-lg-3 col-md-4 col-sm-6" data-groups='["<?php echo strtolower($tax); ?>"]'>
				  <div class="shop-product text-center">
						<?php if ( $product->is_on_sale() ) : ?>
							<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="sale">' . esc_html__( 'Sale!', 'hantus-pro' ) . '</span>', $post, $product ); ?>
						<?php endif; ?>
						<div class="product-img">
							<?php the_post_thumbnail(); ?>
						</div>
						<ul class="rate">
							<li>
								<?php if ($average = $product->get_average_rating()) : ?>
								<?php echo '<i class="fa fa-star" title="'.sprintf(__( 'Rated %s out of 5', 'hantus-pro' ), $average).'"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'hantus-pro' ).'</span></i>'; ?>
								<?php endif; ?>
							</li>
						</ul>
						<h5><?php echo the_title(); ?></h5>
						<div class="price"><?php echo $product->get_price_html(); ?></div>

						<div class="product-action">
							<div class="button-top">
							</div>
							<div class="button-cart">
								<?php woocommerce_template_loop_add_to_cart(); ?>
							</div>
						</div>
					</div>
			</div>
			<?php endwhile; ?>
			<?php  wp_reset_postdata(); ?>
            </div>
			<?php  } ?>
        </div>
    </section>
<div class="clearfix"></div>
	
<?php get_footer(); ?>	