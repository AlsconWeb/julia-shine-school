<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package hantus
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function hantus_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}
	
	// Select Theme
	$select_theme	= get_theme_mod('select_theme','hantus-pro');
	$classes[]		= esc_attr($select_theme);
	if ($select_theme == "thaispa-theme") {
		$classes[]		= 'thaispa-theme header-transparent';
	}
	
	// Transparent Header
	if ( is_page_template( 'templates/template-homepage-two.php' )) {
		$classes[] = 'header-transparent';
	}
	
	return $classes;
}
add_filter( 'body_class', 'hantus_body_classes' );


if (!function_exists('hantus_str_replace_assoc')) {

    /**
     * hantus_str_replace_assoc
     * @param  array $replace
     * @param  array $subject
     * @return array
     */
    function hantus_str_replace_assoc(array $replace, $subject) {
        return str_replace(array_keys($replace), array_values($replace), $subject);
    }
}

/*
 *
 * Social Icon
 */
function hantus_get_social_icon_default() {
	return apply_filters(
		'hantus_get_social_icon_default', json_encode(
				 array(
				array(
					'icon_value'	  =>  esc_html__( 'fa-facebook', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_header_social_001',
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-google-plus', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_header_social_002',
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-twitter', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_header_social_003',
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-linkedin', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_header_social_004',
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-behance', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_header_social_005',
				),
			)
		)
	);
}


/*
 *
 * Payment Icon
 */
 function hantus_get_footer_icon_default() {
	return apply_filters(
		'hantus_get_footer_icon_default', json_encode(
					 array(
				array(
					//'image_url'       => get_template_directory_uri(-icon) . '/assets/images/homepage/service/serviceicon01.png',
					'icon_value'	  =>  esc_html__( 'fa-google-plus', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_001',
					
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-visa', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_002',
				
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-mastercard', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_003',
			
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-cc-amex', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_004',
					
				),
				array(
					'icon_value'	  =>  esc_html__( 'fa-user-md', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_footer_005',
					
				),
			)
		)
	);
}

/*
 *
 * Slider Default
 */
function hantus_get_slides_default() {
	return apply_filters(
		'hantus_get_slides_default', json_encode(
		 array(
			 
            /*Repeater's first item*/
            array("image_url" => get_template_directory_uri().'/assets/images/sliders/slider01.jpg' ,"link" => "#", "title" => "Welcome To Hantus Spa","subtitle" => "Beauty & Spa Wellness", "text" => "The Spa at Sun Valley is a serene oasis amid all the exciting  activities our iconic valley has delivered for decades.", "text2" => "Make an Appoinment","slide_align" => "left", "id" => "customizer_repeater_00070" ), 
			
            /*Repeater's second item*/
            array("image_url" => get_template_directory_uri().'/assets/images/sliders/slider02.jpg' ,"link" => "#", "title" => "Welcome To Hantus Spa","subtitle" => "Beauty & Spa Wellness", "text" => "The Spa at Sun Valley is a serene oasis amid all the exciting  activities our iconic valley has delivered for decades.", "text2" => "Make an Appoinment","slide_align" => "center", "id" => "customizer_repeater_00071" ), 
			
            /*Repeater's third item*/
            array("image_url" => get_template_directory_uri().'/assets/images/sliders/slider03.jpg' ,"link" => "#", "title" => "Welcome To Hantus Spa","subtitle" => "Beauty & Spa Wellness", "text" => "The Spa at Sun Valley is a serene oasis amid all the exciting  activities our iconic valley has delivered for decades.", "text2" => "Make an Appoinment","slide_align" => "right", "id" => "customizer_repeater_00072" ), 
            )
		)
	);
}

/*
 *
 * Service Default
 */
 function hantus_get_service_default() {
	return apply_filters(
		'hantus_get_service_default', json_encode(
				 array(
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service01.png',
					'title'           => esc_html__( 'Oil Massage', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_001',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service02.png',
					'title'           => esc_html__( 'Skin Care', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),						
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_002',
				
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service03.png',
					'title'           => esc_html__( 'Natural Relaxation', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),					
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_003',
			
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service04.png',
					'title'           => esc_html__( 'Nails Design', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),					
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
			)
		)
	);
}


/*
 *
 * Features Default
 */
 function hantus_get_feature_default() {
	return apply_filters(
		'hantus_get_feature_default', json_encode(
			 array(
				array(
					'title'           => esc_html__( 'Hair Expert', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text  the printing and typesetting.', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/feature-icon/feature-icon01.png',
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_features_001',
				),
				array(
					'title'           => esc_html__( 'Stone Massage', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text  the printing and typesetting.', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/feature-icon/feature-icon02.png',
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_features_002',
				
				),
				array(
					'title'           => esc_html__( 'Nail Care', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text  the printing and typesetting.', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/feature-icon/feature-icon03.png',
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_features_003',
			
				),
				array(
					'title'           => esc_html__( 'Aromatherapy Nature', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text  the printing and typesetting.', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/feature-icon/feature-icon04.png',
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_features_004',
					
				),
			)
		)
	);
}


/*
 *
 * Funfact Default
 */
 
 function hantus_get_funfact_default() {
	return apply_filters(
		'hantus_get_funfact_default', json_encode(
			  array(
				array(
					'title'           => esc_html__( '870', 'hantus-pro' ),
					'text'            => esc_html__( 'Win Awards', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/counter/counter-icon01.png',
					'id'              => 'customizer_repeater_funfact_001',
				),
				array(
					'title'           => esc_html__( '1523', 'hantus-pro' ),
					'text'            => esc_html__( 'Happy Clients', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/counter/counter-icon02.png',
					'id'              => 'customizer_repeater_funfact_001',
				
				),
				array(
					'title'           => esc_html__( '100', 'hantus-pro' ),
					'text'            => esc_html__( 'Treatments', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/counter/counter-icon03.png',
					'id'              => 'customizer_repeater_funfact_001',
			
				),
				array(
					'title'           => esc_html__( '50', 'hantus-pro' ),
					'text'            => esc_html__( 'Users', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/counter/counter-icon04.png',
					'id'              => 'customizer_repeater_funfact_001',
					
				),
			)
		)
	);
}

/*
 *
 * Testimonial Default
 */
 
 function hantus_get_testimonial_default() {
	return apply_filters(
		'hantus_get_testimonial_default', json_encode(
			array(
				array(
					'title'           => esc_html__( 'Eric Matision', 'hantus-pro' ),
					'designation'        => esc_html__( 'Forest Hills. NY', 'hantus-pro' ),
					'text'            => esc_html__( 'I am very impressed by the efficiency of your service and your excellent returns policy. It is so pleasant to deal with such a customer focussed website.', 'hantus-pro' ),
					'image_url'		  =>  get_template_directory_uri() . '/assets/images/testimonial/testimonial01.png',
					'id'              => 'customizer_repeater_testimonial_001',
				),
				array(
					'title'           => esc_html__( 'Jennifer Lopez', 'hantus-pro' ),
					'designation'        => esc_html__( 'Forest Hills. NY', 'hantus-pro' ),
					'text'            => esc_html__( 'I am very impressed by the efficiency of your service and your excellent returns policy. It is so pleasant to deal with such a customer focussed website.', 'hantus-pro' ),
					'image_url'		  =>  get_template_directory_uri() . '/assets/images/testimonial/testimonial01.png',
					'id'              => 'customizer_repeater_testimonial_002',
				),
				array(
					'title'           => esc_html__( 'Betty Ross', 'hantus-pro' ),
					'designation'        => esc_html__( 'Developer', 'hantus-pro' ),
					'text'            => esc_html__( 'I am very impressed by the efficiency of your service and your excellent returns policy. It is so pleasant to deal with such a customer focussed website.', 'hantus-pro' ),
					'image_url'		  =>  get_template_directory_uri() . '/assets/images/testimonial/testimonial01.png',
					'id'              => 'customizer_repeater_testimonial_003',
				),
		    )
		)
	);
}


/*
 *
 * Team Default
 */
 function hantus_get_team_default() {
	return apply_filters(
		'hantus_get_team_default', json_encode(
					  array(
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/beauticians/beauticians01.jpg',
					'title'           => esc_html__( 'Eric Matision ', 'hantus-pro' ),
					'designation'        => esc_html__( 'Founder','hantus-pro' ),
					'text'            => esc_html__( 'It is a long established fact that a reader will be distracted by the readable.', 'hantus-pro' ),
					'id'              => 'customizer_repeater_team_0001',
					'social_repeater' => json_encode(
						array(
							array(
								'id'   => 'customizer-repeater-social-repeater-team_001',
								'link' => 'facebook.com',
								'icon' => 'fa-facebook',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_003',
								'link' => 'twitter.com',
								'icon' => 'fa-twitter',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_004',
								'link' => 'linkedin.com',
								'icon' => 'fa-instagram',
							),
						)
					),
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/beauticians/beauticians02.jpg',
					'title'           => esc_html__( 'Emille Jenifer', 'hantus-pro' ),
					'designation'        => esc_html__( 'C.E.O', 'hantus-pro' ),
					'text'            => esc_html__( 'It is a long established fact that a reader will be distracted by the readable.', 'hantus-pro' ),
					'id'              => 'customizer_repeater_team_0002',
					'social_repeater' => json_encode(
						array(
							array(
								'id'   => 'customizer-repeater-social-repeater-team_005',
								'link' => 'facebook.com',
								'icon' => 'fa-facebook',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_006',
								'link' => 'twitter.com',
								'icon' => 'fa-twitter',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_007',
								'link' => 'pinterest.com',
								'icon' => 'fa-instagram',
							),
						)
					),
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/beauticians/beauticians03.jpg',
					'title'           => esc_html__( 'Eric Matision', 'hantus-pro' ),
					'designation'        => esc_html__( 'Skin Expert', 'hantus-pro' ),
					'text'            => esc_html__( 'It is a long established fact that a reader will be distracted by the readable.', 'hantus-pro' ),
					'id'              => 'customizer_repeater_team_0003',
					'social_repeater' => json_encode(
						array(
							array(
								'id'   => 'customizer-repeater-social-repeater-team_009',
								'link' => 'facebook.com',
								'icon' => 'fa-facebook',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_0010',
								'link' => 'twitter.com',
								'icon' => 'fa-twitter',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_0011',
								'link' => 'linkedin.com',
								'icon' => 'fa-instagram',
							),
						)
					),
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/beauticians/beauticians04.jpg',
					'title'           => esc_html__( 'Betty Ross', 'hantus-pro' ),
					'designation'        => esc_html__( 'Massagist', 'hantus-pro' ),
					'text'            => esc_html__( 'It is a long established fact that a reader will be distracted by the readable.', 'hantus-pro' ),
					'id'              => 'customizer_repeater_team_0004',
					'social_repeater' => json_encode(
						array(
							array(
								'id'   => 'customizer-repeater-social-repeater-team_0014',
								'link' => 'facebook.com',
								'icon' => 'fa-facebook',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_0015',
								'link' => 'twitter.com',
								'icon' => 'fa-twitter',
							),
							array(
								'id'   => 'customizer-repeater-social-repeater-team_0016',
								'link' => 'linkedin.com',
								'icon' => 'fa-instagram',
							),
						)
					),
				),
			)
		)
	);
}


/*
 *
 * Sponsor Default
 */
function hantus_get_sponsers_default() {
	return apply_filters(
		'hantus_get_sponsers_default', json_encode(
			 array(
				array(
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-1.png',
					'id'              => 'customizer_repeater_sponsers_001',
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-2.png',
					'id'              => 'customizer_repeater_sponsers_002',
				
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-3.png',
					'id'              => 'customizer_repeater_sponsers_003',
			
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-4.png',
					'id'              => 'customizer_repeater_sponsers_004',
					
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-5.png',
					'id'              => 'customizer_repeater_sponsers_005',
			
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-6.png',
					'id'              => 'customizer_repeater_sponsers_006',
					
				),	
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-5.png',
					'id'              => 'customizer_repeater_sponsers_007',
			
				),
				array(
					'link'           => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/partners/partner-6.png',
					'id'              => 'customizer_repeater_sponsers_008',
					
				),
			)
		)
	);
}

/*
 *
 * Welcome Default
 */
 
 function hantus_get_welcome_default() {
	return apply_filters(
		'hantus_get_welcome_default', json_encode(
				array(
				array(
					'title'           => esc_html__( 'Beauty & Spa', 'hantus-pro' ),
					'text'            => esc_html__( 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In in massa urna, vitae vestibulum orci aecenas quis est', 'hantus-pro' ),
					'text2'           => esc_html__( 'View Services', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/about-page/welcomeimg01.jpg',
					'id'              => 'customizer_repeater_welcome_001',
				),
				array(
					'title'           => esc_html__( 'Package & Pricing', 'hantus-pro' ),
					'text'            => esc_html__( 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In in massa urna, vitae vestibulum orci aecenas quis est', 'hantus-pro' ),
					'text2'           => esc_html__( 'View Services', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/about-page/welcomeimg02.jpg',
					'id'              => 'customizer_repeater_welcome_002',
				),
				array(
					'title'           => esc_html__( 'Package & Pricing', 'hantus-pro' ),
					'text'            => esc_html__( 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In in massa urna, vitae vestibulum orci aecenas quis est', 'hantus-pro' ),
					'text2'           => esc_html__( 'View Services', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/about-page/welcomeimg03.jpg',
					'id'              => 'customizer_repeater_welcome_003',
				),
			)
		)
	);
}


/*
 *
 * About Funfact Default
 */
 function hantus_get_about_funfact_default() {
	return apply_filters(
		'hantus_get_about_funfact_default', json_encode(
			 array(
				array(
					'title'           => esc_html__( '25 K', 'hantus-pro' ),
					'text'            => esc_html__( 'Happy Customers', 'hantus-pro' ),
					'id'              => 'customizer_repeater_about_funfact_001',
				),
				array(
					'title'           => esc_html__( '20', 'hantus-pro' ),
					'text'            => esc_html__( 'Years Experience', 'hantus-pro' ),
					'id'              => 'customizer_repeater_about_funfact_002',
				),
				array(
					'title'           => esc_html__( '100%', 'hantus-pro' ),
					'text'            => esc_html__( 'Satisfaction', 'hantus-pro' ),
					'id'              => 'customizer_repeater_about_funfact_003',
				),
			)
		)
	);
}


/*
 *
 * Gallery Default
 */
 
 function hantus_get_gallery_default() {
	return apply_filters(
		'hantus_get_gallery_default', json_encode(
				 array(
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery01.jpg',
					'id'              => 'customizer_repeater_gallery_001',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery02.jpg',
					'id'              => 'customizer_repeater_gallery_003',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery03.jpg',
					'id'              => 'customizer_repeater_gallery_003',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery04.jpg',
					'id'              => 'customizer_repeater_gallery_004',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery05.jpg',
					'id'              => 'customizer_repeater_gallery_005',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery01.jpg',
					'id'              => 'customizer_repeater_gallery_006',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery07.jpg',
					'id'              => 'customizer_repeater_gallery_007',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery08.jpg',
					'id'              => 'customizer_repeater_gallery_008',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery09.jpg',
					'id'              => 'customizer_repeater_gallery_009',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery10.jpg',
					'id'              => 'customizer_repeater_gallery_0010',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery11.jpg',
					'id'              => 'customizer_repeater_gallery_0011',
				),
				array(
					'title'           => esc_html__( 'Aromatherapy', 'hantus-pro' ),
					'subtitle'            => esc_html__( '26 Feb 2018', 'hantus-pro' ),
					'link'            => esc_html__( '#', 'hantus-pro' ),
					'image_url'       => get_template_directory_uri() . '/assets/images/gallery/gallery12.jpg',
					'id'              => 'customizer_repeater_gallery_0012',
				),
			)
		)
	);
}


/*
 *
 * Service Page Default
 */
 
 function hantus_get_page_service_default() {
	return apply_filters(
		'hantus_get_page_service_default', json_encode(
			 array(
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service01.png',
					'title'           => esc_html__( 'Oil Massage', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_001',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service02.png',
					'title'           => esc_html__( 'Skin Care', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_002',
				
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service03.png',
					'title'           => esc_html__( 'Natural Relaxation', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_003',
			
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service04.png',
					'title'           => esc_html__( 'Nails Design', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service05.png',
					'title'           => esc_html__( 'Make Up', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service06.png',
					'title'           => esc_html__( 'Waxing', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service07.png',
					'title'           => esc_html__( 'Hair Care', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
				array(
					'image_url'       => get_template_directory_uri() . '/assets/images/service/service08.png',
					'title'           => esc_html__( 'Sauna', 'hantus-pro' ),
					'text'            => esc_html__( 'Lorem Ipsum is simply dummy text of  the printing and typesetting.', 'hantus-pro' ),
					'pricing'            => esc_html__( '$57.99', 'hantus-pro' ),
					'text2'	  =>  esc_html__( 'Book now', 'hantus-pro' ),
					'link'	  =>  esc_html__( '#', 'hantus-pro' ),
					'id'              => 'customizer_repeater_service_004',
					
				),
			)
		)
	);
}