<?php
function hantus_custom_code( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Custom Section Panel
	=========================================*/
		$wp_customize->add_section(
			'hantus_custom_setting', array(
				'title' => esc_html__( 'Custom Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 128, 'hantus_Custom' ),
			)
		);
		
	// Setting  Head 
	$wp_customize->add_setting(
		'custom_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'custom_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'hantus_custom_setting',
		)
	);
	
	// Custom Code Settings Section // 
	$wp_customize->add_setting( 
		'hide_show_custom_section' , 
			array(
			'default' =>  0,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_custom_section', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'hantus_custom_setting',
			'settings'    => 'hide_show_custom_section',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header Settings Section // 
	
	
	//  Head 
	$wp_customize->add_setting(
		'custom_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'custom_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'hantus_custom_setting',
		)
	);
	
	// Custom Section Title // 
	$wp_customize->add_setting(
    	'custom_section_title',
    	array(
	        'default'			=> __('Title','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'custom_section_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'hantus_custom_setting',
			'settings' => 'custom_section_title',
			'type' => 'text',
		)  
	);
	
	// Custom Section Description // 
	$wp_customize->add_setting(
    	'custom_section_description',
    	array(
	        'default'			=> __('Description','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'custom_section_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'hantus_custom_setting',
			'settings' => 'custom_section_description',
			'type' => 'textarea',
		)  
	);
	
	//  Content Head 
	$wp_customize->add_setting(
		'custom_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'custom_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'hantus_custom_setting',
		)
	);
	
	// custom content // 
	
	$page_editor_path = trailingslashit( get_template_directory() ) . 'inc/custom-controls/editor/customizer-page-editor.php';
		if ( file_exists( $page_editor_path ) ) {
			require_once( $page_editor_path );
		}
	if ( class_exists( 'hantus_Page_Editor' ) ) {
		$frontpage_id = get_option( 'page_on_front' );
		$default = '';
		if ( ! empty( $frontpage_id ) ) {
			$default = get_post_field( 'post_content', $frontpage_id );
		}
		$wp_customize->add_setting(
			'hantus_page_editor', array(
				'default' => __('Custom Section Description','hantus-pro'),
				'sanitize_callback' => 'wp_kses_post',
				'transport'         => $selective_refresh,
				'priority' => 9,
				
			)
		);

		$wp_customize->add_control(
			new hantus_Page_Editor(
				$wp_customize, 'hantus_page_editor', array(
					'label' => esc_html__( 'Content', 'hantus-pro' ),
					'section' => 'hantus_custom_setting',
					'priority' => 10,
					'needsync' => true,
				)
			)
		);
	}
	$default = '';
	
	
}
add_action( 'customize_register', 'hantus_custom_code' );

// custom section selective refresh
function hantus_customs_section_partials( $wp_customize ){

	// hide_show_custom_section
	$wp_customize->selective_refresh->add_partial(
		'hide_show_custom_section', array(
			'selector' => '#custom_section',
			'container_inclusive' => true,
			'render_callback' => 'hantus_custom_setting',
			'fallback_refresh' => true,
		)
	);
	//info  section first
	$wp_customize->selective_refresh->add_partial( 'custom_section_title', array(
		'selector'            => '#custom_section .section-title h2',
		'settings'            => 'custom_section_title',
		'render_callback'  => 'customs_section_title_render_callback',
	
	) );
	
	$wp_customize->selective_refresh->add_partial( 'custom_section_description', array(
		'selector'            => '#custom_section .section-title p',
		'settings'            => 'custom_section_description',
		'render_callback'  => 'customs_section_dis_render_callback',
	
	) );
	
	$wp_customize->selective_refresh->add_partial( 'hantus_page_editor', array(
		'selector'            => '#custom_section .custom_editor',
		'settings'            => 'hantus_page_editor',
		'render_callback'  => 'customs_section_editor_render_callback',
	
	) );
	}

add_action( 'customize_register', 'hantus_customs_section_partials' );

// cta editor
function customs_section_title_render_callback() {
	return get_theme_mod( 'custom_section_title' );
}
// cta button label
function customs_section_dis_render_callback() {
	return get_theme_mod( 'custom_section_description' );
}

function customs_section_editor_render_callback() {
	return get_theme_mod( 'hantus_page_editor' );
}