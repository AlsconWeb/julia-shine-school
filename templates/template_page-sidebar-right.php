<?php
/**
Template Name: Page Right Sidebar
**/

get_header();
?>

<section id="blog-content" class="full-width section-padding">
        <div class="container">
           <div class="row">	
				
				<div class="col-lg-8 col-md-12" >
					<div class="site-content">
						<?php the_post(); the_content(); ?>
					</div>
					
					<?php comments_template( '', true ); // show comments ?>	
				</div><!-- /.col -->
				<?php get_sidebar(); ?>	
			</div><!-- /.row -->
		
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>

