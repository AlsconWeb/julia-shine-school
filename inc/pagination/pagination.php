<?php 
class Hantus_pagination
{	function Hantus_page($curpage, $post_type_data)
	{	?>
			 <div class="container">	
				<nav class="pagination" aria-label="Page navigation example">
					<ul class="pagination justify-content-center">
					<!-- Pagination -->
						
						<div class="paginations">
							<?php
							if($curpage != 1  )	{  
								echo ' <li class="page-item active disabled"><a class="page-link" href="'.get_pagenum_link(($curpage-1 > 0 ? $curpage-1 : 1)).'"></a></li>'; 
							}
							
							for($i=1;$i<=$post_type_data->max_num_pages;$i++)	{
								echo '<li class="page-item"><a class="'.($i == $curpage ? 'active ' : '').'" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
							}
							
							if($i-1!= $curpage) { 
								echo ' <li class="page-item"><a class="page-link" href="'.get_pagenum_link(($curpage+1 <= $post_type_data->max_num_pages ? $curpage+1 : $post_type_data->max_num_pages)).'"></a></li>'; 
							}
							?>
						</div>
					</ul>
				</nav>	
						<!-- Pagination -->
				</div>
					
		<?php
	} 
}
?>