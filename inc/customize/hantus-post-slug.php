<?php
function hantus_slug_setting( $wp_customize ) {
	/*=========================================
	Slug Settings Panel
	=========================================*/
	$wp_customize->add_panel( 
		'slug_section', 
		array(
			'priority'      => 36,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Custom Slug', 'hantus-pro'),
			'description' 	=> __('This is the Home Page Sectons. Here You can Manage Content of Home Page Sections.','hantus-pro'),
		) 
	);

	/*=========================================
	Post Slug Section
	=========================================*/
	// Post Slug Setting Section // 
	$wp_customize->add_section(
        'slug_setting',
        array(
        	'priority'      => 2,
            'title' 		=> __('Post Slug','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'slug_section',
		)
    );
	
	// Portfolio Post Slug // 
	$wp_customize->add_setting(
    	'portfolio_slug',
    	array(
	        'default'			=> __('portfolio','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'portfolio_slug',
		array(
		    'label'   => __('Portfolio Slug','hantus-pro'),
		    'section' => 'slug_setting',
			'settings'=> 'portfolio_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);
	
	// price Post Slug // 
	$wp_customize->add_setting(
    	'pricing_slug',
    	array(
	        'default'			=> __('pricing','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'pricing_slug',
		array(
		    'label'   => __('Price Slug','hantus-pro'),
		    'section' => 'slug_setting',
			'settings'=> 'pricing_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);
	
	// menu Post Slug // 
	$wp_customize->add_setting(
    	'menu_item_slug',
    	array(
	        'default'			=> __('menu','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'menu_item_slug',
		array(
		    'label'   => __('Menu Slug','hantus-pro'),
		    'section' => 'slug_setting',
			'settings'=> 'menu_item_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);
	
	
	
	/*=========================================
	Texonomy Slug Section
	=========================================*/
	// Texonomy Slug Setting Section // 
	$wp_customize->add_section(
        'texo_slug_setting',
        array(
        	'priority'      => 2,
            'title' 		=> __('Texonomy Slug','hantus-pro'),
            'description' 	=>'',
			'panel'  		=> 'slug_section',
		)
    );
	
	
	// Portfolio Category Slug // 
	$wp_customize->add_setting(
    	'texo_portfolio_slug',
    	array(
	        'default'			=> __('portfolio_category','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'texo_portfolio_slug',
		array(
		    'label'   => __('Portfolio Category Slug','hantus-pro'),
		    'section' => 'texo_slug_setting',
			'settings'=> 'texo_portfolio_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);	
	
	// Pricing Category Slug // 
	$wp_customize->add_setting(
    	'texo_pricing_slug',
    	array(
	        'default'			=> __('pricing_category','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'texo_pricing_slug',
		array(
		    'label'   => __('Pricing Category Slug','hantus-pro'),
		    'section' => 'texo_slug_setting',
			'settings'=> 'texo_pricing_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);	

	// menu Category Slug // 
	$wp_customize->add_setting(
    	'texo_menu_slug',
    	array(
	        'default'			=> __('menu_item_category','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
		)
	);	

	$wp_customize->add_control( 
		'texo_menu_slug',
		array(
		    'label'   => __('Menu Category Slug','hantus-pro'),
		    'section' => 'texo_slug_setting',
			'settings'=> 'texo_menu_slug',
			'type' => 'text',
			'description'    => __( '', 'hantus-pro' ),
		)  
	);	
}
add_action( 'customize_register', 'hantus_slug_setting' );