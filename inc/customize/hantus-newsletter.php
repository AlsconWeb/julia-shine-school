<?php
function hantus_newsletter_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Testimonial Section Panel
	=========================================*/
		$wp_customize->add_section(
			'newsletter_setting', array(
				'title' => esc_html__( 'Newsletter Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 45, 'hantus_Newsletter' ),
			)
		);
	/*=========================================
	newsletter Settings Section
	=========================================*/
	
	// Setting  Head 
	$wp_customize->add_setting(
		'newsletter_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'newsletter_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'newsletter_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'hide_show_newsletter' , 
			array(
			'default' =>  esc_html__( '1', 'hantus-pro' ),
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_newsletter', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'newsletter_setting',
			'settings'    => 'hide_show_newsletter',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// newsletter Content Section //
		
	// Content  Head 
	$wp_customize->add_setting(
		'newsletter_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'newsletter_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'newsletter_setting',
		)
	);
	
	// Newsletter Icon // 
	$wp_customize->add_setting(
    	'newsletter_icon',
    	array(
	        'default'			=>  __('fa-envelope','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize,
		'newsletter_icon',
		array(
		    'label'   => __('Icon','hantus-pro'),
		    'section' => 'newsletter_setting',
			'settings'   	 => 'newsletter_icon',
			'iconset' => 'fa',
		))  
	);
	
	
	//newsletter title//
	
	$wp_customize->add_setting(
    	'newsletter_title',
    	array(
	        'default'			=> __('SIGN UP FOR NEWS AND OFFRERS','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'newsletter_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'newsletter_setting',
			'settings'   	 => 'newsletter_title',
			'type'           => 'text',
		)  
	);
	//newsletter description//
	
	$wp_customize->add_setting(
    	'newsletter_description',
    	array(
	        'default'			=> __('Subcribe to lastest smartphones news & great deals we offer','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 8,
		)
	);	
	
	$wp_customize->add_control( 
		'newsletter_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'newsletter_setting',
			'settings'   	 => 'newsletter_description',
			'type'           => 'textarea',
		)  
	);
	
	
	//newsletter Shortcode //
	$wp_customize->add_setting(
    	'newsletter_shortcode',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'priority' => 9,
		)
	);	
	
	$wp_customize->add_control( 
		'newsletter_shortcode',
		array(
		    'label'   => __('Shortcode','hantus-pro'),
		    'section' => 'newsletter_setting',
			'settings'   	 => 'newsletter_shortcode',
			'type'           => 'textarea',
		)  
	);
}

add_action( 'customize_register', 'hantus_newsletter_setting' );

// Team selective refresh
function hantus_home_newsletter_section_partials( $wp_customize ){

	// hide_show_newsletter
	$wp_customize->selective_refresh->add_partial(
		'hide_show_newsletter', array(
			'selector' => '#subscribe',
			'container_inclusive' => true,
			'render_callback' => 'newsletter_setting',
			'fallback_refresh' => true,
		)
	);
	// icon
	$wp_customize->selective_refresh->add_partial( 'newsletter_icon', array(
		'selector'            => '#subscribe i',
		'settings'            => 'newsletter_icon',
		'render_callback'  => 'home_section_news_icon_render_callback',
	
	) );
	// title
	$wp_customize->selective_refresh->add_partial( 'newsletter_title', array(
		'selector'            => '#subscribe h3',
		'settings'            => 'newsletter_title',
		'render_callback'  => 'home_section_news_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'newsletter_description', array(
		'selector'            => '#subscribe p',
		'settings'            => 'newsletter_description',
		'render_callback'  => 'home_section_news_desc_render_callback',
	
	) );
	
	// button label
	$wp_customize->selective_refresh->add_partial( 'newsletter_button_lbl', array(
		'selector'            => '#subscribe-form button',
		'settings'            => 'newsletter_button_lbl',
		'render_callback'  => 'home_section_news_button_render_callback',
	
	) );
	// placeholder
	$wp_customize->selective_refresh->add_partial( 'newsletter_placeholder', array(
		'selector'            => '#subscribe-form',
		'settings'            => 'newsletter_placeholder',
		'render_callback'  => 'home_section_news_place_render_callback',
	
	) );
	
	}

add_action( 'customize_register', 'hantus_home_newsletter_section_partials' );

// icon
function home_section_news_icon_render_callback() {
	return get_theme_mod( 'newsletter_icon' );
}
// title
function home_section_news_title_render_callback() {
	return get_theme_mod( 'newsletter_title' );
}
// description
function home_section_news_desc_render_callback() {
	return get_theme_mod( 'newsletter_description' );
}

// button label
function home_section_news_button_render_callback() {
	return get_theme_mod( 'newsletter_button_lbl' );
}

// placeholder
function home_section_news_place_render_callback() {
	return get_theme_mod( 'newsletter_placeholder' );
}