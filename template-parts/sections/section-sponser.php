<!-- Start: Fun Fact
    ============================= -->
<?php
/**
 * Get default values for funfact section.
 *
 * @since 1.0
 * @access public
 */
$default_content 			= hantus_get_sponsers_default();
$hide_show_sponser			= get_theme_mod('hide_show_sponser','1');
$sponser_contents			= get_theme_mod('sponser_contents',$default_content);
$sponsor_animation_speed	= get_theme_mod('sponsor_animation_speed','3000'); 
$sponsor_count				= get_theme_mod('sponsor_count','6'); 
$settings					=array('animationSpeed'=>$sponsor_animation_speed,'items'=>$sponsor_count);
wp_register_script('hantus-sponsor',get_template_directory_uri().'/assets/js/homepage/sponsor.js',array('jquery'));
wp_localize_script('hantus-sponsor','sponsor_settings',$settings);
wp_enqueue_script('hantus-sponsor');				
 if($hide_show_sponser == '1') { 
 hantus_before_sponsor_section_trigger();
?>
	    <!-- Start: Our partner
    ============================= -->

    <section id="partner" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<div class="partner-carousel">
					<?php
						if ( ! empty( $sponser_contents ) ) {
						$allowed_html = array(
						'br'     => array(),
						'em'     => array(),
						'strong' => array(),
						'b'      => array(),
						'i'      => array(),
						);
						$sponser_contents = json_decode( $sponser_contents );
						foreach ( $sponser_contents as $sponser_item ) {
							$link = ! empty( $sponser_item->link ) ? apply_filters( 'hantus_translate_single_string', $sponser_item->link, 'sponser section' ) : '';
							$image = ! empty( $sponser_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $sponser_item->image_url, 'sponser section' ) : '';
					?>
						
							<div class="single-partner">
								<div class="inner-partner">
									<a href="<?php echo esc_url( $link ); ?>">
										<?php if ( ! empty( $image ) ) : ?>
										<img src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
									<?php endif; ?>
									</a>
								</div>
							</div>
						
					<?php } }?>	
					</div>				
                </div>
            </div>

        </div>
    </section>
    <!-- End: Our Partner
    ============================= -->
 <?php hantus_after_sponsor_section_trigger(); } ?>
    <!-- End: Fun Fact
    ============================= -->