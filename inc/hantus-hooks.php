<?php
/**
 * List of available hooks
 *
 */
 
/*  Header section */
/**
 * Hook just before the Header
 *
 */ 
function hantus_before_header_section_trigger() {
	do_action( 'hantus_before_header_section_hook' );
}

/**
 * Hook just After the Header
 *
 */ 
function hantus_after_header_section_trigger() {
	do_action( 'hantus_after_header_section_hook' );
}

/*  Slider section */
/**
 * Hook just before the Slider
 *
 */ 
function hantus_before_slider_section_trigger() {
	do_action( 'hantus_before_slider_section_hook' );
}

/**
 * Hook just After the Slider
 *
 */ 
function hantus_after_slider_section_trigger() {
	do_action( 'hantus_after_slider_section_hook' );
}

/*  Info section */
/**
 * Hook just before the Info
 *
 */ 
function hantus_before_info_section_trigger() {
	do_action( 'hantus_before_info_section_hook' );
}

/**
 * Hook just After the Info
 *
 */ 
function hantus_after_info_section_trigger() {
	do_action( 'hantus_after_info_section_hook' );
}

/*  Service section */
/**
 * Hook just before the Service
 *
 */ 
function hantus_before_service_section_trigger() {
	do_action( 'hantus_before_service_section_hook' );
}

/**
 * Hook just After the Service
 *
 */ 
function hantus_after_service_section_trigger() {
	do_action( 'hantus_after_service_section_hook' );
}

/*  Portfolio section */
/**
 * Hook just before the Portfolio
 *
 */ 
function hantus_before_portfolio_section_trigger() {
	do_action( 'hantus_before_portfolio_section_hook' );
}

/**
 * Hook just After the Portfolio
 *
 */ 
function hantus_after_portfolio_section_trigger() {
	do_action( 'hantus_after_portfolio_section_hook' );
}

/*  Feature section */
/**
 * Hook just before the Feature
 *
 */ 
function hantus_before_feature_section_trigger() {
	do_action( 'hantus_before_feature_section_hook' );
}

/**
 * Hook just After the Feature
 *
 */ 
function hantus_after_feature_section_trigger() {
	do_action( 'hantus_after_feature_section_hook' );
}

/*  Pricing section */
/**
 * Hook just before the Pricing
 *
 */ 
function hantus_before_pricing_section_trigger() {
	do_action( 'hantus_before_pricing_section_hook' );
}

/**
 * Hook just After the Pricing
 *
 */ 
function hantus_after_pricing_section_trigger() {
	do_action( 'hantus_after_pricing_section_hook' );
}

/*  Funfact section */
/**
 * Hook just before the Funfact
 *
 */ 
function hantus_before_funfact_section_trigger() {
	do_action( 'hantus_before_funfact_section_hook' );
}

/**
 * Hook just After the Funfact
 *
 */ 
function hantus_after_funfact_section_trigger() {
	do_action( 'hantus_after_funfact_section_hook' );
}

/*  Product section */
/**
 * Hook just before the Product
 *
 */ 
function hantus_before_product_section_trigger() {
	do_action( 'hantus_before_product_section_hook' );
}

/**
 * Hook just After the Product
 *
 */ 
function hantus_after_product_section_trigger() {
	do_action( 'hantus_after_product_section_hook' );
}

/*  Testimonial section */
/**
 * Hook just before the Testimonial
 *
 */ 
function hantus_before_testimonial_section_trigger() {
	do_action( 'hantus_before_testimonial_section_hook' );
}

/**
 * Hook just After the Testimonial
 *
 */ 
function hantus_after_testimonial_section_trigger() {
	do_action( 'hantus_after_testimonial_section_hook' );
}

/*  Team section */
/**
 * Hook just before the Team
 *
 */ 
function hantus_before_team_section_trigger() {
	do_action( 'hantus_before_team_section_hook' );
}

/**
 * Hook just After the Team
 *
 */ 
function hantus_after_team_section_trigger() {
	do_action( 'hantus_after_team_section_hook' );
}

/*  Appointment section */
/**
 * Hook just before the Appointment
 *
 */ 
function hantus_before_appointment_section_trigger() {
	do_action( 'hantus_before_appointment_section_hook' );
}

/**
 * Hook just After the Appointment
 *
 */ 
function hantus_after_appointment_section_trigger() {
	do_action( 'hantus_after_appointment_section_hook' );
}

/*  Sponsor section */
/**
 * Hook just before the Sponsor
 *
 */ 
function hantus_before_sponsor_section_trigger() {
	do_action( 'hantus_before_sponsor_section_hook' );
}

/**
 * Hook just After the Sponsor
 *
 */ 
function hantus_after_sponsor_section_trigger() {
	do_action( 'hantus_after_sponsor_section_hook' );
}

/*  Newsletter section */
/**
 * Hook just before the Newsletter
 *
 */ 
function hantus_before_newsletter_section_trigger() {
	do_action( 'hantus_before_newsletter_section_hook' );
}

/**
 * Hook just After the Newsletter
 *
 */ 
function hantus_after_newsletter_section_trigger() {
	do_action( 'hantus_after_newsletter_section_hook' );
}

/*  Blog section */
/**
 * Hook just before the Blog
 *
 */ 
function hantus_before_blog_section_trigger() {
	do_action( 'hantus_before_blog_section_hook' );
}

/**
 * Hook just After the Blog
 *
 */ 
function hantus_after_blog_section_trigger() {
	do_action( 'hantus_after_blog_section_hook' );
}

/*  Widget section */
/**
 * Hook just before the Widget
 *
 */ 
function hantus_before_widget_section_trigger() {
	do_action( 'hantus_before_widget_section_hook' );
}

/**
 * Hook just After the Widget
 *
 */ 
function hantus_after_widget_section_trigger() {
	do_action( 'hantus_after_widget_section_hook' );
}

/*  Footer section */
/**
 * Hook just before the Footer
 *
 */ 
function hantus_before_footer_section_trigger() {
	do_action( 'hantus_before_footer_section_hook' );
}

/**
 * Hook just After the Footer
 *
 */ 
function hantus_after_footer_section_trigger() {
	do_action( 'hantus_after_footer_section_hook' );
}

/*  About Page Welcome section */
/**
 * Hook just before the About Page Welcome
 *
 */ 
function hantus_before_abt_welcome_section_trigger() {
	do_action( 'hantus_before_abt_welcome_section_hook' );
}

/**
 * Hook just After the About Page Welcome
 *
 */ 
function hantus_after_abt_welcome_section_trigger() {
	do_action( 'hantus_after_abt_welcome_section_hook' );
}

/*  About Page About section */
/**
 * Hook just before the About Page About
 *
 */ 
function hantus_before_abt_about_section_trigger() {
	do_action( 'hantus_before_abt_about_section_hook' );
}

/**
 * Hook just After the About Page About
 *
 */ 
function hantus_after_abt_about_section_trigger() {
	do_action( 'hantus_after_abt_about_section_hook' );
}

/*  About Page Testimonial section */
/**
 * Hook just before the About Page Testimonial
 *
 */ 
function hantus_before_abt_testimonial_section_trigger() {
	do_action( 'hantus_before_abt_testimonial_section_hook' );
}

/**
 * Hook just After the About Page Testimonial
 *
 */ 
function hantus_after_abt_testimonial_section_trigger() {
	do_action( 'hantus_after_abt_testimonial_section_hook' );
}

/*  About Page Sponsor section */
/**
 * Hook just before the About Page Sponsor
 *
 */ 
function hantus_before_abt_sponsor_section_trigger() {
	do_action( 'hantus_before_abt_sponsor_section_hook' );
}

/**
 * Hook just After the About Page Sponsor
 *
 */ 
function hantus_after_abt_sponsor_section_trigger() {
	do_action( 'hantus_after_abt_sponsor_section_hook' );
}

/*  Service Page Service section */
/**
 * Hook just before the Service Page Service
 *
 */ 
function hantus_before_serv_page_section_trigger() {
	do_action( 'hantus_before_serv_page_section_hook' );
}

/**
 * Hook just After the Service Page Service
 *
 */ 
function hantus_after_serv_page_section_trigger() {
	do_action( 'hantus_after_serv_page_section_hook' );
}

/*  Pricing Page Price section */
/**
 * Hook just before the Pricing Page Price
 *
 */ 
function hantus_before_pricing_page_section_trigger() {
	do_action( 'hantus_before_pricing_page_section_hook' );
}

/**
 * Hook just After the Pricing Page Price
 *
 */ 
function hantus_after_pricing_page_section_trigger() {
	do_action( 'hantus_after_pricing_page_section_hook' );
}

/*  Contact Page contact Section */
/**
 * Hook just before the Contact Page
 *
 */ 
function hantus_before_contact_page_section_trigger() {
	do_action( 'hantus_before_contact_page_section_hook' );
}

/**
 * Hook just After the Contact Page
 *
 */ 
function hantus_after_contact_page_section_trigger() {
	do_action( 'hantus_after_contact_page_section_hook' );
}

/*  Gallery Page Gallery Section */
/**
 * Hook just before the Gallery Page
 *
 */ 
function hantus_before_gallery_page_section_trigger() {
	do_action( 'hantus_before_gallery_page_section_hook' );
}

/**
 * Hook just After the Gallery Page
 *
 */ 
function hantus_after_gallery_page_section_trigger() {
	do_action( 'hantus_after_gallery_page_section_hook' );
}

/*  Portfolio Page Portfolio Section */
/**
 * Hook just before the Portfolio Page Portfolio 
 *
 */ 
function hantus_before_port_page_section_trigger() {
	do_action( 'hantus_before_port_page_section_hook' );
}

/**
 * Hook just After the Portfolio Page Portfolio 
 *
 */ 
function hantus_after_port_page_section_trigger() {
	do_action( 'hantus_after_port_page_section_section_hook' );
}
?>