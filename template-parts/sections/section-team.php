<?php
/**
 * Get default values for features section.
 *
 * @since 1.0
 * @access public
 */
$default_content 		= hantus_get_team_default();
$team_contents 			= get_theme_mod('team_contents',$default_content);
$butician_sec_columns	= get_theme_mod('butician_sec_columns','3');
$hide_show_team 		= get_theme_mod('hide_show_team','1');
if($hide_show_team == '1') { 
hantus_before_team_section_trigger();
?>
    <!-- Start: Expert Beauticians
    ============================= -->
    
    <section id="beauticians" class="custom-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
					<?php 
						$team_title = get_theme_mod('team_title','Our Team');
						$team_description = get_theme_mod('team_description','These are the people behind our success and failures. These guys never lose a heart.');
						if(($team_title) || ($team_title)!='' ) { 
					?>
                    <div class="section-title">
                        <?php if($team_title) {?>
							<h2>
								<?php echo $team_title; ?>
							</h2>
						<?php } ?>
						<?php if($team_description) {?>
							<p><?php echo $team_description; ?></p>
						<?php } ?>
                    </div>
					<?php } ?>
                </div>                
            </div>

            <div class="row" id="team-contents">
				<?php
			
					$team_contents = json_decode($team_contents);
					if( $team_contents!='' )
					{
					foreach($team_contents as $team_item){
					$image    = ! empty( $team_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $team_item->image_url, 'Team section' ) : '';
					$title    = ! empty( $team_item->title ) ? apply_filters( 'hantus_translate_single_string', $team_item->title, 'Team section' ) : '';
					$designation = ! empty( $team_item->designation ) ? apply_filters( 'hantus_translate_single_string', $team_item->designation, 'Team section' ) : '';
					$description = ! empty( $team_item->text ) ? apply_filters( 'hantus_translate_single_string', $team_item->text, 'Team section' ) : '';
					
				?>
                <div class="col-lg-<?php echo $butician_sec_columns; ?> col-md-6 col-sm-6 butician-sec-columns">
                    <div class="single-beauticians">
                        <div class="img-wrapper">
                            <?php if ( ! empty( $image ) ) : ?>
								<?php
									echo '<img class="img" src="' . esc_url( $image ) . '"';
										if ( ! empty( $title ) ) {
											echo 'alt="' . esc_attr( $title ) . '" title="' . esc_attr( $title ) . '"';
										}
										echo '/>';
								?>
							<?php endif; ?>
                            <div class="beautician-footer-text">
								<?php if ( ! empty( $title ) ) : ?>
									<h5><?php echo esc_html( $title ); ?></h5>
								<?php endif; ?>	
								<?php if ( ! empty( $designation ) ) : ?>
									<p><?php echo esc_html( $designation ); ?></p>
								<?php endif; ?>		
                            </div>
                        </div>
                        <div class="beautician-content">
                            <div class="inner-content">
								<?php if ( ! empty( $title ) ) : ?>
									<h5><?php echo esc_html( $title ); ?></h5>
								<?php endif; ?>		
								<?php if ( ! empty( $designation ) ) : ?>
									<p class="title"><?php echo esc_html( $designation ); ?></p>
								<?php endif; ?>	
								<?php if ( ! empty( $description ) ) : ?>
									<p><?php echo esc_html( $description ); ?></p>
								<?php endif; ?>
                                <ul class="social">
									<?php if ( ! empty( $team_item->social_repeater ) ) :
									$icons         = html_entity_decode( $team_item->social_repeater );
									$icons_decoded = json_decode( $icons, true );
									if ( ! empty( $icons_decoded ) ) : ?>
									<?php
										foreach ( $icons_decoded as $value ) {
											$social_icon = ! empty( $value['icon'] ) ? apply_filters( 'hantus_translate_single_string', $value['icon'], 'Team section' ) : '';
											$social_link = ! empty( $value['link'] ) ? apply_filters( 'hantus_translate_single_string', $value['link'], 'Team section' ) : '';
											if ( ! empty( $social_icon ) ) {
									?>	
										<li><a  href="<?php echo esc_url( $social_link ); ?>" ><i class="fa <?php echo esc_attr( $social_icon ); ?> "></i></a></li>
									<?php
												}
											}
										endif;
									endif;
									?>	
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
					<?php }} ?>
			</div>
        </div>
    </section>

    <!-- End: Expert Beauticians
    ============================= -->
 <?php hantus_after_team_section_trigger();  } ?>