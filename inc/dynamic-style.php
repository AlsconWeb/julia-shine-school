<?php
if( ! function_exists( 'hantus_dynamic_styles' ) ):
    function hantus_dynamic_styles() {
			$output_css = '';
			$slider_overlay_enable 				 = get_theme_mod('slider_overlay_enable');
			$slide_overlay_color 				 = get_theme_mod('slide_overlay_color','#fff');
			$slider_opacity						 = get_theme_mod('slider_opacity','0.2'); 
			$slide_title_color 					 = get_theme_mod('slide_title_color','#fff');
			$slide_sbtitle_color 				 = get_theme_mod('slide_sbtitle_color','#f22853');
			$slide_desc_color 					 = get_theme_mod('slide_desc_color','#fff');
			 $output_css = '';
			if($slider_overlay_enable == '1') { 
				$output_css .=".header-single-slider:after {
					opacity: " .esc_attr($slider_opacity). ";
					background: " .esc_attr($slide_overlay_color). ";
				}\n";
			}
			
			$output_css .="#slider .theme-content h3 {
					color: " .esc_attr($slide_title_color). ";
				}#slider .theme-content h1 {
					color: " .esc_attr($slide_sbtitle_color). ";
				}#slider .theme-content p {
					color: " .esc_attr($slide_desc_color). ";
				}\n";
				
			$theme_color_enable	= get_theme_mod('theme_color_enable','off');
			$primary_color 		= get_theme_mod('primary_color','#f22853');
			$secondary_color 	= get_theme_mod('secondary_color','#1c314c');
			
			if($theme_color_enable == '1') { 
			$output_css .=".woocommerce-store-notice, p.woocommerce-store-notice, p.demo_store,.select2-container--default li.select2-results__option--highlighted[aria-selected],.select2-container--default li.select2-results__option--highlighted[data-selected],.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:focus,.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:hover,.woocommerce nav.woocommerce-pagination ul.page-numbers li span.current.page-numbers,.btn-info, .pricing-box h3:before, .pricing-box h3:after, .section-title h2:before, .section-title h2:after, .service-box h4:before, .service-box h4:after, .contact-wrapper, #footer-widgets .widget-title:after,#subscribe,.boxed-btn,.days li .active,.feature-box:hover .feature-icon,.feature-box:hover .feature-icon:after,.gallery-page .gallery-item figcaption ul li a:hover,.gallery-tab-sorting li a:after,.header-slider .boxed-btn,.header-slider .owl-next:hover,.header-slider .owl-prev:hover,.header-social li a:hover,.header-social li a:focus,.main-menu>ul>li a:before,.main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a, .nav-tabs .nav-link.active,.portfolio-tab-sorting li a.active,.preloader,.pricing-box hr:after,.pricing-box hr:before,.pricing-box:hover .boxed-btn,.product-carousel .owl-next:hover,.product-carousel .owl-prev:hover,.recomended-text,.scrollup,.search-result .posts-navigation .nav-links a,.section-title hr:after,.section-title hr:before,.service-box figure figcaption .boxed-btn,.single-beauticians .social li a:hover,.shop-product .overlay li a:hover,.shop-product .sale:not(.product),.subscribe-wrapper button,.weekdays,.widget-search input[type=button],.widget-tag li a:hover,.widget_search input[type=submit],li.page-item .page-link:hover,li.page-item.active .page-link,.calendar_wrap thead tr,.pagination a:hover,.pagination span,.widget_product_search button[type=submit],.widget_product_tag_cloud .tagcloud a:hover,.widget_search input[type=button],.widget_tag_cloud .tagcloud a:hover,.woocommerce #payment #place_order,.woocommerce #respond input#submit,.woocommerce .price_slider_wrapper .ui-slider .ui-slider-handle,.woocommerce .price_slider_wrapper .ui-slider-horizontal .ui-slider-range,.woocommerce a.button,.woocommerce button.button,.woocommerce button.button.alt,.woocommerce input.button,.woocommerce input.button.alt,.woocommerce input.button:disabled,.woocommerce input.button:disabled[disabled],.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,.woocommerce-cart table.cart input.button,.woocommerce-page #payment #place_order,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart,button,input.search-btn,input[type=button],input[type=reset],p.form-submit .submit,table th,table#wp-calendar tfoot,td#today,.recomended .boxed-btn,.wpcf7-form .wpcf7-submit {
					background: " .esc_attr($primary_color). ";
				}.woocommerce span.onsale,
				.product-carousel .owl-nav [class*=owl-]:hover,
				.thaispa-theme .inner-thaispa .inner-text-thaispa:after,
				.thaispa-theme .meta-info li.post-date a,
				.thaispa-theme .blog-post a.more-link:hover,
				.thaispa-theme .blog-post a.more-link:focus,
				.thaispa-theme .inner-thaispa .inner-text-thaispa:after,
				.thaispa-theme .strip-hover .strip-hover-wrap:before,
				.thaispa-theme .strip-hover .strip-hover-wrap:after,
				.thaispa-theme .strip-hover .strip-overlay:before,
				.thaispa-theme .strip-hover .strip-overlay:after,
				.thaispa-theme.header-transparent .cart-count {
					background-color: " .esc_attr($primary_color). ";
				}\n";
				
			$output_css .="a.shipping-calculator-button,.woocommerce #review_form #respond input#submit:hover,div.woocommerce-info a,p.woocommerce-info a,div.woocommerce-info::before,p.woocommerce-info::before,.woocommerce-account .addresses .title .edit,.woocommerce table.my_account_orders td.woocommerce-orders-table__cell-order-number a,.woocommerce-account .woocommerce-MyAccount-content p a,.blog-author-social a, .meta-info li a:hover, .meta-info li a:focus, .widget_product_search button[type=submit]:hover, .widget_product_search button[type=submit]:focus, .service-box figure figcaption .boxed-btn:hover, .service-box figure figcaption .boxed-btn:focus, .contact-wrapper a.btn-info, #slider .theme-content h1, .header-info-text .icons-info .icons, #breadcrumb-area ul li a,#counter .single-box i,#header-top p i, #header-top ul:not(.header-social) li i,#subscribe-form button,#wcu ul li:before,#welcome .section-title h3,.blog-post .post-title a:hover,.blog-post:hover a.read-more-link,.cart-remove:hover,.coming-soon-wrapper h1 span,.comment-date:hover,.comment-metadata a:hover,.copyright a,.copyright-text a:hover,.count-area ul li,.gallery-page .gallery-item figcaption ul li a,.gallery-tab-sorting li a.active,.gallery-tab-sorting li a:hover,.header-slider h3,.info-box i,.main-menu li.c-dropdowns.active li>a:hover,.main-menu li.c-dropdowns.focus li>a:hover,.main-menu ul li ul.cr-dropdown-menu li.active>a,.main-menu ul li ul.cr-dropdown-menu li.focus>a,.mc4wp-form input[type=submit],.media ul li.comment-replay,.meta-data ul li a:hover,.nav-tabs a.nav-link:hover,.recent-post h6:hover,.sb-search-input,.scrollup:hover,.search-result .posts-navigation .nav-links a:hover,.service-box p.price,.sidenav .cart-item-description p,.sidenav .close-sidenav,.single-beauticians .social li a:not(:hover),.single-post .post-header .text-right li a:hover,.shop-product .overlay li a:not(:hover),.shop-product .price,.tab-content .tab-list h4 .price,.tab-content .tab-list h4:hover,.testimonial-carousel .owl-next:hover,.testimonial-carousel .owl-prev:hover,.welcome-box:hover a,.widget-acrhives li a:hover,.widget-search input[type=button]:hover,.widget_archive li a:hover,.widget_links ul li a:hover,.widget_search input[type=submit]:hover,.woo-container .button-cart .added_to_cart,.wp-calendar-nav a,div#calendar_wrap tbody a,li.latest-news h6:hover,#recent-posts-2 ul li a:hover,.pricing-box:not(:hover):not(.recomended) .boxed-btn,.product_meta a:hover,.widget_categories ul li a:hover,.widget_meta ul li a:hover,.widget_nav_menu ul li a:hover,.widget_pages ul li a:hover,.widget_product_categories ul li a:hover,.widget_product_tag_cloud .tagcloud a:hover,.widget_products ul li a:hover,.widget_recent_comments ul li a:hover,.widget_recent_entries ul li a:hover,.widget_recently_viewed_products ul li a:hover,.widget_top_rated_products ul li a:hover,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,cite,em,h1.page-title,input.search-btn:hover,input[type=button],input[type=reset],input[type=submit]:hover,p.form-submit .submit:hover,q,.beautician-footer-text h5,.boxed-btn:hover,.boxed-btn:focus,.wpcf7-form .wpcf7-submit:hover, .wpcf7-form .wpcf7-submit:focus, .logged-in-as a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-cart-form__cart-item .product-name a, .boxed-btn:focus:before, .boxed-btn:focus:after, .wpcf7-form .wpcf7-submit:focus:before, .wpcf7-form .wpcf7-submit:focus:after, .form-submit .submit:focus:before, .form-submit .submit:focus:after, .pricing-box.recomended a.boxed-btn:focus, .pricing-box a.boxed-btn:focus, .header-slider .boxed-btn:focus, .main-menu ul.cr-dropdown-menu li.focus > a, .main-menu ul.cr-dropdown-menu li:hover > a,.wpcf7-form .wpcf7-submit:focus, a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart.added {
					color: " .esc_attr($primary_color). ";
				}.woocommerce div.product p.price, .woocommerce div.product span.price,
				.woocommerce .widget_layered_nav_filters ul li a:hover,
				.woocommerce .widget_layered_nav_filters ul li a:focus,
				.widget_recent_reviews a:hover,.widget_recent_reviews a:focus,
				a.woocommerce-review-link:hover,a.woocommerce-review-link:focus,
				.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:hover,
				.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:focus,
				.product-name a,
				.woocommerce .shop-product h5 a:hover,
				.woocommerce .shop-product h5 a:focus,
				.thaispa-theme .service-thaispa .price-thaispa h5,
				.post-comment-area h2,
				.blog-post .post-title a:focus,
				.blog-post ul:not(.meta-info) li a,
				.blog-post ol:not(.meta-info) li a,
				.blog-post table a, strong a, blockquote a, code a, p a,
				span a {
					color: " .esc_attr($primary_color). ";
				}
				.header-slider .theme-slider .theme-content h1 {
					color: " .esc_attr($primary_color). " !important;
				}\n";	
				
			$output_css .="a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:focus,div.woocommerce-info,p.woocommerce-info,.appoinment-wrapper input:focus,.appoinment-wrapper select:focus,.appoinment-wrapper textarea:focus,.feature-box:hover .feature-icon,.pagination span,.portfolio-tab-sorting li a:hover,.pricing-box .boxed-btn,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,.wpcf7-form input:focus,.wpcf7-form textarea:focus,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,input[type=email]:focus,input[type=text]:focus,input[type=url]:focus,li.page-item .page-link:hover,li.page-item.active .page-link,p.form-submit .submit:hover,textarea:focus, .wpcf7-form .wpcf7-submit:hover, .wpcf7-form .wpcf7-submit:focus {
					border-color: " .esc_attr($primary_color). ";
				}.thaispa-theme ._2Pfbi {
					border-color: " .esc_attr($primary_color). " !important;
				}\n";
			
			$output_css .=".input--filled .graphic--hantus,.input__field--hantus:focus+.input__label--hantus .graphic--hantus {
					stroke: " .esc_attr($primary_color). ";
				}\n";
			$output_css .=".woocommerce-MyAccount-navigation ul li.is-active a,.woocommerce-MyAccount-navigation ul li a:hover,.woocommerce-MyAccount-navigation ul li a:focus,blockquote {
					border-left: " .esc_attr($primary_color). ";
				}.coupon input[type='text'] {
					border-bottom: " .esc_attr($primary_color). ";
				}.boxed-btn,.pricing-box .boxed-btn,.recomended .boxed-btn {
					border-color: " .esc_attr($primary_color). ";
				}.pricing-box.recomended a.boxed-btn:hover, .pricing-box a.boxed-btn:hover {
					color: " .esc_attr($primary_color). ";
				}\n";

			$output_css .=".pricing-box.recomended:hover .boxed-btn, .main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a {
				color: #fff ;
			}\n";	
			}
			
			
			$theme_color 	= get_theme_mod('theme_color','#f22853');
			if($theme_color_enable !== '1') {
			$output_css .=".woocommerce-store-notice, p.woocommerce-store-notice, p.demo_store,.select2-container--default li.select2-results__option--highlighted[aria-selected],.select2-container--default li.select2-results__option--highlighted[data-selected],.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:focus,.woocommerce nav.woocommerce-pagination ul.page-numbers li a.page-numbers:hover,.woocommerce nav.woocommerce-pagination ul.page-numbers li span.current.page-numbers,.btn-info, .pricing-box h3:before, .pricing-box h3:after, .section-title h2:before, .section-title h2:after, .service-box h4:before, .service-box h4:after, .contact-wrapper, #footer-widgets .widget-title:after,#subscribe,.boxed-btn,.days li .active,.feature-box:hover .feature-icon,.feature-box:hover .feature-icon:after,.gallery-page .gallery-item figcaption ul li a:hover,.gallery-tab-sorting li a:after,.header-slider .boxed-btn,.header-slider .owl-next:hover,.header-slider .owl-prev:hover,.header-social li a:hover,.header-social li a:focus,.main-menu>ul>li a:before,.main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a, .nav-tabs .nav-link.active,.portfolio-tab-sorting li a.active,.preloader,.pricing-box hr:after,.pricing-box hr:before,.pricing-box:hover .boxed-btn,.product-carousel .owl-next:hover,.product-carousel .owl-prev:hover,.recomended-text,.scrollup,.search-result .posts-navigation .nav-links a,.section-title hr:after,.section-title hr:before,.service-box figure figcaption .boxed-btn,.single-beauticians .social li a:hover,.shop-product .overlay li a:hover,.shop-product .sale:not(.product),.subscribe-wrapper button,.weekdays,.widget-search input[type=button],.widget-tag li a:hover,.widget_search input[type=submit],li.page-item .page-link:hover,li.page-item.active .page-link,.calendar_wrap thead tr,.pagination a:hover,.pagination span,.widget_product_search button[type=submit],.widget_product_tag_cloud .tagcloud a:hover,.widget_search input[type=button],.widget_tag_cloud .tagcloud a:hover,.woocommerce #payment #place_order,.woocommerce #respond input#submit,.woocommerce .price_slider_wrapper .ui-slider .ui-slider-handle,.woocommerce .price_slider_wrapper .ui-slider-horizontal .ui-slider-range,.woocommerce a.button,.woocommerce button.button,.woocommerce button.button.alt,.woocommerce input.button,.woocommerce input.button.alt,.woocommerce input.button:disabled,.woocommerce input.button:disabled[disabled],.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,.woocommerce-cart table.cart input.button,.woocommerce-page #payment #place_order,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart,button,input.search-btn,input[type=button],input[type=reset],p.form-submit .submit,table th,table#wp-calendar tfoot,td#today,.recomended .boxed-btn,.wpcf7-form .wpcf7-submit {
					background: " .esc_attr($theme_color). ";
				}.woocommerce span.onsale,
				.product-carousel .owl-nav [class*=owl-]:hover,
				.thaispa-theme .inner-thaispa .inner-text-thaispa:after,
				.thaispa-theme .meta-info li.post-date a,
				.thaispa-theme .blog-post a.more-link:hover,
				.thaispa-theme .blog-post a.more-link:focus,
				.thaispa-theme .inner-thaispa .inner-text-thaispa:after,
				.thaispa-theme .strip-hover .strip-hover-wrap:before,
				.thaispa-theme .strip-hover .strip-hover-wrap:after,
				.thaispa-theme .strip-hover .strip-overlay:before,
				.thaispa-theme .strip-hover .strip-overlay:after,
				.thaispa-theme.header-transparent .cart-count {
					background-color: " .esc_attr($theme_color). ";
				}\n";
				
			$output_css .="a.shipping-calculator-button,.woocommerce #review_form #respond input#submit:hover,div.woocommerce-info a,p.woocommerce-info a,div.woocommerce-info::before,p.woocommerce-info::before,.woocommerce-account .addresses .title .edit,.woocommerce table.my_account_orders td.woocommerce-orders-table__cell-order-number a,.woocommerce-account .woocommerce-MyAccount-content p a,.blog-author-social a, .meta-info li a:hover, .meta-info li a:focus, .widget_product_search button[type=submit]:hover, .widget_product_search button[type=submit]:focus, .service-box figure figcaption .boxed-btn:hover, .service-box figure figcaption .boxed-btn:focus, .contact-wrapper a.btn-info, #slider .theme-content h1, .header-info-text .icons-info .icons, .header-info-text .info .info-subtitle .dot, #breadcrumb-area ul li a,#counter .single-box i,#header-top p i, #header-top ul:not(.header-social) li i,#subscribe-form button,#wcu ul li:before,#welcome .section-title h3,.blog-post .post-title a:hover,.blog-post:hover a.read-more-link,.cart-remove:hover,.coming-soon-wrapper h1 span,.comment-date:hover,.comment-metadata a:hover,.copyright a,.copyright-text a:hover,.count-area ul li,.gallery-page .gallery-item figcaption ul li a,.gallery-tab-sorting li a.active,.gallery-tab-sorting li a:hover,.header-slider h3,.info-box i,.main-menu li.c-dropdowns.active li>a:hover,.main-menu li.c-dropdowns.focus li>a:hover,.main-menu ul li ul.cr-dropdown-menu li.active>a,.main-menu ul li ul.cr-dropdown-menu li.focus>a,.mc4wp-form input[type=submit],.media ul li.comment-replay,.meta-data ul li a:hover,.nav-tabs a.nav-link:hover,.recent-post h6:hover,.sb-search-input,.scrollup:hover,.search-result .posts-navigation .nav-links a:hover,.service-box p.price,.sidenav .cart-item-description p,.sidenav .close-sidenav,.single-beauticians .social li a:not(:hover),.single-post .post-header .text-right li a:hover,.shop-product .overlay li a:not(:hover),.shop-product .price,.tab-content .tab-list h4 .price,.tab-content .tab-list h4:hover,.testimonial-carousel .owl-next:hover,.testimonial-carousel .owl-prev:hover,.welcome-box:hover a,.widget-acrhives li a:hover,.widget-search input[type=button]:hover,.widget_archive li a:hover,.widget_links ul li a:hover,.widget_search input[type=submit]:hover,.woo-container .button-cart .added_to_cart,.wp-calendar-nav a,div#calendar_wrap tbody a,li.latest-news h6:hover,#recent-posts-2 ul li a:hover,.pricing-box:not(:hover):not(.recomended) .boxed-btn,.product_meta a:hover,.widget_categories ul li a:hover,.widget_meta ul li a:hover,.widget_nav_menu ul li a:hover,.widget_pages ul li a:hover,.widget_product_categories ul li a:hover,.widget_product_tag_cloud .tagcloud a:hover,.widget_products ul li a:hover,.widget_recent_comments ul li a:hover,.widget_recent_entries ul li a:hover,.widget_recently_viewed_products ul li a:hover,.widget_top_rated_products ul li a:hover,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,cite,em,h1.page-title,input.search-btn:hover,input[type=button],input[type=reset],input[type=submit]:hover,p.form-submit .submit:hover,q,.beautician-footer-text h5,.boxed-btn:hover,.boxed-btn:focus,.wpcf7-form .wpcf7-submit:hover, .wpcf7-form .wpcf7-submit:focus, .logged-in-as a, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce-cart-form__cart-item .product-name a, .boxed-btn:focus:before, .boxed-btn:focus:after, .wpcf7-form .wpcf7-submit:focus:before, .wpcf7-form .wpcf7-submit:focus:after, .form-submit .submit:focus:before, .form-submit .submit:focus:after, .pricing-box.recomended a.boxed-btn:focus, .pricing-box a.boxed-btn:focus, .header-slider .boxed-btn:focus, .main-menu ul.cr-dropdown-menu li.focus > a, .main-menu ul.cr-dropdown-menu li:hover > a,.wpcf7-form .wpcf7-submit:focus, a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart.added {
					color: " .esc_attr($theme_color). ";
				}.woocommerce div.product p.price, .woocommerce div.product span.price,
				.woocommerce .widget_layered_nav_filters ul li a:hover,
				.woocommerce .widget_layered_nav_filters ul li a:focus,
				.widget_recent_reviews a:hover,.widget_recent_reviews a:focus,
				a.woocommerce-review-link:hover,a.woocommerce-review-link:focus,
				.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:hover,
				.widget_layered_nav ul.woocommerce-widget-layered-nav-list li.wc-layered-nav-term a:focus,
				.product-name a,
				.woocommerce .shop-product h5 a:hover,
				.woocommerce .shop-product h5 a:focus,
				.thaispa-theme .service-thaispa .price-thaispa h5,
				.post-comment-area h2,
				.blog-post .post-title a:focus,
				.blog-post ul:not(.meta-info) li a,
				.blog-post ol:not(.meta-info) li a,
				.blog-post table a, strong a, blockquote a, code a, p a,
				span a {
					color: " .esc_attr($theme_color). ";
				}
				.header-slider .theme-slider .theme-content h1 {
					color: " .esc_attr($theme_color). " !important;
				}\n";	
				
			$output_css .="a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:focus,div.woocommerce-info,p.woocommerce-info,.appoinment-wrapper input:focus,.appoinment-wrapper select:focus,.appoinment-wrapper textarea:focus,.feature-box:hover .feature-icon,.pagination span,.portfolio-tab-sorting li a:hover,.pricing-box .boxed-btn,.woocommerce #payment #place_order:hover,.woocommerce a.button:hover,.woocommerce button.button.alt:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce-page #payment #place_order:hover,.wpcf7-form input:focus,.wpcf7-form textarea:focus,a.button.product_type_simple.add_to_cart_button.ajax_add_to_cart:hover,input[type=email]:focus,input[type=text]:focus,input[type=url]:focus,li.page-item .page-link:hover,li.page-item.active .page-link,p.form-submit .submit:hover,textarea:focus, .wpcf7-form .wpcf7-submit:hover, .wpcf7-form .wpcf7-submit:focus {
					border-color: " .esc_attr($theme_color). ";
				}.thaispa-theme ._2Pfbi {
					border-color: " .esc_attr($theme_color). " !important;
				}\n";
			
			$output_css .=".input--filled .graphic--hantus,.input__field--hantus:focus+.input__label--hantus .graphic--hantus {
					stroke: " .esc_attr($theme_color). ";
				}\n";
			$output_css .=".woocommerce-MyAccount-navigation ul li.is-active a,.woocommerce-MyAccount-navigation ul li a:hover,.woocommerce-MyAccount-navigation ul li a:focus,blockquote {
					border-left: " .esc_attr($theme_color). ";
				}.coupon input[type='text'] {
					border-bottom: " .esc_attr($theme_color). ";
				}.boxed-btn,.pricing-box .boxed-btn,.recomended .boxed-btn {
					border-color: " .esc_attr($theme_color). ";
				}.pricing-box.recomended a.boxed-btn:hover, .pricing-box a.boxed-btn:hover {
					color: " .esc_attr($theme_color). ";
				}\n";	
			$output_css .=".pricing-box.recomended:hover .boxed-btn, .main-menu>ul>li.active>a,.main-menu>ul>li.focus>a,.main-menu>ul>li:hover>a {
				color: #ffffff;
			}\n";
			}	
			
			$hide_show_typography= get_theme_mod('hide_show_typography','off');
		if( $hide_show_typography == '1' ){
			/**
			 *  Typography Body
			 */
			$body_font_family 				= get_theme_mod('body_font_family');
			$body_typography_font_weight 	= get_theme_mod('body_typography_font_weight','normal');
			$body_font_size 				= get_theme_mod('body_font_size','16');
			$body_line_height 				= get_theme_mod('body_line_height','1.6');
			$output_css .=" body{ 
				font-family:" .esc_attr($body_font_family). ";
				font-size: " .esc_attr($body_font_size). "px;
				line-height: " .esc_attr($body_line_height). ";
				font-style: " .esc_attr($body_typography_font_weight). ";
			}\n";
			
			/**
			 *  Typography Menu
			 */
			$menu_text_transform 			= get_theme_mod('menu_text_transform','inherit');
			$menu_font_weight 				= get_theme_mod('menu_font_weight','normal');
			$menu_font_size 				= get_theme_mod('menu_font_size','15');
			$menu_line_height 				= get_theme_mod('menu_line_height','1.6');
			$output_css .=" .main-menu li a{ 
				text-transform:" .esc_attr($menu_text_transform). ";
				font-size: " .esc_attr($menu_font_size). "px;
				line-height: " .esc_attr($menu_line_height). ";
				font-style: " .esc_attr($menu_font_weight). ";
			}\n";
			
			/**
			 *  Typography Section
			 */
			$sec_ttl_text_transform 		= get_theme_mod('sec_ttl_text_transform','inherit');
			$section_tit_font_weight 		= get_theme_mod('section_tit_font_weight','normal');
			$section_tit_font_size 			= get_theme_mod('section_tit_font_size','36');
			$section_ttl_line_height 		= get_theme_mod('section_ttl_line_height','1.6');
			
			$sec_desc_text_transform 		= get_theme_mod('sec_desc_text_transform','inherit');
			$section_des_font_weight 		= get_theme_mod('section_des_font_weight','normal');
			$section_desc_font_size 			= get_theme_mod('section_desc_font_size','16');
			$section_desc_line_height 		= get_theme_mod('section_desc_line_height','1.6');
			
			$output_css .=" .section-title h2{ 
				text-transform:" .esc_attr($sec_ttl_text_transform). ";
				font-size: " .esc_attr($section_tit_font_size). "px;
				line-height: " .esc_attr($section_ttl_line_height). ";
				font-style: " .esc_attr($section_tit_font_weight). ";
			}\n";
			
			$output_css .=" .section-title p{ 
				text-transform:" .esc_attr($sec_desc_text_transform). ";
				font-size: " .esc_attr($section_desc_font_size). "px;
				line-height: " .esc_attr($section_desc_line_height). ";
				font-style: " .esc_attr($section_des_font_weight). ";
			}\n";
			
			/**
			 *  Typography Heading
			 */
			 for ( $i = 1; $i <= 6; $i++ ) {
			if($i  == '1'){$j=36;}elseif($i  == '2'){$j=32;}elseif($i  == '3'){$j=28;}elseif($i  == '4'){$j=24;}elseif($i  == '5'){$j=20;}else{$j=16;}
				 $heading_font_weight	    	= get_theme_mod('h' . $i . '_font_weight','normal');	
				 $heading_font_size	 			= get_theme_mod('h' . $i . '_font_size',$j);
				 $heading_line_height			= get_theme_mod('h' . $i . '_line_height','1.2');
				 $heading_text_transform		= get_theme_mod('h' . $i . '_text_transform','inherit');
				 
				 $output_css .=" h" . $i . "{ 
					font-style: " .esc_attr($heading_font_weight). ";
					text-transform: " .esc_attr($heading_text_transform). ";
					font-size: " .esc_attr($heading_font_size). "px;
					line-height: " .esc_attr($heading_line_height). ";
				}\n";
			 }
		}
	 wp_add_inline_style( 'hantus-style', $output_css );
    }
endif;
add_action( 'wp_enqueue_scripts', 'hantus_dynamic_styles' );
?>