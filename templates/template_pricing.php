<?php
/**
Template Name: Pricing Page
**/

get_header();
?>
	<?php 
		$Pricing_newsletter_hide_show= get_theme_mod('Pricing_newsletter_hide_show','1'); 
		$Pricing_price_hide_show= get_theme_mod('Pricing_price_hide_show','1');
		$pricing_pg_title= get_theme_mod('pricing_pg_title','Pricing');
		$pricing_pg_description= get_theme_mod('pricing_pg_description','You can select a package from the list below to save more');
	?>

	<?php 	
		if($Pricing_price_hide_show == '1' ) {
		hantus_before_pricing_page_section_trigger();	
	?>
	<section id="pricing" class="custom-padding price-pg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
						<?php if( ($pricing_pg_title) || ($pricing_pg_description)!='' ) { ?>
							<h2><?php echo esc_html($pricing_pg_title); ?></h2>
							<p><?php echo esc_html($pricing_pg_description); ?></p>
						<?php } ?>
                    </div>
                </div>
            </div>
			<?php if ( function_exists( 'hantus_pricing' ) ){ ?>	
            <div class="row">
			<?php 
				if ( ! empty( $pricing_pg_title )) {
				$args = array( 'post_type' => 'hantuss_pricing','posts_per_page' => 100);  	
				$plans = new WP_Query( $args ); 
				if( $plans->have_posts() )
				{
					while ( $plans->have_posts() ) : $plans->the_post();
			?>
			
			<?php
				
				
			?>
                <div class="col-lg-4 col-md-6 price-contents">
                    <?php get_template_part('template-parts/pricing/pricing','content'); ?>
                </div>
				<?php 
				
					endwhile;
						} 
				 }
			?>
            </div>
			<?php
				}else{
				echo __('Please install & Activate Hantus Feature plugin for add Pricing','hantus-pro');
				}
			?>
        </div>
    </section>
	<?php	
		hantus_after_pricing_page_section_trigger(); }
	 ?>
	<?php 	
		if($Pricing_newsletter_hide_show == '1' ) {
	get_template_part('template-parts/sections/section','newsletter');
		}
	 ?>
<?php get_footer(); ?>