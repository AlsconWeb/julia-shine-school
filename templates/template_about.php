<?php
/**
Template Name: About Page
**/

get_header();
$default_content_testimonial 		= hantus_get_testimonial_default();
$hide_show_testimonial				= get_theme_mod('hide_show_testimonial','1'); 
$testimonial_contents				= get_theme_mod('testimonial_contents',$default_content_testimonial);
$testimonial_background_setting		= get_theme_mod('testimonial_background_setting',get_template_directory_uri() .'/assets/images/bg/testimonial-bg.jpg');
$testimonial_background_position	= get_theme_mod('testimonial_background_position','fixed');

$about_team_hide_show			= get_theme_mod('about_team_hide_show','1'); 
$about_welcome_hide_show		= get_theme_mod('about_welcome_hide_show','1'); 
$about_testimonial_hide_show	= get_theme_mod('about_testimonial_hide_show','1');
$appoinment_hide_show			= get_theme_mod('appoinment_hide_show','1');	
$sponsers_hide_show				= get_theme_mod('sponsers_hide_show','1');	
$about_page_contents_title		= get_theme_mod('about_page_contents_title','Why Choose Us'); 
$about_page_content_subtitle	= get_theme_mod('about_page_content_subtitle','We Proudly give quality, thorough chiropractic to the group and the encompassing regions.'); 
$about_page_content_description	= get_theme_mod('about_page_content_description','<li>Created from natural herbs</li> <li>100% safe for your skin</li> <li>Quality product from SpaLabs</li><li>Special gifts & offers for you</li><li>Created from natural herbs</li><li>100% safe for your skin</li>'); 
$about_page_btn_lbl_title		= get_theme_mod('about_page_btn_lbl_title','Watch More'); 
$about_page_btn_lbl_link		= get_theme_mod('about_page_btn_lbl_link','https://www.youtube.com/watch?v=pGbIOC83-So&t=35s'); 
$about_page_video				= get_theme_mod('about_page_video','#'); 
$about_video_img_setting		= get_theme_mod('about_video_img_setting',get_template_directory_uri() . '/assets/images/about-page/wcu.jpg');

$about_welcome_title			= get_theme_mod('about_welcome_title','Welcome to'); 
$about_welcome_subtitle			= get_theme_mod('about_welcome_subtitle','Hantus Spa'); 
$about_welcome_description		= get_theme_mod('about_welcome_description','It is a long established fact that a reader will be distracted by the readable.'); 
$default_contents 				= hantus_get_welcome_default();
$welcome_contents				= get_theme_mod('welcome_contents',$default_contents); 
$default_content			    = hantus_get_about_funfact_default();
$about_page_funfacts_contents	= get_theme_mod('about_page_funfacts_contents',$default_content); 
$about_wel_sec_column= get_theme_mod('about_wel_sec_column','4');
?>


    <!-- Start: Welcome
    ============================= -->
  <?php if($about_welcome_hide_show == '1') {?>
	<?php hantus_before_abt_welcome_section_trigger(); ?>
    <section id="welcome" class="custom-padding">
        <div class="container">            
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title">
						<?php if ( ! empty( $about_welcome_title ) ) : ?>
							<h3><?php echo wp_kses_post( $about_welcome_title ); ?></h3>
						<?php endif; ?>
						<?php if ( ! empty( $about_welcome_subtitle ) ) : ?>
							<h2><?php echo wp_kses_post( $about_welcome_subtitle ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $about_welcome_description ) ) : ?>
							<p><?php echo wp_kses_post( $about_welcome_description ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
				<?php
						if ( ! empty( $welcome_contents ) ) {
						$allowed_html = array(
						'br'     => array(),
						'em'     => array(),
						'strong' => array(),
						'b'      => array(),
						'i'      => array(),
						);
						$welcome_contents = json_decode( $welcome_contents );
						foreach ( $welcome_contents as $welcome_item ) {
							$title = ! empty( $welcome_item->title ) ? apply_filters( 'hantus_translate_single_string', $welcome_item->title, 'Welcome section' ) : '';
							$text = ! empty( $welcome_item->text ) ? apply_filters( 'hantus_translate_single_string', $welcome_item->text, 'Welcome section' ) : '';
							$button_lbl = ! empty( $welcome_item->text2 ) ? apply_filters( 'hantus_translate_single_string', $welcome_item->text2, 'Welcome section' ) : '';
							$link = ! empty( $welcome_item->link ) ? apply_filters( 'hantus_translate_single_string', $welcome_item->link, 'Welcome section' ) : '';
							$image = ! empty( $welcome_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $welcome_item->image_url, 'Welcome section' ) : '';
				?>
                <div class="col-lg-<?php echo $about_wel_sec_column; ?> col-md-6 mb-4 mb-lg-4 mx-sm-auto text-center about-wel-sec-column">
                    <div class="welcome-box">
                        <?php if ( ! empty( $image ) ) : ?>
							<img src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
						<?php endif; ?>
                        <div class="welcome-content">
							<?php if ( ! empty( $title ) ) : ?>
								<h4><?php echo esc_attr( $title ); ?></h4>
							<?php endif; ?>
							<?php if ( ! empty( $text ) ) : ?>
								<p><?php echo esc_attr( $text ); ?></p>
							<?php endif; ?>
                            <a href="<?php echo esc_url( $link ); ?>"><?php echo esc_attr($button_lbl); ?> <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
				 <?php
					}
				} 
				?>
            </div>
        </div>
    </section>
	<?php hantus_after_abt_welcome_section_trigger(); ?>
  <?php } ?>
    <!-- End: Welcome
    ============================= -->

    <!-- Start: Why choose us
    ============================= -->
  <?php hantus_before_abt_about_section_trigger(); ?>
    <section id="wcu">
        <div class="container">
            <div class="row">      
                <div class="video-section" style="background: url('<?php echo esc_url( $about_video_img_setting ); ?>') no-repeat center / cover;">
                    <div class="play-icon">
						<?php if ( ! empty( $about_page_video ) ) : ?>
							<a href="https://www.youtube.com/watch?v=pGbIOC83-So&t=35s" class="playbtn mfp-iframe"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/about-page/play-icon.png" alt=""></a>
						<?php endif; ?>	
                    </div>
                    <div class="watch-more">
						<?php if ( ! empty( $about_page_btn_lbl_title ) ) : ?>
							<a href="<?php echo esc_url( $about_page_btn_lbl_link ); ?>"><?php echo wp_kses_post( $about_page_btn_lbl_title ); ?><i class="fa fa-youtube"></i></a>
						<?php endif; ?>	
                    </div>
                </div>

                <div class="offset-lg-6 col-lg-6 col-12 order-1 order-lg-2 wcu-content">
					<?php if ( ! empty( $about_page_contents_title ) ) : ?>
						<h2 id="wcu-content-h2"><?php echo wp_kses_post( $about_page_contents_title ); ?></h2>
					<?php endif; ?>		
					<?php if ( ! empty( $about_page_content_subtitle ) ) : ?>
						<p id="wcu-content-p"><b><?php echo wp_kses_post( $about_page_content_subtitle ); ?></b></p>
					<?php endif; ?>	
                    <ul>
						<?php if ( ! empty( $about_page_content_description ) ) : ?>
							<?php echo wp_kses_post( $about_page_content_description ); ?>
						<?php endif; ?>	
                    </ul>

                    <div class="row fun-fact">
						<?php
							if ( ! empty( $about_page_funfacts_contents ) ) {
								$allowed_html = array(
								'br'     => array(),
								'em'     => array(),
								'strong' => array(),
								'b'      => array(),
								'i'      => array(),
								);
								$about_page_funfacts_contents = json_decode( $about_page_funfacts_contents );
								foreach ( $about_page_funfacts_contents as $about_funfact_item ) {
									$title = ! empty( $about_funfact_item->title ) ? apply_filters( 'hantus_translate_single_string', $about_funfact_item->title, 'Funfact section' ) : '';
									$text = ! empty( $about_funfact_item->text ) ? apply_filters( 'hantus_translate_single_string', $about_funfact_item->text, 'Funfact section' ) : '';
						?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6 mb-4 mb-sm-0 mx-auto text-sm-left text-center fun-about">
							<?php if ( ! empty( $title ) ) : ?>
								<h2><?php echo esc_attr( $title ); ?></h2>
							<?php endif; ?>
							<?php if ( ! empty( $text ) ) : ?>
								<p><?php echo esc_attr( $text ); ?></p>
							<?php endif; ?>	
                        </div>
								<?php }} ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php hantus_after_abt_about_section_trigger(); ?>
    <!-- End: Why choose us
    ============================= -->
    <!-- Start: Expert Beauticians
    ============================= -->
    <?php if($about_team_hide_show == '1') {?>
		<?php get_template_part('template-parts/sections/section','team');	 ?>
    <?php } ?>
    <!-- End: Expert Beauticians
    ============================= -->

    <!-- Start: Testimonial
    ============================= -->
<?php if($about_testimonial_hide_show == '1') {?>
<?php hantus_before_abt_testimonial_section_trigger(); ?>
     <section id="testimonial" class="section-padding" style="background: url('<?php echo esc_url($testimonial_background_setting); ?>') no-repeat center / 100% 100% <?php echo esc_attr($testimonial_background_position); ?>;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-carousel text-center">
                    	<div class="testimonial-content owl-carousel">
							<?php
								if ( ! empty( $testimonial_contents ) ) {
								$allowed_html = array(
								'br'     => array(),
								'em'     => array(),
								'strong' => array(),
								'b'      => array(),
								'i'      => array(),
								);
								$testimonial_contents = json_decode( $testimonial_contents );
								foreach ( $testimonial_contents as $testimonial_item ) {
									
									$title = ! empty( $testimonial_item->title ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->title, 'testimonial section' ) : '';
									$designation = ! empty( $testimonial_item->designation ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->designation, 'testimonial section' ) : '';
									$text = ! empty( $testimonial_item->text ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->text, 'testimonial section' ) : '';
									$image = ! empty( $testimonial_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $testimonial_item->image_url, 'testimonial section' ) : '';
							?>
	                        <div class="single-testimonial">
								<?php if ( ! empty( $text ) ) : ?>
									<p>“<?php echo esc_attr( $text ); ?>”</p>
								<?php endif; ?>		
								<?php if ( ! empty( $title ) ) : ?>
									<h5><?php echo esc_attr( $title ); ?></h5>
								<?php endif; ?>	
								<?php if ( ! empty( $designation ) ) : ?>
									<p class="title"><?php echo esc_attr( $designation ); ?></p>
								<?php endif; ?>	
	                        </div>
	                        <?php 
									}
								} 
							?>
						</div>
						<div class="testimonial-thumb owl-carousel text-center">
							<?php
								if ( ! empty( $testimonial_contents ) ) {								
									foreach ( $testimonial_contents as $testimonial_item ) {
										echo '<img src='.esc_url( $image ).' class="item" />';
									}
								}
							?>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php hantus_after_abt_testimonial_section_trigger(); ?>
<?php } ?>
    <!-- End: Testimonial
    ============================= -->

    <!-- Start: Appoinment
    ============================= -->
    
   <?php if($appoinment_hide_show == '1') {?>
		<?php get_template_part('template-parts/sections/section','appointment');	 ?>
    <?php } ?>

    <!-- End: Appoinment
    ============================= -->

    <!-- Start: Our partner
    ============================= -->
<?php 
$default_content_sponsor 	= hantus_get_sponsers_default();
$sponser_contents			= get_theme_mod('sponser_contents',$default_content_sponsor);
	?>
	<?php if($sponsers_hide_show == '1') {?>
		<?php hantus_before_abt_sponsor_section_trigger(); ?>
		<section id="about-partner" class="">
			<div class="container">
				<div class="row">
					<?php
							if ( ! empty( $sponser_contents ) ) {
							$allowed_html = array(
							'br'     => array(),
							'em'     => array(),
							'strong' => array(),
							'b'      => array(),
							'i'      => array(),
							);
							$sponser_contents = json_decode( $sponser_contents );
							foreach ( $sponser_contents as $sponser_item ) {
								$link = ! empty( $sponser_item->link ) ? apply_filters( 'hantus_translate_single_string', $sponser_item->link, 'sponser section' ) : '';
								$image = ! empty( $sponser_item->image_url ) ? apply_filters( 'hantus_translate_single_string', $sponser_item->image_url, 'sponser section' ) : '';
						?>
					<div class="col-lg-3 col-md-3 col-sm-4 col-6 single-partner">
						<div class="inner-partner">
							<a href="<?php echo esc_url( $link ); ?>">
								<?php if ( ! empty( $image ) ) : ?>
								<img src="<?php echo esc_url( $image ); ?>" <?php if ( ! empty( $title ) ) : ?> alt="<?php echo esc_attr( $title ); ?>" title="<?php echo esc_attr( $title ); ?>" <?php endif; ?> />
							<?php endif; ?>
							</a>
						</div>               
					</div>
					<?php } }?>	
				</div>
			</div>
		</section>
		<?php hantus_after_abt_sponsor_section_trigger(); ?>
    <?php } ?>
    <!-- End: Our Partner
    ============================= -->
<?php get_footer(); ?>