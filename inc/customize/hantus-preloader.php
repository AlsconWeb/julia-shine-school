<?php
function hantus_preloader_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';	
	
	/*=========================================
	Client Section Panel
	=========================================*/
	$wp_customize->add_panel( 
		'preloader_panel', 
		array(
			'priority'      => 35,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Preloader & Scroller', 'hantus-pro'),
		) 
	);
	
	
	/*=========================================
	preloader Settings Section
	=========================================*/
	// preloader Settings Section // 
	$wp_customize->add_section(
        'preloader_setting',
        array(
        	'priority'      => 1,
            'title' 		=> __('Preloader','hantus-pro'),
			'panel'  		=> 'preloader_panel',
		)
    );
	
	// preloader Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_preloader' , 
			array(
			'default' => 0,
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_preloader', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'preloader_setting',
			'settings'    => 'hide_show_preloader',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	
	// preloader Settings Section // 
	$wp_customize->add_section(
        'scroller_setting',
        array(
        	'priority'      => 1,
            'title' 		=> __('Page Scroller','hantus-pro'),
			'panel'  		=> 'preloader_panel',
		)
    );
	
	// top scroller Hide/ Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_scroller' , 
			array(
			'default' => '1',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'hide_show_scroller', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'scroller_setting',
			'settings'    => 'hide_show_scroller',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Scroller icon // 
	$wp_customize->add_setting(
    	'scroller_icon',
    	array(
	        'default' => 'fa-arrow-up',
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Hantus_Icon_Picker_Control($wp_customize, 
		'scroller_icon',
		array(
		    'label'   		=> __('Scroller Icon','hantus-pro'),
		    'section' 		=> 'scroller_setting',
			'iconset' => 'fa',
			'settings' 		 => 'scroller_icon',
			'description'   => __( '', 'hantus-pro' ),
			
		))  
	);
}
add_action( 'customize_register', 'hantus_preloader_setting' );

// breadcrumb selective refresh
function hantus_home_scroller_section_partials( $wp_customize ){

	// hide_show_scroller
	$wp_customize->selective_refresh->add_partial(
		'hide_show_scroller', array(
			'selector' => '#footer-copyright .scrollup',
			'container_inclusive' => true,
			'render_callback' => 'scroller_setting',
			'fallback_refresh' => true,
		)
	);
	
	}
add_action( 'customize_register', 'hantus_home_scroller_section_partials' );