jQuery(document).ready(function ($) {
  $("div#header-top p.time-details a").attr("href", "/product/lesson-level-1/");

  // $( "div.single-testimonial p.title" ).each(function( index )
  // {
  // 	const me = $(this);
  // 	const youTubeHtml = me.text();
  // 	$( me.parent() ).prepend( youTubeHtml );
  // 	me.remove();
  // });

  $("div.header-single-slider div.theme-content h1").each(function (index) {
    const me = $(this);
    me.html(
      '<div class="first-line">Video training:</div><div class="second-line">New "language" for your body</div>'
    );
  });
  if ($(".theme-content").length) {
    $(".theme-content").each(function () {
      $(this).after(
        '<div class="plashka-style"><img class="money-back" src="/wp-content/uploads/assets/money_back.webp" alt="" "></div> '
      );
    });
  }
  $("div.testimonial-carousel").prepend("<h2>Feedbacks</h2>");

  $("section br").remove();

  $($("section.move-section").get().reverse()).each(function (index) {
    const me = $(this);

    me.insertAfter(me.parent().parent().parent().parent());

    me.removeClass("move-section");
  });

  if (document.querySelector(".page-id-21")) {
    function createGoDownElement() {
      const goDownElement = document.createElement("div");
      goDownElement.className = "go-down";

      const arrowIcon = document.createElement("i");
      arrowIcon.className = "fa fa-arrow-down";

      goDownElement.appendChild(arrowIcon);

      return goDownElement;
    }

    function handleGoDownFromSlider() {
      const servicesSection = document.getElementById("services");

      if (servicesSection) {
        servicesSection.scrollIntoView({
          behavior: "smooth",
        });
      }
    }

    function handleGoDownFromYouTube() {
      const footer = document.getElementById("footer-widgets");

      if (footer) {
        footer.scrollIntoView({
          behavior: "smooth",
        });
      }
    }

    function countDown() {
      countDownObject.now = new Date();
      countDownObject.timeLeft = countDownObject.endDate - countDownObject.now;

      const timeLeft2 = countDownObject.timeLeft / 1000;

      countDownObject.days = Math.floor(timeLeft2 / 86400);
      countDownObject.hours = Math.floor(
        (timeLeft2 - countDownObject.days * 86400) / 3600
      );
      countDownObject.minutes = Math.floor(
        (timeLeft2 -
          countDownObject.days * 86400 -
          countDownObject.hours * 3600) /
        60
      );
      countDownObject.seconds = Math.floor(
        timeLeft2 -
        countDownObject.days * 86400 -
        countDownObject.hours * 3600 -
        countDownObject.minutes * 60
      );

      if (hours < 10) countDownObject.hours = "0" + countDownObject.hours;
      if (minutes < 10) countDownObject.minutes = "0" + countDownObject.minutes;
      if (seconds < 10) countDownObject.seconds = "0" + countDownObject.seconds;

      $("#days").html(
        '<span class="countDown">' +
        countDownObject.days +
        '</span><span class="camp">Days</span>'
      );
      $("#hours").html(
        '<span class="countDown">' +
        countDownObject.hours +
        '</span><span class="camp">Hours</span>'
      );
      $("#minutes").html(
        '<span class="countDown">' +
        countDownObject.minutes +
        '</span><span class="camp">Minutes</span>'
      );
      $("#seconds").html(
        '<span class="countDown">' +
        countDownObject.seconds +
        '</span><span class="camp">Seconds</span>'
      );
    }

    //const slider = document.getElementById("slider");
    const testimonial = document.getElementById("testimonial");
    //const goDownFromSlider = createGoDownElement();
    const goDownFromYouTube = createGoDownElement();
    //slider.appendChild(goDownFromSlider);
    testimonial.appendChild(goDownFromYouTube);

    //goDownFromSlider.addEventListener("click", handleGoDownFromSlider);
    goDownFromYouTube.addEventListener("click", handleGoDownFromYouTube);

    window.addEventListener("scroll", function () {
      const scrollPosition = window.scrollY;
/*
      if (scrollPosition < 100) {
        goDownFromSlider.style.display = "block";
      } else {
        goDownFromSlider.style.display = "none";
      }
*/
      if (
        scrollPosition > testimonial.offsetTop &&
        scrollPosition < testimonial.offsetTop + testimonial.offsetHeight / 2
      ) {
        goDownFromYouTube.style.display = "block";
      } else if (
        scrollPosition >
        testimonial.offsetTop + testimonial.offsetHeight / 2
      ) {
        goDownFromYouTube.style.display = "none";
      } else {
        goDownFromYouTube.style.display = "none";
      }
    });

    const queryString = window.location.search;
    //console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const girls = urlParams.get("girls");
    const welcome = urlParams.get("welcome");
    const india = urlParams.get("india");
    //console.log(product);

    $("section#custom_section_whales").addClass("start-section");
    $("section#custom_section_hole2").addClass("start-section");
    $("section#section_subscribe").addClass("start-section");
    $("section#custom_section_videos2").addClass("start-section");

    $("section#services").addClass("start-section");
    $("section#custom_section").addClass("start-section");
    $("section#product").addClass("start-section");
    $("section#testimonial").addClass("start-section");


    $("section#custom_section_whales").remove();
    //$("section#custom_section_videos2").remove();

    $("section#custom_section_girl").remove();

    if (girls) 
    {
      $("section#custom_section_girls10").removeClass("start-section");

      $("section#services_girls_why").removeClass("start-section");
      $("section#services_girls_love_beautiful").removeClass("start-section");
      $("section#services_girls_smart_women").removeClass("start-section");
      $("section#services_girls_special_women").removeClass("start-section");

      $("section#custom_section_money_back").removeClass("start-section");
      $("section#custom_section_money_back2").removeClass("start-section");
      $("section#custom_section_videos2").removeClass("start-section");
      $("section#custom_section_about").removeClass("start-section");
      $("section#custom_section_effects").removeClass("start-section");
      $("section#custom_section_roots").removeClass("start-section");
      $("section#testimonial").removeClass("start-section");
      $("section#custom_section_counter2").removeClass("start-section");
      $("section#custom_section_results").removeClass("start-section");

      $("section#custom_section_girls10").insertBefore("section#custom_section_videos2");

      $("section#services_girls_why").insertBefore("section#custom_section_girls10");
      $("section#services_girls_love_beautiful").insertBefore("section#custom_section_girls10");
      $("section#services_girls_smart_women").insertBefore("section#custom_section_girls10");
      $("section#services_girls_special_women").insertBefore("section#custom_section_girls10");


      $("section.start-section").remove();

      $("div.header-single-slider div.theme-content p").text(
        "Pump up your sexuality to have in 5 times more money!"
      );
      $("div.header-single-slider div.theme-content a").attr(
        "href",
        "/product/test-drive-level-0/"
      );

      $('.navbar-area.sticky-nav').remove();
      $('.navbar-wrapper').css('min-height', '0px');

      var countDownObject = {
        endDate: new Date("2024-03-01 00:00"),
        now: undefined,
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
        timeLeft: 0,
      };

      setInterval(function () {
        countDown();
      }, 1000);
    } 
    else 
    {
      $("section#custom_section_girls10").remove();

      $("section#services_girls_why").remove();
      $("section#services_girls_love_beautiful").remove();
      $("section#services_girls_smart_women").remove();
      $("section#services_girls_special_women").remove();


    //  if (welcome) 
//      {
//          $("section#custom_section_whales").remove();
//          $("section#custom_section_videos2").remove();
      //}

      $("section.start-section").removeClass("start-section");


      
      
      $('#custom_section_videos2').insertBefore('#section_subscribe');

      

      $('section#custom_section_main').insertBefore('#services');

      $('#custom_section_counter2').insertBefore('#services');


      $('section#custom_section_contact').insertBefore('#footer');
    }


    if (india)
    {
      $("section#services").remove();
      $('section#custom_section_india').insertBefore('#custom_section_counter2');


      var countDownObject = {
        endDate: new Date("2024-03-01 00:00"),
        now: undefined,
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
        timeLeft: 0,
      };

      setInterval(function () {
        countDown();
      }, 1000);
    }
    else
    {
      $("section#custom_section_india").remove();
    }

    // initCounterFollowers();
    initSectionNav();
    var convasGrafic;
    var indexCorrect = 0;
    var titl = "";
    var massivAll;
    var res = false;
    var responseCounterTopShowDetails = true;

    var setArrayTime = [];
    var csvRows;

    const regular = /\B(?=(\d{3})+(?!\d))/g;

    let globalFollowersNumber = 0;
    let liveSessionsHeldNumber = 0;

    const calcSessionsObject = {};
    const calcFollowersObject = {};
    const calcOtherObject = [];

    getPriceXml().then(() => {
      totalGifts = setArrayTime[1] - setArrayTime[0];

      var totalGifts = totalGifts;
      var numberOfDays = 30;
      var giftsPerDay = [];
      var giftsForDay = Math.floor(totalGifts / numberOfDays);

      for (var i = 0; i < numberOfDays; i++) {
        giftsPerDay.push(giftsForDay);
        totalGifts -= giftsForDay;
      }

      giftsPerDay[giftsPerDay.length - 1] += totalGifts;

      function dateTimeOld(old = "") {
        var targetDate;
        if (old === "") {
          targetDate = new Date();
        } else {
          targetDate = new Date(old);
        }
        var currentDate = new Date();
        var timeDifference = currentDate - targetDate;
        var daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
        return daysDifference;
      }

      var firstTwoElements = giftsPerDay.slice(
        0,
        dateTimeOld(localStorage.getItem("oldData"))
      );

      function isDayPassed(lastUpdateDate) {
        var currentDate = new Date();
        var lastUpdate = new Date(lastUpdateDate);

        return (
          currentDate.getFullYear() !== lastUpdate.getFullYear() ||
          currentDate.getMonth() !== lastUpdate.getMonth() ||
          currentDate.getDate() !== lastUpdate.getDate()
        );
      }

      function updateData() {
        var currentDate = new Date();
        localStorage.setItem("lastUpdate", currentDate.toISOString());
      }

      var lastUpdateDate = localStorage.getItem("lastUpdate");

      if (!lastUpdateDate || isDayPassed(lastUpdateDate)) {
        updateData();
      }

      // var storedObj = JSON.parse(localStorage.getItem('todayData'));

      if (!lastUpdateDate || isDayPassed(lastUpdateDate)) {
        yourFunction();
      }

      function yourFunction() {
        var sum =
          firstTwoElements.reduce((acc, num) => acc + num, 0) +
          Number(localStorage.getItem("NumberTrend"));
        localStorage.setItem("NumberTrend", sum);
      }
    });

    async function getPriceXml() {
      await fetch(
        "https://docs.google.com/spreadsheets/d/e/2PACX-1vQOincWI2MkdeGsENpFs7X6hZqPlsDWTjDjU3wln2JLJPhUZCFwulPK0xIpN5A_5bI2TG_TqcDpK8vM/pub?gid=0&single=true&output=csv"
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("Error while downloading counter");
          }

          return response.text();
        })
        .then((csvText) => {
          massivAll = csvText.split(",");

          csvRows = csvText.split("\n");
          const chartRows = [];

          setVarsForSessionsCalc();
          setVarsForFollowersCalc();

          const chartContainers = initCounters(
            csvRows[0],
            csvRows[csvRows.length - 1]
          );

          for (let i = 1; i < csvRows.length; i++) {
            chartRows.push(csvRows[i].split(","));
          }

          setArrayTime = chartRows.slice(-2).map(function (array) {
            return array[2];
          });

          localStorage.setItem("oldData", chartRows.slice(-1)[0][0]);

          const filteredArrays = chartRows.map((nestedArray) => {
            return [
              nestedArray[0],
              nestedArray[getMyVariable()],
              nestedArray[getMyVariable() + 1],
            ];
          });


          queueMicrotask(() => {
            initCharts(filteredArrays, chartContainers);
          });
        });
    }

    function getRes() {
      return res;
    }

    function getResTopShow() {
      return responseCounterTopShowDetails;
    }

    function getMyVariable() {
      return indexCorrect;
    }

    function getLiveSessionsHeldNumber() {
      setVarsForSessionsCalc()
      return parseInt(calcSessionsObject.currentValue);
    }

    function getGlobalFlowers() {
      setVarsForFollowersCalc();
      return parseInt(calcFollowersObject.currentValue)
    }

    function getOtherNumber(index) {
      setVarsForOtherCalc(index)
      return parseInt(calcOtherObject[index].currentValue);
    }

    function getTitles() {
      return massivAll[getMyVariable()];
    }

    function getTitlePopup(index) {
      return massivAll[index];
    }

    function getDataToday() {
      var currentDate = new Date();
      var year = currentDate.getFullYear();
      var month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
      var day = currentDate.getDate().toString().padStart(2, "0");
      var formattedDate = year + "-" + month + "-" + day;
      return formattedDate;
    }

    function setVarsForSessionsCalc() {
      calcSessionsObject.firstData = dateToSeconds(new Date(csvRows[csvRows.length - 2].split(",")[0]));
      calcSessionsObject.lastData = dateToSeconds(new Date(csvRows[csvRows.length - 1].split(",")[0]));
      calcSessionsObject.firstValue = parseInt(csvRows[csvRows.length - 2].split(",")[2]);
      calcSessionsObject.lastValue = parseInt(csvRows[csvRows.length - 1].split(",")[2]);

      const { m, b} = calculateLineCoefficients(calcSessionsObject.firstData, calcSessionsObject.firstValue, calcSessionsObject.lastData, calcSessionsObject.lastValue);
      calcSessionsObject.currentValue = calculateValue(new Date(), m, b);
    }

    function setVarsForFollowersCalc() {
      calcFollowersObject.firstData = dateToSeconds(new Date(csvRows[csvRows.length - 2].split(",")[0]));
      calcFollowersObject.lastData = dateToSeconds(new Date(csvRows[csvRows.length - 1].split(",")[0]));
      calcFollowersObject.firstValue = parseInt(csvRows[csvRows.length - 2].split(",")[1]);
      calcFollowersObject.lastValue = parseInt(csvRows[csvRows.length - 1].split(",")[1]);

      const { m, b} = calculateLineCoefficients(calcFollowersObject.firstData, calcFollowersObject.firstValue, calcFollowersObject.lastData, calcFollowersObject.lastValue);
      calcFollowersObject.currentValue = calculateValue(new Date(), m, b);
    }

    function setVarsForOtherCalc(index) {
      calcOtherObject[index] = {};
      calcOtherObject[index].firstData = dateToSeconds(new Date(csvRows[csvRows.length - 2].split(",")[0]));
      calcOtherObject[index].lastData = dateToSeconds(new Date(csvRows[csvRows.length - 1].split(",")[0]));
      calcOtherObject[index].firstValue = parseInt(csvRows[csvRows.length - 2].split(",")[index]);
      calcOtherObject[index].lastValue = parseInt(csvRows[csvRows.length - 1].split(",")[index]);

      const { m, b} = calculateLineCoefficients(calcOtherObject[index].firstData, calcOtherObject[index].firstValue, calcOtherObject[index].lastData, calcOtherObject[index].lastValue);
      calcOtherObject[index].currentValue = calculateValue(new Date(), m, b);
      calcOtherObject[index].previousValue = calcOtherObject[index].previousValue || parseInt(calcOtherObject[index].currentValue);
    }

		function dateToSeconds(date) {
			const startDate = new Date();
			const diffTime = Math.abs(date - startDate);
			return Math.floor(diffTime / 1000);
		}

		function calculateLineCoefficients(x1, y1, x2, y2) {
			const m = (y2 - y1) / (x2 - x1);
			const b = y1 - m * x1;
			return {m, b};
		}

		function calculateValue(dateStr, m, b) {
			const x = dateToSeconds(dateStr);
			return m * x + b;
		}

		function initCounterFollowers() {
			const main = document.querySelector("#counterElem");
			const container = document.createElement("div");
			const container1 = document.createElement("div");
			const container2 = document.createElement("div");
			const counter = document.createElement("a");
			const counter1 = document.createElement("a");
			const counter2 = document.createElement("a");

			counter.classList.add("counter-value");
			counter1.classList.add("counter-value");
			counter2.classList.add("counter-value");

			counter.href = "https://www.instagram.com/discovering.body.in/";
			counter.target = "_blank";

			const title = document.createElement("h3");
			const title1 = document.createElement("h3");
			const title2 = document.createElement("h3");
			title.innerHTML = "Global Followers";
			title1.innerHTML = "Live Sessions held";
      title2.innerHTML = "Total Masters Trained";

			container.classList.add("counter-container1");
			container1.classList.add("counter-container2");
			container2.classList.add("counter-container3");
			container.appendChild(title);
			container1.appendChild(title1);
			container2.appendChild(title2);

      initCounter(counter, globalFollowersNumber);
      initCounter(counter1, liveSessionsHeldNumber);
      initCounter(counter2, sumArray());

			container.appendChild(counter);
			container1.appendChild(counter1);
			container2.appendChild(counter2);

			main.appendChild(container);
			main.appendChild(container1);
			main.appendChild(container2);
		}

    function sumArray() {
      let sum = 0;

      for (let i = 0; i < calcOtherObject.length; i++) {
        if (calcOtherObject[i] && !isNaN(calcOtherObject[i].previousValue)) {
          sum += calcOtherObject[i].previousValue;
        }
      }

      return sum;
    }

		function initCounters(titles, values) {
			const chartContainers = [];

			titles = titles.split(",");
			values = values.split(",");

			titles.shift();
			values.shift();

			const resultArray = [];

			const str = [
				"1",
				"2-3",
				"4-5",
				"6-7",
				"8-9",
				"10-11",
				"12-13",
				"14-15",
				"16-17",
				"18-19",
				"20-21",
			];

      var objToStore = {data: getDataToday(), value: Number(values[1])};
      localStorage.setItem("todayData", JSON.stringify(objToStore));


			// for (let i = 0, k = 0; i < values.length; ++k, i += 2) {
			// 	const amount = Number(values[i + 1]);
			// 	const price = Number(values[i + 2]);
			// 	arrayata.push(Number(values[i + 1]));
      //
			// 	text1 = Number(values[1]);
			// 	text2 = Number(values[2]);
      //
      //
			// 	let amounts = document.querySelector(
			// 		`#count_${str[k + 1].split("-")[0]}`
			// 	);
			// 	let prices = document.querySelector(
			// 		`#count_${str[k + 1]?.split("-")[1]}`
			// 	);
      //
			// 	if (amounts !== null) {
			// 		amounts.textContent = amount
			// 			.toString()
			// 			.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			// 	}
      //
			// 	if (prices !== null) {
			// 		prices.textContent =
			// 			price >= 999999
			// 				? "Price is negotiable"
			// 				: "$" + price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			// 	}
      //
			// 	if (!isNaN(amount) && !isNaN(price)) {
			// 		resultArray.push({amount, price, title: titles[i + 1]});
			// 	}
			// }

      for (let i = 4, k = 2; i < values.length; ++k, i += 2) {
        getOtherNumber(i);

        const amount = calcOtherObject[i].previousValue;
        const price = Number(values[i]);

        let amounts = document.querySelector(
          `#count_${str[k].split("-")[0]}`
        );
        let prices = document.querySelector(
          `#count_${str[k]?.split("-")[1]}`
        );

        if (amounts !== null) {
          amounts.textContent = amount
            .toString()
            .replace(regular, ",");
        }

        if (prices !== null) {
          prices.textContent =
            price >= 999999
              ? "Price is negotiable"
              : "$" + price.toString().replace(regular, ",");
        }

        if (!isNaN(amount) && !isNaN(price)) {
          resultArray.push({amount, price, title: titles[i + 1]});
        }
      }

      liveSessionsHeldNumber = getLiveSessionsHeldNumber();
      globalFollowersNumber = getGlobalFlowers();

      if (getResTopShow()) {
        initCounterFollowers();
      }

      document.querySelector("a.counter-value").innerHTML = globalFollowersNumber.toString().replace(regular, ",");
      document.querySelector("#count_1").textContent = globalFollowersNumber.toString().replace(regular, ",");
      document.querySelector("#count_2").textContent = liveSessionsHeldNumber.toString().replace(regular, ",");

			chartContainers.push(createCounterTemplate());
			return chartContainers;
		}

		const modalBtn = $(".modal-btn-grafik");
		if (modalBtn.length) {
			modalBtn.click(function (e) {
				indexCorrect = Number(e.target.dataset.count);
				e.preventDefault();

				responseCounterTopShowDetails = false
				getPriceXml().then((data) => {
					$("#master-modal .modal-body .content").empty();
					$("#master-modal .modal-body .image").empty();

					$("#master-modal .modal-body .content").append(
						'<div class="chart-wrapper"></div>'
					);
					$("#master-modal .modal-body .image").append(
						'<div class="title-chart-wrapper"></div>'
					);
					$("#master-modal .modal-body .image .title-chart-wrapper").text(
						getTitles()
					);

					$("#master-modal .modal-body .content .chart-wrapper").append(
						convasGrafic
					);
					$("#master-modal").modal("show");
				});
			});
		}

		const tableInfoBtn = $("#buttonShowDetails");
		if (tableInfoBtn.length) {
			tableInfoBtn.click(function (e) {
				e.preventDefault();
				res = !res;
				if (getRes()) {
					$("#buttonShowDetails").text("Hide details...");
					$("#custom_section_results").addClass("visible");
				} else {
					$("#buttonShowDetails").text("Show details...");
					$("#custom_section_results").removeClass("visible");
				}
			});
		}

		function createCounterTemplate() {
      return createChartTemplate();
		}
/*
		function sharedTotalModal() {
			const chartWrapper = document.createElement("div");
			const block = document.getElementById("footer-copyright");
			chartWrapper.classList.add("chart-modal-get");
			block.appendChild(chartWrapper);
		}

		sharedTotalModal();
*/
    setInterval(() => {
      checkAndUpdateFollowers();
      checkAndUpdateLiveSessionsHeld();
      checkAndUpdateOther();
    }, 15000);

    function checkAndUpdateFollowers() {
      if (globalFollowersNumber < getGlobalFlowers()) {

        showPopup(`<div style='text-align:center'><p>Followes</p> <h4 style='color:#fff'>+${getGlobalFlowers() - globalFollowersNumber}</h4></dv>`);

        globalFollowersNumber = getGlobalFlowers();

        document.querySelector(".counter-container1 > .counter-value").textContent = globalFollowersNumber.toString().replace(regular, ",");
        document.querySelector("#count_1").textContent = globalFollowersNumber.toString().replace(regular, ",");
      }
    }

    function checkAndUpdateLiveSessionsHeld() {
      if (liveSessionsHeldNumber < getLiveSessionsHeldNumber()) {

        showPopup(`<div style='text-align:center'><p>New Live Sessions held</p> <h4 style='color:#fff'>+${getLiveSessionsHeldNumber() - liveSessionsHeldNumber}</h4></dv>`);

        liveSessionsHeldNumber = getLiveSessionsHeldNumber();

        document.querySelector(".counter-container2 > .counter-value").textContent = liveSessionsHeldNumber.toString().replace(regular, ",");
        document.querySelector("#count_2").textContent = liveSessionsHeldNumber.toString().replace(regular, ",");
      }
    }

    function checkAndUpdateOther() {
      const str = [
        "1",
        "2-3",
        "4-5",
        "6-7",
        "8-9",
        "10-11",
        "12-13",
        "14-15",
        "16-17",
        "18-19",
        "20-21",
      ];

      const values = csvRows[csvRows.length - 1].split(",");
      values.shift();

      for (let i = 4, k = 2; i < values.length; ++k, i += 2) {
        const value = getOtherNumber(i);
        //console.log(value > calcOtherObject[i].previousValue, value, calcOtherObject[i].previousValue, calcOtherObject[i].currentValue, );

        if (value > calcOtherObject[i].previousValue) {
          showPopup(`<div style='text-align:center'><p>${getTitlePopup(i)}</p> <h4 style='color:#fff'>+${value - calcOtherObject[i].previousValue}</h4></dv>`);

          calcOtherObject[i].previousValue = value;

          const amount = calcOtherObject[i].previousValue;

          let amounts = document.querySelector(
            `#count_${str[k].split("-")[0]}`
          );

          if (amounts !== null) {
            amounts.textContent = amount
              .toString()
              .replace(regular, ",");
          }

          document.querySelector(".counter-container3 > .counter-value").textContent = sumArray().toString().replace(regular, ",");
        }
      }
    }

    function showPopup(html) {
      /* TODO
      document.querySelector(".chart-modal-get").classList.add("visible");

      document.querySelector(".chart-modal-get").innerHTML = html;

      setTimeout(() => {
        document
          .querySelector(".chart-modal-get")
          .classList.remove("visible");
      }, 3000);
      */
    }

		function createChartTemplate() {
			const chartWrapper = document.createElement("div");
			convasGrafic = document.createElement("canvas");
			chartWrapper.classList.add("chart-wrapper");
			chartWrapper.appendChild(convasGrafic);
			return chartWrapper;
		}

		function initCounter(container, value) {
			let counter = new CountUp(container, value);
			counter.start();
		}

		function initCharts(chartRows, chartContainers) {
			const dates = [];
			const pricesMatrix = [];
			const amountMatrix = [];

			chartRows.forEach((row) => {
				dates.push(row[0]);

				const prices = [];
				const amounts = [];

				amounts.push(row[1]);
				prices.push(row[2]);

				pricesMatrix.push(prices);
				amountMatrix.push(amounts);
			});

			const transformedPriceMatrix = transposeMatrix(pricesMatrix);
			const transformedAmountMatrix = transposeMatrix(amountMatrix);

			transformedPriceMatrix.forEach((prices, i) => {
				if (prices[1] == "999999\r") {
					//hack \r\n for CVS parsing
					initChartSingle(
						chartContainers[i],
						dates,
						transformedAmountMatrix[i]
					);
				} else {
					initChart(
						chartContainers[i],
						dates,
						prices,
						transformedAmountMatrix[i]
					);
				}
			});
		}

		function initChart(container, dates, priceData, amountData) {
			const ii = JSON.parse(localStorage.getItem("todayData"));

			if (String(ii.value) === amountData[amountData.length - 1]) {
				amountData[amountData.length - 1] = localStorage.getItem("NumberTrend");
			}
			const data = {
				labels: dates,
				datasets: [
					{
						label: "Rising prices",
						data: priceData,
						yAxisID: "priceYAxis",
						fill: false,
						borderColor: "#D89048",
						tension: 0.1,
					},
					{
						label: "Rising orders",
						data: amountData,
						yAxisID: "ordersYAxis",
						fill: false,
						borderColor: "#f22853",
						tension: 0.1,
					},
				],
			};

			//console.log(container)

			new Chart(container.firstChild, {
				type: "line",
				data: data,
				options: {
					interaction: {
						mode: "index",
						intersect: false,
					},
					stacked: false,
					scales: {
						priceYAxis: {
							beginAtZero: true,
							type: "linear",
							position: "left",
							display: true,
							ticks: {
								beginAtZero: true,
							},
						},
						ordersYAxis: {
							beginAtZero: true,
							type: "linear",
							position: "right",
							display: true,
							ticks: {
								beginAtZero: true,
							},
						},
					},
				},
			});
		}

		function initChartSingle(container, dates, amountData) {
			const data = {
				labels: dates,
				datasets: [
					{
						label: "Rising orders",
						data: amountData,
						yAxisID: "ordersYAxis",
						fill: false,
						borderColor: "#f22853",
						tension: 0.1,
					},
				],
			};

			//console.log(container)

			new Chart(container.firstChild, {
				type: "line",
				data: data,
				options: {
					interaction: {
						mode: "index",
						intersect: false,
					},
					stacked: false,
					scales: {
						ordersYAxis: {
							beginAtZero: true,
							type: "linear",
							position: "right",
							display: true,
							ticks: {
								beginAtZero: true,
							},
						},
					},
				},
			});
		}

		function transposeMatrix(matrix) {
			const transposedMatrix = [];
			const rows = matrix.length;
			const cols = matrix[0].length;

			for (let j = 0; j < cols; j++) {
				transposedMatrix[j] = [];
				for (let i = 0; i < rows; i++) {
					transposedMatrix[j][i] = matrix[i][j];
				}
			}

			return transposedMatrix;
		}

		function initSectionNav() {
			const allSections = document.querySelectorAll("body > section");
			const nav = document.createElement("nav");

			generateMenu();

			const links = document.querySelectorAll("nav ul li a");
			const darkSections = [
				"slider",
				"custom_section_hole",
				"custom_section_hole2",
			];

			nav.classList.add("dark");

			links.forEach((link) => {
				link.addEventListener("click", function (e) {
					// e.preventDefault();
					const targetSection = document.querySelector(
						this.getAttribute("href")
					);

					const topPosition =
						targetSection.getBoundingClientRect().top + window.pageYOffset - 55;

					window.scrollTo({
						top: topPosition,
						behavior: "smooth",
					});
				});
			});

			document.addEventListener("scroll", function () {
				let currentActiveSection;
				allSections.forEach((section) => {
					const sectionTop = section.offsetTop;
					const sectionHeight = section.offsetHeight;
					if (
						pageYOffset >= sectionTop - sectionHeight / 3 &&
						pageYOffset < sectionTop + sectionHeight - sectionHeight / 3
					) {
						currentActiveSection = section;
					}
				});

				if (
					currentActiveSection &&
					darkSections.includes(currentActiveSection.id)
				) {
					nav.classList.add("dark");
				} else {
					nav.classList.remove("dark");
				}

				links.forEach((link) => {
					link.classList.remove("active");

					if (
						currentActiveSection &&
						link.getAttribute("href") === `#${currentActiveSection.id}`
					) {
						link.classList.add("active");
					}
				});
			});

			function generateMenu() {
				const sections = [...allSections].filter((section) =>
					section.querySelector("h2")
				);
				const ul = document.createElement("ul");

				nav.classList.add("section-nav");

				sections.forEach((section, index) => {
					const li = document.createElement("li");
					const a = document.createElement("a");
					a.href = `#${section.id}`;

					const text = document.createElement("span");
					text.innerText =
						section.querySelector("h2")?.innerText || "Section " + (index + 1);

					const number = document.createElement("span");
					number.innerText = "<" + (index + 1) + ">";

					a.prepend(number);
					a.appendChild(text);
					li.appendChild(a);
					ul.appendChild(li);
				});

				nav.appendChild(ul);
				document.body.insertBefore(nav, document.body.firstChild);
			}
		}
	}
});
  