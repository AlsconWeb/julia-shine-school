<!-- Start: Recent Blog
============================= -->
<?php 
	$hide_show_blog = get_theme_mod('hide_show_blog','1');
	$blog_title = get_theme_mod('blog_title','Recent Blog');
	$blog_description = get_theme_mod('blog_description','Publishing packages and web page editors now use Lorem Ipsum as their default model text');
	$blog_category_id = get_theme_mod('blog_category_id');
	$blog_display_num = get_theme_mod('blog_display_num','3');	
	$blog_display_col = get_theme_mod('blog_display_col','4');
	$blog_read_more = get_theme_mod('blog_read_more');
	   if($hide_show_blog == '1') { 
	   hantus_before_blog_section_trigger();  
?>
 <section id="blog-content" class="section-padding">
        <div class="container">
			<div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
						<div class="section-title service-section">
					<?php if($blog_title) { ?>
						<h2><?php echo esc_html($blog_title); ?></h2>
					<?php } ?>
					<?php if($blog_description) { ?>
						<p><?php echo esc_html($blog_description); ?></p>
					<?php } ?>	
					</div>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
						<?php 	
							$args = array( 'post_type' => 'post', 'category__in' => $blog_category_id, 'posts_per_page' => $blog_display_num,'post__not_in'=>get_option("sticky_posts")) ; 	
								query_posts( $args );
								if(query_posts( $args ))
								{	
								while(have_posts()):the_post(); ?>
                        <div class="col-lg-<?php echo $blog_display_col; ?> col-md-6 col-sm-12 mb-lg-0 mb-4">
                            <article class="blog-post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="post-thumb">
									<?php get_template_part('template-parts/content/content','sticky'); ?>
									<?php if ( has_post_thumbnail() ) { ?>
										<?php the_post_thumbnail(); ?>
									<?php } ?>	
                                </div>

                                <div class="post-content">
                                    <ul class="meta-info">
                                        <li class="post-date"><a href="<?php echo esc_url(the_date('Y/m/d')); ?>">On <?php echo esc_html(get_the_date('j M Y')); ?></a></li>
                                        <li class="posted-by"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>">by <?php the_author(); ?></a></li>
                                    </ul>
									<?php     
										if ( is_single() ) :
										
										the_title('<h4  class="post-title">', '</h4 >' );
										
										else:
										
										the_title( sprintf( '<h4  class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4 >' );
										
										endif; 
									?> 
									<?php if ( ! empty( $blog_read_more )) { 
										the_content();
									?>
										 <a href="<?php esc_url(the_permalink()); ?>" class="read-more"><?php echo esc_html($blog_read_more); ?> <i class="fa fa-angle-double-right"></i></a>
									 <?php 
										}else{
										the_content( 
											sprintf( 
												__( 'Read More', 'hantus-pro' ), 
												'<span class="screen-reader-text">  '.get_the_title().'</span>' 
											) 
										);
									}
									?>
                                   
                                </div>
                            </article>                            
                        </div>
						<?php 
							endwhile; 
							}
						?>
                    </div>                   
                </div>
            </div>
        </div>
    </section>
	   <?php hantus_after_blog_section_trigger(); } ?>	