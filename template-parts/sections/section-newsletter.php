<!-- Start: Subscribe
    ============================= -->
	
	<?php
			$hide_show_newsletter			= get_theme_mod('hide_show_newsletter','1');
			$newsletter_icon				= get_theme_mod('newsletter_icon','fa-envelope');
			$newsletter_title				= get_theme_mod('newsletter_title','SIGN UP FOR NEWS AND OFFRERS');
			$newsletter_description			= get_theme_mod('newsletter_description','Subcribe to lastest smartphones news & great deals we offer');
			$newsletter_shortcode			= get_theme_mod('newsletter_shortcode');
	?>
	<?php if($hide_show_newsletter) {?>
	<?php hantus_before_newsletter_section_trigger(); ?>
	<!-- Start: Subscribe
    ============================= -->
    <section id="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 text-lg-left text-center mb-lg-0 mb-3">
                    <i class="fa <?php echo esc_attr($newsletter_icon); ?>"></i>
                    	<?php if($newsletter_title) {?>
							<h3><?php echo esc_attr($newsletter_title); ?></h3>
						<?php } ?>	
						<?php if($newsletter_description) {?>
							<p><?php echo esc_attr($newsletter_description); ?></p>
						<?php } ?>	
                </div>
                <div class="col-lg-6 col-md-12 text-center">
                   <?php 
						if($newsletter_shortcode != '') {
							echo do_shortcode( $newsletter_shortcode );
						}
					?>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Subscribe
    ============================= -->
	<?php hantus_after_newsletter_section_trigger(); } ?>

    <!-- End: Subscribe
    ============================= -->