<?php
function hantus_portfolio_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Portfolio Section Panel
	=========================================*/
		$wp_customize->add_section(
			'portfolio_setting', array(
				'title' => esc_html__( 'Menu Item Section', 'hantus-pro' ),
				'panel' => 'hantus_frontpage_sections',
				'priority' => apply_filters( 'hantus_section_priority', 26, 'hantus_Portfolio' ),
			)
		);
		
	// Setting  Head 
	$wp_customize->add_setting(
		'portfolio_Setting_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 1,
		)
	);

	$wp_customize->add_control(
	'portfolio_Setting_head',
		array(
			'type' => 'hidden',
			'label' => __('Setting','hantus-pro'),
			'section' => 'portfolio_setting',
		)
	);
	
	$wp_customize->add_setting( 
		'portfolio_settings' , 
			array(
			'default' => esc_html__( '1', 'hantus-pro' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
			'priority' => 2,
		) 
	);
	
	$wp_customize->add_control( new Customizer_Toggle_Control( $wp_customize, 
	'portfolio_settings', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'hantus-pro' ),
			'section'     => 'portfolio_setting',
			'settings'    => 'portfolio_settings',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Portfolio Header Section // 
	
	//  Head 
	$wp_customize->add_setting(
		'portfolio_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 5,
		)
	);

	$wp_customize->add_control(
	'portfolio_head',
		array(
			'type' => 'hidden',
			'label' => __('Header','hantus-pro'),
			'section' => 'portfolio_setting',
		)
	);
	
	// Portfolio Title // 
	$wp_customize->add_setting(
    	'portfolio_title',
    	array(
	        'default'			=> __('Portfolio','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_html',
			'transport'         => $selective_refresh,
			'priority' => 6,
		)
	);	
	
	$wp_customize->add_control( 
		'portfolio_title',
		array(
		    'label'   => __('Title','hantus-pro'),
		    'section' => 'portfolio_setting',
			'settings'   	 => 'portfolio_title',
			'type'           => 'text',
		)  
	);
	
	// Service Description // 
	$wp_customize->add_setting(
    	'portfolio_description',
    	array(
	        'default'			=> __('You can judge my work by the portfolio we have done','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'transport'         => $selective_refresh,
			'priority' => 7,
		)
	);	
	
	$wp_customize->add_control( 
		'portfolio_description',
		array(
		    'label'   => __('Description','hantus-pro'),
		    'section' => 'portfolio_setting',
			'settings'   	 => 'portfolio_description',
			'type'           => 'textarea',
		)  
	);


	//  Content Head 
	$wp_customize->add_setting(
		'portfolio_content_head'
			,array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'hantus_sanitize_text',
			'priority' => 8,
		)
	);

	$wp_customize->add_control(
	'portfolio_content_head',
		array(
			'type' => 'hidden',
			'label' => __('Content','hantus-pro'),
			'section' => 'portfolio_setting',
		)
	);
	
	// Portfolio Display Setting // 
	$wp_customize->add_setting(
    	'portfolio_display_num',
    	array(
	        'default'			=> __('3','hantus-pro'),
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'priority' => 9,
		)
	);
	
	$wp_customize->add_control( 
	new Hantus_Customizer_Range_Slider_Control( $wp_customize, 'portfolio_display_num', 
		array(
			'section'  => 'portfolio_setting',
			'settings' => 'portfolio_display_num',
			'label'    => __( 'No of Menu Item Display','hantus-pro' ),
			'input_attrs' => array(
				'min'    => 1,
				'max'    => 500,
				'step'   => 1,
				//'suffix' => 'px', //optional suffix
			),
		) ) 
	);
}
add_action( 'customize_register', 'hantus_portfolio_setting' );

// portfolio 
function hantus_home_portfolio_section_partials( $wp_customize ){
	// portfolio_settings
	$wp_customize->selective_refresh->add_partial(
		'portfolio_settings', array(
			'selector' => '#portfolio',
			'container_inclusive' => true,
			'render_callback' => 'portfolio_setting',
			'fallback_refresh' => true,
		)
	);
	
	//title
	$wp_customize->selective_refresh->add_partial( 'portfolio_title', array(
		'selector'            => '.port-section .section-title h2',
		'settings'            => 'portfolio_title',
		'render_callback'  => 'portfolio_section_title_render_callback',
	
	) );
	// description
	$wp_customize->selective_refresh->add_partial( 'portfolio_description', array(
		'selector'            => '.port-section .section-title p',
		'settings'            => 'portfolio_description',
		'render_callback'  => 'portfolio_section_desc_render_callback',
	
	) );
	}

add_action( 'customize_register', 'hantus_home_portfolio_section_partials' );

// portfolio title
function portfolio_section_title_render_callback() {
	return get_theme_mod( 'portfolio_title' );
}
// portfolio description
function portfolio_section_desc_render_callback() {
	return get_theme_mod( 'portfolio_description' );
}